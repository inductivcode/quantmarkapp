package com.inductivtechnologies.quantmarkapp.store.repositories;

import android.content.Context;
import android.util.Log;

import com.inductivtechnologies.quantmarkapp.models.entities.Token;
import com.inductivtechnologies.quantmarkapp.store.db.DatabaseHelper;
import com.inductivtechnologies.quantmarkapp.store.db.DbHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;

import java.sql.SQLException;
import java.util.List;

/**
 * SimpleUserRepository : Accès aux données de type Token
 */

@SuppressWarnings({"FieldCanBeLocal", "unused"})
public class TokenRepository {

    private static final String TAG = TokenRepository.class.getSimpleName();

    // Référence pour l'accès à la base de données
    private DatabaseHelper databaseHelper;

    // Context
    private Context context;

    // Dao
    private Dao<Token, Integer> tokenDao;

    //Contructeur
    public TokenRepository(Context context) {
        this.databaseHelper = DbHelper.getDataBaseHelper(context);
        this.context = context;

        try {
            this.tokenDao = databaseHelper.getTokenDao();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    // Insère un nouveau token
    public void insertToken(Token token){
        try{
            this.tokenDao.createOrUpdate(token);

            Log.i(TAG, "SUCCES token inséré");
        } catch (SQLException e) {
            Log.i(TAG, "ECHEC token non inséré");
            e.printStackTrace();
        }
    }

    // Tous les tokens
    public List<Token> getAllTokens() {
        List<Token> tokens = null;
        try {
            tokens = this.tokenDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tokens;
    }

    //Supprime un token
    public int deleteToken(Token token){
        int result = -1;
        try{
            result = this.tokenDao.delete(token);

            Log.i(TAG, "SUCCES token supprimé");
        } catch (SQLException e) {
            Log.i(TAG, "SUCCES token non supprimé");
            e.printStackTrace();
        }
        return result;
    }

    //Supprime tous les tokens
    public int deleteAllTokens(){
        int result = -1;
        try{
            DeleteBuilder<Token, Integer> deleteBuilder = tokenDao.deleteBuilder();
            result = deleteBuilder.delete();

            Log.i(TAG, "SUCCES tokens supprimés");
        } catch (SQLException e) {
            Log.i(TAG, "ECHEC tokens non supprimés");
            e.printStackTrace();
        }

        return result;
    }
}
