package com.inductivtechnologies.quantmarkapp.store.repositories;

import android.content.Context;
import android.util.Log;

import com.inductivtechnologies.quantmarkapp.models.display.MarketOfferDisplay;
import com.inductivtechnologies.quantmarkapp.models.entities.CartItem;
import com.inductivtechnologies.quantmarkapp.store.db.DatabaseHelper;
import com.inductivtechnologies.quantmarkapp.store.db.DbHelper;
import com.inductivtechnologies.quantmarkapp.utils.EntitiesUtils;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * SimpleUserRepository : Accès aux données de type Cartitem
 */

@SuppressWarnings({"FieldCanBeLocal", "unused"})
public class CartItemRepository {

    private static final String TAG = CartItemRepository.class.getSimpleName();

    // Référence pour l'accès à la base de données
    private final DatabaseHelper databaseHelper;

    // Context
    private final Context context;

    // Dao
    private Dao<CartItem, Integer> cartItemDao;

    //Contructeur
    public CartItemRepository(Context context){
        this.databaseHelper = DbHelper.getDataBaseHelper(context);
        this.context = context;
        try {
            this.cartItemDao = databaseHelper.getCartItemDao();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    //Recherche un cartitem
    public CartItem findCartitemByIdMarketOffer(final String idHexMarketoffer){
        List<CartItem> cartitems = null;
        try {
            cartitems = this.cartItemDao.queryBuilder()
                    .where()
                    .eq(CartItem.FIELD_NAME_OBJECTID_HEX, idHexMarketoffer)
                    .query();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }

        if(cartitems != null && cartitems.size() != 0) return cartitems.get(0);
        return null;
    }

    // Crée un nouveau cartitem dans la base de données
    public long insertCartItem(CartItem cartitem){
        long result = - 1;
        try{
            result = this.cartItemDao.create(cartitem);
            Log.i(TAG, "SUCCES cartitem inséré");
        } catch (SQLException e) {
            e.printStackTrace();
            Log.i(TAG, "ECHEC cartitem non inséré");
        }
        return result;
    }

    // Tous les enregistrements cartItems
    public  List<CartItem> getAllCartitems(){
        List<CartItem> cartitems = null;
        try{
            cartitems = this.cartItemDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cartitems;
    }

    //Mise à jour
    public long  updateCartItem(CartItem cartitem) {
        long result = -1;
        try{
            result = this.cartItemDao.update(cartitem);
            Log.i(TAG, "SUCCES cartitem mis à jour");
        } catch (SQLException e) {
            e.printStackTrace();
            Log.i(TAG, "ECHEC cartitem non mis à jour");
        }
        return result;
    }

    public List<CartItem> refreshData(List<MarketOfferDisplay> marketOffers) {
        List<CartItem> cartItemsFinal = new ArrayList<>();
        try{
            List<CartItem> cartItems = getAllCartitems();
            if(cartItems != null){
                for(CartItem cartitem : cartItems){
                    int position = EntitiesUtils.isContainCartItem(marketOffers, cartitem);

                    if(position < 0){
                        cartItemDao.delete(cartitem);
                    }else{
                        cartItemsFinal.add(cartitem);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return cartItemsFinal;
    }

    //Supprimer un cartitem
    public long deleteCartitem(CartItem cartitem){
        long result = -1;
        try{
            result = cartItemDao.delete(cartitem);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public int deleteCartItem(String idHexCartitem){
        int result = -1;
        DeleteBuilder<CartItem, Integer> deleteBuilder = cartItemDao.deleteBuilder();
        try {
            deleteBuilder.where().eq(CartItem.FIELD_NAME_OBJECTID_HEX, idHexCartitem);
            result = deleteBuilder.delete();
        } catch (SQLException e) {
            e.printStackTrace();
        }
     return result;
    }

    //Vide le panier
    public int deleteAllCartitems(){
        int result = -1;
        try{
            DeleteBuilder<CartItem, Integer> deleteBuilder = this.cartItemDao.deleteBuilder();
            result = deleteBuilder.delete();

            Log.i(TAG, "SUCCES articles du panier supprimés");
        } catch (SQLException e) {
            Log.i(TAG, "ECHEC articles du panier non supprimés");
            e.printStackTrace();
        }

        return result;
    }

    public long countCartItem() {
        long result = 0;
        List<CartItem> cartItem = getAllCartitems();
        if(cartItem != null){
            result = cartItem.size();
        }
        return result;
    }

}
