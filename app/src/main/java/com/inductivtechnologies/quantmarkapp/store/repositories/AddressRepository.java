package com.inductivtechnologies.quantmarkapp.store.repositories;

import android.content.Context;
import android.util.Log;

import com.inductivtechnologies.quantmarkapp.models.entities.Address;
import com.inductivtechnologies.quantmarkapp.store.db.DatabaseHelper;
import com.inductivtechnologies.quantmarkapp.store.db.DbHelper;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;

/**
 * SimpleUserRepository : Accès aux données de type Simpleuser
 */

@SuppressWarnings({"FieldCanBeLocal", "unused"})
public class AddressRepository {

    private static final String TAG = AddressRepository.class.getSimpleName();

    // Référence pour l'accès à la base de données
    private final DatabaseHelper databaseHelper;

    // Context
    private final Context context;

    // Dao
    private Dao<Address, Integer> addressDao;

    //Contructeur
    public AddressRepository(Context context) {
        this.databaseHelper = DbHelper.getDataBaseHelper(context);
        this.context = context;

        try {
            this.addressDao = databaseHelper.getAddressDao();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    // Insère une adresse
    public void insertAddress(Address address){
        try{
            this.addressDao.createOrUpdate(address);

            Log.i(TAG, "SUCCES adresse insérée");
        } catch (SQLException e) {
            Log.i(TAG, "ECHEC adresse non insérée");
            e.printStackTrace();
        }
    }

    // Met à jour une adresse
    public long updateAddress(Address address){
        long result = - 1;
        try{
            result = this.addressDao.update(address);

            Log.i(TAG, "SUCCES adresse MAJ");
        } catch (SQLException e) {
            Log.i(TAG, "ECHEC adresse non MAJ");
            e.printStackTrace();
        }
        return result;
    }

    // Supprime une adresse
    public long deleteAddress(Address address){
        long result = - 1;
        try{
            result = this.addressDao.delete(address);

            Log.i(TAG, "SUCCES adresse supprimée");
        } catch (SQLException e) {
            Log.i(TAG, "ECHEC adresse non supprimée");
            e.printStackTrace();
        }
        return result;
    }

}
