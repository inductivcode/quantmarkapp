package com.inductivtechnologies.quantmarkapp.store.db;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;

/**
 * DbHelper : Accès à la base de données
 */

public class DbHelper {

    // Instance du helper donnant accès à la base de données
    public static DatabaseHelper getDataBaseHelper(Context context) {
        return OpenHelperManager.getHelper(context, DatabaseHelper.class);
    }

}



