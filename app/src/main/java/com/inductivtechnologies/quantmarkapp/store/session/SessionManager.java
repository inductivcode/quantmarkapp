package com.inductivtechnologies.quantmarkapp.store.session;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * SessionManager : Gestion de la session utilisateur avec les préférences partagées
 */

@SuppressWarnings({"FieldCanBeLocal", "unused"})
public class SessionManager {

    // Préférences partagées
    private SharedPreferences sharedPreference;

    // Editeur : utile pour lire et écrire dans les préférences partagées
    private SharedPreferences.Editor editor;

    // Context
    private Context context;

    // Mode d'écriture et de lecture
    private int PRIVATE_MODE = 0;

    // Nom du fichier
    private static final String PREF_NAME = "quantmarkSharedPreferences";

    // Clé d'accès
    private static final String IS_LOGIN = "IsLoggedIn";

    private static final String VALIDATION_CODE = "validationCode";

    // Constructeur
    @SuppressLint("CommitPrefEdits")
    public SessionManager(Context context){
        this.context = context;
        this.sharedPreference = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = sharedPreference.edit();
    }

    // Crée une session lorsque l'utilisateur se connecte
    public void createLoginSession(){
        // On positionne isLoggedIn a true
        editor.putBoolean(IS_LOGIN, true);
        // On valide les changements grace a l'editor
        editor.commit();
    }

    // Existe il une session ouverte
    public boolean isLoggedIn(){
        return sharedPreference.getBoolean(IS_LOGIN, false);
    }

    // Supprime la session
    public void deleteSessionLogin(){
        // On sitionne isLoggedIn a true
        editor.putBoolean(IS_LOGIN, false);
        // On valide les changements grace a l'editor
        editor.commit();
    }

    public void addValidationCode(String code){
        editor.putString(VALIDATION_CODE, code);
        // On valide les changements grace a l'editor
        editor.commit();
    }

    public void deleteValidationCode(){
        editor.remove(VALIDATION_CODE);
        // On valide les changements grace a l'editor
        editor.commit();
    }

    public String getValidationCode(){
        return sharedPreference.getString(VALIDATION_CODE, "");
    }
}
