package com.inductivtechnologies.quantmarkapp.store.repositories;

import android.content.Context;
import android.util.Log;

import com.inductivtechnologies.quantmarkapp.comparators.SearchTraceComparator;
import com.inductivtechnologies.quantmarkapp.models.entities.SearchTrace;
import com.inductivtechnologies.quantmarkapp.store.db.DatabaseHelper;
import com.inductivtechnologies.quantmarkapp.store.db.DbHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

/**
 * SearchTraceRepository : Accès aux données de type SearchTrace
 */

@SuppressWarnings({"FieldCanBeLocal", "unused"})
public class SearchTraceRepository {

    private static final String TAG = SearchTraceRepository.class.getSimpleName();

    // Référence pour l'accès à la base de données
    private final DatabaseHelper databaseHelper;

    // Context
    private final Context context;

    // Dao
    private Dao<SearchTrace, Integer> searchTraceDao;

    //Contructeur
    public SearchTraceRepository(Context context) {
        this.databaseHelper = DbHelper.getDataBaseHelper(context);
        this.context = context;

        try {
            this.searchTraceDao = databaseHelper.getSearchTraceDao();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    // Insèrtion
    public void insertSearchItem(SearchTrace searchTrace){
        SearchTrace updateSeacrhItem = findSearchItemByContentValue(searchTrace.getIdHex());

        try{
            if(updateSeacrhItem == null){
                this.searchTraceDao.create(searchTrace);
            }else{
                updateSeacrhItem.setCreationDate(System.currentTimeMillis());
                this.searchTraceDao.update(updateSeacrhItem);
            }

            Log.i(TAG, "SUCCES search item inséré");
        } catch (SQLException e) {
            Log.i(TAG, "ECHEC search item non inséré");
            e.printStackTrace();
        }
    }

    //Recherche un search item à partir de son contenu
    private SearchTrace findSearchItemByContentValue(String idHex){
        List<SearchTrace> searchItem = null;

        try {
            searchItem = this.searchTraceDao.queryBuilder()
                    .where()
                    .eq(SearchTrace.FIELD_ID_HEX, idHex)
                    .query();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }

        if(searchItem != null && searchItem.size() != 0) return searchItem.get(0);
        return null;
    }

    // Tous les enregistrements
    public List<SearchTrace> getAllSearchTrace(){
        List<SearchTrace> searchHistory = null;
        try{
            searchHistory = this.searchTraceDao.queryForAll();
            if(searchHistory != null){
                Collections.sort(searchHistory, new SearchTraceComparator());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return searchHistory;
    }

    //Supprime tous les enregistrements
    public int deleteAll(){
        int result = -1;
        try{
            DeleteBuilder<SearchTrace, Integer> deleteBuilder = this.searchTraceDao.deleteBuilder();
            result = deleteBuilder.delete();

            Log.i(TAG, "SUCCES historique de recherche vidé");
        } catch (SQLException e) {
            Log.i(TAG, "ECHEC historique de recherche non vidé");
            e.printStackTrace();
        }

        return result;
    }
}
