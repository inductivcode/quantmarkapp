package com.inductivtechnologies.quantmarkapp.store.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.inductivtechnologies.quantmarkapp.models.entities.Address;
import com.inductivtechnologies.quantmarkapp.models.entities.CartItem;
import com.inductivtechnologies.quantmarkapp.models.entities.Favorite;
import com.inductivtechnologies.quantmarkapp.models.entities.Phone;
import com.inductivtechnologies.quantmarkapp.models.entities.SearchTrace;
import com.inductivtechnologies.quantmarkapp.models.entities.Token;
import com.inductivtechnologies.quantmarkapp.models.entities.User;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * DatabaseHelper : Configuration de la base de données
 * Cette classe crée et met a jour la base et aussi fourie les objets DAO
 */

public class DatabaseHelper  extends OrmLiteSqliteOpenHelper {

    // Nom de la base de données
    private static final String DATABASE_NAME = "quantmarkApp.db";
    // Version de la base de données
    private static final int DATABASE_VERSION = 1;

    // Exposition des objects DAO
    private Dao<User, Integer> userDao;
    private Dao<Address, Integer> addressDao;
    private Dao<Phone, Integer> phoneDao;
    private Dao<Token, Integer> tokenDao;
    private Dao<SearchTrace, Integer> searchTraceDao;
    private Dao<CartItem, Integer> cartItemDao;
    private Dao<Favorite, Integer> favoriteDao;

    // Constructeur
    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creation de la base de données et des tables
    // OnCreate : Invoquer unique tout au long du cycle de vie de l'application
    @Override
    public void onCreate(SQLiteDatabase sqliteDatabase, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, User.class);
            TableUtils.createTable(connectionSource, Address.class);
            TableUtils.createTable(connectionSource, Phone.class);
            TableUtils.createTable(connectionSource, Token.class);
            TableUtils.createTable(connectionSource, SearchTrace.class);
            TableUtils.createTable(connectionSource, CartItem.class);
            TableUtils.createTable(connectionSource, Favorite.class);
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    // Mise à jour de la base de données
    // Supprime et recré la base de données puis raffraichi les données
    @Override
    public void onUpgrade(SQLiteDatabase sqliteDatabase, ConnectionSource connectionSource, int oldVer, int newVer) {
        try {
            TableUtils.dropTable(connectionSource, User.class, true);
            TableUtils.dropTable(connectionSource, Address.class, true);
            TableUtils.dropTable(connectionSource, Phone.class, true);
            TableUtils.dropTable(connectionSource, Token.class, true);
            TableUtils.dropTable(connectionSource, CartItem.class, true);
            TableUtils.dropTable(connectionSource, SearchTrace.class, true);
            TableUtils.dropTable(connectionSource, Favorite.class, true);

            onCreate(sqliteDatabase, connectionSource);
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public void close() {
        userDao = null;
        addressDao = null;
        phoneDao = null;
        tokenDao = null;
        searchTraceDao = null;
        cartItemDao = null;
        favoriteDao = null;

        super.close();
    }

    // DAOS

    public Dao<User, Integer> getUserDao() throws SQLException {
        if (userDao == null) {
            userDao = getDao(User.class);
        }
        return userDao;
    }

    public Dao<Address, Integer> getAddressDao() throws SQLException {
        if (addressDao == null) {
            addressDao = getDao(Address.class);
        }
        return addressDao;
    }

    public Dao<Phone, Integer> getPhoneDao() throws SQLException {
        if (phoneDao == null) {
            phoneDao = getDao(Phone.class);
        }
        return phoneDao;
    }

    public Dao<Token, Integer> getTokenDao() throws SQLException {
        if (tokenDao == null) {
            tokenDao = getDao(Token.class);
        }
        return tokenDao;
    }

    public Dao<CartItem, Integer> getCartItemDao() throws SQLException {
        if (cartItemDao == null) {
            cartItemDao = getDao(CartItem.class);
        }
        return cartItemDao;
    }

    public Dao<SearchTrace, Integer> getSearchTraceDao() throws SQLException {
        if (searchTraceDao == null) {
            searchTraceDao = getDao(SearchTrace.class);
        }
        return searchTraceDao;
    }

    public Dao<Favorite, Integer> getFavoriteDao() throws SQLException {
        if (favoriteDao == null) {
            favoriteDao = getDao(Favorite.class);
        }
        return favoriteDao;
    }

}
