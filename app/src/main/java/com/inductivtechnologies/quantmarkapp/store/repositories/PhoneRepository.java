package com.inductivtechnologies.quantmarkapp.store.repositories;

import android.content.Context;
import android.util.Log;

import com.inductivtechnologies.quantmarkapp.models.entities.Phone;
import com.inductivtechnologies.quantmarkapp.store.db.DatabaseHelper;
import com.inductivtechnologies.quantmarkapp.store.db.DbHelper;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;

/**
 * SimpleUserRepository : Accès aux données de type Simpleuser
 */

@SuppressWarnings({"FieldCanBeLocal", "unused"})
public class PhoneRepository {

    private static final String TAG = PhoneRepository.class.getSimpleName();

    // Référence pour l'accès à la base de données
    private final DatabaseHelper databaseHelper;

    // Context
    private final Context context;

    // Dao
    private Dao<Phone, Integer> phoneDao;

    //Contructeur
    public PhoneRepository(Context context) {
        this.databaseHelper = DbHelper.getDataBaseHelper(context);
        this.context = context;

        try {
            this.phoneDao = databaseHelper.getPhoneDao();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    // Insère une phone
    public void insertPhone(Phone phone){
        try{
            this.phoneDao.createOrUpdate(phone);

            Log.i(TAG, "SUCCES phone inséré");
        } catch (SQLException e) {
            Log.i(TAG, "ECHEC phone non inséré");
            e.printStackTrace();
        }
    }

    // Supprime un phone
    public long deletePhone(Phone phone){
        long result = - 1;
        try{
            result = this.phoneDao.delete(phone);

            Log.i(TAG, "SUCCES phone supprimé");
        } catch (SQLException e) {
            Log.i(TAG, "ECHEC phone non supprimé");
            e.printStackTrace();
        }
        return result;
    }

    // Met à jour un phone
    public long updatePhone(Phone phone){
        long result = - 1;
        try{
            result = this.phoneDao.update(phone);

            Log.i(TAG, "SUCCES phone MAJ");
        } catch (SQLException e) {
            Log.i(TAG, "ECHEC phone non MAJ");
            e.printStackTrace();
        }
        return result;
    }

}
