package com.inductivtechnologies.quantmarkapp.store.repositories;

import android.content.Context;
import android.util.Log;

import com.inductivtechnologies.quantmarkapp.comparators.FavoriteComparator;
import com.inductivtechnologies.quantmarkapp.models.entities.Favorite;
import com.inductivtechnologies.quantmarkapp.store.db.DatabaseHelper;
import com.inductivtechnologies.quantmarkapp.store.db.DbHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

/**
 * FavoriteRepository. Accès aux données de type Favorite
 */

@SuppressWarnings({"FieldCanBeLocal", "unused"})
public class FavoriteRepository {

    private static final String TAG = FavoriteRepository.class.getSimpleName();

    // Référence pour l'accès à la base de données
    private final DatabaseHelper databaseHelper;

    // Context
    private final Context context;

    // Dao
    private Dao<Favorite, Integer> favoriteDao;

    //Contructeur
    public FavoriteRepository(Context context) {
        this.databaseHelper = DbHelper.getDataBaseHelper(context);
        this.context = context;

        try {
            this.favoriteDao = databaseHelper.getFavoriteDao();
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    // Insèrtion
    public int insertFavorite(Favorite favorite){
        int result = -1;
        try{
            result = this.favoriteDao.create(favorite);
            Log.i(TAG, "SUCCES favori inséré");

        } catch (SQLException e) {
            Log.i(TAG, "ECHEC favori non inséré");
            e.printStackTrace();
        }

        return result;
    }

    public Favorite findFavorite(String idHexMarketOffer){
        List<Favorite> favorites = null;

        try {
            favorites = this.favoriteDao.queryBuilder()
                    .where()
                    .eq(Favorite.FILED_ID_HEX_MARKETOFFER, idHexMarketOffer)
                    .query();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }

        if(favorites != null && favorites.size() != 0) return favorites.get(0);
        return null;
    }

    public int deleteFavorite(String idHexMarketOffer){
        int result = -1;
        DeleteBuilder<Favorite, Integer> deleteBuilder = this.favoriteDao.deleteBuilder();
        try {
            deleteBuilder.where().eq(Favorite.FILED_ID_HEX_MARKETOFFER, idHexMarketOffer);
            result = deleteBuilder.delete();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    // Tous les enregistrements
    public List<Favorite> getAllFavorites(){
        List<Favorite> favorites = null;
        try{
            favorites = this.favoriteDao.queryForAll();
            if(favorites != null){
                Collections.sort(favorites, new FavoriteComparator());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return favorites;
    }
}
