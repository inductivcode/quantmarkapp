package com.inductivtechnologies.quantmarkapp.store.repositories;

import android.content.Context;
import android.util.Log;

import com.inductivtechnologies.quantmarkapp.models.entities.User;
import com.inductivtechnologies.quantmarkapp.store.db.DatabaseHelper;
import com.inductivtechnologies.quantmarkapp.store.db.DbHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;

import java.sql.SQLException;
import java.util.List;

/**
 * SimpleUserRepository : Accès aux données de type Simpleuser
 */

@SuppressWarnings({"FieldCanBeLocal", "unused"})
public class UserRepository {

    private static final String TAG = UserRepository.class.getSimpleName();

    // Référence pour l'accès à la base de données
    private final DatabaseHelper databaseHelper;

    // Context
    private final Context context;

    // Dao
    private Dao<User, Integer> userDao;

    //Contructeur
    public UserRepository(Context context) {
        this.databaseHelper = DbHelper.getDataBaseHelper(context);
        this.context = context;

        try {
            this.userDao = databaseHelper.getUserDao();

        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    // Insère un nouvel utilisateur
    public long insertUser(User user){
        long result = - 1;
        try{
            result = this.userDao.create(user);

            Log.i(TAG, "SUCCES utilisateur inséré");
        } catch (SQLException e) {
            Log.i(TAG, "ECHEC utilisateur non inséré");
            e.printStackTrace();
        }
        return result;
    }

    // Tous les utilisateurs
    public List<User> getAllUsers(){
        List<User> users = null;
        try{
            users = this.userDao.queryForAll();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }

    // Mettre à jour un utilisateur
    public int updateUser(User user){
        int result = -1;
        try{
            result = this.userDao.update(user);

            Log.i(TAG, "SUCCES utilisateur mis à jour");
        } catch (SQLException e) {
            Log.i(TAG, "SUCCES utilisateur non mis à jour");
            e.printStackTrace();
        }
        return result;
    }

    // Mettre à jour un utilisateur
    public int updateUserByObjectId(User user){
        int result = -1;
        try{
            User currentUser = this.userDao.queryBuilder()
                    .where()
                    .eq(User.FIELD_NAME_OBJECTID_HEX, user.getObjectIdHexUser())
                    .query().get(0);
            user.setIdUser(currentUser.getIdUser());
            result = this.userDao.update(user);

            Log.i(TAG, "SUCCES utilisateur mis à jour");
        } catch (SQLException e) {
            Log.i(TAG, "SUCCES utilisateur non mis à jour");
            e.printStackTrace();
        }
        return result;
    }

    //Supprime un utilisateur
    public int deleteUser(User use){
        int result = -1;
        try{
            result = this.userDao.delete(use);

            Log.i(TAG, "SUCCES utilisateur supprimé");
        } catch (SQLException e) {
            Log.i(TAG, "ECHEC utilisateur non supprimé");
            e.printStackTrace();
        }
        return result;
    }

    //Supprime tous les utilisateurs
    public int deleteAllUsers(){
        int result = -1;
        try{
            DeleteBuilder<User, Integer> deleteBuilder = userDao.deleteBuilder();
            result = deleteBuilder.delete();

            Log.i(TAG, "SUCCES utilisateurs supprimés");
        } catch (SQLException e) {
            Log.i(TAG, "ECHEC utilisateurs non supprimés");
            e.printStackTrace();
        }

        return result;
    }

}
