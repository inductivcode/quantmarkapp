package com.inductivtechnologies.quantmarkapp.callbacks;

import org.json.JSONObject;

/**
 * HttpCallback : Callback utilisé pour impléménter les actions à effecturer avant/pendant/après une requête HTTP
 */
public interface HttpCallback {

    void beforeExecute();
    void afterExecute(JSONObject response);
    void whenErrors();

}
