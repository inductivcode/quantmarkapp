package com.inductivtechnologies.quantmarkapp.callbacks;

/**
 * OnLoadMoreListener : Pour charger les éléments complémentaires dans les listes
 */
public interface OnLoadMoreListener {
    void onLoadMore();
}
