package com.inductivtechnologies.quantmarkapp.adapters.category;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.models.display.CategoryDisplay;
import com.inductivtechnologies.quantmarkapp.utils.LanguageDisolay;
import com.inductivtechnologies.quantmarkapp.viewholders.category.CategoryListViewHolder;

import java.util.List;

/**
 * CategoryListAdapter : Adaptateur pour gerer l'affichage des categories en liste
 */
public class CategoryListAdapter  extends RecyclerView.Adapter<CategoryListViewHolder>{

    private final Context mContext;
    private final List<CategoryDisplay> categories;

    public CategoryListAdapter(Context mContext, List<CategoryDisplay> categories) {
        this.mContext = mContext;
        this.categories = categories;
    }

    @Override
    public CategoryListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_categories_list_items, parent, false);

        return new CategoryListViewHolder(itemView, this.mContext);
    }

    @Override
    public void onBindViewHolder(final CategoryListViewHolder viewHolder, int position) {
        CategoryDisplay category = categories.get(position);

        // Add data
        viewHolder.setTvContentCategory((String)LanguageDisolay
                .displayCategory(category.getCategory()).get(LanguageDisolay.FIELD_TITLE));
        viewHolder.setTvContentSubcategory(category.getNbOfMarketOffers());
        viewHolder.setThumbmail(category.getCategory().getImage());
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }
}
