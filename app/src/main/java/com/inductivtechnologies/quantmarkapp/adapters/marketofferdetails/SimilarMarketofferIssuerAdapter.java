package com.inductivtechnologies.quantmarkapp.adapters.marketofferdetails;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.models.display.MarketOfferDisplay;
import com.inductivtechnologies.quantmarkapp.utils.BindDataIntoViewholder;
import com.inductivtechnologies.quantmarkapp.viewholders.marketofferdetails.SimilarMarkerofferIssuerViewHolder;

import java.util.List;

/**
 * SimilarOfferardIssuerAdapter : Pour gerer l'affichage des offres du même vendeur
 */
public class SimilarMarketofferIssuerAdapter extends RecyclerView.Adapter<SimilarMarkerofferIssuerViewHolder>{

    private final Context context;
    private final List<MarketOfferDisplay> marketoffers;

    public SimilarMarketofferIssuerAdapter(Context context, List<MarketOfferDisplay> marketoffers) {
        this.context = context;
        this.marketoffers = marketoffers;
    }

    @Override
    public SimilarMarkerofferIssuerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_similar_issuer_marketoffer_item, parent, false);

        return new SimilarMarkerofferIssuerViewHolder(itemView, this.context);
    }

    @Override
    public void onBindViewHolder(final SimilarMarkerofferIssuerViewHolder viewHolder, int position) {
        MarketOfferDisplay marketoffer = marketoffers.get(position);
        // Add data
        BindDataIntoViewholder.addSimilarMarketofferToLayout(marketoffer, viewHolder);
    }

    @Override
    public int getItemCount() {
        return marketoffers.size();
    }
}
