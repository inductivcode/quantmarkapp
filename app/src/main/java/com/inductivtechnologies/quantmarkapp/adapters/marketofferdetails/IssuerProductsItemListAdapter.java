package com.inductivtechnologies.quantmarkapp.adapters.marketofferdetails;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.models.display.MarketOfferDisplay;
import com.inductivtechnologies.quantmarkapp.models.utils.MarketOfferDisplayCustom;
import com.inductivtechnologies.quantmarkapp.utils.BindDataIntoViewholder;
import com.inductivtechnologies.quantmarkapp.viewholders.marketofferdetails.IssuerProductsItemListViewHolder;
import com.inductivtechnologies.quantmarkapp.viewholders.others.LoadMoreViewholder;

import java.util.List;

/**
 * IssuerProductsItemAdapter : Adaptateur qui gérera le recyclerview
 */
@SuppressWarnings("FieldCanBeLocal")
public class IssuerProductsItemListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    public interface OnLoadMoreListener {
        void onLoadMore();
    }

    //Context
    private final Context context;

    //Les données
    private final List<MarketOfferDisplayCustom> marketoffers;

    //Indique si le load more  a été ajouté
    private boolean withLoadMore;

    private OnLoadMoreListener loadMoreListener;

    public IssuerProductsItemListAdapter(Context context, List<MarketOfferDisplayCustom>  marketoffers) {
        this.context = context;
        this.marketoffers = marketoffers;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch(viewType){
            case MarketOfferDisplayCustom.TYPE_LOAD_MORE :
                View twiceView = inflater.inflate(R.layout.load_more_item, parent, false);
                viewHolder = new LoadMoreViewholder(twiceView, context);
                break;
            case MarketOfferDisplayCustom.TYPE_MARKETOFFER :
                View OneView = inflater.inflate(R.layout.recyclerview_issuer_products_item_list, parent, false);
                viewHolder = new IssuerProductsItemListViewHolder(OneView, context);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof IssuerProductsItemListViewHolder) {
            IssuerProductsItemListViewHolder viewHolder = (IssuerProductsItemListViewHolder) holder;
            MarketOfferDisplay marketOfferDisplay = (MarketOfferDisplay) marketoffers.get(position);
            BindDataIntoViewholder.addIssuerMarketofferOfList(marketOfferDisplay, viewHolder);
        } else {
            LoadMoreViewholder loadMoreViewHolder = (LoadMoreViewholder) holder;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return marketoffers.get(position).getType();
    }

    @Override
    public int getItemCount() {
        return marketoffers != null ? marketoffers.size() : 0;
    }

    //Active l'ecouteur

    public void setWithLoadMore(boolean value){
        withLoadMore = value;
    }

    public void setLoadMoreListener(OnLoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }
}
