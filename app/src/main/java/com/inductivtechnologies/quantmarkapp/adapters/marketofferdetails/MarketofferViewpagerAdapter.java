package com.inductivtechnologies.quantmarkapp.adapters.marketofferdetails;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.components.GlideComponent;
import com.inductivtechnologies.quantmarkapp.configuration.UrlRestApiConfig;

import java.util.ArrayList;

/**
 *MarketofferViewpagerAdapter : Permet de gérer le slide des images des offres
 */
@SuppressWarnings("RedundantCast")
public class MarketofferViewpagerAdapter extends PagerAdapter {

    private final ArrayList<String> imagesSliders;
    private final Context context;

    public MarketofferViewpagerAdapter(Context context, ArrayList<String> imagesSliders) {
        this.imagesSliders = imagesSliders;
        this.context = context;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = null;
        if (layoutInflater != null) {
            view = layoutInflater.inflate(R.layout.image_slide, container, false);
        }

        ImageView imageViewPreview = null;
        if (view != null) {
            imageViewPreview = view.findViewById(R.id.item_slide);
        }
        String imageItem = imagesSliders.get(position);

        //Load Image
        GlideComponent.simpleGlideComponentWithoutPlaceholder(context,
                UrlRestApiConfig.IMAGE_RESOURCE_URL + imageItem, imageViewPreview);

        if (imageViewPreview != null) {
            imageViewPreview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });
        }

        container.addView(view);
        return view;
    }

    @Override
    public int getCount() {
        return imagesSliders.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object obj) {
        return view == ((View) obj);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

}
