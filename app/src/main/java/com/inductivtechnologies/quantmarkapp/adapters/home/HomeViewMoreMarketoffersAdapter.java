package com.inductivtechnologies.quantmarkapp.adapters.home;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.models.display.MarketOfferDisplay;
import com.inductivtechnologies.quantmarkapp.models.utils.MarketOfferDisplayCustom;
import com.inductivtechnologies.quantmarkapp.utils.BindDataIntoViewholder;
import com.inductivtechnologies.quantmarkapp.viewholders.home.HomeViewmoreMarketoffersItemViewholder;
import com.inductivtechnologies.quantmarkapp.viewholders.others.LoadMoreViewholder;

import java.util.List;

/**
 * HomeViewMoreMarketoffersAdapter
 */
@SuppressWarnings("FieldCanBeLocal")
public class HomeViewMoreMarketoffersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context context;
    private final List<MarketOfferDisplayCustom> marketOffers;

    private boolean withLoadMore;

    public HomeViewMoreMarketoffersAdapter(Context mContext, List<MarketOfferDisplayCustom> marketOffers) {
        this.context = mContext;
        this.marketOffers = marketOffers;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch(viewType){
            case MarketOfferDisplayCustom.TYPE_MARKETOFFER :
                View twiceView = inflater.inflate(R.layout.recyclerview_home_view_more_marketoffers_item,
                        parent, false);
                viewHolder = new HomeViewmoreMarketoffersItemViewholder(twiceView, context);
                break;
            case MarketOfferDisplayCustom.TYPE_LOAD_MORE :
                View OneView = inflater.inflate(R.layout.load_more_item, parent, false);
                viewHolder = new LoadMoreViewholder(OneView, context);

                StaggeredGridLayoutManager.LayoutParams layoutParams =
                        (StaggeredGridLayoutManager.LayoutParams) viewHolder.itemView.getLayoutParams();
                layoutParams.setFullSpan(true);

                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof HomeViewmoreMarketoffersItemViewholder) {
            HomeViewmoreMarketoffersItemViewholder viewHolder = (HomeViewmoreMarketoffersItemViewholder) holder;

            MarketOfferDisplay marketOfferDisplay = (MarketOfferDisplay) marketOffers.get(position);
            BindDataIntoViewholder.addHomeViewMoreOffers(marketOfferDisplay, viewHolder);
        } else {
            LoadMoreViewholder loadMoreViewHolder = (LoadMoreViewholder) holder;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return marketOffers.get(position).getType();
    }

    @Override
    public int getItemCount() {
        return marketOffers != null ? marketOffers.size() : 0;
    }

    //Active l'ecouteur
    public void setWithLoadMore(boolean value){
         this.withLoadMore = value;
    }
}
