package com.inductivtechnologies.quantmarkapp.adapters.category;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.callbacks.OnLoadMoreListener;
import com.inductivtechnologies.quantmarkapp.models.display.MarketOfferDisplay;
import com.inductivtechnologies.quantmarkapp.models.utils.MarketOfferDisplayCustom;
import com.inductivtechnologies.quantmarkapp.utils.BindDataIntoViewholder;
import com.inductivtechnologies.quantmarkapp.viewholders.category.SubCategoryMarketofferGridViewHolder;
import com.inductivtechnologies.quantmarkapp.viewholders.others.LoadMoreViewholder;

import java.util.List;

/**
 *SubCategoryMarketofferGridAdapter
 */
@SuppressWarnings("ALL")
public class SubCategoryMarketofferGridAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    //Context
    private final Context context;

    //:Les données
    private final List<MarketOfferDisplayCustom> marketoffers;

    //Indique si le load more  a été ajouté
    private boolean withLoadMore = false;

    //Ecouteur sur le load more : permet de charger plus de données
    private OnLoadMoreListener loadMoreListener;

    public SubCategoryMarketofferGridAdapter(Context context, List<MarketOfferDisplayCustom>  marketoffers) {
        this.context = context;
        this.marketoffers = marketoffers;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch(viewType){
            case MarketOfferDisplayCustom.TYPE_MARKETOFFER :
                View twiceView = inflater.inflate(R.layout.recyclerview_subcategories_marketoffer_grid_item, parent, false);
                viewHolder = new SubCategoryMarketofferGridViewHolder(twiceView, context);
                break;
            case MarketOfferDisplayCustom.TYPE_LOAD_MORE :
                View OneView = inflater.inflate(R.layout.load_more_item, parent, false);
                viewHolder = new LoadMoreViewholder(OneView, context);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof SubCategoryMarketofferGridViewHolder) {
            SubCategoryMarketofferGridViewHolder viewHolder = (SubCategoryMarketofferGridViewHolder) holder;
            MarketOfferDisplay marketOfferDisplay = (MarketOfferDisplay) marketoffers.get(position);
            BindDataIntoViewholder.addMarketofferOfGridSubcategory(marketOfferDisplay, viewHolder);
        } else {
            LoadMoreViewholder loadMoreViewHolder = (LoadMoreViewholder) holder;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return marketoffers.get(position).getType();
    }

    @Override
    public int getItemCount() {
        return marketoffers != null ? marketoffers.size() : 0;
    }

    //Active l'ecouteur
    public void setWithLoadMore(boolean value){
        withLoadMore = value;
    }

    // Retourne true si on est à la position du loadmore dans le recyclerview
    public boolean isPositionLoadMore(int position) {
        return position == getItemCount() - 1 && withLoadMore;
    }

    public void setLoadMoreListener(OnLoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }
}
