package com.inductivtechnologies.quantmarkapp.adapters.category;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.models.display.ProductTypeDisplay;
import com.inductivtechnologies.quantmarkapp.models.utils.ProductTypeGroup;
import com.inductivtechnologies.quantmarkapp.models.utils.ProductTypeGroupCustom;
import com.inductivtechnologies.quantmarkapp.utils.LanguageDisolay;
import com.inductivtechnologies.quantmarkapp.viewholders.category.SubCategoriesGridViewHolder;
import com.inductivtechnologies.quantmarkapp.viewholders.category.SubCategoriesGroupGridViewHolder;

import java.util.List;

/**
 * SubCategoryGroupAdapter
 */
public class SubCategoryGridAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    //Le context
    private final Context context;

    //Les données
    private final List<ProductTypeGroupCustom> subCategories;

    public SubCategoryGridAdapter(Context context, List<ProductTypeGroupCustom> subCategoriesGroup) {
        this.context = context;
        this.subCategories = subCategoriesGroup;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch(viewType){
            case ProductTypeGroupCustom.VIEW_TYPE_GROUP_PRODUCT_TYPE :
                View twiceView = inflater.inflate(R.layout.recyclerview_subcategories_group_grid_item, parent, false);
                viewHolder = new SubCategoriesGroupGridViewHolder(twiceView, context);
                break;
            case ProductTypeGroupCustom.VIEW_TYPE_SINGLE_PRODUCT_TYPE :
                View OneView =  inflater.inflate(R.layout.recyclerview_subcategories_grid_item, parent, false);
                viewHolder = new SubCategoriesGridViewHolder(OneView, context);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        int itemType = getItemViewType(position);

        switch(itemType){
            case ProductTypeGroupCustom.VIEW_TYPE_GROUP_PRODUCT_TYPE :
                ProductTypeGroup subcategoriesGroup = (ProductTypeGroup) subCategories.get(position);

                //On ajoute les données
                ((SubCategoriesGroupGridViewHolder) holder).setTitle(subcategoriesGroup.getLabel());
                ((SubCategoriesGroupGridViewHolder) holder).setSubTitle(subcategoriesGroup.getNbMarketOffers());

                break;
            case ProductTypeGroupCustom.VIEW_TYPE_SINGLE_PRODUCT_TYPE :
                ProductTypeDisplay subcategoriesDisplay = (ProductTypeDisplay) subCategories.get(position);

                //On ajoute les données
                ((SubCategoriesGridViewHolder) holder).seTvTitle((String) LanguageDisolay
                        .displaySubCategory(subcategoriesDisplay.getSubCategory()).get(LanguageDisolay.FIELD_TITLE));
                ((SubCategoriesGridViewHolder) holder).setThumbmail(subcategoriesDisplay.getSubCategory().getImage());

                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return subCategories.get(position).getType();
    }

    @Override
    public int getItemCount() {
        return subCategories != null ? subCategories.size() : 0;
    }
}
