package com.inductivtechnologies.quantmarkapp.adapters.home;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.models.display.MarketOfferDisplay;
import com.inductivtechnologies.quantmarkapp.utils.BindDataIntoViewholder;
import com.inductivtechnologies.quantmarkapp.viewholders.home.PromotionOffersViewholder;

import java.util.List;

/**
 * PromotionOfferAdapter
 */
public class PromotionOfferAdapter extends RecyclerView.Adapter<PromotionOffersViewholder>{

    //Context
    private final Context context;

    //:Les données
    private final List<MarketOfferDisplay> marketoffers;

    public PromotionOfferAdapter(Context context, List<MarketOfferDisplay>  marketoffers) {
        this.context = context;
        this.marketoffers = marketoffers;
    }

    @Override
    public PromotionOffersViewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_promotion_marketoffer_item,
                        parent, false);

        return new PromotionOffersViewholder(view, context);
    }

    @Override
    public void onBindViewHolder(PromotionOffersViewholder holder, int position) {
        MarketOfferDisplay marketOfferDisplay = marketoffers.get(position);

        BindDataIntoViewholder.addPromotionOffers(marketOfferDisplay, holder);
    }

    @Override
    public int getItemCount() {
        return marketoffers != null ? marketoffers.size() : 0;
    }

}
