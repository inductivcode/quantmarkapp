package com.inductivtechnologies.quantmarkapp.adapters.search;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.models.display.SearchHistoryDisplay;
import com.inductivtechnologies.quantmarkapp.models.display.SearchItemDisplay;
import com.inductivtechnologies.quantmarkapp.models.display.SearchNoticeDisplay;
import com.inductivtechnologies.quantmarkapp.models.utils.SearchItemCustom;
import com.inductivtechnologies.quantmarkapp.viewholders.search.SearchItemContainerHistoryViewHolder;
import com.inductivtechnologies.quantmarkapp.viewholders.search.SearchItemNoticeViewholder;
import com.inductivtechnologies.quantmarkapp.viewholders.search.SearchItemResultViewholder;

import java.util.List;

/**
 * SearchAdapter
 */
@SuppressWarnings("SameParameterValue")
public class SearchAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    //Le context
    private final Context context;

    //Les données
    private final List<SearchItemCustom> searchItems;
    private String searchValue;

    public SearchAdapter(Context context, List<SearchItemCustom> searchItems, String searchValue) {
        this.context = context;
        this.searchItems = searchItems;
        this.searchValue = searchValue;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch(viewType){
            case SearchItemCustom.VIEW_TYPE_HISTORY_ITEM :
                View containerView = inflater.inflate(R.layout.recyclerview_search_history_item, parent, false);
                viewHolder = new SearchItemContainerHistoryViewHolder(containerView, context);
                break;
            case SearchItemCustom.VIEW_TYPE_RESULT_ITEM :
                View twiceView = inflater.inflate(R.layout.recyclerview_search_item, parent, false);
                viewHolder = new SearchItemResultViewholder(twiceView, context);
                break;
            case SearchItemCustom.VIEW_TYPE_NOTICE_ITEM :
                View OneView =  inflater.inflate(R.layout.recyclerview_search_item_notice, parent, false);
                viewHolder = new SearchItemNoticeViewholder(OneView, context);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        int itemType = getItemViewType(position);

        switch(itemType){
            case SearchItemCustom.VIEW_TYPE_HISTORY_ITEM :
                SearchHistoryDisplay searchHistoryDisplay = (SearchHistoryDisplay) searchItems.get(position);

                //On ajoute les données
                ((SearchItemContainerHistoryViewHolder) holder).
                        initSearchistories(searchHistoryDisplay.getSearchHistories());

                break;
            case SearchItemCustom.VIEW_TYPE_RESULT_ITEM :
                SearchItemDisplay searchResult = (SearchItemDisplay) searchItems.get(position);

                //On ajoute les données
                ((SearchItemResultViewholder) holder).setTitle(searchResult.getItemTitle(),  this.searchValue);
                ((SearchItemResultViewholder) holder).setCountValue(searchResult.getItemNbResult());

                break;
            case SearchItemCustom.VIEW_TYPE_NOTICE_ITEM :
                SearchNoticeDisplay searchNotice = (SearchNoticeDisplay) searchItems.get(position);

                //On ajoute les données
                ((SearchItemNoticeViewholder) holder).setContent(searchNotice.getContent());

                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return searchItems.get(position).getType();
    }

    @Override
    public int getItemCount() {
        return searchItems != null ? searchItems.size() : 0;
    }

    public void setSearchValue(String searchValue) {
        this.searchValue = searchValue;
    }
}
