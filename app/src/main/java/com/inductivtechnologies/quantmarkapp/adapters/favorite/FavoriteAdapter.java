package com.inductivtechnologies.quantmarkapp.adapters.favorite;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.models.display.MarketOfferDisplay;
import com.inductivtechnologies.quantmarkapp.models.utils.MarketOfferDisplayCustom;
import com.inductivtechnologies.quantmarkapp.utils.BindDataIntoViewholder;
import com.inductivtechnologies.quantmarkapp.viewholders.favorite.FavoriteItemViewHolder;
import com.inductivtechnologies.quantmarkapp.viewholders.others.LoadMoreViewholder;

import java.util.List;

/**
 * FavoriteAdapter
 */
@SuppressWarnings("unused")
public class FavoriteAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    public interface OnLoadMoreListener {
        void onLoadMore();
    }

    //Context
    private final Context context;

    //:Les données
    private final List<MarketOfferDisplayCustom> favoritesDisplay;

    //Indique si le load more  a été ajouté
    private boolean withLoadMore = false;

    //Ecouteur sur le load more : permet de charger plus de données
    private OnLoadMoreListener loadMoreListener;

    public FavoriteAdapter(Context context, List<MarketOfferDisplayCustom>  favoritesDisplay) {
        this.context = context;
        this.favoritesDisplay = favoritesDisplay;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch(viewType){
            case MarketOfferDisplayCustom.TYPE_MARKETOFFER :
                View twiceView = inflater.inflate(R.layout.recyclerview_favorite_item, parent, false);
                viewHolder = new FavoriteItemViewHolder(twiceView, context);
                break;
            case MarketOfferDisplayCustom.TYPE_LOAD_MORE :
                View OneView = inflater.inflate(R.layout.load_more_item, parent, false);
                viewHolder = new LoadMoreViewholder(OneView, context);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof FavoriteItemViewHolder) {

            FavoriteItemViewHolder viewHolder = (FavoriteItemViewHolder) holder;
            MarketOfferDisplay marketOfferDisplay = (MarketOfferDisplay) favoritesDisplay.get(position);

            BindDataIntoViewholder.addFavoriteItems(marketOfferDisplay, viewHolder);

        } else {
            LoadMoreViewholder loadMoreViewHolder = (LoadMoreViewholder) holder;
        }
    }

    public List<MarketOfferDisplayCustom> getFavoritesDisplay() {
        return favoritesDisplay;
    }

    @Override
    public int getItemViewType(int position) {
        return favoritesDisplay.get(position).getType();
    }

    @Override
    public int getItemCount() {
        return favoritesDisplay != null ? favoritesDisplay.size() : 0;
    }

    //Active l'ecouteur

    public void setWithLoadMore(boolean value){
        withLoadMore = value;
    }

    // Retourne true si on est à la position du loadmore dans le recyclerview
    public boolean isPositionLoadMore(int position) {
        return position == getItemCount() - 1 && withLoadMore;
    }

    public FavoriteAdapter.OnLoadMoreListener getLoadMoreListener() {
        return loadMoreListener;
    }

    public void setLoadMoreListener(OnLoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }
}
