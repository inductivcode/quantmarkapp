package com.inductivtechnologies.quantmarkapp.adapters.others;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.viewholders.others.SelectItemRecyclerviewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * SelectItemRecyclerviewAdapter
 */
@SuppressWarnings("unchecked")
public class SelectItemRecyclerviewAdapter extends RecyclerView.Adapter<SelectItemRecyclerviewHolder> implements Filterable {

    //Context
    private final Context context;

    //Les données
    private final List<String> data;
    private List<String> dataFiltered;

    public SelectItemRecyclerviewAdapter(Context context, List<String> data) {
        this.context = context;
        this.data = data;
        this.dataFiltered = data;
    }

    @Override
    public SelectItemRecyclerviewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_select_item, parent, false);

        return new SelectItemRecyclerviewHolder(itemView, context);
    }

    @Override
    public void onBindViewHolder(final SelectItemRecyclerviewHolder viewHolder, int position) {
        // Add data
        String content = dataFiltered.get(position);
        viewHolder.setTvContentItem(content.toLowerCase());
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();

                if(charString.isEmpty()) {
                    dataFiltered = data;
                }else {
                    List<String> filteredList = new ArrayList<>();
                    for (String item : data) {
                        if (item.toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(item);
                        }
                    }

                    dataFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = dataFiltered;

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                dataFiltered = (ArrayList<String>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public List<String> getDataFiltered() {
        return dataFiltered;
    }

    @Override
    public int getItemCount() {
        return dataFiltered.size();
    }
}
