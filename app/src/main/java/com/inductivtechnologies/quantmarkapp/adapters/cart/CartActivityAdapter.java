package com.inductivtechnologies.quantmarkapp.adapters.cart;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.activities.cart.CartActivity;
import com.inductivtechnologies.quantmarkapp.listeners.cart.CartActivityListener;
import com.inductivtechnologies.quantmarkapp.models.entities.CartItem;
import com.inductivtechnologies.quantmarkapp.utils.BindDataIntoViewholder;
import com.inductivtechnologies.quantmarkapp.viewholders.cart.CartViewHolder;

import java.util.List;

/**
 * CartACtivityAdapter
 */
public class CartActivityAdapter extends RecyclerView.Adapter<CartViewHolder>{

    //Le context
    private final CartActivity context;
    //Les données du panier
    private final List<CartItem> cartitems;

    public CartActivityAdapter(CartActivity context, List<CartItem> cartitems) {
        this.context = context;
        this.cartitems = cartitems;
    }

    @Override
    public CartViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_cart_item, parent, false);

        return new CartViewHolder(context, itemView);
    }

    @Override
    public void onBindViewHolder(final CartViewHolder viewHolder, int position) {
        CartItem cartitem = cartitems.get(position);

        // Add data
        BindDataIntoViewholder.addCartItemToLayout(cartitem, viewHolder);

        //Gestion des évènements de click
        new CartActivityListener(context, this, cartitems, position, viewHolder);
    }

    @Override
    public int getItemCount() {
        return cartitems.size();
    }
}
