package com.inductivtechnologies.quantmarkapp.adapters.cart;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.fragments.cart.CartFragment;
import com.inductivtechnologies.quantmarkapp.listeners.cart.CartItemListener;
import com.inductivtechnologies.quantmarkapp.models.entities.CartItem;
import com.inductivtechnologies.quantmarkapp.utils.BindDataIntoViewholder;
import com.inductivtechnologies.quantmarkapp.viewholders.cart.CartViewHolder;

import java.util.List;

/**
 * CartItemAdapter
 */
public class CartItemAdapter extends RecyclerView.Adapter<CartViewHolder>{

    //Le fragment
    private final CartFragment fragment;
    //Les données du panier
    private final List<CartItem> cartitems;

    public CartItemAdapter(CartFragment fragment, List<CartItem> cartitems) {
        this.fragment = fragment;
        this.cartitems = cartitems;
    }

    @Override
    public CartViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_cart_item, parent, false);

        return new CartViewHolder(this.fragment.getActivity(), itemView);
    }

    @Override
    public void onBindViewHolder(final CartViewHolder viewHolder, int position) {
        CartItem cartitem = cartitems.get(position);

        // Add data
        BindDataIntoViewholder.addCartItemToLayout(cartitem, viewHolder);

        //Gestion des évènements de click
        new CartItemListener(this.fragment, this, cartitems, position, viewHolder);
    }

    @Override
    public int getItemCount() {
        return cartitems.size();
    }
}
