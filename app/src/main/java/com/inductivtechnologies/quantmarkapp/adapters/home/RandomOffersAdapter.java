package com.inductivtechnologies.quantmarkapp.adapters.home;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.models.display.MarketOfferDisplay;
import com.inductivtechnologies.quantmarkapp.utils.BindDataIntoViewholder;
import com.inductivtechnologies.quantmarkapp.viewholders.home.RandomOffersItemViewholder;

import java.util.List;

/**
 * HomeOfferAdapter : Implémente l'adaptateur pour l'affichage des offres sur la page d'accueil
 */
public class RandomOffersAdapter extends RecyclerView.Adapter<RandomOffersItemViewholder>{

    private final Context context;
    private final List<MarketOfferDisplay> marketOffersDisplay;

    public RandomOffersAdapter(Context mContext, List<MarketOfferDisplay> marketOffersDisplay) {
        this.context = mContext;
        this.marketOffersDisplay = marketOffersDisplay;
    }

    @Override
    public RandomOffersItemViewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_random_marketoffers_item, parent, false);

        return new RandomOffersItemViewholder(itemView, this.context);
    }

    @Override
    public void onBindViewHolder(final RandomOffersItemViewholder viewHolder, int position) {
        MarketOfferDisplay marketOfferDisplay = marketOffersDisplay.get(position);
        // Add data
        BindDataIntoViewholder.addRandomOffers(marketOfferDisplay, viewHolder);
    }

    @Override
    public int getItemCount() {
        return marketOffersDisplay.size();
    }
}
