package com.inductivtechnologies.quantmarkapp.adapters.home;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.models.display.ProductTypeDisplay;
import com.inductivtechnologies.quantmarkapp.utils.LanguageDisolay;
import com.inductivtechnologies.quantmarkapp.viewholders.home.StarCategoriesViewholder;

import java.util.List;

/**
 * StarCategoryAdapter
 */
public class StarCategoryAdapter extends RecyclerView.Adapter<StarCategoriesViewholder>{

    private final Context mContext;
    private final List<ProductTypeDisplay> subcategories;

    public StarCategoryAdapter(Context mContext, List<ProductTypeDisplay> subcategories) {
        this.mContext = mContext;
        this.subcategories = subcategories;
    }

    @Override
    public StarCategoriesViewholder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_star_category_item, parent, false);

        return new StarCategoriesViewholder(itemView, this.mContext);
    }

    @Override
    public void onBindViewHolder(final StarCategoriesViewholder viewHolder, int position) {
        ProductTypeDisplay subcategory = subcategories.get(position);

        // Add data
        viewHolder.setTvcontent((String) LanguageDisolay
                .displaySubCategory(subcategory.getSubCategory()).get(LanguageDisolay.FIELD_TITLE));
        viewHolder.setTvSubcontent(subcategory.getNbOfMarketOffer());
        viewHolder.setThumbmail(subcategory.getSubCategory().getImage());
    }

    @Override
    public int getItemCount() {
        return subcategories.size();
    }
}
