package com.inductivtechnologies.quantmarkapp.adapters.profile;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.callbacks.OnLoadMoreListener;
import com.inductivtechnologies.quantmarkapp.models.display.BuyDisplay;
import com.inductivtechnologies.quantmarkapp.models.utils.BuyDisplayCustom;
import com.inductivtechnologies.quantmarkapp.utils.BindDataIntoViewholder;
import com.inductivtechnologies.quantmarkapp.viewholders.others.LoadMoreViewholder;
import com.inductivtechnologies.quantmarkapp.viewholders.profile.OrdersItemViewholder;

import java.util.List;

/**
 * OrdersAdapter
 */
@SuppressWarnings("FieldCanBeLocal")
public class OrdersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    //Context
    private final Context context;

    //Les données
    private final List<BuyDisplayCustom>  ordersItems;

    //Indique si le load more  a été ajouté
    private boolean withLoadMore = false;

    private OnLoadMoreListener onLoadMoreListener;

    public OrdersAdapter(Context context, List<BuyDisplayCustom> ordersItems) {
        this.context = context;
        this.ordersItems = ordersItems;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch(viewType){
            case BuyDisplayCustom.VIEW_TYPE_BUY :
                View twiceView = inflater.inflate(R.layout.recyclerview_orders_item, parent, false);
                viewHolder = new OrdersItemViewholder(twiceView, context);
                break;
            case BuyDisplayCustom.VIEW_TYPE_LOAD_MORE :
                View OneView =  inflater.inflate(R.layout.load_more_item, parent, false);
                viewHolder = new LoadMoreViewholder(OneView, context);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof OrdersItemViewholder) {
            OrdersItemViewholder viewHolder = (OrdersItemViewholder) holder;

            BuyDisplay buyDisplay = (BuyDisplay) ordersItems.get(position);

            BindDataIntoViewholder.addOrdersData(buyDisplay, viewHolder);
        } else {
            LoadMoreViewholder loadMoreViewHolder = (LoadMoreViewholder) holder;
        }
    }

    @Override
    public int getItemViewType(int position) {
        return ordersItems.get(position).getType();
    }

    @Override
    public int getItemCount() {
        return ordersItems != null ? ordersItems.size() : 0;
    }

    public void setWithLoadMore(boolean value){
        withLoadMore = value;
    }

    // Retourne true si on est à la position du loadmore dans le recyclerview
    public boolean isPositionLoadMore(int position) {
        return position == getItemCount() - 1 && withLoadMore;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }
}
