package com.inductivtechnologies.quantmarkapp.design;

import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

/**
 * DisplayLargeNumber : Affichage des nombres large
 * Exemple : 1000 => 1K, 2 000 000 => 2M
 */
public class DisplayLargeNumber {

    private static final NavigableMap<Long, String> suffixesNumber = new TreeMap<>();
    static {
        suffixesNumber.put(1_000L, "K");
        suffixesNumber.put(1_000_000L, "M");
        suffixesNumber.put(1_000_000_000L, "G");
        suffixesNumber.put(1_000_000_000_000L, "T");
        suffixesNumber.put(1_000_000_000_000_000L, "P");
        suffixesNumber.put(1_000_000_000_000_000_000L, "E");
    }

    public static String smartLargeNumber(long value) {
        if (value < 1000) return Long.toString(value);

        Map.Entry<Long, String> entry = suffixesNumber.floorEntry(value);
        Long keyDividedBy = entry.getKey();
        String suffixNumberValue = entry.getValue();

        long truncated = value / (keyDividedBy / 10);
        boolean hasDecimal = truncated < 100 && (truncated / 10d) != (truncated / 10);
        return hasDecimal ? (truncated / 10d) + suffixNumberValue : (truncated / 10) + suffixNumberValue;
    }
    
}
