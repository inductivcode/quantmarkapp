package com.inductivtechnologies.quantmarkapp.models.http;

import com.inductivtechnologies.quantmarkapp.models.display.MarketOfferDisplay;
import com.inductivtechnologies.quantmarkapp.models.entities.Buy;
import com.inductivtechnologies.quantmarkapp.models.entities.Token;
import com.inductivtechnologies.quantmarkapp.models.entities.User;
import com.inductivtechnologies.quantmarkapp.utils.EntitiesUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * HttpResponseObject : Objet utile pour récupérer les données HTTP
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class HttpResponseObject {

    public static final int ERROR_CODE = 10;

    public static final int SUCCESS_CODE = 20;

    //Utilisateur inconnue
    public static final int UNKNOW_USER_ERROR_CODE = 30;
    //Login existant
    public static final int LOGIN_USER_EXIST_ERROR_CODE = 31;
    //Email existant
    public static final int EMAIL_USER_EXIST_ERROR_CODE = 32;
    //Login incorrect
    public static final int LOGIN_NO_MATCH_ERROR_CODE = 33;
    //Mot de passe incorrect
    public static final int PASSWORD_NO_MATCH_ERROR_CODE = 34;
    //Token inexistant
    public static final int TOKEN_UNEXIST_ERROR_CODE = 40;
    //L'achat
    public static final int PURCHASE_NOT_EXIST_CODE = 50;
    //La commande n'existe plus
    public static final int ORDER_NOT_EXIST_CODE = 60;

    public static final String CODE_STR = "code";
    public static final String MESSAGE_STR = "message";
    public static final String OBJ_VALUE_STR = "objectValue";

    public static final String TOKEN_STR = "token";
    public static final String BUYER_STR = "buyer";
    public static final String PURCHASE_STR = "purchase";
    public static final String PURCHASES_STR = "purchases";
    public static final String ORDER_STR = "order";
    public static final String ORDERS_STR = "orders";
    public static final String ISSUER_NB_ARTICLE_STR = "issuerNbArticle";

    public static final String MARKETOFFER_STR = "marketoffer";
    public static final String MARKETOFFERS_STR = "marketoffers";
    public static final String COUNT_STR = "count";

    public static final String DATA_IS_COMPLETE = "dataIsComplete";

    public static final String SMART_SUBCATEGORIES_STR = "smartSubcategories";
    public static final String LOW_COST_MARKETOFFERS_STR = "lowCostMarketOffers";
    public static final String PROMOTION_MARKETOFFERS_STR = "promotionMarketOffers";
    public static final String RANDOM_MARKETOFFERS_STR = "randomMarketOffers";

    public static final String VALIDATION_CODE_STR = "validationCode";

    private int code;
    private String message;
    private Map<String, Object> objectValue;

    //Constructeur
    public HttpResponseObject(int code, String message, Map<String, Object> objectValue) {
        this.code = code;
        this.message = message;
        this.objectValue = objectValue;
    }

    public HttpResponseObject(JSONObject jsonObject) {
        try{
            this.code = jsonObject.getInt(CODE_STR);
            this.message = jsonObject.getString(MESSAGE_STR);

            this.objectValue = new HashMap<>();
        }catch(JSONException exception){
            exception.printStackTrace();
        }
    }

    //Les méthodes

    public void addUserLoginResults(JSONObject jsonObject){
        try{
            JSONObject jsonInter = jsonObject.getJSONObject(OBJ_VALUE_STR);

            this.objectValue.put(TOKEN_STR, new Token(jsonInter.getJSONObject(TOKEN_STR)));
            this.objectValue.put(BUYER_STR, new User(jsonInter.getJSONObject(BUYER_STR)));
        }catch(JSONException exception){
            exception.printStackTrace();
        }
    }

    public void addUserSignupResults(JSONObject jsonObject){
        try{
            JSONObject jsonInter = jsonObject.getJSONObject(OBJ_VALUE_STR);

            this.objectValue.put(TOKEN_STR, new Token(jsonInter.getJSONObject(TOKEN_STR)));
            this.objectValue.put(BUYER_STR, new User(jsonInter.getJSONObject(BUYER_STR)));
            this.objectValue.put(VALIDATION_CODE_STR, jsonInter.getString(VALIDATION_CODE_STR));
        }catch(JSONException exception){
            exception.printStackTrace();
        }
    }

    public void addMarketofferDetailsResult(JSONObject jsonObject){
        try{
            JSONObject jsonInter = jsonObject.getJSONObject(OBJ_VALUE_STR);

            this.objectValue.put(COUNT_STR, jsonInter.getInt(COUNT_STR));
            this.objectValue.put(MARKETOFFER_STR, new MarketOfferDisplay(jsonInter.getJSONObject(MARKETOFFER_STR)));
            this.objectValue.put(MARKETOFFERS_STR, EntitiesUtils
                    .getMarketOffersDisplayByJSONArray(jsonInter.getJSONArray(MARKETOFFERS_STR)));
        }catch(JSONException exception){
            exception.printStackTrace();
        }
    }

    public void addPurchaseResults(JSONObject jsonObject){
        try{
            JSONObject jsonInter = jsonObject.getJSONObject(OBJ_VALUE_STR);

            this.objectValue.put(PURCHASE_STR, new Buy(jsonInter.getJSONObject(PURCHASE_STR)));
            this.objectValue.put(ISSUER_NB_ARTICLE_STR, jsonInter.getInt(ISSUER_NB_ARTICLE_STR));
        }catch(JSONException exception){
            exception.printStackTrace();
        }
    }

    public void addPurchasesResult(JSONObject jsonObject) {
        try{
            JSONObject jsonInter = jsonObject.getJSONObject(OBJ_VALUE_STR);

            this.objectValue.put(PURCHASES_STR, EntitiesUtils
                    .getBuyDisplayByJSONArray(jsonInter.getJSONArray(PURCHASES_STR)));
            this.objectValue.put(DATA_IS_COMPLETE, jsonInter.getBoolean(DATA_IS_COMPLETE));

        }catch(JSONException exception){
            exception.printStackTrace();
        }
    }

    public void addOrdersResult(JSONObject jsonObject) {
        try{
            JSONObject jsonInter = jsonObject.getJSONObject(OBJ_VALUE_STR);

            this.objectValue.put(ORDERS_STR, EntitiesUtils
                    .getBuyDisplayByJSONArray(jsonInter.getJSONArray(ORDERS_STR)));
            this.objectValue.put(DATA_IS_COMPLETE, jsonInter.getBoolean(DATA_IS_COMPLETE));

        }catch(JSONException exception){
            exception.printStackTrace();
        }
    }

    public void addOrderResults(JSONObject jsonObject){
        try{
            JSONObject jsonInter = jsonObject.getJSONObject(OBJ_VALUE_STR);

            this.objectValue.put(ORDER_STR, new Buy(jsonInter.getJSONObject(ORDER_STR)));
            this.objectValue.put(ISSUER_NB_ARTICLE_STR, jsonInter.getInt(ISSUER_NB_ARTICLE_STR));
        }catch(JSONException exception){
            exception.printStackTrace();
        }
    }

    public void addHomeResults(JSONObject jsonObject) {
        try{
            JSONObject jsonInter = jsonObject.getJSONObject(OBJ_VALUE_STR);

            this.objectValue.put(MARKETOFFERS_STR, EntitiesUtils
                    .getMarketOffersDisplayByJSONArray(jsonInter.getJSONArray(MARKETOFFERS_STR)));
            this.objectValue.put(DATA_IS_COMPLETE, jsonInter.getBoolean(DATA_IS_COMPLETE));

        }catch(JSONException exception){
            exception.printStackTrace();
        }
    }

    //Getters et setters

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Map<String, Object> getObjectValue() {
        return objectValue;
    }

}
