package com.inductivtechnologies.quantmarkapp.models.entities;

import com.inductivtechnologies.quantmarkapp.utils.EntitiesUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

/**
 *
 * Un Term est la classe utilisée pour representer les termes et conditions
 * de l'achat des products sur un marketOffer.
 *
 **/

@SuppressWarnings("unused")
public class Term{

    /**
     * idHexTerm identifiant unique de l'objet, modifiable
     */
    private String idHexTerm;

    /**
     * titleEn, titre du terme
     *
     */
    private String title;

    /**
     * descriptionFr, description du terme
     *
     */
    private String description;

    /**
     * creationDate, date de creation, modifiable
     *
     */
    private Date creationDate;

    // Constructeur(s)

    public Term() {
    }

    public Term(JSONObject jsonObject) {
        try{
            this.idHexTerm = jsonObject.isNull("idTerm") ? null : EntitiesUtils
                    .getObjectIdHexByJSONObject(jsonObject.getJSONObject("idTerm"));

            this.title = jsonObject.isNull("titleFr") ? ""
                    : jsonObject.getString("titleFr");
            this.description = jsonObject.isNull("descriptionFr") ? ""
                    : jsonObject.getString("descriptionFr");
            this.creationDate = jsonObject.isNull("creationDate") ? null :
                    new Date(jsonObject.getLong("creationDate"));
        }catch (JSONException excJSON){
            excJSON.printStackTrace();
        }
    }

    public Term(String idHexTerm, String title, String description, Date creationDate) {
        this.idHexTerm = idHexTerm;
        this.title = title;
        this.description = description;
        this.creationDate = creationDate;
    }

    // Override methods

    // Getters et setters

    public String getIdHexTerm() {
        return idHexTerm;
    }

    public void setIdHexTerm(String idHexTerm) {
        this.idHexTerm = idHexTerm;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
}
