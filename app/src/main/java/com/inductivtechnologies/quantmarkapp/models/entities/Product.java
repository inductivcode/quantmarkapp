package com.inductivtechnologies.quantmarkapp.models.entities;

import com.inductivtechnologies.quantmarkapp.utils.EntitiesUtils;

import org.bson.Document;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.List;

/**
 * Product
 **/

@SuppressWarnings("unused")
public class Product{

    /**
     * idHexProduct, représentation hexadécimale de objectid de product
     *
     */
    private String idHexProduct;

    /**
     * imageList, les images correspondant au produit
     *
     */
    private List<Image> imageList;

    /**
     * image, nom de l'image du product, modifiable
     *
     */
    private String image;

    /**
     * titleFr, titleFr du product
     *
     */
    private String titleFr;

    /**
     * descriptionFr, description du product
     *
     */
    private String descriptionFr;

    /**
     * titleEn, titleFr du product
     *
     */
    private String titleEn;

    /**
     * descriptionEn, description du product
     *
     */
    private String descriptionEn;

    /**
     * quantity, quantité disponible
     *
     */
    private int quantity;

    /**
     * detailFr, le detail sur le produit
     *
     */
    private Document detailFr;

    /**
     * detail, le detail sur le produit
     *
     */
    private Document detailEn;

    /**
     * issuer, c'est le commercant, personne detenant le product, modifiable
     */
    private Issuer issuer;

    /**
     * color
     */
    private String color;

    /**
     * creationDate, date de creation, modifiable
     *
     */
    private Date creationDate;
    
    /**
     * productCode, this field also identifier product, it's use to facilitate the management
     * of product between the issuer and developper Exemple if the client want to update information of
     * a product he will only send the productCode
     *
     */
    private String productCode;

    /**
     * originalprice
     *
     */
    private double originalprice;

    /**
     * tailles, Différentes tailles disponibles pour le produit
     *
     */
    private List<String> tailles;

   // Constructeur(s)

    public Product(){
    }

    public Product(JSONObject jsonObject) {
        try{
            this.idHexProduct = jsonObject.getJSONObject("idProduct") == null ? null : EntitiesUtils
                    .getObjectIdHexByJSONObject(jsonObject.getJSONObject("idProduct"));
            this.imageList = EntitiesUtils.getImagesByJSONArray(jsonObject.isNull("imageList") ? null
                    : jsonObject.getJSONArray("imageList"));
            this.tailles = EntitiesUtils.getListOfStringByJSONArray(jsonObject.isNull("tailles") ? null
                    : jsonObject.getJSONArray("tailles"));

            this.detailFr = EntitiesUtils.getDocumentByJSONObject(jsonObject.isNull("detailFr") ? null
                    : jsonObject.getJSONObject("detailFr"));
            this.detailEn = EntitiesUtils.getDocumentByJSONObject(jsonObject.isNull("detailEn") ? null
                    : jsonObject.getJSONObject("detailEn"));

            this.issuer = jsonObject.isNull("issuer") ? null :
                    new Issuer(jsonObject.getJSONObject("issuer"));

            this.image = jsonObject.isNull("image") ? "" :
                    jsonObject.getString("image");

            this.titleFr = jsonObject.isNull("titleFr") ? "" :
                    jsonObject.getString("titleFr");
            this.titleEn = jsonObject.isNull("titleEn") ? "" :
                    jsonObject.getString("titleEn");
            this.descriptionFr = jsonObject.isNull("descriptionFr") ? "" :
                    jsonObject.getString("descriptionFr");
            this.descriptionEn = jsonObject.isNull("descriptionEn") ? "" :
                    jsonObject.getString("descriptionEn");

            this.quantity = jsonObject.isNull("quantity") ? 0 :
                    jsonObject.getInt("quantity");
            this.color = jsonObject.isNull("color") ? "" :
                    jsonObject.getString("color");
            this.creationDate = jsonObject.isNull("creationDate") ? null :
                    new Date(jsonObject.getLong("creationDate"));
            this.productCode = jsonObject.isNull("productCode") ? ""
                    : jsonObject.getString("productCode");
            this.originalprice = jsonObject.getDouble("originalprice");
        }catch(JSONException jsonExc){
            jsonExc.printStackTrace();
        }
    }

    // Override methods

    // Getters et setters

    public String getIdHexProduct() {
        return idHexProduct;
    }

    public void setIdHexProduct(String idHexProduct) {
        this.idHexProduct = idHexProduct;
    }

    public List<Image> getImageList() {
        return imageList;
    }

    public void setImageList(List<Image> imageList) {
        this.imageList = imageList;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public List<String> getTailles() {
        return tailles;
    }

    public void setTailles(List<String> tailles) {
        this.tailles = tailles;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Document getDetailFr() {
        return detailFr;
    }

    public void setDetailFr(Document detail) {
        this.detailFr = detail;
    }

    public Document getDetailEn() {
        return detailEn;
    }

    public void setDetailEn(Document detail) {
        this.detailEn = detail;
    }

    public Issuer getIssuer() {
        return issuer;
    }

    public void setIssuer(Issuer issuer) {
        this.issuer = issuer;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public double getOriginalprice() {
        return originalprice;
    }

    public void setOriginalprice(double originalprice) {
        this.originalprice = originalprice;
    }

    public String getTitleFr() {
        return titleFr;
    }

    public void setTitleFr(String titleFr) {
        this.titleFr = titleFr;
    }

    public String getDescriptionFr() {
        return descriptionFr;
    }

    public void setDescriptionFr(String descriptionFr) {
        this.descriptionFr = descriptionFr;
    }

    public String getTitleEn() {
        return titleEn;
    }

    public void setTitleEn(String titleEn) {
        this.titleEn = titleEn;
    }

    public String getDescriptionEn() {
        return descriptionEn;
    }

    public void setDescriptionEn(String descriptionEn) {
        this.descriptionEn = descriptionEn;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
