package com.inductivtechnologies.quantmarkapp.models.display;

import com.inductivtechnologies.quantmarkapp.models.entities.SearchTrace;
import com.inductivtechnologies.quantmarkapp.models.utils.SearchItemCustom;

import java.util.List;

/**
 * SearchHistoryDisplay
 */

@SuppressWarnings("unused")
public class SearchHistoryDisplay extends SearchItemCustom {

    private List<SearchTrace> searchHistories;

    public SearchHistoryDisplay(List<SearchTrace> searchHistories) {
        this.searchHistories = searchHistories;
    }

    @Override
    public int getType() {
        return VIEW_TYPE_HISTORY_ITEM;
    }

    public List<SearchTrace> getSearchHistories() {
        return searchHistories;
    }

    public void setSearchHistories(List<SearchTrace> searchHistories) {
        this.searchHistories = searchHistories;
    }
}
