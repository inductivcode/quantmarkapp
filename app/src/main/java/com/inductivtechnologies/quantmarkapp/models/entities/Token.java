package com.inductivtechnologies.quantmarkapp.models.entities;

import com.inductivtechnologies.quantmarkapp.utils.EntitiesUtils;
import com.j256.ormlite.field.DatabaseField;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Token :Tokens
 *
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class Token{

    public final static String FIELD_NAME_ID = "idToken";
    public final static String FIELD_OBJECTID_HEX = "objectIdHexToken";
    public final static String FIELD_TOKEN_VALUE = "tokenValue";
    public final static String FIELD_TOKEN_CTEATION_DATE = "creationDate";

    /**
     * idToken, id dans la base de données salite
     */
    @DatabaseField(generatedId = true)
    private long idToken;

    /**
     * idToken, identifiant unique de l'objet
     */
    @DatabaseField
    private String objectIdHexToken;

    /**
     * tokenValue, La valeur du token
     */
    @DatabaseField
    private String tokenValue;

    /**
     * creationDate : La date de création du Token
     */
    @DatabaseField
    private long creationDate;

    // Constructeur

    public Token() {
        this.creationDate = System.currentTimeMillis();
    }

    public Token(String objectIdHexToken, String tokenValue, long creationDate) {
        this.objectIdHexToken = objectIdHexToken;
        this.tokenValue = tokenValue;
        this.creationDate = creationDate;
    }

    public Token(JSONObject jsonObject) {
        try{
            this.objectIdHexToken = jsonObject.isNull(FIELD_NAME_ID) ? null : EntitiesUtils
                    .getObjectIdHexByJSONObject(jsonObject.getJSONObject(FIELD_NAME_ID));
            this.tokenValue = jsonObject.isNull(FIELD_TOKEN_VALUE) ? null : jsonObject.getString(FIELD_TOKEN_VALUE);
            this.creationDate = jsonObject.getLong(FIELD_TOKEN_CTEATION_DATE);
        }catch(JSONException exception){
            exception.printStackTrace();
        }
    }

    //Override

    // Getters et setters

    public long getIdToken() {
        return idToken;
    }

    public void setIdToken(long idToken) {
        this.idToken = idToken;
    }

    public String getObjectIdHexToken() {
        return objectIdHexToken;
    }

    public void setObjectIdHexToken(String objectIdHexToken) {
        this.objectIdHexToken = objectIdHexToken;
    }

    public String getTokenValue() {
        return tokenValue;
    }

    public void setTokenValue(String tokenValue) {
        this.tokenValue = tokenValue;
    }

    public long getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(long creationDate) {
        this.creationDate = creationDate;
    }
}
