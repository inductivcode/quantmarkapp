package com.inductivtechnologies.quantmarkapp.models.http;

import java.util.HashMap;
import java.util.Map;

/**
 * NameValuePair : Classe utilisée pour sauvegarder les paramètres de requêtes
 */
@SuppressWarnings({"CanBeFinal", "unused"})
public class NameValuePair {

    private Map<String, Object> paramsMap;

    // Constructeur(s)

    public NameValuePair() {
        this.paramsMap = new HashMap<>();
    }

    // Getters et setters

    public Map<String, Object> getParamsMap() {
        return paramsMap;
    }

    public void setParamsMap(String key, Object value) {
        this.paramsMap.put(key, value);
    }

}
