package com.inductivtechnologies.quantmarkapp.models.utils;

/**
 * ProductTypeGroupCustom
 */
public abstract class ProductTypeGroupCustom {

    public static final int VIEW_TYPE_SINGLE_PRODUCT_TYPE = 0;
    public static final int VIEW_TYPE_GROUP_PRODUCT_TYPE = 1;

    //Retourne le type d'une sous catégorie
    abstract public int getType();

}
