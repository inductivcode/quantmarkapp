package com.inductivtechnologies.quantmarkapp.models.entities;

import com.j256.ormlite.field.DatabaseField;

/**
 * CartItem : Implémnet un élément du panier
 */

@SuppressWarnings("unused")
public class CartItem{

    // Champs statiques
    public static final String FIELD_NAME_OBJECTID_HEX = "idHexMarketOffer";

    /**
     * idCartItem : id dans la base de données
     * Non null
     */
    @DatabaseField(generatedId = true)
    private long idCartItem;

    /**
     * idHexMarketOffer : représentation hexadecimale de l'objectId du marché(marketOffer)
     * Non null
     */
    @DatabaseField
    private String idHexMarketOffer;

    /**
     * image : L'image
     */
    @DatabaseField
    private String image;

    /**
     * titleFr : Titre du produit correspondant
     */
    @DatabaseField
    private String titleFr;

    /**
     * description : Description du produit correspondant
     */
    @DatabaseField
    private String titleEn;

    /**
     * issuerName : Nom du vendeur
     */
    @DatabaseField
    private String issuerName;

    /**
     * quantity : Quantité demandée
     */
    @DatabaseField
    private int quantity;

    /**
     * code : Le code du produit
     */
    @DatabaseField
    private String code;

    /**
     * singlePrice : Prix unitaire après réduction si il en a
     */
    @DatabaseField
    private double singlePrice;

    /**
     * reductionPrice : Prix réduit
     */
    @DatabaseField
    private double reductionPrice;

    /**
     * reductionValue : La valuer de la reduction
     */
    @DatabaseField
    private String reductionValue;

    /**
     * totalPrice : Prix total pour cet article du panier
     */
    @DatabaseField
    private double totalPrice;

    /**
     * lastDateConnexion : Date d'ajout
     */
    @DatabaseField
    private long creationDate;

    //Constructeur(s)

    public CartItem() {
        this.creationDate = System.currentTimeMillis();
    }

    // Getters et setteus

    public String getIdHexMarketOffer() {
        return idHexMarketOffer;
    }

    public void setIdHexMarketOffer(String idHexMarketOffer) {
        this.idHexMarketOffer = idHexMarketOffer;
    }

    public long getIdCartItem() {
        return idCartItem;
    }

    public void setIdCartItem(long idCartItem) {
        this.idCartItem = idCartItem;
    }

    public String getTitleFr() {
        return titleFr;
    }

    public void setTitleFr(String titleFr) {
        this.titleFr = titleFr;
    }

    public String getTitleEn() {
        return titleEn;
    }

    public void setTitleEn(String titleEn) {
        this.titleEn = titleEn;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getIssuerName() {
        return issuerName;
    }

    public void setIssuerName(String issuerName) {
        this.issuerName = issuerName;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getSinglePrice() {
        return singlePrice;
    }

    public void setSinglePrice(double singlePrice) {
        this.singlePrice = singlePrice;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public long getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(long creationDate) {
        this.creationDate = creationDate;
    }

    public String getReductionValue() {
        return reductionValue;
    }

    public void setReductionValue(String reductionValue) {
        this.reductionValue = reductionValue;
    }
    public double getReductionPrice() {
        return reductionPrice;
    }

    public void setReductionPrice(double reductionPrice) {
        this.reductionPrice = reductionPrice;
    }

}
