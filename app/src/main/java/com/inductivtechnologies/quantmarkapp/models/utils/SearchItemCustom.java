package com.inductivtechnologies.quantmarkapp.models.utils;

/**
 * SearchItemCustom
 */
public abstract class SearchItemCustom {

    public static final int VIEW_TYPE_RESULT_ITEM = 0;
    public static final int VIEW_TYPE_NOTICE_ITEM = 1;
    public static final int VIEW_TYPE_HISTORY_ITEM = 2;

    //Retourne le type d'une sous catégorie
    abstract public int getType();

}
