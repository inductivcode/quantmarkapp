package com.inductivtechnologies.quantmarkapp.models.entities;

import com.j256.ormlite.field.DatabaseField;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Address est la classe représentant l'adresse d'un utilisateur
 *
 **/

@SuppressWarnings("unused")
public class Address {

    public static final String FIELD_NAME_ID = "idAddress";
    public static final String FIELD_NAME_CITY = "city";
    public static final String FIELD_NAME_COUNTRY = "country";
    public static final String FIELD_NAME_DISTRICT = "street";
    public static final String FIELD_NAME_EMAIL = "email";
    public static final String FIELD_NAME_POBOX = "poBox";

    /**
     * idAddress : id dans la base de données
     * Non null
     */
    @DatabaseField(generatedId = true)
    private transient long idAddress;

    /**
     * email contenu dans l'adresse
     *
     */
    @DatabaseField
    private String email;

    /**
     * valide, boolean indiquant si l'adresse est validée ou pas, elle est effectuée pendant l'inscription
     *  de l'utilisateur.
     */
    @DatabaseField
    private boolean valide;

    /**
     * country, pays faisant partie de l'adresse
     *
     */
    @DatabaseField
    private String country;

    /**
     * country, pays faisant partie de l'adresse
     *
     */
    @DatabaseField
    private String city;

    /**
     * street, rue contenu dans l'adresse
     *
     */
    @DatabaseField
    private String street;

    /**
     * poBox, code postal de l'adresse
     *
     */
    @DatabaseField
    private String poBox;

    // Constructeur(s)

    public Address(){
    }

    public Address(JSONObject jsonObject){
        try{
            this.email = jsonObject.isNull("email") ? "" :
                    jsonObject.getString("email");
            this.valide = jsonObject.getBoolean("valide");
            this.country = jsonObject.isNull("country") ? "" :
                    jsonObject.getString("country");
            this.city = jsonObject.isNull("city") ? "" :
                    jsonObject.getString("city");
            this.street = jsonObject.isNull("street") ? "" :
                    jsonObject.getString("street");
            this.poBox = jsonObject.isNull("poBox") ? "" :
                    jsonObject.getString("poBox");
        }catch (JSONException excJSON){
            excJSON.printStackTrace();
        }
    }

    public Address(String email, String country, String city,
                   String street, String poBox) {
        this.email = email;
        this.country = country;
        this.city = city;
        this.street = street;
        this.poBox = poBox;
    }

    // Override methods

    // Getters et setters

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isValide() {
        return valide;
    }

    public void setValide(boolean valide) {
        this.valide = valide;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getPoBox() {
        return poBox;
    }

    public void setPoBox(String poBox) {
        this.poBox = poBox;
    }
}
