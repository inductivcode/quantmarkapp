package com.inductivtechnologies.quantmarkapp.models.entities;

import com.inductivtechnologies.quantmarkapp.utils.EntitiesUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.List;

/**
 * MarketOffer : Représentation d'un marché
 */

@SuppressWarnings("unused")
public class MarketOffer {

    /**
     * idHexMarketOffer : représentation hexadecimale de l'objectId d'un marcketoffer
     */
    private String idHexMarketOffer;

    /**
     * price, prix du marché
     *
     */
    private double price;

    /**
     * creationDate, date de creation du marché, modifiable
     *
     */
    private Date creationDate;

    /**
     * expirationDate, date d'expiration du marché, modifiable
     *
     */
    private Date expirationDate;

    /**
     * product, produit du marché, modifiable
     */
    private Product product;

    /**
     * coupon, offre du Marketoffer, modifiable
     */
    private Coupon coupon;

    /**
     * terms, liste des termes et conditions, modifiable
     */
    private List<Term> terms;

    // Constructeur(s)

    public MarketOffer() {
    }

    public MarketOffer(JSONObject jsonObject) {
        try{
            this.idHexMarketOffer = jsonObject.isNull("idMarketOffer") ? null : EntitiesUtils
                    .getObjectIdHexByJSONObject(jsonObject.getJSONObject("idMarketOffer"));

            this.creationDate = jsonObject.isNull("creationDate") ? null :
                    new Date(jsonObject.getLong("creationDate"));
            this.expirationDate = jsonObject.isNull("expirationDate") ? null :
                    new Date(jsonObject.getLong("expirationDate"));

            this.product = jsonObject.isNull("product") ? null :
                    new Product(jsonObject.getJSONObject("product"));
            this.coupon = jsonObject.isNull("coupon") ? null :
                    new Coupon(jsonObject.getJSONObject("coupon"));

            this.price = jsonObject.getDouble("price");

            this.terms = EntitiesUtils.getTermsByJSONArray(jsonObject.isNull("terms") ? null :
                    jsonObject.getJSONArray("terms"));
        }catch (JSONException excJSON){
            excJSON.printStackTrace();
        }
    }

    public MarketOffer(String idHexMarketOffer, double price, Date creationDate, Date expirationDate, Product product, Coupon coupon, List<Term> terms) {
        this.idHexMarketOffer = idHexMarketOffer;
        this.price = price;
        this.creationDate = creationDate;
        this.expirationDate = expirationDate;
        this.product = product;
        this.coupon = coupon;
        this.terms = terms;
    }


    // Override methods

    // Getters et setters

    public String getIdHexMarketOffer() {
        return idHexMarketOffer;
    }

    public void setIdHexMarketOffer(String idHexMarketOffer) {
        this.idHexMarketOffer = idHexMarketOffer;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Coupon getCoupon() {
        return coupon;
    }

    public void setCoupon(Coupon coupon) {
        this.coupon = coupon;
    }

    public List<Term> getTerms() {
        return terms;
    }

    public void setTerms(List<Term> terms) {
        this.terms = terms;
    }
}
