package com.inductivtechnologies.quantmarkapp.models.display;

import com.inductivtechnologies.quantmarkapp.models.utils.SearchItemCustom;

/**
 * SearchDisplayNotice
 */
@SuppressWarnings("CanBeFinal")
public class SearchNoticeDisplay extends SearchItemCustom {

    /**
     * content : Contenu
     */
    private String content;

    public SearchNoticeDisplay(String content) {
        this.content = content;
    }

    @Override
    public int getType() {
        return VIEW_TYPE_NOTICE_ITEM;
    }

    public String getContent() {
        return content;
    }
}
