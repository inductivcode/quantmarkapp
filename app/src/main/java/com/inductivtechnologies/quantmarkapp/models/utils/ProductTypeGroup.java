package com.inductivtechnologies.quantmarkapp.models.utils;

import com.inductivtechnologies.quantmarkapp.configuration.StringConfig;
import com.inductivtechnologies.quantmarkapp.models.display.ProductTypeDisplay;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * ProductTypeGroup
 */
@SuppressWarnings("unused")
public class ProductTypeGroup extends ProductTypeGroupCustom{

    private static final String MAP_KEY_OTHER = "Others";

    /**
     * groupName : Libellé du groupe
     */
    private String label;

    /**
     * subCategoriesDisplay : Les sous-catégories du groupe
     */
    private List<ProductTypeDisplay> subCategoriesDisplay;

    /**
     * nbMarketOffers :Nombre de marché dans la groupe
     */
    private long nbMarketOffers;

    //Constructeur(s)

    public ProductTypeGroup(JSONArray jsonArray) {
        this.subCategoriesDisplay = new ArrayList<>();

        try{
            String interStr;

            switch(Locale.getDefault().getLanguage()){
                case StringConfig.DEFAULT_FR :
                    interStr = jsonArray.getJSONObject(0).getJSONObject("subCategory").getString("groupeFr");
                    break;
                case StringConfig.DEFAULT_EN :
                    interStr = jsonArray.getJSONObject(0).getJSONObject("subCategory").getString("groupeEn");
                    break;
                default :
                    interStr = jsonArray.getJSONObject(0).getJSONObject("subCategory").getString("groupeFr");
                    break;
            }

            this.label = interStr == null ? "" : interStr;

           for(int i=0; i < jsonArray.length(); i++){
                this.subCategoriesDisplay.add(new ProductTypeDisplay(jsonArray.getJSONObject(i)));
            }
        }catch(JSONException exception){
            exception.printStackTrace();
        }

        this.nbMarketOffers = getNbMarketOffers();
    }

    //Override

    @Override
    public int getType() {
        return VIEW_TYPE_GROUP_PRODUCT_TYPE;
    }

    //Getters et setters

    public List<ProductTypeDisplay> getSubCategoriesDisplay() {
        return subCategoriesDisplay;
    }

    public void setSubCategoriesDisplay(List<ProductTypeDisplay> subCategoriesDisplay) {
        this.subCategoriesDisplay = subCategoriesDisplay;
    }

    public long getNbMarketOffers() {
        nbMarketOffers = 0;

        for(ProductTypeDisplay productTypeDisplay : subCategoriesDisplay){
            nbMarketOffers += productTypeDisplay.getNbOfMarketOffer();
        }

        return nbMarketOffers;
    }

    public void setNbMarketOffers(long nbMarketOffers) {
        this.nbMarketOffers = nbMarketOffers;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
