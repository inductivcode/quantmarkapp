package com.inductivtechnologies.quantmarkapp.models.entities;

import com.j256.ormlite.field.DatabaseField;

/**
 * SearchTrace : Pour garder une trace des récentes recherches
 *
 **/
@SuppressWarnings("unused")
public class SearchTrace {

    public static final String FIELD_ID_HEX = "idHex";

    /**
     * id : id dans la base de données
     */
    @DatabaseField(generatedId = true)
    private long id;

    /**
     * value label de la recherche
     *
     */
    @DatabaseField
    private String value;

    /**
     * idHex id de la source
     *
     */
    @DatabaseField
    private String idHex;

    /**
     * searchFilter indique quel filtre de recherche a été utilisé
     *
     */
    @DatabaseField
    private int searchFilter;

    /**
     * nbItems,nombre de résultats
     */
    @DatabaseField
    private long nbItems;

    /**
     * creationDate,Date de création
     */
    @DatabaseField
    private long creationDate;

    //Constructeur(s)

    public SearchTrace() {
        this.creationDate = System.currentTimeMillis();
    }

    //Getters et setters

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getIdHex() {
        return idHex;
    }

    public void setIdHex(String idHex) {
        this.idHex = idHex;
    }

    public long getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(long creationDate) {
        this.creationDate = creationDate;
    }

    public long getNbItems() {
        return nbItems;
    }

    public void setNbItems(long nbItems) {
        this.nbItems = nbItems;
    }

    public int getSearchFilter() {
        return searchFilter;
    }

    public void setSearchFilter(int searchFilter) {
        this.searchFilter = searchFilter;
    }
}
