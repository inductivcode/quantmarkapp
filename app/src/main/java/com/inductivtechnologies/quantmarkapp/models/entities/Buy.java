package com.inductivtechnologies.quantmarkapp.models.entities;

import com.inductivtechnologies.quantmarkapp.utils.EntitiesUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

/**
 * Buy : Représentation d'un achat
 */

@SuppressWarnings("unused")
public class Buy {

    /**
     * idHexBuy, identifiant unique de l'objet
     */
    private String idHexBuy;

    /**
     * quantity, quantité de l'achat
     */
    private int quantity;

    /**
     * isComplete, état de l'achat
     */
    private boolean isCompleted;

    /**
     * isCompleteBuyer, état de l'achat côte buyer, spécifie que
     * l'acheteur est bien allé dans le magasin
     */
    private boolean isCompletedBuyer;

    /**
     * point, point acquis dans le magasin ou l'entreprise,
     */
    private int point;

    /**
     * purchaseDate, date de l'achat sur la plateforme (recuperation du code)
     */
    private Date purchaseDate;

    /**
     * completeDate, date à laquelle l'acheteur va dans le magasin
     */
    private Date completeDate;

    /**
     * marketOffer, marché sur lequel l'achat à été effectué
     */
    private MarketOffer marketOffer;

    /**
     *   code avec lequel l'achéteur se présente dans le magasin pour la validation de l'achat,
     *   ce code est unique, il est généré à partir d'un algorithme
     */
    private String code;

    /**
     * deliverPlace, lieu de livraison
     */
    private String deliverPlace;

    /**
     * paymentType, Type de payement
     */
    private String paymentType;

    /**
     * emailAlt : email de notification
     */
    private String emailAlt;

    /**
     * telAlt : numéro de notification
     */
    private String telAlt;

    /**
     * nameAlt : Nom de la parsonne qui récupère
     */
    private String nameAlt;

    /**
     * firstNameAlt  Prénom de la parsonne qui récupère
     */
    private String firstNameAlt;

    /**
     * totalBuy : prix d'achat total de l'article
     */
    private double totalBuy ;

    //Constructeur(s)

    public Buy() {
    }

    public Buy(JSONObject jsonObject) {
        try {
            this.idHexBuy= jsonObject.isNull("idBuy") ? null : EntitiesUtils
                    .getObjectIdHexByJSONObject(jsonObject.getJSONObject("idBuy"));

            this.purchaseDate = jsonObject.isNull("purchaseDate") ? null :
                    new Date(jsonObject.getLong("purchaseDate"));
            this.completeDate = jsonObject.isNull("completeDate") ? null :
                    new Date(jsonObject.getLong("completeDate"));

            this.marketOffer = jsonObject.isNull("marketOffer") ? null :
                    new MarketOffer(jsonObject.getJSONObject("marketOffer"));

            this.quantity = jsonObject.isNull("quantity") ? 0
                    : jsonObject.getInt("quantity");
            this.isCompleted = jsonObject.getBoolean("isCompleted");
            this.isCompletedBuyer = jsonObject.getBoolean("isCompletedBuyer");
            this.point =  jsonObject.isNull("point") ? 0
                    : jsonObject.getInt("point");

            this.code = jsonObject.isNull("code") ? "" :
                    jsonObject.getString("code");
            this.deliverPlace = jsonObject.isNull("deliverPlace") ? "" :
                    jsonObject.getString("deliverPlace");
            this.paymentType = jsonObject.isNull("paymentType") ? "" :
                    jsonObject.getString("paymentType");

            this.emailAlt = jsonObject.isNull("emailAlt") ? "" :
                    jsonObject.getString("emailAlt");
            this.telAlt = jsonObject.isNull("telAlt") ? "" :
                    jsonObject.getString("telAlt");
            this.nameAlt = jsonObject.isNull("nameAlt") ? "" :
                    jsonObject.getString("nameAlt");
            this.firstNameAlt = jsonObject.isNull("firstNameAlt") ? "" :
                    jsonObject.getString("firstNameAlt");

            this.totalBuy = jsonObject.isNull("totalBuy") ? 0 :
                    jsonObject.getDouble("totalBuy");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //Getters et setters

    public String getIdHexBuy() {
        return idHexBuy;
    }

    public void setIdHexBuy(String idHexBuy) {
        this.idHexBuy = idHexBuy;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public boolean isCompleted() {
        return isCompleted;
    }

    public void setCompleted(boolean completed) {
        isCompleted = completed;
    }

    public boolean isCompletedBuyer() {
        return isCompletedBuyer;
    }

    public void setCompletedBuyer(boolean completedBuyer) {
        isCompletedBuyer = completedBuyer;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public Date getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(Date purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public Date getCompleteDate() {
        return completeDate;
    }

    public void setCompleteDate(Date completeDate) {
        this.completeDate = completeDate;
    }

    public MarketOffer getMarketOffer() {
        return marketOffer;
    }

    public void setMarketOffer(MarketOffer marketOffer) {
        this.marketOffer = marketOffer;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDeliverPlace() {
        return deliverPlace;
    }

    public void setDeliverPlace(String deliverPlace) {
        this.deliverPlace = deliverPlace;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getEmailAlt() {
        return emailAlt;
    }

    public void setEmailAlt(String emailAlt) {
        this.emailAlt = emailAlt;
    }

    public String getTelAlt() {
        return telAlt;
    }

    public void setTelAlt(String telAlt) {
        this.telAlt = telAlt;
    }

    public String getNameAlt() {
        return nameAlt;
    }

    public void setNameAlt(String nameAlt) {
        this.nameAlt = nameAlt;
    }

    public String getFirstNameAlt() {
        return firstNameAlt;
    }

    public void setFirstNameAlt(String firstNameAlt) {
        this.firstNameAlt = firstNameAlt;
    }

    public double getTotalBuy() {
        return totalBuy;
    }

    public void setTotalBuy(double totalBuy) {
        this.totalBuy = totalBuy;
    }
}
