package com.inductivtechnologies.quantmarkapp.models.display;

import com.inductivtechnologies.quantmarkapp.models.utils.MarketOfferDisplayCustom;

/**
 * LoadMoreMarketOfferDisplay
 */

public class LoadMoreMarketOfferDisplay extends MarketOfferDisplayCustom {

    @Override
    public int getType() {
        return TYPE_LOAD_MORE;
    }
}
