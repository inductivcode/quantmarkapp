package com.inductivtechnologies.quantmarkapp.models.entities;

import org.json.JSONObject;

/**
 *
 * Issuer est la classe representant un commercant dans la plateforme quantmark
 **/

@SuppressWarnings("unused")
public class Issuer extends User{

    // Constructeur(s)

    public Issuer(){
    }

    public Issuer(JSONObject jsonObject){
        super(jsonObject);
    }

    //Override

    // Getters et setters
}
