package com.inductivtechnologies.quantmarkapp.models.display;

import com.inductivtechnologies.quantmarkapp.models.entities.ProductType;
import com.inductivtechnologies.quantmarkapp.models.utils.ProductTypeGroupCustom;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * SubcategoriesDisplay : Utiliser pour l'affichage des sous-catégories
 */

@SuppressWarnings("unused")
public class ProductTypeDisplay extends ProductTypeGroupCustom {

    /**
     * subCategory : La sous categorie
     */
    private ProductType subCategory;

    /**
     * nbOfMarketOffer : Nombre de marchés dans la sous catégorie
     */
    private long nbOfMarketOffer;

    //Constructeur(s)

    public ProductTypeDisplay() {
    }

    public ProductTypeDisplay(JSONObject jsonObject) {
        try {
            this.subCategory = new ProductType(jsonObject.getJSONObject("subCategory"));
            this.nbOfMarketOffer = jsonObject.getLong("nbOfMarketOffer");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public ProductTypeDisplay(ProductType subCategory, long nbOfMarketOffer) {
        this.subCategory = subCategory;
        this.nbOfMarketOffer = nbOfMarketOffer;
    }

    //Override

    @Override
    public int getType() {
        return VIEW_TYPE_SINGLE_PRODUCT_TYPE;
    }

    //Getters et setters

    public ProductType getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(ProductType subCategory) {
        this.subCategory = subCategory;
    }

    public long getNbOfMarketOffer() {
        return nbOfMarketOffer;
    }

    public void setNbOfMarketOffer(long nbOfMarketOffer) {
        this.nbOfMarketOffer = nbOfMarketOffer;
    }
}
