package com.inductivtechnologies.quantmarkapp.models.display;

import org.json.JSONObject;

/**
 * PropertiesNumber : Pour initialiser certaines valeur dans le profil de l'utilisateur
 * Exemple : le nombre de point, le nombre d'achats
 */

@SuppressWarnings("unused")
public class PropertiesNumber{

    /**
     * nbOfOrders : Nombre de commandes
     */
    private long nbOfOrders;

    /**
     * nbOfOrders : Nombre de commandes
     */
    private long nbOfPurchases;

    /**
     * nbPoints : Nombre de commandes
     */
    private long nbPoints;

    public PropertiesNumber(){

    }

    public PropertiesNumber(JSONObject jsonObject){
        nbOfOrders = jsonObject.optLong("nbOfOrders");
        nbOfPurchases = jsonObject.optLong("nbOfPurchases");
        nbPoints = jsonObject.optLong("nbPoints");
    }

    public long getNbOfOrders() {
        return nbOfOrders;
    }

    public void setNbOfOrders(long nbOfOrders) {
        this.nbOfOrders = nbOfOrders;
    }

    public long getNbOfPurchases() {
        return nbOfPurchases;
    }

    public void setNbOfPurchases(long nbOfPurchases) {
        this.nbOfPurchases = nbOfPurchases;
    }

    public long getNbPoints() {
        return nbPoints;
    }

    public void setNbPoints(long nbPoints) {
        this.nbPoints = nbPoints;
    }
}
