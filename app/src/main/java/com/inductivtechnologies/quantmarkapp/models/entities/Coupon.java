package com.inductivtechnologies.quantmarkapp.models.entities;

import com.inductivtechnologies.quantmarkapp.utils.EntitiesUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.List;

/**
 *
 * Coupon
 **/

@SuppressWarnings("unused")
public class Coupon{

    /**
     * idHexCoupon, identifiant unique de l'objet, modifiable
     */
    private String idHexCoupon;

    /**
     * value, valeur de la reduction du coupon, modifiable
     *
     **/
    private String value;

    /**
     * creationDate, date de creation, modifiable
     */
    private Date creationDate;

    // Contructeur(s)

    public Coupon() {
    }

    public Coupon(JSONObject jsonObject) {
        try{
            this.idHexCoupon= jsonObject.isNull("idCoupon") ? null : EntitiesUtils
                    .getObjectIdHexByJSONObject(jsonObject.getJSONObject("idCoupon"));

            this.value = jsonObject.isNull("value") ? "" :
                    jsonObject.getString("value");
            this.creationDate = jsonObject.isNull("creationDate") ? null :
                    new Date(jsonObject.getLong("creationDate"));
        }catch (JSONException excJSON){
            excJSON.printStackTrace();
        }
    }

    public Coupon(String idHexCoupon, String type, String value, String title, String description,
                  Date creationDate, List<MarketOffer> marketOffers) {
        this.idHexCoupon = idHexCoupon;
        this.value = value;
        this.creationDate = creationDate;
    }

    // Override methods

    // Getters et setters

    public String getIdHexCoupon() {
        return idHexCoupon;
    }

    public void setIdHexCoupon(String idHexCoupon) {
        this.idHexCoupon = idHexCoupon;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
}
