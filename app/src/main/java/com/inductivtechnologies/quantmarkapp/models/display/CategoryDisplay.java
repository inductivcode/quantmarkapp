package com.inductivtechnologies.quantmarkapp.models.display;


import com.inductivtechnologies.quantmarkapp.models.entities.Category;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * CategoryEnriched : Cette classe représente la classe Category enrichie de quelques propriétés
 */

@SuppressWarnings("unused")
public class CategoryDisplay {

    /**
     * category : La categorie
     */
    private Category category;

    /**
     * nbOfMarketOffers : Nombre de marché actis
     */
    private long nbOfMarketOffers;

    //Constructeur(s)

    public CategoryDisplay(JSONObject jsonObject) {
        try {
            this.category = new Category(jsonObject.getJSONObject("category"));
            this.nbOfMarketOffers = jsonObject.getLong("nbOfMarketOffers");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //Getters et setters

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public long getNbOfMarketOffers() {
        return nbOfMarketOffers;
    }
}
