package com.inductivtechnologies.quantmarkapp.models.display;

import com.inductivtechnologies.quantmarkapp.models.entities.MarketOffer;
import com.inductivtechnologies.quantmarkapp.models.utils.MarketOfferDisplayCustom;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * MarketOfferDisplay : Classe utilisée pour afficher les produits en grille ou en liste
 */

@SuppressWarnings("unused")
public class MarketOfferDisplay extends MarketOfferDisplayCustom {

    private MarketOffer marketOffer;

    /**
     * nbOrder : Nombre de commandes
     */
    private long nbOrder;

    //Constructeur(s)

    public MarketOfferDisplay(JSONObject jsonObject) {
        try {
            this.marketOffer = new MarketOffer(jsonObject.getJSONObject("marketOffer"));
            this.nbOrder = jsonObject.getLong("nbOrder");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getType() {
        return TYPE_MARKETOFFER;
    }

    //Getters et setters

    public MarketOffer getMarketOffer() {
        return marketOffer;
    }

    public void setMarketOffer(MarketOffer marketOffer) {
        this.marketOffer = marketOffer;
    }

    public long getNbOrder() {
        return nbOrder;
    }

    public void setNbOrder(long nbOrder) {
        this.nbOrder = nbOrder;
    }
}
