package com.inductivtechnologies.quantmarkapp.models.utils;

/**
 * BuyDisplayCustom
 */
public abstract class BuyDisplayCustom {

    public static final int VIEW_TYPE_BUY = 0;
    public static final int VIEW_TYPE_LOAD_MORE = 1;

    //Retourne le type d'une sous catégorie
    abstract public int getType();
}
