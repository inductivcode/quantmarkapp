package com.inductivtechnologies.quantmarkapp.models.entities;

import com.j256.ormlite.field.DatabaseField;

/**
 * Favorite : Les produits favoris
 */

@SuppressWarnings("unused")
public class Favorite {

    public static final String FILED_ID_HEX_MARKETOFFER = "idHexMarketOffer";

    /**
     * idFavorite : id dans la base de données
     * Non null
     */
    @DatabaseField(generatedId = true)
    private transient long idFavorite;

    /**
     * idHexMarketOffer Représentation hexadécimale
     * de l'id du marché favori
     */
    @DatabaseField
    private String idHexMarketOffer;

    /**
     * creationDate Date de création
     */
    @DatabaseField
    private long creationDate;

    //Constructeur(s)

    public Favorite(){
    }

    public Favorite(String idHexMarketOffer) {
        this.idHexMarketOffer = idHexMarketOffer;
        this.creationDate = System.currentTimeMillis();
    }

    // Getters et setters

    public String getIdHexMarketOffer() {
        return idHexMarketOffer;
    }

    public void setIdHexMarketOffer(String idHexMarketOffer) {
        this.idHexMarketOffer = idHexMarketOffer;
    }

    public long getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(long creationDate) {
        this.creationDate = creationDate;
    }
}
