package com.inductivtechnologies.quantmarkapp.models.display;

import com.inductivtechnologies.quantmarkapp.models.utils.BuyDisplayCustom;

/**
 * LoadMoreDisplay : Utiliser pour afficher le message "charger plus de de données"
 */

public class LoadMoreDisplay  extends BuyDisplayCustom {

    @Override
    public int getType() {
        return VIEW_TYPE_LOAD_MORE;
    }
}
