package com.inductivtechnologies.quantmarkapp.models.utils;

/**
 * CMarketOfferDisplayCustom
 */
public abstract class MarketOfferDisplayCustom {

    public static final int TYPE_MARKETOFFER = 0;
    public static final int TYPE_LOAD_MORE = 1;

    //Retourne le type d'une sous catégorie
    abstract public int getType();

}
