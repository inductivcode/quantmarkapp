package com.inductivtechnologies.quantmarkapp.models.utils;

import java.util.Date;

/**
 * Order : Entité utilisé pour faires des commandes
 */
@SuppressWarnings("unused")
public class Order {

    public static final String ORDER_CLASS_NAME = "orders";

    /**
     * quantity, quantité de l'achat
     */
    private int quantity;

    /**
     * isComplete, état de l'achat
     */
    private boolean isCompleted;

    /**
     * isCompleteBuyer, état de l'achat côte buyer, spécifie que
     * l'acheteur est bien allé dans le magasin
     */
    private boolean isCompletedBuyer;

    /**
     * purchaseDate, date de l'achat sur la plateforme (recuperation du code)
     */
    private Date purchaseDate;

    /**
     * completeDate, date à laquelle l'acheteur va dans le magasin
     */
    private Date completeDate;

    /**
     * objectIdHexmarketOffer, représentation hexadecimale de l'objet
     * id du marketoffer
     */
    private String  objectIdHexmarketOffer;

    /**
     *   idHexbuyer, eprésentation hexadecimale de l'objet
     * id du client
     */
    private String idHexbuyer;

    /**
     * toBeDelivered, spécifie si l'achat doit etre livré ou non
     */
    private boolean toBeDelivered;

    /**
     * deliverPlace, lieu de livraison
     */
    private String deliverPlace = "";

    /**
     * paymentType, Type de payement
     */
    private String paymentType = "";

    /**
     * emailAlt : email de notification
     */
    private String emailAlt = "";

    /**
     * telAlt : numéro de notification
     */
    private String telAlt = "";

    /**
     * nameAlt : Nom de la parsonne qui récupère
     */
    private String nameAlt = "";

    /**
     * firstNameAlt  Prénom de la parsonne qui récupère
     */
    private String firstNameAlt = "";

    /**
     * totalBuy : prix d'achat total de l'article
     */
    private double totalBuy ;
    //Constructeur(s)

    public Order() {
    }

    //Getters et setters

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public boolean isCompleted() {
        return isCompleted;
    }

    public void setCompleted(boolean completed) {
        isCompleted = completed;
    }

    public boolean isCompletedBuyer() {
        return isCompletedBuyer;
    }

    public void setCompletedBuyer(boolean completedBuyer) {
        isCompletedBuyer = completedBuyer;
    }

    public Date getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(Date purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public Date getCompleteDate() {
        return completeDate;
    }

    public void setCompleteDate(Date completeDate) {
        this.completeDate = completeDate;
    }

    public String getObjectIdHexmarketOffer() {
        return objectIdHexmarketOffer;
    }

    public void setObjectIdHexmarketOffer(String objectIdHexmarketOffer) {
        this.objectIdHexmarketOffer = objectIdHexmarketOffer;
    }

    public String getIdHexbuyer() {
        return idHexbuyer;
    }

    public void setIdHexbuyer(String idHexbuyer) {
        this.idHexbuyer = idHexbuyer;
    }

    public boolean isToBeDelivered() {
        return toBeDelivered;
    }

    public void setToBeDelivered(boolean toBeDelivered) {
        this.toBeDelivered = toBeDelivered;
    }

    public String getDeliverPlace() {
        return deliverPlace;
    }

    public void setDeliverPlace(String deliverPlace) {
        this.deliverPlace = deliverPlace;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getEmailAlt() {
        return emailAlt;
    }

    public void setEmailAlt(String emailAlt) {
        this.emailAlt = emailAlt;
    }

    public String getTelAlt() {
        return telAlt;
    }

    public void setTelAlt(String telAlt) {
        this.telAlt = telAlt;
    }

    public String getNameAlt() {
        return nameAlt;
    }

    public void setNameAlt(String nameAlt) {
        this.nameAlt = nameAlt;
    }

    public String getFirstNameAlt() {
        return firstNameAlt;
    }

    public void setFirstNameAlt(String firstNameAlt) {
        this.firstNameAlt = firstNameAlt;
    }

    public double getTotalBuy() {
        return totalBuy;
    }

    public void setTotalBuy(double totalBuy) {
        this.totalBuy = totalBuy;
    }
}
