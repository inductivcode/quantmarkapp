package com.inductivtechnologies.quantmarkapp.models.entities;

import com.inductivtechnologies.quantmarkapp.utils.EntitiesUtils;
import com.j256.ormlite.field.DatabaseField;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

/**
 * User : Implémente un utilisateur
 */
@SuppressWarnings("unused")
public class User{

    // Champs statiques
    public static final String USER_CLASS_NAME = "buyer";

    public static final String FIELD_NAME_ID = "idUser";
    public static final String FIELD_NAME_OBJECTID_HEX = "objectIdHexUser";
    public static final String FIELD_NAME_TOKEN = "token";
    public static final String FIELD_NAME_LAST_CONNEXION = "lastDateConnexion";
    public static final String FIELD_NAME_FOREIGN_ADDR = "address_id";
    public static final String FIELD_NAME_FOREIGN_PHONE = "phone_id";
    public static final String FIELD_NAME_FOREIGN_TOKEN = "token_id";

    public static final String FIELD_NAME_LOGIN = "login";
    public static final String FIELD_NAME_PASSWORD = "password";
    public static final String FIELD_NAME_NAME = "name";
    public static final String FIELD_NAME_FIRSTNAME = "firstname";

    /**
     * idUser, id dans la base de données salite
     */
    @DatabaseField(generatedId = true)
    protected transient long idUser;

    /**
     * objectIdHexUser : représentation hexadecimale de l'objectId d'un utilisateur
     * Servira lors de la connexion pour récuper l'objectid d'un acheteur(Buyer)
     */
    @DatabaseField
    protected transient String objectIdHexUser;

    /**
     * address, l'adresse de l'utilisateur, modifiable
     *
     */
    @DatabaseField(columnName = FIELD_NAME_FOREIGN_ADDR, canBeNull = false, foreign = true, foreignAutoRefresh = true)
    protected Address address;

    /**
     * telephone de l'utilisateur, modifiable
     *
     */
    @DatabaseField(columnName = FIELD_NAME_FOREIGN_PHONE, canBeNull = false, foreign = true, foreignAutoRefresh = true)
    protected Phone phone;

    /**
     * login : login de l'utilisateur, modifiable
     *
     **/
    @DatabaseField
    protected String login;

    /**
     * password : mot de passe de l'utilisateur, modifiable
     **/
    @DatabaseField
    protected String password;

    /**
     * completeName : om complet de l'utilisateur, modifiable
     *
     **/
    @DatabaseField
    protected String completeName;

    /**
     * lastname, , nom
     *
     */
    @DatabaseField
    protected String name;

    /**
     * firstName, prénom
     *
     */
    @DatabaseField
    protected String firstName;

    /**
     * image, nom de l'image de l'utilisateur, modifiable
     *
     */
    @DatabaseField
    protected String image;

    /**
     * birthDate, date de naissance de l'utilisateur, modifiable
     *
     */
    @DatabaseField
    protected Date birthDate;

    /**
     * creationDate, date de creation de l'utilisateur, modifiable
     *
     */
    @DatabaseField
    protected Date creationDate;

    /**
     * sexe : genre
     */
    @DatabaseField
    protected String sexe;

    /**
     * idHolder , Le titulaire du token
     */
    @DatabaseField(columnName = FIELD_NAME_FOREIGN_TOKEN, canBeNull = false, foreign = true, foreignAutoRefresh = true)
    private transient Token token;

    //Constructeur(s)

    public User() {
    }

    public User(JSONObject jsonObject) {
        try{
            this.objectIdHexUser = jsonObject.isNull("idUser") ? null : EntitiesUtils
                    .getObjectIdHexByJSONObject(jsonObject.getJSONObject("idUser"));
            this.address = jsonObject.isNull("address") ? null :
                    new Address(jsonObject.getJSONObject("address"));
            this.phone = jsonObject.isNull("phone") ? null
                    : new Phone(jsonObject.getJSONObject("phone"));

            this.login = jsonObject.isNull("login") ? "" :
                    jsonObject.getString("login");
            this.password = jsonObject.isNull("password") ? "" :
                    jsonObject.getString("password");
            this.completeName = jsonObject.isNull("completeName") ? ""
                    : jsonObject.getString("completeName");
            this.name = jsonObject.isNull("name") ? "" :
                    jsonObject.getString("name");
            this.firstName = jsonObject.isNull("firstName") ? "" :
                    jsonObject.getString("firstName");
            this.image = jsonObject.isNull("image") ? "" :
                    jsonObject.getString("image");

            this.birthDate = jsonObject.isNull("birthDate") ? null :
                    new Date(jsonObject.getLong("birthDate"));
            this.creationDate = jsonObject.isNull("creationDate") ? null :
                    new Date(jsonObject.getLong("creationDate"));
            this.sexe = jsonObject.isNull("sexe") ? "" :
                    jsonObject.getString("sexe");

        }catch (JSONException excJSON){
            excJSON.printStackTrace();
        }
    }

    public User(String objectIdHexUser, Address address, Phone phone,
                      String login, String password, String completeName,
                      String lastname, String firstName, String image,
                      Date birthDate, Date creationDate, String sexe) {
        this.objectIdHexUser = objectIdHexUser;
        this.address = address;
        this.phone = phone;
        this.login = login;
        this.password = password;
        this.completeName = completeName;
        this.name = lastname;
        this.firstName = firstName;
        this.image = image;
        this.birthDate = birthDate;
        this.creationDate = creationDate;
        this.sexe = sexe;
    }

    //Override

    // Getters et setters

    public long getIdUser() {
        return idUser;
    }

    public void setIdUser(long idUser) {
        this.idUser = idUser;
    }

    public String getObjectIdHexUser() {
        return objectIdHexUser;
    }

    public void setObjectIdHexUser(String objectIdHexUser) {
        this.objectIdHexUser = objectIdHexUser;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Phone getPhone() {
        return phone;
    }

    public void setPhone(Phone phone) {
        this.phone = phone;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCompleteName() {
        return completeName;
    }

    public void setCompleteName() {
        this.completeName = this.name + " " + this.firstName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public Token getToken() {
        return token;
    }

    public void setToken(Token token) {
        this.token = token;
    }
}
