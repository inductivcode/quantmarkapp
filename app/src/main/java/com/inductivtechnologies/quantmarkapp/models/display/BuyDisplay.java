package com.inductivtechnologies.quantmarkapp.models.display;

import com.inductivtechnologies.quantmarkapp.models.utils.BuyDisplayCustom;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

/**
 * BuyDisplay : Utilisée pour récuper et afficher les commandes ou les achats dans un recyclerview
 */

@SuppressWarnings("unused")
public class BuyDisplay extends BuyDisplayCustom {

    /**
     * idHexBuy : Représentation hexadécimale de l'objet id
     */
    private String idHexBuy;

    /**
     * date : Date de l'achat ou de la commande
     */
    private Date date;

    /**
     * titleFr : Titre de l'achat ou de la commande
     */
    private String titleFr;

    /**
     * titleEn : Titre de l'achat ou de la commande
     */
    private String titleEn;

    /**
     * issuerName : Nom du vendeur
     */
    private String issuerName;

    /**
     * imageName : Nom de l'image
     */
    private String imageName;

    /**
     * code : code du produit qui fait l'objet de la vente
     */
    private String code;

    /**
     * reductionPrice : Le prix réduit sil il y a une réduction
     */
    private double reductionPrice;

    /**
     * originalPrice : Prix original sans réduction
     */
    private double originalPrice;

    // Constructeur(s)

    public BuyDisplay(JSONObject jsonObject) {
        try{
            this.idHexBuy = jsonObject.isNull("idHexBuy") ? "" :
                    jsonObject.getString("idHexBuy");
            this.imageName = jsonObject.isNull("imageName") ? "" :
                    jsonObject.getString("imageName");
            this.issuerName = jsonObject.isNull("issuerName") ? "" :
                    jsonObject.getString("issuerName");
            this.titleFr = jsonObject.isNull("titleFr") ? "" :
                    jsonObject.getString("titleFr");
            this.titleEn = jsonObject.isNull("titleEn") ? "" :
                    jsonObject.getString("titleEn");
            this.reductionPrice = jsonObject.getDouble("reductionPrice");

            this.code= jsonObject.getString("code");
            this.originalPrice = jsonObject.getDouble("originalPrice");

            this.date = new Date(jsonObject.getLong("date"));

        }catch(JSONException jsonError){
            jsonError.printStackTrace();
        }
    }

    //Override

    @Override
    public int getType() {
        return VIEW_TYPE_BUY;
    }

    // Getters et setters

    public String getIdHexBuy() {
        return idHexBuy;
    }

    public void setIdHexBuy(String idHexBuy) {
        this.idHexBuy = idHexBuy;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTitleFr() {
        return titleFr;
    }

    public void setTitleFr(String titleFr) {
        this.titleFr = titleFr;
    }

    public String getTitleEn() {
        return titleEn;
    }

    public void setTitleEn(String titleEn) {
        this.titleEn = titleEn;
    }

    public String getIssuerName() {
        return issuerName;
    }

    public void setIssuerName(String issuerName) {
        this.issuerName = issuerName;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public double getReductionPrice() {
        return reductionPrice;
    }

    public void setReductionPrice(double reductionPrice) {
        this.reductionPrice = reductionPrice;
    }

    public double getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(double originalPrice) {
        this.originalPrice = originalPrice;
    }
}
