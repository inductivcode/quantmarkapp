package com.inductivtechnologies.quantmarkapp.models.entities;

import android.os.Parcel;

import com.j256.ormlite.field.DatabaseField;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Phone : Implémente le contact téléphonique d'un utilisateur de la plateforme quantmark
 **/

@SuppressWarnings("unused")
public class Phone{

    public static final String FIELD_NAME_ID = "idPhone";
    public static final String FIELD_NAME_TYPE = "type";
    public static final String FIELD_NAME_NUMBER = "number";

    /**
     * idPhone : id dans la base de données
     * Non null
     */
    @DatabaseField(generatedId = true)
    private transient long idPhone;

    /**
     * type de numero du telephone, modifiable
     */
    @DatabaseField
    private String type;

    /**
     * numero de telephone, modifiable
     */
    @DatabaseField
    private String number;

    // Constructeur(s)

    public Phone(){
    }

    public Phone(JSONObject jsonObject){
        try{
            this.type = jsonObject.isNull("type") ? "" :
                    jsonObject.getString("type");
            this.number = jsonObject.isNull("number") ? "" :
                    jsonObject.getString("number");
        }catch(JSONException excJSON){
            excJSON.printStackTrace();
        }
    }

    public Phone(String type, String number) {
        this.type = type;
        this.number = number;
    }

    protected Phone(Parcel in) {
        this.type = in.readString();
        this.number = in.readString();
    }

    // Override methods

    // Getters et setters

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
