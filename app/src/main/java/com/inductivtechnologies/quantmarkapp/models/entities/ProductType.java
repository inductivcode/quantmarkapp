package com.inductivtechnologies.quantmarkapp.models.entities;

import com.inductivtechnologies.quantmarkapp.utils.EntitiesUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

/**
 * SubCategory : Sous catégorie
 */

@SuppressWarnings("unused")
public class ProductType {

    /**
     * idHexType, Représentation hexadecimale de l'objet id
     */
    private String idHexType;

    /**
     * titleFr, titre de la categorie en francais
     *
     */
    private String titleFr;

    /**
     * titleEn, titre de la categorie en anglais
     *
     */
    private String titleEn;

    /**
     * descriptionFr, description de la categorie en francais
     *
     */
    private String descriptionFr;

    /**
     * descriptionEn, description de la categorie en anglais
     *
     */
    private String descriptionEn;

    /**
     * creationDate, date de creation
     */
    private Date creationDate;

    /**
     * groupeFr, permet de grouper les sous catégorie
     */
    private String groupeFr;

    /**
     * groupeEn, permet de grouper les sous catégorie
     */
    private String groupeEn;

    /**
     * image, nom de l'image du productType
     */
    private String image;

    //Constructeur(s)

    public ProductType() {
    }

    public ProductType(JSONObject jsonObject) {
        try {
            this.idHexType =  jsonObject.getJSONObject("idType") == null ? null : EntitiesUtils
                    .getObjectIdHexByJSONObject(jsonObject.getJSONObject("idType"));
            this.titleFr = jsonObject.isNull("titleFr") ? "" :
                    jsonObject.getString("titleFr");
            this.titleEn = jsonObject.isNull("titleEn") ? "" :
                    jsonObject.getString("titleEn");
            this.descriptionFr = jsonObject.isNull("descriptionFr") ? ""
                    : jsonObject.getString("descriptionFr");
            this.descriptionEn = jsonObject.isNull("descriptionEn") ? ""
                    : jsonObject.getString("descriptionEn");
            this.creationDate = jsonObject.isNull("creationDate") ? null :
                    new Date(jsonObject.getLong("creationDate"));
            this.groupeFr = jsonObject.isNull("groupeFr") ? ""
                    : jsonObject.getString("groupeFr");
            this.groupeEn = jsonObject.isNull("groupeEn") ? ""
                    : jsonObject.getString("groupeEn");
            this.image = jsonObject.isNull("image") ? ""
                    : jsonObject.getString("image");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //Getters et setters


    public String getIdHexType() {
        return idHexType;
    }

    public void setIdHexType(String idHexType) {
        this.idHexType = idHexType;
    }

    public String getTitleFr() {
        return titleFr;
    }

    public void setTitleFr(String titleFr) {
        this.titleFr = titleFr;
    }

    public String getTitleEn() {
        return titleEn;
    }

    public void setTitleEn(String titleEn) {
        this.titleEn = titleEn;
    }

    public String getDescriptionFr() {
        return descriptionFr;
    }

    public void setDescriptionFr(String descriptionFr) {
        this.descriptionFr = descriptionFr;
    }

    public String getDescriptionEn() {
        return descriptionEn;
    }

    public void setDescriptionEn(String descriptionEn) {
        this.descriptionEn = descriptionEn;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getGroupeFr() {
        return groupeFr;
    }

    public void setGroupeFr(String groupeFr) {
        this.groupeFr = groupeFr;
    }

    public String getGroupeEn() {
        return groupeEn;
    }

    public void setGroupeEn(String groupeEn) {
        this.groupeEn = groupeEn;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
