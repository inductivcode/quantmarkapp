package com.inductivtechnologies.quantmarkapp.models.entities;

import com.inductivtechnologies.quantmarkapp.utils.EntitiesUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

/**
 *
 * Category est la classe utilisée pour representer les categories de produits dans la pateforme quantmark
 **/

@SuppressWarnings("unused")
public class Category {

    /**
     * idHexCategory, identifiant unique de l'objet, modifiable
     */
    private String idHexCategory;

    /**
     * titleFr, titre de la categorie en francais
     *
     */
    private String titleFr;

    /**
     * titleEn, titre de la categorie en anglais
     *
     */
    private String titleEn;

    /**
     * descriptionFr, description de la categorie en francais
     *
     */
    private String descriptionFr;

    /**
     * descriptionEn, description de la categorie en anglais
     *
     */
    private String descriptionEn;

    /**
     * creationDate, date de creation, modifiable
     */
    private Date creationDate;

    /**
     * categoryOrder, numero de la categorie, servant à ordonner les categories, modifiable
     */
    private int categoryOrder;

    /**
     * image, nom de l'image de la category, modifiable
     *
     */
    private String image;

    // Constructeur(s)

    public Category(JSONObject jsonObject) {

        try{
            this.idHexCategory= jsonObject.isNull("idCategory") ? null : EntitiesUtils
                    .getObjectIdHexByJSONObject(jsonObject.getJSONObject("idCategory"));
            this.titleFr = jsonObject.isNull("titleFr") ? "" :
                    jsonObject.getString("titleFr");
            this.titleEn = jsonObject.isNull("titleEn") ? "" :
                    jsonObject.getString("titleEn");
            this.descriptionFr = jsonObject.isNull("descriptionFr") ? ""
                    : jsonObject.getString("descriptionFr");
            this.descriptionEn = jsonObject.isNull("descriptionEn") ? ""
                    : jsonObject.getString("descriptionEn");
            this.creationDate = jsonObject.isNull("creationDate") ? null :
                    new Date(jsonObject.getLong("creationDate"));

            this.categoryOrder = jsonObject.getInt("categoryOder");
            this.image = jsonObject.isNull("image") ? "" : jsonObject.getString("image");
        }catch (JSONException excJSON){
            excJSON.printStackTrace();
        }
    }

    // Override methods

    // Getters et setters

    public String getIdHexCategory() {
        return idHexCategory;
    }

    public void setIdHexCategory(String idHexCategory) {
        this.idHexCategory = idHexCategory;
    }

    public String getTitleFr() {
        return titleFr;
    }

    public void setTitleFr(String titleFr) {
        this.titleFr = titleFr;
    }

    public String getTitleEn() {
        return titleEn;
    }

    public void setTitleEn(String titleEn) {
        this.titleEn = titleEn;
    }

    public String getDescriptionFr() {
        return descriptionFr;
    }

    public void setDescriptionFr(String descriptionFr) {
        this.descriptionFr = descriptionFr;
    }

    public String getDescriptionEn() {
        return descriptionEn;
    }

    public void setDescriptionEn(String descriptionEn) {
        this.descriptionEn = descriptionEn;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public int getCategoryOrder() {
        return categoryOrder;
    }

    public void setCategoryOrder(int categoryOrder) {
        this.categoryOrder = categoryOrder;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
