package com.inductivtechnologies.quantmarkapp.models.display;

import com.inductivtechnologies.quantmarkapp.models.utils.SearchItemCustom;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * SearchItemDisplay : Utilisée pour afficher les pré résultats d'une recherche
 */

@SuppressWarnings("unused")
public class SearchItemDisplay extends SearchItemCustom {

    /**
     * idHex : Représentation hexadecimale de l'object ID
     */
    private String idHex;

    /**
     * itemTitle : Le titre de l'item
     */
    private String itemTitle;

    /**
     * itemNbResult : Le nombre de résultat pour cet item
     */
    private long itemNbResult;

    //Constructeur(s)

    public SearchItemDisplay(String idHex, String itemTitle, long itemNbResult) {
        this.idHex = idHex;
        this.itemTitle = itemTitle;
        this.itemNbResult = itemNbResult;
    }

    public SearchItemDisplay(JSONObject jsonObject ) {
        try{
            this.idHex = jsonObject.isNull("idHex") ? "" :
                    jsonObject.getString("idHex");

            this.itemTitle = jsonObject.isNull("itemTitle") ? "" :
                    jsonObject.getString("itemTitle");

            this.itemNbResult = jsonObject.getLong("itemNbResult");

        }catch(JSONException exception){
            exception.printStackTrace();
        }
    }

    @Override
    public int getType() {
        return VIEW_TYPE_RESULT_ITEM;
    }

    //Getters et setters

    public String getIdHex() {
        return idHex;
    }

    public void setIdHex(String idHex) {
        this.idHex = idHex;
    }

    public String getItemTitle() {
        return itemTitle;
    }

    public void setItemTitle(String itemTitle) {
        this.itemTitle = itemTitle;
    }

    public long getItemNbResult() {
        return itemNbResult;
    }

    public void setItemNbResult(long itemNbResult) {
        this.itemNbResult = itemNbResult;
    }
}
