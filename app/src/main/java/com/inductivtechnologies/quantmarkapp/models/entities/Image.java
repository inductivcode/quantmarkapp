package com.inductivtechnologies.quantmarkapp.models.entities;

import com.inductivtechnologies.quantmarkapp.utils.EntitiesUtils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Cette classe implémente les images associées aux produits
 */

@SuppressWarnings("unused")
public class Image{

    /**
     * idHexImage : Représentation hexa de l'objectID
     */
    private String idHexImage;

    /**
     * name : La nom de l'image
     *
     */
    private String name;

    // Constructeur(s)

    public Image() {
    }

    public Image(JSONObject jsonObject) {
        try {
            this.idHexImage= jsonObject.isNull("idImage") ? null : EntitiesUtils
                    .getObjectIdHexByJSONObject(jsonObject.getJSONObject("idImage"));

            this.name = jsonObject.isNull("name") ? null : jsonObject.getString("name");

        }catch(JSONException excJSON){
            excJSON.printStackTrace();
        }
    }

    // Getters et setters

    public String getIdHexImage() {
        return idHexImage;
    }

    public void setIdHexImage(String idHexImage) {
        this.idHexImage = idHexImage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    // Override methods
}
