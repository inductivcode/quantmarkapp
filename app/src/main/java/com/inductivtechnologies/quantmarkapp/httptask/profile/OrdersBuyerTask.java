package com.inductivtechnologies.quantmarkapp.httptask.profile;

import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.activities.profile.UserProfileActivity;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.configuration.NumberConfig;
import com.inductivtechnologies.quantmarkapp.configuration.UrlRestApiConfig;
import com.inductivtechnologies.quantmarkapp.fragments.profile.OrdersFragment;
import com.inductivtechnologies.quantmarkapp.models.display.BuyDisplay;
import com.inductivtechnologies.quantmarkapp.models.entities.Token;
import com.inductivtechnologies.quantmarkapp.models.entities.User;
import com.inductivtechnologies.quantmarkapp.models.http.HttpResponseObject;
import com.inductivtechnologies.quantmarkapp.utils.DesignUtils;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * OrdersBuyerTask
 */
@SuppressWarnings({"unchecked", "SameParameterValue"})
public class OrdersBuyerTask {

    //Context
    private final OrdersFragment fragment;

    //Volley instance singleton
    private final AppController appController;

    //Constructeur(s)

    public OrdersBuyerTask(OrdersFragment fragment, AppController appController) {
        this.fragment = fragment;
        this.appController = appController;
    }

    public void makeRequest(final User user, int limit, int skip){
        //Url de la requête
        String urlRequest = String.format(Locale.getDefault(), UrlRestApiConfig.ORDERS_URL,
                user.getObjectIdHexUser(), limit, skip);

        //Le progress bar
        if(fragment.getSwipeRefreshLayout() != null){
            fragment.getSwipeRefreshLayout().setRefreshing(true);
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,
                urlRequest,
                null,

                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        HttpResponseObject httpResponse = new HttpResponseObject(response);
                        final int statusCode = httpResponse.getCode();

                        switch (statusCode){
                            case HttpResponseObject.SUCCESS_CODE :

                                httpResponse.addOrdersResult(response);
                                List<BuyDisplay> buysDisplay = (List<BuyDisplay>) httpResponse
                                        .getObjectValue().get(HttpResponseObject.ORDERS_STR);
                                //Permet de savoir si il ya encore des données à charger
                                boolean dataIsComplete = (boolean) httpResponse.getObjectValue()
                                        .get(HttpResponseObject.DATA_IS_COMPLETE);

                                for(BuyDisplay buyDisplay : buysDisplay){
                                    fragment.getOrderItems().add(buyDisplay);
                                }

                                if(fragment.getOrderItems().size() == 0){
                                    //On affiche le message pour indiquer le contenu vide
                                    fragment.displayContainerEmpty(true);
                                    fragment.getRecyclerview().setVisibility(View.GONE);
                                }else{
                                    //On cache le message qui indique le contenu vide
                                    fragment.displayContainerEmpty(false);
                                    fragment.getRecyclerview().setVisibility(View.VISIBLE);

                                    //On prévoie le chargement de plus de données
                                    fragment.loadMoreAsyn(dataIsComplete);
                                    //On notifie l'adaptateur
                                    fragment.getAdapter().notifyDataSetChanged();

                                    //On modifie la plage des données reçues par le serveur
                                    fragment.setRange();
                                }

                                break;
                            case HttpResponseObject.TOKEN_UNEXIST_ERROR_CODE:
                                break;
                        }

                        //On cache le progress bar
                        if(fragment.getSwipeRefreshLayout() != null){
                            fragment.getSwipeRefreshLayout().setRefreshing(false);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();

                        if(fragment.getSwipeRefreshLayout() != null){
                            fragment.getSwipeRefreshLayout().setRefreshing(false);
                        }

                        DesignUtils.makeSnackbar(fragment.getContext().getString(R.string.network_message_error),
                                ((UserProfileActivity) fragment.getActivity()).getCoordinatorLayout(), fragment.getContext());
                    }
                }
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put(Token.FIELD_TOKEN_VALUE, user.getToken().getTokenValue());
                return headers;
            }
        };

        RetryPolicy policy = new DefaultRetryPolicy(NumberConfig.NB_SOCKET_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        // Adding request to request queue
        this.appController.addToRequestQueue(request, OrdersFragment.PROFILE_ORDERS_TASK);
    }

}
