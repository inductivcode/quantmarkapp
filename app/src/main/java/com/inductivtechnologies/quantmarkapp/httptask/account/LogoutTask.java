package com.inductivtechnologies.quantmarkapp.httptask.account;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.activities.profile.UserProfileActivity;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.bottomsheets.BottomSheetProgress;
import com.inductivtechnologies.quantmarkapp.configuration.UrlRestApiConfig;
import com.inductivtechnologies.quantmarkapp.fragments.profile.ProfileInfoFragment;
import com.inductivtechnologies.quantmarkapp.models.entities.Token;
import com.inductivtechnologies.quantmarkapp.models.entities.User;
import com.inductivtechnologies.quantmarkapp.models.http.HttpResponseObject;
import com.inductivtechnologies.quantmarkapp.store.repositories.AddressRepository;
import com.inductivtechnologies.quantmarkapp.store.repositories.CartItemRepository;
import com.inductivtechnologies.quantmarkapp.store.repositories.PhoneRepository;
import com.inductivtechnologies.quantmarkapp.store.repositories.TokenRepository;
import com.inductivtechnologies.quantmarkapp.store.repositories.UserRepository;
import com.inductivtechnologies.quantmarkapp.store.session.SessionManager;
import com.inductivtechnologies.quantmarkapp.utils.DesignUtils;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * LogoutTask : Déconnecte un utilisateur ( Supprime son token )
 */
public class LogoutTask {

    //Context
    private final UserProfileActivity context;

    //Volley instance singleton
    private final AppController appController;

    //Session manager
    private final SessionManager sessionManager;

    //Repositories
    private final TokenRepository tokenRepo;
    private final UserRepository userRepo;
    private final CartItemRepository cartitemRepo;
    private final AddressRepository addressRepo;
    private final PhoneRepository phoneRepo;

    //Dialog
    private final BottomSheetProgress progress;

    //Constructeur(s)

    public LogoutTask(UserProfileActivity context, AppController appController) {
        this.context = context;
        this.appController = appController;

        this.tokenRepo = new TokenRepository(context);
        this.userRepo = new UserRepository(context);
        this.cartitemRepo = new CartItemRepository(context);
        this.addressRepo = new AddressRepository(context);
        this.phoneRepo = new PhoneRepository(context);

        this.sessionManager = new SessionManager(context);

        //Progress
        this.progress = new BottomSheetProgress(context);
    }

    public void makeRequest(final User user){
        //Url de la requête
        String urlRequest = String.format(Locale.getDefault(), UrlRestApiConfig.LOGOUT_URL,
                user.getObjectIdHexUser());

        //Le progress bar
        progress.show(context.getResources().getString(R.string.label_logout_message));

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.DELETE,
                urlRequest ,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        //Vérification
                        final int statusCode = response.optInt(HttpResponseObject.CODE_STR);

                        switch(statusCode){
                            case HttpResponseObject.SUCCESS_CODE :

                                //On supprime l'adresse et le téléphone
                                long phoneResult = phoneRepo.deletePhone(user.getPhone());
                                long addressResult = addressRepo.deleteAddress(user.getAddress());

                                if(phoneResult > 0 && addressResult > 0){
                                    //On supprime tous les utilisateurs
                                    long resultUser = userRepo.deleteAllUsers();

                                    //On supprime la session
                                    sessionManager.deleteSessionLogin();

                                    //On supprime son token
                                    long tokenResuult = tokenRepo.deleteAllTokens();
                                    //On vide le panier

                                    if(resultUser > 0 && tokenResuult > 0){
                                        cartitemRepo.deleteAllCartitems();

                                        //On termine l'activité
                                        context.finish();
                                    }else{
                                        DesignUtils.makeSnackbar(context.getResources().getString(R.string.unknow_error_message),
                                                context.getCoordinatorLayout(), context.getApplicationContext());
                                    }
                                }else{
                                    DesignUtils.makeSnackbar(context.getResources().getString(R.string.unknow_error_message),
                                            context.getCoordinatorLayout(), context.getApplicationContext());
                                }
                                break;
                            case HttpResponseObject.TOKEN_UNEXIST_ERROR_CODE :
                                break;
                        }

                        //Le progress bar
                        progress.hide();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();

                        progress.hide();

                        DesignUtils.makeSnackbar(context.getResources().getString(R.string.network_message_error),
                                context.getCoordinatorLayout(), context.getApplicationContext());
                    }
                }
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put(Token.FIELD_TOKEN_VALUE, user.getToken().getTokenValue());
                return headers;
            }
        };

        request.setRetryPolicy(
                new DefaultRetryPolicy(
                        0,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                )
        );

        // Adding request to request queue
        this.appController.addToRequestQueue(request, ProfileInfoFragment.PROFILE_LOGOUT_TASK);
    }
}
