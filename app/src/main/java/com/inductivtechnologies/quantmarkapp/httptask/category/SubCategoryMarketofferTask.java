package com.inductivtechnologies.quantmarkapp.httptask.category;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.activities.category.SubCategoryMarketofferActivity;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.configuration.NumberConfig;
import com.inductivtechnologies.quantmarkapp.configuration.UrlRestApiConfig;
import com.inductivtechnologies.quantmarkapp.models.display.MarketOfferDisplay;
import com.inductivtechnologies.quantmarkapp.utils.DesignUtils;
import com.inductivtechnologies.quantmarkapp.utils.EntitiesUtils;

import org.json.JSONArray;

import java.util.List;
import java.util.Locale;

/**
 * SubCategoryMarketofferTask : Charge les marché d'une sous catégorie
 */
@SuppressWarnings("SameParameterValue")
public class SubCategoryMarketofferTask {

    //Context
    private final SubCategoryMarketofferActivity context;

    //Volley instance singleton
    private final AppController appController;

    //Constructeur

    public SubCategoryMarketofferTask(SubCategoryMarketofferActivity context, AppController appController) {
        this.context = context;
        this.appController = appController;
    }

    public void makeRequest(final String idSubcategory, final int limit, final int skip, final int sortMode){
        //Url de la requête
        String urlRequest = String.format(Locale.getDefault(), UrlRestApiConfig.SUB_CATEGORy_MARKETOFFERS_URL,
                idSubcategory, limit, skip, sortMode);

        if(context.getSwipeRefreshLayout() != null){
            context.getSwipeRefreshLayout().setRefreshing(true);
        }

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET,
                urlRequest,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        List<MarketOfferDisplay> marketOffers = EntitiesUtils.getMarketOffersDisplayByJSONArray(response);

                        if(marketOffers != null){

                            for(MarketOfferDisplay marketOffer : marketOffers){
                                context.getMarketOffers().add(marketOffer);
                            }
                            context.loadMoreAsyn();
                            context.getRecyclerviewAdapter().notifyDataSetChanged();

                            //Range
                            context.setRange();
                        }

                        if(context.getSwipeRefreshLayout() != null){
                            context.getSwipeRefreshLayout().setRefreshing(false);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();

                        if(context.getSwipeRefreshLayout() != null){
                            context.getSwipeRefreshLayout().setRefreshing(false);
                        }

                        DesignUtils.makeSnackbar(context.getString(R.string.network_message_error),
                                context.getCoordinatorLayout(), context);
                    }
                }
        );

        RetryPolicy policy = new DefaultRetryPolicy(NumberConfig.NB_SOCKET_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        // Adding request to request queue
        this.appController.addToRequestQueue(request, SubCategoryMarketofferActivity.SUB_CATEGORY_MARKETOFFER_TASK);
    }
}
