package com.inductivtechnologies.quantmarkapp.httptask.account;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.callbacks.HttpCallback;
import com.inductivtechnologies.quantmarkapp.configuration.UrlRestApiConfig;
import com.inductivtechnologies.quantmarkapp.fragments.account.FragmentLogin;
import com.inductivtechnologies.quantmarkapp.models.http.NameValuePair;

import org.json.JSONObject;

import java.lang.ref.WeakReference;

/**
 * LoginTask : Connexion d'un utilisateur
 */
public class LoginTask {

    ///Callback
    private final WeakReference<HttpCallback> callback;

    //Volley instance singleton
    private final AppController appController;

    //Constructeur(s)
    public LoginTask(HttpCallback callback, AppController appController) {
        this.appController = appController;
        this.callback = new WeakReference<>(callback);
    }

    // Methodes

    public void makeRequest(NameValuePair paramValues){
        if(callback.get() != null) callback.get().beforeExecute();

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                UrlRestApiConfig.LOGIN_URL,
                new JSONObject(paramValues.getParamsMap()),
                createRequestSuccessListener(),
                createRequestErrorListener()
        );

        request.setRetryPolicy(
                new DefaultRetryPolicy(
                        0,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                )
        );

        this.appController.addToRequestQueue(request, FragmentLogin.LOGIN_TASK);
    }

    private Response.Listener<JSONObject> createRequestSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if(callback.get() != null) callback.get().afterExecute(response);
            }
        };
    }

    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                if(callback.get() != null) callback.get().whenErrors();
            }
        };
    }

}
