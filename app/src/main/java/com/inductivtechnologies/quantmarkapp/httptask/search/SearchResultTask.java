package com.inductivtechnologies.quantmarkapp.httptask.search;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.activities.search.SearchResultActivity;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.configuration.NumberConfig;
import com.inductivtechnologies.quantmarkapp.configuration.UrlRestApiConfig;
import com.inductivtechnologies.quantmarkapp.models.display.MarketOfferDisplay;
import com.inductivtechnologies.quantmarkapp.utils.DesignUtils;
import com.inductivtechnologies.quantmarkapp.utils.EntitiesUtils;

import org.json.JSONArray;

import java.util.List;
import java.util.Locale;

/**
 * SearchResultTask
 */
@SuppressWarnings("SameParameterValue")
public class SearchResultTask {

    //Context
    private final SearchResultActivity context;

    //Volley instance singleton
    private final AppController appController;
    //Constructeur

    public SearchResultTask(SearchResultActivity context, AppController appController) {
        this.context = context;
        this.appController = appController;
    }

    public void makeRequest(final int searchLabelValue, final String idHex,
                            final int limit, final int skip, final int sortMode){

        String urlRequest;

        //Selon le filtre on éffectue la requête correspondante
        switch(searchLabelValue){
            case NumberConfig.NB_SUBCATEGORY_SEARCH :
                urlRequest = String.format(Locale.getDefault(), UrlRestApiConfig.SUB_CATEGORy_MARKETOFFERS_URL,
                        idHex, limit, skip, sortMode);
                break;
            case NumberConfig.NB_ISSUER_SEARCH :
                urlRequest = String.format(Locale.getDefault(), UrlRestApiConfig.ISSUER_MARKETOFFERS_URL,
                        idHex, limit, skip, sortMode);
                break;
            default :
                urlRequest = String.format(Locale.getDefault(), UrlRestApiConfig.SUB_CATEGORy_MARKETOFFERS_URL,
                        idHex, limit, skip, sortMode);
        }

        if(context.getSwipeRefreshLayout() != null){
            context.getSwipeRefreshLayout().setRefreshing(true);
        }

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET,
                urlRequest,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        List<MarketOfferDisplay> marketOffers = EntitiesUtils.getMarketOffersDisplayByJSONArray(response);

                        if(marketOffers != null){

                            for(MarketOfferDisplay marketOffer : marketOffers){
                                context.getMarketOffers().add(marketOffer);
                            }
                            context.loadMoreAsyn();
                            context.getRecyclerviewAdapter().notifyDataSetChanged();

                            //Range
                            context.setSkip();
                        }

                        if(context.getSwipeRefreshLayout() != null){
                            context.getSwipeRefreshLayout().setRefreshing(false);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();

                        if(context.getSwipeRefreshLayout() != null){
                            context.getSwipeRefreshLayout().setRefreshing(false);
                        }

                        DesignUtils.makeSnackbar(context.getString(R.string.network_message_error),
                                context.getCoordinator(), context);
                    }
                }
        );

        RetryPolicy policy = new DefaultRetryPolicy(NumberConfig.NB_SOCKET_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        // Adding request to request queue
        this.appController.addToRequestQueue(request, SearchResultActivity.SEARCH_RESULT_TASK);
    }

}
