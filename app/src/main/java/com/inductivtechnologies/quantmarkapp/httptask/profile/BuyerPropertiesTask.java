package com.inductivtechnologies.quantmarkapp.httptask.profile;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.activities.profile.UserProfileActivity;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.configuration.NumberConfig;
import com.inductivtechnologies.quantmarkapp.configuration.UrlRestApiConfig;
import com.inductivtechnologies.quantmarkapp.fragments.profile.ProfileInfoFragment;
import com.inductivtechnologies.quantmarkapp.models.display.PropertiesNumber;
import com.inductivtechnologies.quantmarkapp.models.entities.Token;
import com.inductivtechnologies.quantmarkapp.utils.DesignUtils;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * BuyerPropertiesTask
 */
public class BuyerPropertiesTask {

    //Context
    private final ProfileInfoFragment fragment;

    //Volley instance singleton
    private final AppController appController;

    public BuyerPropertiesTask(ProfileInfoFragment fragment, AppController appController) {
        this.fragment = fragment;
        this.appController = appController;
    }

    public void makeRequest(final String token, final String idHexUser){
        //Url de la requête
        String urlRequest = String.format(Locale.getDefault(), UrlRestApiConfig.LOAD_PROPERTIES_URL,
                idHexUser);

        if(fragment.getSwipeRefreshLayout() != null){
            fragment.getSwipeRefreshLayout().setRefreshing(true);
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,
                urlRequest,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        if(fragment.getSwipeRefreshLayout() != null){
                            fragment.getSwipeRefreshLayout().setRefreshing(false);
                        }

                        PropertiesNumber properties = new PropertiesNumber(response);
                        fragment.initQuantitySection(properties);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();

                        if(fragment.getSwipeRefreshLayout() != null){
                            fragment.getSwipeRefreshLayout().setRefreshing(false);
                        }

                        //Message d'erreur
                        DesignUtils.makeSnackbar(fragment.getContext().getString(R.string.network_message_error),
                                ((UserProfileActivity) fragment.getActivity()).getCoordinatorLayout(), fragment.getContext());
                    }
                }
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put(Token.FIELD_TOKEN_VALUE, token);
                return headers;
            }
        };

        RetryPolicy policy = new DefaultRetryPolicy(NumberConfig.NB_SOCKET_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        // Adding request to request queue
        this.appController.addToRequestQueue(request, ProfileInfoFragment.PROFILE_PROPERTIES_TASK);
    }

}
