package com.inductivtechnologies.quantmarkapp.httptask.cart;

import android.content.Intent;
import android.util.Log;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.activities.cart.ResumeOrdersActivity;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.bottomsheets.BottomSheetProgress;
import com.inductivtechnologies.quantmarkapp.bottomsheets.BottomsheetCustom;
import com.inductivtechnologies.quantmarkapp.configuration.NumberConfig;
import com.inductivtechnologies.quantmarkapp.configuration.PaypalConfig;
import com.inductivtechnologies.quantmarkapp.configuration.UrlRestApiConfig;
import com.inductivtechnologies.quantmarkapp.models.entities.Token;
import com.inductivtechnologies.quantmarkapp.models.entities.User;
import com.inductivtechnologies.quantmarkapp.models.http.HttpResponseObject;
import com.inductivtechnologies.quantmarkapp.models.utils.Order;
import com.inductivtechnologies.quantmarkapp.store.repositories.CartItemRepository;
import com.inductivtechnologies.quantmarkapp.utils.DesignUtils;
import com.paypal.android.sdk.payments.LoginActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * PaypalPaymentTask : Requête HTTP pour faire le payement et la vérification PAYPAL
 */
public class PaypalPaymentTask {

    // Pour afficher les informations dans le log android
    private static final String TAG = PaypalPaymentTask.class.getSimpleName();

    //Context
    private final ResumeOrdersActivity activity;

    //Volley instance singleton
    private final AppController appController;
    //Repo
    private final CartItemRepository cartRepo;

    //Sheet dialog
    private final BottomSheetProgress progressDialog;

    //Constructeur

    public PaypalPaymentTask(ResumeOrdersActivity activity, AppController appController) {
        this.activity = activity;
        this.appController = appController;

        this.progressDialog = new BottomSheetProgress(activity);
        this.cartRepo = new CartItemRepository(activity);
    }

    private JSONObject makeParams(final List<Order> orders, String paymentId, JSONObject paypalPaymentDetails){
        JSONObject jsonrRequest = new JSONObject();
        JSONArray JsonArray = new JSONArray();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").create();

        try {
            for(Order order : orders){
                String jsonStr = gson.toJson(order);
                JSONObject jsonObject = new JSONObject(jsonStr);
                JsonArray.put(jsonObject);
            }

            jsonrRequest.put(Order.ORDER_CLASS_NAME, JsonArray);
            jsonrRequest.put(PaypalConfig.PAYPAL_PAYMENT_ID, paymentId);
            jsonrRequest.put(PaypalConfig.PAYPAL_PAYMENT_DETAILS, paypalPaymentDetails);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonrRequest;
    }

    public void makeRequest(final User user, final List<Order> orders, String paymentId, JSONObject paypalPaymentDetails) {
        //Url de la requête
        String urlRequest = String.format(Locale.getDefault(), UrlRestApiConfig.ADD_AND_CHECK_ORDERS_PAYPAL_PAYMENT_URL,
                user.getObjectIdHexUser());

        //Progress
        progressDialog.show(activity.getResources().getString(R.string.progress_save_order_content));

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                urlRequest,
                makeParams(orders, paymentId, paypalPaymentDetails),

                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            Log.i(TAG, response.toString(4));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        //Reponse formatée
                        HttpResponseObject responseObject = new HttpResponseObject(response);

                        switch(responseObject.getCode()) {
                            case HttpResponseObject.SUCCESS_CODE:

                                //On ouvre le dialog
                                BottomsheetCustom customDialog = new BottomsheetCustom(activity);
                                customDialog.setContent(activity.getResources().getString(R.string.text_success_make_order));
                                customDialog.setButtonTitle(activity.getResources().getString(R.string.text_continue));
                                customDialog.show();

                                //Action du bouton
                                customDialog.getBtnDialog().setOnClickListener(new View.OnClickListener(){
                                    @Override
                                    public void onClick(View v) {
                                        String paramIntent = activity.getIdMarketoffer();
                                        if(paramIntent.isEmpty()){
                                            //O a commandé avec tous les éléments du panier
                                            cartRepo.deleteAllCartitems();
                                        }else{
                                            //On a commander avec un seul élément dont ID = paramIntent
                                            //On le supprime du panier
                                            cartRepo.deleteCartItem(paramIntent);
                                        }
                                        //On termine l'activité
                                        activity.finish();
                                    }
                                });

                                break;
                            case HttpResponseObject.TOKEN_UNEXIST_ERROR_CODE:
                                activity.startActivity(new Intent(activity, LoginActivity.class));
                                break;
                            default:
                                //On vide la liste des items paypal
                                activity.getPaypalItems().clear();

                                //Message d'erreur
                                DesignUtils.makeSnackbar(activity.getString(R.string.label_unknow_error_message),
                                        activity.getCoordinatorLayout(), activity);
                                break;
                        }

                        //Progress
                        progressDialog.hide();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();

                        //Progress
                        progressDialog.hide();

                        //On vide la liste des items paypal
                        activity.getPaypalItems().clear();

                        //Message d'erreur
                        DesignUtils.makeSnackbar(activity.getString(R.string.network_message_error),
                                activity.getCoordinatorLayout(), activity);
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put(Token.FIELD_TOKEN_VALUE, user.getToken().getTokenValue());
                return headers;
            }
        };

        RetryPolicy policy = new DefaultRetryPolicy(NumberConfig.NB_SOCKET_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        // Adding request to request queue
        this.appController.addToRequestQueue(request, ResumeOrdersActivity.SAVE_ORDERS_WITH_PAYPAL_PAYMENT_TASK);
    }
}
