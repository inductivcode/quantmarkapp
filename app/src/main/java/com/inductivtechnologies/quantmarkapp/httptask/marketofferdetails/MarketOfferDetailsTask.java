package com.inductivtechnologies.quantmarkapp.httptask.marketofferdetails;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.activities.marketofferdetails.MarketofferDetails;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.configuration.NumberConfig;
import com.inductivtechnologies.quantmarkapp.configuration.UrlRestApiConfig;
import com.inductivtechnologies.quantmarkapp.models.display.MarketOfferDisplay;
import com.inductivtechnologies.quantmarkapp.models.http.HttpResponseObject;
import com.inductivtechnologies.quantmarkapp.utils.DesignUtils;

import org.json.JSONObject;

import java.util.List;
import java.util.Locale;

/**
 * MarketOfferDetailsTask : Charge les détails d'un produit et les produits venant du même vendeur
 */
@SuppressWarnings("unchecked")
public class MarketOfferDetailsTask {

    //Context
    private final MarketofferDetails context;

    //Volley instance singleton
    private final AppController appController;

    //Constructeur

    public MarketOfferDetailsTask(MarketofferDetails context, AppController appController) {
        this.context = context;
        this.appController = appController;
    }

    //Méthodes

    public void makeRequest(final String marketofferHexId){
        //Url de la requête
        String urlRequest = String.format(Locale.getDefault(), UrlRestApiConfig.MARKETOFFER_DETAILS_URL,
                marketofferHexId);

        if(context.getSwipeRefreshLayout() != null){
            context.getSwipeRefreshLayout().setRefreshing(true);
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,
                urlRequest,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        //Vérification
                        final int statusCode = response.optInt(HttpResponseObject.CODE_STR);
                        switch(statusCode){
                            case HttpResponseObject.SUCCESS_CODE :
                                //On recupère les offres
                                HttpResponseObject responseObject = new HttpResponseObject(response);
                                responseObject.addMarketofferDetailsResult(response);

                                //Une offre
                                MarketOfferDisplay marketofferDisplay = (MarketOfferDisplay) responseObject.getObjectValue()
                                        .get(HttpResponseObject.MARKETOFFER_STR);
                                //Les offres similaires
                                List<MarketOfferDisplay> marketoffersDisplay = (List<MarketOfferDisplay>) responseObject.getObjectValue()
                                        .get(HttpResponseObject.MARKETOFFERS_STR);
                                //Le nombre total d'offres similaires
                                int count = (int) responseObject.getObjectValue().get(HttpResponseObject.COUNT_STR);

                                if(marketofferDisplay != null) {
                                    //On modifie les données de l'activité
                                    context.setMarketOfferDisplay(marketofferDisplay);
                                    context.setSimilarCount(count);

                                    //On rempli les vues
                                    context.initViewpager();
                                    context.globalInit();

                                    //Initialisation du conteneur des offres similaires
                                    context.initRecyclerView(marketoffersDisplay);
                                }

                                break;
                            default:
                                DesignUtils.makeSnackbar(context.getString(R.string.label_unknow_error_message),
                                        context.getCoordinatorLayout(), context);
                                break;
                        }

                        if(context.getSwipeRefreshLayout() != null){
                            context.getSwipeRefreshLayout().setRefreshing(false);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();

                        if(context.getSwipeRefreshLayout() != null){
                            context.getSwipeRefreshLayout().setRefreshing(false);
                        }

                        DesignUtils.makeSnackbar(context.getString(R.string.network_message_error),
                                context.getCoordinatorLayout(), context);
                    }
                }
        );

        RetryPolicy policy = new DefaultRetryPolicy(NumberConfig.NB_SOCKET_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        // Adding request to request queue
        this.appController.addToRequestQueue(request, MarketofferDetails.MARKETOFFER_DETAILS_TASK);
    }

}
