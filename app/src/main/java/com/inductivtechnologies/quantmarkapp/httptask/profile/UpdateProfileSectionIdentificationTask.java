package com.inductivtechnologies.quantmarkapp.httptask.profile;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.activities.profile.ProfileUpdateSectionIdentificationActivity;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.bottomsheets.BottomSheetProgress;
import com.inductivtechnologies.quantmarkapp.configuration.NumberConfig;
import com.inductivtechnologies.quantmarkapp.configuration.UrlRestApiConfig;
import com.inductivtechnologies.quantmarkapp.models.entities.Token;
import com.inductivtechnologies.quantmarkapp.models.entities.User;
import com.inductivtechnologies.quantmarkapp.models.http.HttpResponseObject;
import com.inductivtechnologies.quantmarkapp.store.repositories.UserRepository;
import com.inductivtechnologies.quantmarkapp.utils.DesignUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * UpdateProfileSectionIdentificationTask : Modification des informations d'un utilisateur
 */
public class UpdateProfileSectionIdentificationTask {

    //Context
    private final ProfileUpdateSectionIdentificationActivity context;

    //Volley instance singleton
    private final AppController appController;

    //Repositories
    private final UserRepository simpleuserRepo;
    //Dialog
    private final BottomSheetProgress progress;

    //Constructeur(s)

    public UpdateProfileSectionIdentificationTask(ProfileUpdateSectionIdentificationActivity context, AppController appController) {
        this.context = context;
        this.appController = appController;

        this.simpleuserRepo = new UserRepository(context);

        //Progress
        this.progress = new BottomSheetProgress(context);
    }

    private JSONObject makeParams(User user){
        //Json object
        JSONObject jsonParams = null;
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").create();

        //On contruiit les paramètres à envoyer
        Map<String, User> paramValues = new HashMap<>();
        paramValues.put(User.USER_CLASS_NAME, user);
        String stringParams = gson.toJson(paramValues);

        try{
            jsonParams = new JSONObject(stringParams);
        }catch(JSONException exception){
            exception.printStackTrace();
        }
        return jsonParams;
    }

    public void makeRequest(final User user){
        //Url de la requête
        String urlRequest = String.format(UrlRestApiConfig.UPDATE_USER_URL, user.getObjectIdHexUser());
        //Le progress bar
        progress.show(context.getResources().getString(R.string.label_data_treatment_message));

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.PUT,
                urlRequest,
                makeParams(user),

                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        progress.hide();

                        //Vérification
                        final int statusCode = response.optInt(HttpResponseObject.CODE_STR);

                        switch(statusCode) {
                            case HttpResponseObject.SUCCESS_CODE:
                                //On met à jour dans la base de données interne

                                if(simpleuserRepo.updateUser(user) > 0){
                                    //on termine l'activité
                                    context.finish();
                                }else{
                                    DesignUtils.makeSnackbar(context.getResources().getString(R.string.label_unknow_error_message),
                                            context.getCoordinatorLayout(), context.getApplicationContext());
                                }
                                break;
                            default:
                                //Message d'erreur
                                DesignUtils.makeSnackbar(context.getResources().getString(R.string.label_unknow_error_message),
                                        context.getCoordinatorLayout(), context.getApplicationContext());
                                break;
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();

                        //Progress
                        progress.hide();

                        //Message d'erreur
                        DesignUtils.makeSnackbar(context.getResources().getString(R.string.network_message_error),
                                context.getCoordinatorLayout(), context.getApplicationContext());
                    }
                }
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put(Token.FIELD_TOKEN_VALUE, user.getToken().getTokenValue());
                return headers;
            }
        };

        RetryPolicy policy = new DefaultRetryPolicy(NumberConfig.NB_SOCKET_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        // Adding request to request queue
        this.appController.addToRequestQueue(request, ProfileUpdateSectionIdentificationActivity.UPDATE_IND_TASK);
    }
}
