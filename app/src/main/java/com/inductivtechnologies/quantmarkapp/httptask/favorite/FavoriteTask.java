package com.inductivtechnologies.quantmarkapp.httptask.favorite;

import android.view.View;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.activities.main.MainActivity;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.configuration.NumberConfig;
import com.inductivtechnologies.quantmarkapp.configuration.UrlRestApiConfig;
import com.inductivtechnologies.quantmarkapp.fragments.favorite.FavoriteFragment;
import com.inductivtechnologies.quantmarkapp.models.display.MarketOfferDisplay;
import com.inductivtechnologies.quantmarkapp.utils.DesignUtils;
import com.inductivtechnologies.quantmarkapp.utils.EntitiesUtils;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

/**
 * FavoriteTask
 */
public class FavoriteTask {

    private final FavoriteFragment fragment;

    //Volley instance singleton
    private final AppController appController;

    //Constructeur

    public FavoriteTask(FavoriteFragment fragment, AppController appController) {
        this.fragment = fragment;
        this.appController = appController;
    }

    public void makeRequest(final ArrayList<String> params) {

        if (fragment.getSwipeRefreshLayout() != null) {
            fragment.getSwipeRefreshLayout().setRefreshing(true);
        }

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.POST,
                UrlRestApiConfig.SEARCH_FAVORITES_URL,
                new JSONArray(params),
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        //On recupère les offres
                        List<MarketOfferDisplay> marketOffersDisplay = EntitiesUtils.getMarketOffersDisplayByJSONArray(response);

                        if(marketOffersDisplay != null && marketOffersDisplay.size() != 0){
                            fragment.llContainerEmpty.setVisibility(View.GONE);
                            fragment.getRecyclerView().setVisibility(View.VISIBLE);

                            for(MarketOfferDisplay marketOfferDisplay : marketOffersDisplay){
                                fragment.getFavorites().add(marketOfferDisplay);
                            }

                            //On notifie l'adaptateur
                            fragment.getAdapter().notifyDataSetChanged();
                        }else{
                            //On cache tout
                            fragment.llContainerEmpty.setVisibility(View.VISIBLE);
                            fragment.getRecyclerView().setVisibility(View.GONE);
                        }

                        if (fragment.getSwipeRefreshLayout() != null) {
                            fragment.getSwipeRefreshLayout().setRefreshing(false);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();

                        if (fragment.getSwipeRefreshLayout() != null) {
                            fragment.getSwipeRefreshLayout().setRefreshing(false);
                        }

                        DesignUtils.makeSnackbar(fragment.getContext().getString(R.string.network_message_error),
                                ((MainActivity) fragment.getActivity()).getCoordinatorLayout(), fragment.getContext());
                    }
                }
        );

        RetryPolicy policy = new DefaultRetryPolicy(NumberConfig.NB_SOCKET_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        // Adding request to request queue
        this.appController.addToRequestQueue(request, FavoriteFragment.FAVORITES_TASK);
    }
}
