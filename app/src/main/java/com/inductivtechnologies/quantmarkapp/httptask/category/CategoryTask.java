package com.inductivtechnologies.quantmarkapp.httptask.category;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.activities.main.MainActivity;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.comparators.CategoryDisplayComparator;
import com.inductivtechnologies.quantmarkapp.configuration.NumberConfig;
import com.inductivtechnologies.quantmarkapp.configuration.UrlRestApiConfig;
import com.inductivtechnologies.quantmarkapp.fragments.category.CategoryFragment;
import com.inductivtechnologies.quantmarkapp.models.display.CategoryDisplay;
import com.inductivtechnologies.quantmarkapp.utils.DesignUtils;
import com.inductivtechnologies.quantmarkapp.utils.EntitiesUtils;

import org.json.JSONArray;

import java.util.Collections;
import java.util.List;

/**
 * CategoryTask : Charge toutes les categoris
 */
public class CategoryTask {

    //Context
    private final CategoryFragment fragment;

    //Volley instance singleton
    private final AppController appController;

    //Constructeur

    public CategoryTask(CategoryFragment fragment, AppController appController) {
        this.fragment = fragment;
        this.appController = appController;
    }

    public void makeRequest(){

        if(fragment.getSwipeRefreshLayout() != null){
            fragment.getSwipeRefreshLayout().setRefreshing(true);
        }

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET,
                UrlRestApiConfig.CATEGORIES_URL,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        if(fragment.getSwipeRefreshLayout() != null){
                            fragment.getSwipeRefreshLayout().setRefreshing(false);
                        }

                        List<CategoryDisplay> categoriesDisplay = EntitiesUtils.getCategoryDisplayByJSONArray(response);

                        if(categoriesDisplay != null && categoriesDisplay.size() != 0){
                            //On raffraichi les anciennes données
                            fragment.getCategories().clear();

                            for(CategoryDisplay categoryDisplay : categoriesDisplay){
                                fragment.getCategories().add(categoryDisplay);
                            }

                            //On trie les résultats
                            Collections.sort(fragment.getCategories(), new CategoryDisplayComparator());

                            //On notifie l'adaptateur
                            fragment.getAdapter().notifyDataSetChanged();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();

                        if(fragment.getSwipeRefreshLayout() != null){
                            fragment.getSwipeRefreshLayout().setRefreshing(false);
                        }

                        DesignUtils.makeSnackbar(fragment.getContext().getString(R.string.network_message_error),
                                ((MainActivity) fragment.getActivity()).getCoordinatorLayout(), fragment.getContext());
                    }
                }
        );

        RetryPolicy policy = new DefaultRetryPolicy(NumberConfig.NB_SOCKET_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        // Adding request to request queue
        this.appController.addToRequestQueue(request, CategoryFragment.CATEGORY_TASK);
    }

}