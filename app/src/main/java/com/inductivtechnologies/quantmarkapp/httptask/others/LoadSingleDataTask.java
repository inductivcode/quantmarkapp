package com.inductivtechnologies.quantmarkapp.httptask.others;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.activities.others.SelectRecyclerviewItemActivity;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.bottomsheets.BottomSheetProgress;
import com.inductivtechnologies.quantmarkapp.configuration.NumberConfig;
import com.inductivtechnologies.quantmarkapp.utils.DesignUtils;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/**
 * LoadSingleDataTask :
 */
@SuppressWarnings("SameParameterValue")
public class LoadSingleDataTask {

    //Context
    private final SelectRecyclerviewItemActivity context;

    //Volley instance singleton
    private final AppController appController;

    //Dialog
    private final BottomSheetProgress progress;

    //Constructeur(s)

    public LoadSingleDataTask(SelectRecyclerviewItemActivity context, AppController appController) {
        this.context = context;
        this.appController = appController;

        //Progress
        this.progress = new BottomSheetProgress(context);
    }

    public void makeRequest(String url){
        //Le progress bar
        progress.show(context.getResources().getString(R.string.progress_content));

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET,
                url ,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        progress.hide();

                        List<String> data = new ArrayList<>();
                        for(int i = 0; i < response.length(); i++){
                            try {
                                data.add(response.getString(i));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        context.initRecyclerViewSelectItem(data);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();

                        //Progress
                        progress.hide();

                        //Message d'erreur
                        DesignUtils.makeSnackbar(context.getResources().getString(R.string.network_message_error),
                                context.getCoordinatorLayout(), context);
                    }
                }
        );

        RetryPolicy policy = new DefaultRetryPolicy(NumberConfig.NB_SOCKET_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        // Adding request to request queue
        this.appController.addToRequestQueue(request, SelectRecyclerviewItemActivity.LOAD_DATA_TASK);
    }

}
