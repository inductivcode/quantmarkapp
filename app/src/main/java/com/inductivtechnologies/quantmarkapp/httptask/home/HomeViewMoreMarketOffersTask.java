package com.inductivtechnologies.quantmarkapp.httptask.home;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.activities.main.HomeViewMoreMarketoffersActivity;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.configuration.NumberConfig;
import com.inductivtechnologies.quantmarkapp.models.display.MarketOfferDisplay;
import com.inductivtechnologies.quantmarkapp.models.http.HttpResponseObject;
import com.inductivtechnologies.quantmarkapp.utils.DesignUtils;

import org.json.JSONObject;

import java.util.List;
/**
 * HomeViewMoreMarketOffersTask
 */
@SuppressWarnings("unchecked")
public class HomeViewMoreMarketOffersTask {

    //Context
    private final HomeViewMoreMarketoffersActivity context;

    //Volley instance singleton
    private final AppController appController;

    //Constructeur(s)

    public HomeViewMoreMarketOffersTask(HomeViewMoreMarketoffersActivity context, AppController appController) {
        this.context = context;
        this.appController = appController;
    }

    public void makeRequest(final String urlRequest){
        //Le progress bar
        if(context.getSwipeRefreshLayout() != null){
            context.getSwipeRefreshLayout().setRefreshing(true);
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,
                urlRequest
                , null,

                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        HttpResponseObject httpResponse = new HttpResponseObject(response);

                        httpResponse.addHomeResults(response);
                        List<MarketOfferDisplay> markets = (List<MarketOfferDisplay>) httpResponse
                                .getObjectValue().get(HttpResponseObject.MARKETOFFERS_STR);
                        //Permet de savoir si il ya encore des données à charger
                        boolean dataIsComplete = (boolean) httpResponse.getObjectValue()
                                .get(HttpResponseObject.DATA_IS_COMPLETE);

                        for(MarketOfferDisplay market : markets){
                            context.getMarketOffers().add(market);
                        }

                        //On prévoie le chargement de plus de données
                         context.loadMoreAsyn(dataIsComplete);
                        //On notifie l'adaptateur
                        context.getAdapter().notifyDataSetChanged();

                        //On modifie la plage des données reçues par le serveur
                        context.setSkip();

                        //On cache le progress bar
                        if(context.getSwipeRefreshLayout() != null){
                            context.getSwipeRefreshLayout().setRefreshing(false);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();

                        if(context.getSwipeRefreshLayout() != null){
                            context.getSwipeRefreshLayout().setRefreshing(false);
                        }

                        DesignUtils.makeSnackbar(context.getString(R.string.network_message_error),
                                context.getCoordinator(), context);
                    }
                }
        );

        RetryPolicy policy = new DefaultRetryPolicy(NumberConfig.NB_SOCKET_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        // Adding request to request queue
        this.appController.addToRequestQueue(request,
                HomeViewMoreMarketoffersActivity.VIEW_MORE_MARKETOFFERS_TASK);
    }
}
