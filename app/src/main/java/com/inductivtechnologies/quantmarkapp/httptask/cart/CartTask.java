package com.inductivtechnologies.quantmarkapp.httptask.cart;

import android.view.View;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.activities.main.MainActivity;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.configuration.NumberConfig;
import com.inductivtechnologies.quantmarkapp.configuration.UrlRestApiConfig;
import com.inductivtechnologies.quantmarkapp.fragments.cart.CartFragment;
import com.inductivtechnologies.quantmarkapp.models.display.MarketOfferDisplay;
import com.inductivtechnologies.quantmarkapp.models.entities.CartItem;
import com.inductivtechnologies.quantmarkapp.utils.DesignUtils;
import com.inductivtechnologies.quantmarkapp.utils.EntitiesUtils;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

/**
 * CartTask
 */
public class CartTask {

    //Context
    private final CartFragment fragment;

    //Volley instance singleton
    private final AppController appController;

    //Constructeur

    public CartTask(CartFragment fragment, AppController appController) {
        this.fragment = fragment;
        this.appController = appController;
    }

    public void makeRequest(ArrayList<String> idsHex){

        if(fragment.getSwipeRefreshLayout() != null){
            fragment.getSwipeRefreshLayout().setRefreshing(true);
        }

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.POST,
                UrlRestApiConfig.CARTITEMS_URL,
                new JSONArray(idsHex),
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        //On recupère les offres
                        List<MarketOfferDisplay> marketOffersDisplay = EntitiesUtils.getMarketOffersDisplayByJSONArray(response);

                        if(marketOffersDisplay != null && marketOffersDisplay.size() != 0){
                            fragment.llContainerEmptyCart.setVisibility(View.GONE);
                            fragment.getRecyclerView().setVisibility(View.VISIBLE);

                            //On raffraichi les anciennes données
                            fragment.getCartitems().clear();

                            // On raffraichi la base de données
                            List<CartItem> cartitems = fragment.refreshCartItems(marketOffersDisplay);

                            for(CartItem cartitem : cartitems){
                                fragment.getCartitems().add(cartitem);
                            }

                            //Et on met à jour le badge
                            ((MainActivity) fragment.getActivity()).setBagde(fragment.getCartitems().size());

                            //On notifie l'adaptateur
                            fragment.getAdapter().notifyDataSetChanged();

                            //Etat du bouton Acheter
                            fragment.getBtnOrder().setEnabled(true);
                            fragment.getBtnOrder().setText(fragment.getContext().getResources()
                                    .getString(R.string.text_display_label_order, fragment.getCartitems().size()));
                            //Le total
                            fragment.setTvTotalCart();
                        }else{
                            //Etat du bouton Acheter
                            fragment.getBtnOrder().setEnabled(false);

                            //On cache tout
                            fragment.llContainerEmptyCart.setVisibility(View.VISIBLE);
                            fragment.getRecyclerView().setVisibility(View.GONE);
                        }

                        if(fragment.getSwipeRefreshLayout() != null){
                            fragment.getSwipeRefreshLayout().setRefreshing(false);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();

                        if(fragment.getSwipeRefreshLayout() != null){
                            fragment.getSwipeRefreshLayout().setRefreshing(false);
                        }

                        DesignUtils.makeSnackbar(fragment.getContext().getString(R.string.network_message_error),
                                ((MainActivity) fragment.getActivity()).getCoordinatorLayout(), fragment.getContext());
                    }
                }
        );

        RetryPolicy policy = new DefaultRetryPolicy(NumberConfig.NB_SOCKET_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        // Adding request to request queue
        this.appController.addToRequestQueue(request, CartFragment.CARTITEMS_TASK);
    }
}
