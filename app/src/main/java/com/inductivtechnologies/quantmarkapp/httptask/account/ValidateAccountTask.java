package com.inductivtechnologies.quantmarkapp.httptask.account;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.inductivtechnologies.quantmarkapp.activities.account.ValidateAccountActivity;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.callbacks.HttpCallback;
import com.inductivtechnologies.quantmarkapp.configuration.NumberConfig;
import com.inductivtechnologies.quantmarkapp.configuration.UrlRestApiConfig;
import com.inductivtechnologies.quantmarkapp.models.entities.Token;
import com.inductivtechnologies.quantmarkapp.models.entities.User;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

/**
 * ValidateAccountTask
 */
public class ValidateAccountTask {

    ///Callback
    private final WeakReference<HttpCallback> callback;

    //Volley instance singleton
    private final AppController appController;

    //Constructeur(s)
    public ValidateAccountTask(HttpCallback callback, AppController appController) {
        this.appController = appController;
        this.callback = new WeakReference<>(callback);
    }

    private JSONObject makeParams(User user){
        //Json object
        JSONObject jsonParams = null;
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").create();

        //On contruiit les paramètres à envoyer
        Map<String, User> paramValues = new HashMap<>();
        paramValues.put(User.USER_CLASS_NAME, user);
        String stringParams = gson.toJson(paramValues);

        try{
            jsonParams = new JSONObject(stringParams);
        }catch(JSONException exception){
            exception.printStackTrace();
        }
        return jsonParams;
    }

    public void makeRequest(final User user){
        //Url de la requête
        String urlRequest = String.format(UrlRestApiConfig.UPDATE_USER_URL, user.getObjectIdHexUser());

        if(callback.get() != null) callback.get().beforeExecute();

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.PUT,
                urlRequest ,
                makeParams(user),
                createRequestSuccessListener(),
                createRequestErrorListener()
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put(Token.FIELD_TOKEN_VALUE, user.getToken().getTokenValue());
                return headers;
            }
        };

        RetryPolicy policy = new DefaultRetryPolicy(NumberConfig.NB_SOCKET_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        // Adding request to request queue
        this.appController.addToRequestQueue(request, ValidateAccountActivity.VALIDATION_ACCOUNT_TASK);
    }

    private Response.Listener<JSONObject> createRequestSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if(callback.get() != null) callback.get().afterExecute(response);
            }
        };
    }

    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                if(callback.get() != null) callback.get().whenErrors();
            }
        };
    }
    
}
