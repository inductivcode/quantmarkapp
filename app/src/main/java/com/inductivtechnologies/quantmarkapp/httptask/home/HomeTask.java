package com.inductivtechnologies.quantmarkapp.httptask.home;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.callbacks.HttpCallback;
import com.inductivtechnologies.quantmarkapp.configuration.NumberConfig;
import com.inductivtechnologies.quantmarkapp.configuration.UrlRestApiConfig;
import com.inductivtechnologies.quantmarkapp.fragments.home.HomeFragment;

import org.json.JSONObject;

import java.lang.ref.WeakReference;

/**
 * HomeTask : Charge les offres de la page d'accueil
 */
public class HomeTask {

    ///Callback
    private final WeakReference<HttpCallback> callback;

    private final AppController appController;

    //Constructeur
    public HomeTask(HttpCallback callback, AppController appController) {
        this.appController = appController;
        this.callback = new WeakReference<>(callback);
    }

    //Méthodes

    public void makeRequest(){
        if(callback.get() != null) callback.get().beforeExecute();

        JsonObjectRequest request = new JsonObjectRequest(
                Request.Method.GET,
                UrlRestApiConfig.HOME_PRESENTATION_OFFERS_URL,
                null,
                createRequestSuccessListener(),
                createRequestErrorListener());

        RetryPolicy policy = new DefaultRetryPolicy(NumberConfig.NB_SOCKET_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        this.appController.addToRequestQueue(request, HomeFragment.TASK_HOME_FRAGMENT);
    }

    private Response.Listener<JSONObject> createRequestSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if(callback.get() != null) callback.get().afterExecute(response);
            }
        };
    }

    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                if(callback.get() != null) callback.get().whenErrors();
            }
        };
    }
}
