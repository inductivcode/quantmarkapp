package com.inductivtechnologies.quantmarkapp.httptask.account;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.callbacks.HttpCallback;
import com.inductivtechnologies.quantmarkapp.configuration.UrlRestApiConfig;
import com.inductivtechnologies.quantmarkapp.fragments.account.FragmentSignup;
import com.inductivtechnologies.quantmarkapp.models.entities.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

/**
 * SignupTask :Enregistre un utilisateur
 */
public class SignupTask {

    //Volley instance singleton
    private final AppController appController;

    ///Callback
    private final WeakReference<HttpCallback> callback;

    //Constructeur(s)

    public SignupTask(HttpCallback callback, AppController appController) {
        this.appController = appController;
        this.callback = new WeakReference<>(callback);
    }

    private JSONObject makeParams(User user){
        //Json object
        JSONObject jsonParams = null;
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").create();

        //On contruiit les paramètres à envoyer
        Map<String, User> paramValues = new HashMap<>();
        paramValues.put(User.USER_CLASS_NAME, user);
        String stringParams = gson.toJson(paramValues);

        try{
            jsonParams = new JSONObject(stringParams);
        }catch(JSONException exception){
            exception.printStackTrace();
        }
        return jsonParams;
    }

    public void makeRequest(final User userRegistered){
        JSONObject jsonObjectParams = makeParams(userRegistered);

        if(callback.get() != null) callback.get().beforeExecute();

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                UrlRestApiConfig.SIGNUP_URL,
                jsonObjectParams,
                createRequestSuccessListener(),
                createRequestErrorListener()
        );

        request.setRetryPolicy(
                new DefaultRetryPolicy(
                        0,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                )
        );

        this.appController.addToRequestQueue(request, FragmentSignup.SIGNUP_TASK);
    }

    private Response.Listener<JSONObject> createRequestSuccessListener() {
        return new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if(callback.get() != null) callback.get().afterExecute(response);
            }
        };
    }

    private Response.ErrorListener createRequestErrorListener() {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                if(callback.get() != null) callback.get().whenErrors();
            }
        };
    }

}