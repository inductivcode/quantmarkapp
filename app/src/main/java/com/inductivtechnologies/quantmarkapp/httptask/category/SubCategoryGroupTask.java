package com.inductivtechnologies.quantmarkapp.httptask.category;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.activities.category.SubCategoriesGroupActivity;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.configuration.NumberConfig;
import com.inductivtechnologies.quantmarkapp.configuration.UrlRestApiConfig;
import com.inductivtechnologies.quantmarkapp.models.display.ProductTypeDisplay;
import com.inductivtechnologies.quantmarkapp.utils.DesignUtils;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * SubCategoryGroupTask
 */
public class SubCategoryGroupTask {

    //Context
    private final SubCategoriesGroupActivity context;

    //Volley instance singleton
    private final AppController appController;

    //Constructeur

    public SubCategoryGroupTask(SubCategoriesGroupActivity context, AppController appController) {
        this.context = context;
        this.appController = appController;
    }

    public void makeRequest(ArrayList<String> idsHexCategory) {
        if (context.getSwipeRefreshLayout() != null) {
            context.getSwipeRefreshLayout().setRefreshing(true);
        }

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.POST,
                UrlRestApiConfig.SUB_CATEGORIES_GROUP_URL,
                new JSONArray(idsHexCategory),
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        //On traite le résultat
                        if (response != null && response.length() != 0) {
                            for(int i = 0; i < response.length(); i++){
                                ProductTypeDisplay productTypeDisplay = new ProductTypeDisplay(response.optJSONObject(i));
                                context.getSubCategories().add(productTypeDisplay);
                            }

                            //On notifie l'adaptateur
                            context.getAdapter().notifyDataSetChanged();
                        }

                        if (context.getSwipeRefreshLayout() != null) {
                            context.getSwipeRefreshLayout().setRefreshing(false);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();

                        if (context.getSwipeRefreshLayout() != null) {
                            context.getSwipeRefreshLayout().setRefreshing(false);
                        }

                        DesignUtils.makeSnackbar(context.getString(R.string.network_message_error),
                                context.getCoordinatorLayout(), context);
                    }
                }
        );

        RetryPolicy policy = new DefaultRetryPolicy(NumberConfig.NB_SOCKET_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        // Adding request to request queue
        this.appController.addToRequestQueue(request, SubCategoriesGroupActivity.SUB_CATEGORY_GROUP_TASK);
    }

}
