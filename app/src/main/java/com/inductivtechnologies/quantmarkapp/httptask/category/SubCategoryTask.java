package com.inductivtechnologies.quantmarkapp.httptask.category;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.activities.category.SubCategoriesActivity;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.configuration.NumberConfig;
import com.inductivtechnologies.quantmarkapp.configuration.UrlRestApiConfig;
import com.inductivtechnologies.quantmarkapp.models.display.ProductTypeDisplay;
import com.inductivtechnologies.quantmarkapp.models.utils.ProductTypeGroup;
import com.inductivtechnologies.quantmarkapp.utils.DesignUtils;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.Locale;

/**
 * SubCategoryGroupTask
 */
public class SubCategoryTask {

    //Context
    private final SubCategoriesActivity context;

    //Volley instance singleton
    private final AppController appController;

    //Constructeur

    public SubCategoryTask(SubCategoriesActivity context, AppController appController) {
        this.context = context;
        this.appController = appController;
    }

    public void makeRequest(String idHexCategory) {
        //Url de la requête
        String urlRequest = String.format(Locale.getDefault(), UrlRestApiConfig.SUB_CATEGORIES_URL,
                idHexCategory);

        if (context.getSwipeRefreshLayout() != null) {
            context.getSwipeRefreshLayout().setRefreshing(true);
        }

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET,
                urlRequest,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        //On traite le résultat
                        if (response != null && response.length() != 0) {
                            try {
                                if(response.length() == 1){
                                    for (int i = 0; i < response.getJSONArray(0).length(); i++) {
                                        ProductTypeDisplay productTypeDisplay = new ProductTypeDisplay(response.getJSONArray(0).getJSONObject(i));
                                        context.getSubCategories().add(productTypeDisplay);
                                    }
                                }else{
                                    for (int i = 0; i < response.length(); i++) {
                                        String interStr = response.getJSONArray(i).getJSONObject(0)
                                                .getJSONObject("subCategory").getString("groupeFr");
                                        JSONArray arrayProductTypeDisplay = response.getJSONArray(i);

                                        if(interStr.isEmpty()) {
                                            for(int j = 0; j < arrayProductTypeDisplay.length(); j++){
                                                ProductTypeDisplay productTypeDisplay = new ProductTypeDisplay(arrayProductTypeDisplay.getJSONObject(j));
                                                context.getSubCategories().add(productTypeDisplay);
                                            }
                                        }else{
                                            ProductTypeGroup productTypeGroup = new ProductTypeGroup(arrayProductTypeDisplay);
                                            context.getSubCategories().add(productTypeGroup);
                                        }
                                    }
                                }

                                //On notifie l'adaptateur
                                context.getAdapter().notifyDataSetChanged();

                                if (context.getSwipeRefreshLayout() != null) {
                                    context.getSwipeRefreshLayout().setRefreshing(false);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();

                        if (context.getSwipeRefreshLayout() != null) {
                            context.getSwipeRefreshLayout().setRefreshing(false);
                        }

                        DesignUtils.makeSnackbar(context.getString(R.string.network_message_error),
                                context.getCoordinatorLayout(), context);
                    }
                }
        );

        RetryPolicy policy = new DefaultRetryPolicy(NumberConfig.NB_SOCKET_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        // Adding request to request queue
        this.appController.addToRequestQueue(request, SubCategoriesActivity.SUB_CATEGORY_TASK);
    }
}
