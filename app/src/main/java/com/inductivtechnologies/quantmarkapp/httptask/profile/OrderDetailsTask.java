package com.inductivtechnologies.quantmarkapp.httptask.profile;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.activities.profile.OrderDetailsActivity;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.configuration.NumberConfig;
import com.inductivtechnologies.quantmarkapp.configuration.UrlRestApiConfig;
import com.inductivtechnologies.quantmarkapp.models.entities.Buy;
import com.inductivtechnologies.quantmarkapp.models.entities.Token;
import com.inductivtechnologies.quantmarkapp.models.entities.User;
import com.inductivtechnologies.quantmarkapp.models.http.HttpResponseObject;
import com.inductivtechnologies.quantmarkapp.utils.DesignUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * OrderDetailsTask
 */
public class OrderDetailsTask {

    //Context
    private final OrderDetailsActivity context;

    //Volley instance singleton
    private final AppController appController;

    //Constructeur(s)

    public OrderDetailsTask(OrderDetailsActivity context, AppController appController) {
        this.context = context;
        this.appController = appController;
    }

    public void makeRequest(final User user, final String idHexOrder){
        //Url de la requête
        String urlRequest = String.format(Locale.getDefault(), UrlRestApiConfig.ORDER_DETAILS_URL,
                user.getObjectIdHexUser(), idHexOrder);

        //Le progress bar
        if(context.getSwipeRefreshLayout() != null){
            context.getSwipeRefreshLayout().setRefreshing(true);
        }

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,
                urlRequest ,
                null,

                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //Vérification
                        try {
                            int statutCode = response.getInt(HttpResponseObject.CODE_STR);

                            switch(statutCode){
                                case HttpResponseObject.SUCCESS_CODE :
                                    HttpResponseObject httpResponseObject = new HttpResponseObject(response);
                                    httpResponseObject.addOrderResults(response);

                                    context.setIssuerNbArticles((int) httpResponseObject
                                            .getObjectValue().get(HttpResponseObject.ISSUER_NB_ARTICLE_STR));
                                    context.setPurchase((Buy) httpResponseObject
                                            .getObjectValue().get(HttpResponseObject.ORDER_STR));

                                    //Initialisationde tout
                                    context.globalInit();
                                    break;
                                case HttpResponseObject.ORDER_NOT_EXIST_CODE :
                                    break;
                                case HttpResponseObject.TOKEN_UNEXIST_ERROR_CODE :
                                    break;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        //On cache le progress bar
                        if(context.getSwipeRefreshLayout() != null){
                            context.getSwipeRefreshLayout().setRefreshing(false);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();

                        //On cache le progress bar
                        if(context.getSwipeRefreshLayout() != null){
                            context.getSwipeRefreshLayout().setRefreshing(false);
                        }

                        DesignUtils.makeSnackbar(context.getString(R.string.network_message_error),
                                context.getCoordinator(),context);
                    }
                }
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put(Token.FIELD_TOKEN_VALUE, user.getToken().getTokenValue());
                return headers;
            }
        };

        RetryPolicy policy = new DefaultRetryPolicy(NumberConfig.NB_SOCKET_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        // Adding request to request queue
        this.appController.addToRequestQueue(request, OrderDetailsActivity.ORDER_DETAILS_TASK);
    }

}
