package com.inductivtechnologies.quantmarkapp.httptask.marketofferdetails;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.activities.marketofferdetails.MarketofferDetails;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.configuration.NumberConfig;
import com.inductivtechnologies.quantmarkapp.configuration.UrlRestApiConfig;
import com.inductivtechnologies.quantmarkapp.models.entities.Favorite;
import com.inductivtechnologies.quantmarkapp.models.http.HttpResponseObject;
import com.inductivtechnologies.quantmarkapp.store.repositories.FavoriteRepository;
import com.inductivtechnologies.quantmarkapp.utils.DesignUtils;

import org.json.JSONObject;

import java.util.Locale;

/**
 * AddFavoriteTask : Ajoute un favori
 */
public class AddFavoriteTask {

    //Context
    private final MarketofferDetails context;

    //Volley instance singleton
    private final AppController appController;

    //Repo
    private final FavoriteRepository favoriteRepo;

    //Constructeur

    public AddFavoriteTask(MarketofferDetails context, AppController appController) {
        this.context = context;
        this.appController = appController;
        this.favoriteRepo = new FavoriteRepository(context);
    }

    //Méthodes

    public void makeRequest(final String marketofferHexId){
        //Url de la requête
        String urlRequest = String.format(Locale.getDefault(), UrlRestApiConfig.ADD_FAVORITES_URL,
                marketofferHexId);

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
                urlRequest,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        //Vérification
                        final int statusCode = response.optInt(HttpResponseObject.CODE_STR);

                        switch(statusCode){
                            case HttpResponseObject.SUCCESS_CODE :

                                Favorite favorite = new Favorite(marketofferHexId);
                                int result = favoriteRepo.insertFavorite(favorite);

                                if(result >0){
                                    context.getBtnAddFavorite()
                                            .setImageDrawable(context.getResources()
                                                    .getDrawable(R.drawable.ic_action_add_favorite_success));

                                    context.getTvFavorite().setText(context.getResources()
                                            .getString(R.string.marketoffer_details_label_delete_favorite));
                                }

                                break;
                            default:
                                DesignUtils.makeSnackbar(context.getString(R.string.label_unknow_error_message),
                                        context.getCoordinatorLayout(), context);
                                break;
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();

                        DesignUtils.makeSnackbar(context.getString(R.string.network_message_error),
                                context.getCoordinatorLayout(), context);
                    }
                }
        );

        RetryPolicy policy = new DefaultRetryPolicy(NumberConfig.NB_SOCKET_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        // Adding request to request queue
        this.appController.addToRequestQueue(request, MarketofferDetails.ADD_FAVORITE_TASK);
    }
}
