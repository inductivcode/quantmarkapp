package com.inductivtechnologies.quantmarkapp.httptask.cart;

import android.view.View;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.activities.cart.CartActivity;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.configuration.NumberConfig;
import com.inductivtechnologies.quantmarkapp.configuration.UrlRestApiConfig;
import com.inductivtechnologies.quantmarkapp.models.display.MarketOfferDisplay;
import com.inductivtechnologies.quantmarkapp.models.entities.CartItem;
import com.inductivtechnologies.quantmarkapp.utils.DesignUtils;
import com.inductivtechnologies.quantmarkapp.utils.EntitiesUtils;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

/**
 * CartActivityTask
 */
public class CartActivityTask {

    //Context
    private final CartActivity context;

    //Volley instance singleton
    private final AppController appController;

    //Constructeur

    public CartActivityTask(CartActivity context, AppController appController) {
        this.context = context;
        this.appController = appController;
    }

    public void makeRequest(ArrayList<String> idsHex) {

        if (context.getSwipeRefreshLayout() != null) {
            context.getSwipeRefreshLayout().setRefreshing(true);
        }

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.POST,
                UrlRestApiConfig.CARTITEMS_URL,
                new JSONArray(idsHex),

                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        //On recupère les offres
                        List<MarketOfferDisplay> marketOffersDisplay = EntitiesUtils
                                .getMarketOffersDisplayByJSONArray(response);

                        if (marketOffersDisplay != null && marketOffersDisplay.size() != 0) {
                            //On affiche le recyclerview
                            context.llContainerEmptyCart.setVisibility(View.GONE);
                            context.getRecyclerView().setVisibility(View.VISIBLE);

                            //On raffraichi les anciennes données
                            context.getCartitems().clear();

                            // On raffraichi la base de données
                            List<CartItem> cartitems = context.refreshCartItems(marketOffersDisplay);

                            //On recupère les données
                            for (CartItem cartitem : cartitems) {
                                context.getCartitems().add(cartitem);
                            }

                            //On notifie l'adaptateur
                            context.getAdapter().notifyDataSetChanged();

                            //On affiche le bouton pour vider le panier
                            context.getBtnClearCart().setVisibility(View.VISIBLE);
                            //Etat du bouton Acheter
                            context.getBtnOrder().setEnabled(true);
                            context.getBtnOrder().setText(context.getResources()
                                    .getString(R.string.text_display_label_order, context.getCartitems().size()));

                            //Le total du panier
                            context.setTvTotalCart();
                        } else {
                            //Etat du bouton Acheter
                            context.getBtnOrder().setEnabled(false);

                            //On cache tout
                            context.llContainerEmptyCart.setVisibility(View.VISIBLE);
                            context.getRecyclerView().setVisibility(View.GONE);
                        }

                        if (context.getSwipeRefreshLayout() != null) {
                            context.getSwipeRefreshLayout().setRefreshing(false);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();

                        if (context.getSwipeRefreshLayout() != null) {
                            context.getSwipeRefreshLayout().setRefreshing(false);
                        }

                        DesignUtils.makeSnackbar(context.getResources().getString(R.string.network_message_error),
                                context.getCoordinatorLayout(), context);
                    }
                }
        );

        RetryPolicy policy = new DefaultRetryPolicy(NumberConfig.NB_SOCKET_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        // Adding request to request queue
        this.appController.addToRequestQueue(request, CartActivity.CART_ACTIVITY_TASK);
    }
}
