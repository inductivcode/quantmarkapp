package com.inductivtechnologies.quantmarkapp.httptask.search;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.activities.search.SearchActivity;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.configuration.NumberConfig;
import com.inductivtechnologies.quantmarkapp.configuration.UrlRestApiConfig;
import com.inductivtechnologies.quantmarkapp.models.display.SearchItemDisplay;
import com.inductivtechnologies.quantmarkapp.models.display.SearchNoticeDisplay;
import com.inductivtechnologies.quantmarkapp.utils.DesignUtils;

import org.json.JSONArray;

import java.util.Locale;

/**
 * SearchTask
 */
public class SearchTask {

    //Context
    private final SearchActivity context;

    //Volley instance singleton
    private final AppController appController;

    //Constructeur

    public SearchTask(SearchActivity context, AppController appController) {
        this.context = context;
        this.appController = appController;
    }

    public void makeRequest(final String searchValue, final int searchFilterValue){

        String urlRequest = String.format(Locale.getDefault(),
                UrlRestApiConfig.SEARCH_ITEM_URL, searchFilterValue, searchValue);

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET,
                 urlRequest,
                null,

                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        //On vide la liste
                        context.getSearchItems().clear();
                        //On notifie l'adaptateur
                        context.getAdapter().notifyDataSetChanged();

                        if (response.length() != 0) {
                            //On  ajoute la notice
                            SearchNoticeDisplay searchNotice = new SearchNoticeDisplay(searchValue);
                            context.getSearchItems().add(searchNotice);

                            for(int i = 0; i < response.length(); i++){
                                SearchItemDisplay searchResult = new SearchItemDisplay(response.optJSONObject(i));
                                context.getSearchItems().add(searchResult);
                            }

                            //On notifie l'adaptateur
                            context.getAdapter().setSearchValue(searchValue);
                            context.getAdapter().notifyDataSetChanged();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();

                        DesignUtils.makeSnackbar(context.getResources().getString(R.string.network_message_error),
                                context.getCoordinatorLayout(), context);
                    }
                }
        );

        RetryPolicy policy = new DefaultRetryPolicy(NumberConfig.NB_SOCKET_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        // Adding request to request queue
        this.appController.addToRequestQueue(request, SearchActivity.SEARCH_ITEM_TASK);
    }
}
