package com.inductivtechnologies.quantmarkapp.httptask.profile;

import android.app.Activity;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.activities.profile.OrderDetailsActivity;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.bottomsheets.BottomSheetProgress;
import com.inductivtechnologies.quantmarkapp.configuration.NumberConfig;
import com.inductivtechnologies.quantmarkapp.configuration.UrlRestApiConfig;
import com.inductivtechnologies.quantmarkapp.models.entities.Token;
import com.inductivtechnologies.quantmarkapp.models.entities.User;
import com.inductivtechnologies.quantmarkapp.models.http.HttpResponseObject;
import com.inductivtechnologies.quantmarkapp.utils.DesignUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * CancelOrderTask
 */
public class CancelOrderTask {

    //Context
    private final OrderDetailsActivity context;

    //Volley instance singleton
    private final AppController appController;

    //Dialog
    private final BottomSheetProgress progress;

    //Constructeur(s)

    public CancelOrderTask(OrderDetailsActivity context, AppController appController) {
        this.context = context;
        this.appController = appController;

        //Progress
        this.progress = new BottomSheetProgress(context);
    }

    public void makeRequest(final User user, final String idHexOrder){
        //Url de la requête
        String urlRequest = String.format(Locale.getDefault(), UrlRestApiConfig.CANCEL_ORDER_URL,
                idHexOrder, user.getObjectIdHexUser());

        //Le progress bar
        progress.show(context.getResources()
                .getString(R.string.progress_cancel_order_message));

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.DELETE,
                urlRequest ,
                null,

                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //Vérification
                        try {
                            int statutCode = response.getInt(HttpResponseObject.CODE_STR);

                            switch(statutCode){
                                case HttpResponseObject.SUCCESS_CODE :
                                    //On termine l'activité avec le code OK
                                    context.setResult(Activity.RESULT_OK);
                                    context.finish();
                                    break;
                                case HttpResponseObject.ERROR_CODE :
                                    DesignUtils.makeSnackbar(context.getString(R.string.unknow_error_message),
                                            context.getCoordinator(),context);
                                    break;
                                case HttpResponseObject.TOKEN_UNEXIST_ERROR_CODE :
                                    break;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        //On cache le progress bar
                        progress.hide();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();

                        //On cache le progress bar
                        progress.hide();

                        DesignUtils.makeSnackbar(context.getString(R.string.network_message_error),
                                context.getCoordinator(),context);
                    }
                }
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put(Token.FIELD_TOKEN_VALUE, user.getToken().getTokenValue());
                return headers;
            }
        };

        RetryPolicy policy = new DefaultRetryPolicy(NumberConfig.NB_SOCKET_TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        // Adding request to request queue
        this.appController.addToRequestQueue(request, OrderDetailsActivity.CANCEL_ORDER_TASK);
    }
}
