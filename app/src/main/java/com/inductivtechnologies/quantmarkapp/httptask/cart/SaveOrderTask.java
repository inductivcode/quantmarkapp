package com.inductivtechnologies.quantmarkapp.httptask.cart;

import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.activities.cart.ResumeOrdersActivity;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.bottomsheets.BottomSheetProgress;
import com.inductivtechnologies.quantmarkapp.bottomsheets.BottomsheetCustom;
import com.inductivtechnologies.quantmarkapp.configuration.UrlRestApiConfig;
import com.inductivtechnologies.quantmarkapp.models.entities.Token;
import com.inductivtechnologies.quantmarkapp.models.entities.User;
import com.inductivtechnologies.quantmarkapp.models.http.HttpResponseObject;
import com.inductivtechnologies.quantmarkapp.models.utils.Order;
import com.inductivtechnologies.quantmarkapp.store.repositories.CartItemRepository;
import com.inductivtechnologies.quantmarkapp.utils.DesignUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * OrderTask : Requete HTTP qui sauvegarde la commande sur le serveur
 */
public class SaveOrderTask {

    //Context
    private final ResumeOrdersActivity activity;

    //Volley instance singleton
    private final AppController appController;

    //Repositories
    private final CartItemRepository cartRepo;

    //Sheet dialog
    private final BottomSheetProgress progressDialog;

    //Constructeur

    public SaveOrderTask(ResumeOrdersActivity activity, AppController appController) {
        this.activity = activity;
        this.appController = appController;
        this.cartRepo = new CartItemRepository(activity);

        this.progressDialog = new BottomSheetProgress(activity);

    }

    private JSONArray makeParams( List<Order> orders){
        //Json array
        JSONArray jsonParams = new JSONArray();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").create();

        //On contruit les paramètres à envoyer
        for(Order order : orders){
            String stringParams = gson.toJson(order);
            try{
                JSONObject jsonObject = new JSONObject(stringParams);
                jsonParams.put(jsonObject);
            }catch(JSONException exception){
                exception.printStackTrace();
            }
        }
        return jsonParams;
    }

    public void makeRequest(final User user, List<Order> orders) {
        //Url de la requête
        String urlRequest = String.format(Locale.getDefault(), UrlRestApiConfig.MAKE_ORDER_URL,
                user.getObjectIdHexUser());
        //Progress
        progressDialog.show(activity.getResources().getString(R.string.progress_save_order_content));

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.POST,
                urlRequest,
                makeParams(orders),

                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        //On recupère les données
                        try {
                            JSONObject jsonObject = response.getJSONObject(0);
                            HttpResponseObject responseObject = new HttpResponseObject(jsonObject);

                            switch(responseObject.getCode()){
                                case HttpResponseObject.SUCCESS_CODE :

                                    //On ouvre le dialog
                                    BottomsheetCustom customDialog = new BottomsheetCustom(activity);
                                    customDialog.setContent(activity.getResources().getString(R.string.text_success_make_order));
                                    customDialog.setButtonTitle(activity.getResources().getString(R.string.text_continue));
                                    customDialog.show();

                                    //Action du bouton
                                    customDialog.getBtnDialog().setOnClickListener(new View.OnClickListener(){
                                        @Override
                                        public void onClick(View v) {
                                            String paramIntent = activity.getIdMarketoffer();
                                            if(paramIntent.isEmpty()){
                                                //O a commandé avec tous les éléments du panier
                                                cartRepo.deleteAllCartitems();
                                            }else{
                                                //On a commander avec un seul élément dont ID = paramIntent
                                                //On le supprime du panier
                                                cartRepo.deleteCartItem(paramIntent);
                                            }
                                            //On termine l'activité
                                            activity.finish();
                                        }
                                    });

                                    break;
                                case HttpResponseObject.TOKEN_UNEXIST_ERROR_CODE :
                                    break;
                                case HttpResponseObject.ERROR_CODE:
                                    //Message d'erreur
                                    DesignUtils.makeSnackbar(activity.getResources().getString(R.string.label_unknow_error_message),
                                            activity.getCoordinatorLayout(), activity);
                                    break;
                            }

                            //Progress
                            progressDialog.hide();

                        } catch (JSONException error) {
                            error.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();

                        //Progress
                        progressDialog.hide();

                        DesignUtils.makeSnackbar(activity.getResources().getString(R.string.network_message_error),
                                activity.getCoordinatorLayout(), activity);
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put(Token.FIELD_TOKEN_VALUE, user.getToken().getTokenValue());
                return headers;
            }
        };

        int socketTimeout = 60000;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(policy);

        // Adding request to request queue
        this.appController.addToRequestQueue(request, ResumeOrdersActivity.SAVE_ORDERS_TASK);
    }
}
