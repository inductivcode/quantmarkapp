package com.inductivtechnologies.quantmarkapp.configuration;

/**
 * NumberConfig : Collection de nombres statiques
 */
public class NumberConfig {

    //Mode de livraison
    public static final int NB_STORE_DELIVERY = 0;
    public static final int NB_HOME_DELIVERY = 1;
    public static final int NB_MOKOLO_DELIVERY = 2;
    public static final int NB_CENTRAL_MARKET_DELIVERY = 3;

    //Mode de payement
    public static final int NB_STORE_OR_PLACE_PAYMENT = 0;
    public static final int NB_MTN_PAYMENT = 1;
    public static final int NB_PAYPAL_PAYMENT = 2;

    //Filtre de recherche
    public static final int NB_SUBCATEGORY_SEARCH = 0;
    public static final int NB_ISSUER_SEARCH = 1;

    //Convertion en dollars us
    public static final double CURRENCIES_XAF_CHANGE = 655.88;

    //Largeur maximale des offres recues du serveur
    public static final int NB_WIDTH_DATA = 10;

    //Si la quantité d'une offre est inférieur à cette valeur, l'offre est dite en quantité limitée
    //public static final int NB_OFFER_MAX_QUANTITY = 20;

    //Mode de trie
    public static final int NB_DEFAULT_SORT = 0;
    public static final int NB_ASCENDING_PRICE_SORT = 1;
    public static final int NB_DESCENDING_PRICE_SORT = 2;
    public static final int NB_NEW_ARTICLES = 3;

    //Pour charger les données ( les quartiers par exemple)
    public static final int NB_LOAD_DISTRICT = 0;
    public static final int NB_LOAD_COUNTRIES = 1;
    public static final int NB_LOAD_CITY = 2;

    //Socket timeout
    public static final int NB_SOCKET_TIMEOUT = 40000;
}
