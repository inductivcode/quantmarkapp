package com.inductivtechnologies.quantmarkapp.configuration;

/**
 * AppConig : Repertoire des adresses HTTP de l'api
 */
public class UrlRestApiConfig {

    //Url de base
    private static final String HOST_URL = "https://www.quantmark-cm.com";
    //private static final String HOST_URL = "http://192.168.100.20:8080";
    //private static final String HOST_URL = "http://10.0.0.2:8080";
    private static final String API_BASE_URL = HOST_URL + "/restapi";

    //Url des ressources images
    public static final String IMAGE_RESOURCE_URL = HOST_URL + "/image/";

    //Ressource utilisateur
    public static final String LOGIN_URL = API_BASE_URL + "/buyer/login";
    public static final String SIGNUP_URL = API_BASE_URL + "/buyer/signup";
    public static final String UPDATE_USER_URL = API_BASE_URL + "/buyer/%1$s/update";
    public static final String LOAD_PROPERTIES_URL = API_BASE_URL + "/buyer/%1$s/properties";
    public static final String LOGOUT_URL = API_BASE_URL + "/buyer/%1$s/logout";
    public static final String PURCHASES_URL = API_BASE_URL + "/buyer/%1$s/purchases/%2$d/%3$d";
    public static final String ORDERS_URL = API_BASE_URL + "/buyer/%1$s/orders/%2$d/%3$d";
    public static final String PURCHASE_DETAILS_URL = API_BASE_URL + "/buyer/%1$s/purchase/%2$s";
    public static final String ORDER_DETAILS_URL = API_BASE_URL + "/buyer/%1$s/order/%2$s";

    //Ressources achats et commandes
    public static final String CANCEL_ORDER_URL = API_BASE_URL + "/buy/order/%1$s/cancel/%2$s";
    public static final String MAKE_ORDER_URL = API_BASE_URL + "/buy/makeorder/%1$s";
    public static final String ADD_AND_CHECK_ORDERS_PAYPAL_PAYMENT_URL = API_BASE_URL +
            "/buy/paypal/payment/check/%1$s";

    //Ressource Category
    public static final String CATEGORIES_URL = API_BASE_URL + "/category/all";
    public static final String SUB_CATEGORIES_URL = API_BASE_URL + "/category/%1$s/subcategories";
    public static final String SUB_CATEGORy_MARKETOFFERS_URL = API_BASE_URL +
            "/category/subcategory/%1$s/marketoffers/%2$d/%3$d/%4$d";
    public static final String SUB_CATEGORIES_GROUP_URL = API_BASE_URL + "/category/subcategory/list";

    //Url des ressources offres
    //public static final String HOME_OFFERS_URL = API_BASE_URL + "/offers/exclusives";
    public static final String HOME_PRESENTATION_OFFERS_URL = API_BASE_URL + "/offers/presentation";
    public static final String HOME_MORE_PROMOTION_OFFERS_URL = API_BASE_URL + "/offers/promotion/%1$d/%2$d/%3$d";
    public static final String HOME_MORE_LOWCOST_OFFERS_URL = API_BASE_URL + "/offers/lowcost/%1$d/%2$d/%3$d";
    public static final String MARKETOFFER_DETAILS_URL = API_BASE_URL + "/offers/marketoffer/%1$s";
    public static final String CARTITEMS_URL = API_BASE_URL + "/offers/marketoffer/list";
    public static final String ISSUER_MARKETOFFERS_URL = API_BASE_URL + "/offers/issuer/%1$s/marketoffers/%2$d/%3$d/%4$d";
    public static final String SEARCH_ITEM_URL = API_BASE_URL + "/offers/search/%1$d/%2$s";

    //Ressource favori
    public static final String SEARCH_FAVORITES_URL = API_BASE_URL + "/favorite/list";
    public static final String ADD_FAVORITES_URL = API_BASE_URL + "/favorite/add/%1$s";
    public static final String REMOVE_FAVORITES_URL = API_BASE_URL + "/favorite/delete/%1$s";

    //Autres
    public static final String LOAD_DISTRICTS_ADDR =  API_BASE_URL + "/others/districts";
}
