package com.inductivtechnologies.quantmarkapp.configuration;

import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;

/**
 * PaypalConfig : Configuration pour le SDK Paypal
 */
public class PaypalConfig {

    public static final String PAYPAL_PAYMENT_DETAILS = "paypalPaymentDetails";
    public static final String PAYPAL_PAYMENT_ID = "paymentId";

    //Id de de l'application cliente
    //Sandbox
    //public static final String PAYPAL_CLIENT_ID = "AVnvTcxz0uJdu22iMcI2d2DOB1zHf1eSvRTNZBRV-Cd3Y4B96rGRzI57V2SOWsKISrQELu4o6ioAXGXa";
    //Production
    public static final String PAYPAL_CLIENT_ID = "AXYGtCLyeZLR9eqZ4-mgMM0if-XBBbmcKMcIgX2ynkXb8kz9HOFqv-bvTJT510iPz7LSP-OtjZyJMyGD";
    //Mot de passe de l'application cliente
    //public static final String PAYPAL_CLIENT_SECRET = "EJVtlQ_vnA3iTTg-WD1913rsz_-ciBwiTpKrDjNILDSzOYNsX-wXlgnK-c3qAS17jOma4uu6vgWooky7";

    //Environnement de test
    //public static final String PAYPAL_SANDBOX_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_SANDBOX;
    //Environnement live(production)
    public static final String PAYPAL_LIVE_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_PRODUCTION;

    //Intent de vente
    public static final String PAYMENT_INTENT = PayPalPayment.PAYMENT_INTENT_SALE;
    //Devise par défaut
    public static final String DEFAULT_EUR_CURRENCY = "EUR";
}
