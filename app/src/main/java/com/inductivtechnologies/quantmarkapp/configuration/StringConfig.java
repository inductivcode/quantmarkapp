package com.inductivtechnologies.quantmarkapp.configuration;

import android.content.Context;

import com.inductivtechnologies.quantmarkapp.R;

/**
 * StringConfig : Collection de chaines de caractères constantes
 */
public class StringConfig {

    //SSl keystore passwor
    public static final String SSL_KEYSTORE_PASSWORD = "Inductiv";

    public static final String STR_COLOR_FR = "Couleur";
    public static final String STR_COLOR_EN = "Color";

    //Les langues
    public static final String DEFAULT_FR  = "fr";
    public static final String DEFAULT_EN  = "en";

    public static final String NB_IND_PHONE_CMR  = "00237";

    public static final String INTENT_HOME_TITLE = "INTENT_HOME_TITLE";

    public static final String DEFAULT_EMPTY_PURCHASES = "default_empty_purchases.png";
    public static final String DEFAULT_EMPTY_ORDERS = "default_empty_orders.png";
    public static final String DEFAULT_EMPTY_CART = "default_empty_cart.png";
    public static final String DEFAULT_EMPTY_FAVORITE = "default_empty_favorite.png";

    //Pour recuperer la valeur renvoyer par l'activity Select
    public static final String STR_SELECT_DATA = "STR_SELECT_DATA";

    //Pour envoyer les valeurs à l'activité select afin de modifier le pays la ville et le quartier
    public static final String UPDATE_ADDR_INTENT_EXTRA = "UPDATE_ADDR_INTENT_EXTRA";

    //Pour transmettre certaines valeurs à l'activité sous catégories
    public static final String INTENT_CATEGORY_TITLE = "INTENT_CATEGORY_TITLE";
    public static final String INTENT_CATEGORY_HEX_ID = "INTENT_CATEGORY_HEX_ID";
    public static final String INTENT_CATEGORY_SUBTITLE = "INTENT_CATEGORY_SUBTITLE";
    public static final String INTENT_CATEGORY_IMAGE_NAME = "INTENT_CATEGORY_IMAGE_NAME";

    //Pour transmettre certaines valeurs à l'activité sous catégories afin d'afficher les marchés
    public static final String INTENT_SUBCATEGORY_TITLE = "INTENT_SUBCATEGORY_TITLE";
    public static final String INTENT_SUBCATEGORY_HEX_ID = "INTENT_SUBCATEGORY_HEX_ID";
    public static final String INTENT_SUBCATEGORY_SUBTITLE = "INTENT_SUBCATEGORY_SUBTITLE";
    public static final String INTENT_SUBCATEGORY_IMAGE_NAME = "INTENT_SUBCATEGORY_IMAGE_NAME";
    public static final String INTENT_SUBCATEGORY_NB_MARKETOFFER = "INTENT_SUBCATEGORY_NB_MARKETOFFER";

    //Pour transmettre les données à l'activité pruchaseDetails
    public static final String INTENT_BUY_HEX_ID = "INTENT_BUY_HEX_ID";

    //Pour envoyer les données à l'activite MarketOfferDetails
    public static final String INTENT_MARKETOFFER_HEX_ID = "INTENT_MARKETOFFER_HEX_ID";

    //Pour envoyer les données à l'activite IssuerProductsActivity
    public static final String INTENT_ISSUER_NB_MARKET_MARKETOFFERS = "INTENT_ISSUER_NB_MARKET_MARKETOFFERS";
    public static final String INTENT_ISSUER_COMPLETE_NAME = "INTENT_ISSUER_COMPLETE_NAME";
    public static final String INTENT_ISSUER_HEX_ID = "INTENT_ISSUER_HEX_ID";

    //Pour transmettre les valeurs a l'activité ResumeOrderActivity
    public static final String  INTENT_DELIVERY_METHOD = "INTENT_DELIVERY_METHOD";

    public static final String STR_RESULT_ADD_NEW_ADDRESS = "STR_RESULT_ADD_NEW_ADDRESS";

    //Pour transmettre certaines valeurs à l'activité sous recherche afin d'afficher les marchés
    public static final String INTENT_SEARCH_LABEL = "INTENT_SEARCH_LABEL";
    public static final String INTENT_SEARCH_ID_HEX = "INTENT_SEARCH_ID_HEX";
    public static final String INTENT_SEARCH_NB_ITEM = "INTENT_SEARCH_NB_ITEM";
    public static final String INTENT_SEARCH_FILTER = "INTENT_SEARCH_FILTER";

    //Ville
    public static final String[] cities = {
            "Yaoundé"
    };

    //Sexe
    public static String[] getDefaultArrayGender(Context context){
        return context.getResources().getStringArray(R.array.gender_array);
    }

    public static String[] getLocaleArrayGender(Context context){
        return context.getResources().getStringArray(R.array.gender_array_display);
    }

    //Pays
    public static String[] getDefaultArrayCountries(Context context){
        return context.getResources().getStringArray(R.array.countries_array);
    }

    public static String[] getLocaleArrayCountries(Context context){
        return context.getResources().getStringArray(R.array.countries_array_display);
    }
}
