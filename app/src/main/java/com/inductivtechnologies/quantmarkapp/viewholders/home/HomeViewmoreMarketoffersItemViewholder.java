package com.inductivtechnologies.quantmarkapp.viewholders.home;

import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.components.GlideComponent;
import com.inductivtechnologies.quantmarkapp.configuration.UrlRestApiConfig;
import com.inductivtechnologies.quantmarkapp.utils.CalculationUtils;
import com.inductivtechnologies.quantmarkapp.utils.StringUtils;

import java.text.NumberFormat;

/**
 * HomeViewmoreMarketoffersItemViewholder
 */

public class HomeViewmoreMarketoffersItemViewholder extends RecyclerView.ViewHolder{

    //Le context
    private final Context context;

    //Injections des vues
    private TextView tvNbFavorite;
    private TextView tvReductionNotice;

    private TextView tvPrice;
    private TextView tvFinalPrice;
    private ImageView ivThumbnail;

    public HomeViewmoreMarketoffersItemViewholder(View view, Context context) {
        super(view);
        this.context = context;

        //Les bues
        finfAllViews(view);
    }

    private void finfAllViews(View rootView){
        tvNbFavorite = rootView.findViewById(R.id.nb_favorite);
        tvReductionNotice = rootView.findViewById(R.id.value_reduction);

        tvPrice = rootView.findViewById(R.id.item_price);
        tvFinalPrice = rootView.findViewById(R.id.item_final_price);
        ivThumbnail = rootView.findViewById(R.id.item_thumbnail);
    }

    public void setTvNbFavorite(String txtValue) {
        this.tvNbFavorite.setText(StringUtils.toUpperFirstChar(txtValue));
    }

    public void setTvReductionNotice(String txtValue) {
        if(txtValue.isEmpty()){
            this.tvReductionNotice.setVisibility(View.GONE);
        }else{
            this.tvReductionNotice.setVisibility(View.VISIBLE);
            String reductionValue = "- " + txtValue + "%";
            this.tvReductionNotice.setText(reductionValue);
        }
    }

    public void setTvPrice(double reduction, double originalPrice) {
        if(CalculationUtils.isReduction(reduction, originalPrice)){
            this.tvPrice.setVisibility(View.VISIBLE);
            this.tvPrice.setText(this.context.getString(R.string.display_price,
                    NumberFormat.getIntegerInstance().format(originalPrice)));
            this.tvPrice.setPaintFlags(this.tvPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }else{
            this.tvPrice.setVisibility(View.INVISIBLE);
        }
    }

    public void setTvFinalPrice(double reductionPrice) {
        this.tvFinalPrice.setText(this.context.getString(R.string.display_price,
                NumberFormat.getIntegerInstance().format(reductionPrice)));
    }

    public void setIvThumbnail(String imageName) {
        GlideComponent.simpleGlideComponent(this.context,
                UrlRestApiConfig.IMAGE_RESOURCE_URL + imageName, ivThumbnail);
    }

}
