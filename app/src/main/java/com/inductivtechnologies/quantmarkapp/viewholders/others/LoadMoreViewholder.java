package com.inductivtechnologies.quantmarkapp.viewholders.others;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.inductivtechnologies.quantmarkapp.R;

/**
 * ProgressBarItemViewHolder : View holder du progress bar
 */
@SuppressWarnings({"FieldCanBeLocal", "unused"})
public class LoadMoreViewholder extends RecyclerView.ViewHolder{

    //Le context
    private final Context context;

    //Injections des vues
    private final TextView tvLoadMore;

    public LoadMoreViewholder(View view, Context context) {
        super(view);
        this.context = context;

        tvLoadMore = view.findViewById(R.id.tv_load_more);
    }
}
