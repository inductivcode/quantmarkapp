package com.inductivtechnologies.quantmarkapp.viewholders.home;

import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.components.GlideComponent;
import com.inductivtechnologies.quantmarkapp.configuration.UrlRestApiConfig;
import com.inductivtechnologies.quantmarkapp.utils.CalculationUtils;

import java.text.NumberFormat;

/**
 * PromotionOffersViewholder
 */

public class PromotionOffersViewholder extends RecyclerView.ViewHolder{

    //Le context
    private final Context context;

    //Injections des vues
    private TextView tvPrice;
    private TextView tvFinalPrice;
    private TextView tvReductionValue;

    private ImageView ivThumbnail;

    public PromotionOffersViewholder(View view, Context context) {
        super(view);
        this.context = context;

        //Les bues
        finfAllViews(view);
    }

    private void finfAllViews(View rootView){
        tvPrice = rootView.findViewById(R.id.item_price);
        tvFinalPrice = rootView.findViewById(R.id.item_final_price);
        tvReductionValue = rootView.findViewById(R.id.value_reduction);

        ivThumbnail = rootView.findViewById(R.id.thumbnail);
    }

    //Getters et setters

    public void setTvFinalPrice(double finalPrice) {
        this.tvFinalPrice.setText(this.context.getString(R.string.display_price,
                NumberFormat.getIntegerInstance().format(finalPrice)));
    }

    public void setTvReductionValue(String strValue) {
        String reductionValue = "- " + strValue + "%";
        this.tvReductionValue.setText(reductionValue);
    }

    public void setTvPrice(double reduction, double originalPrice) {
        if(CalculationUtils.isReduction(reduction, originalPrice)){
            this.tvPrice.setVisibility(View.VISIBLE);
            this.tvPrice.setText(this.context.getString(R.string.display_price,
                    NumberFormat.getIntegerInstance().format(originalPrice)));
            this.tvPrice.setPaintFlags(this.tvPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }else{
            this.tvPrice.setVisibility(View.INVISIBLE);
        }
    }

    public void setIvThumbnail(String imageName) {
        GlideComponent.glideComponentWithCustomPlaceholder(this.context,
                UrlRestApiConfig.IMAGE_RESOURCE_URL + imageName, 20,ivThumbnail);
    }

}
