package com.inductivtechnologies.quantmarkapp.viewholders.category;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.components.GlideComponent;
import com.inductivtechnologies.quantmarkapp.configuration.UrlRestApiConfig;
import com.inductivtechnologies.quantmarkapp.utils.StringUtils;

/**
 * SubCategoriesViewHolder
 */

public class SubCategoriesGridViewHolder extends RecyclerView.ViewHolder{

    //Le context
    private final Context context;

    //Injections des vues
    private TextView tvTitle;

    private ImageView thumbmail;

    public SubCategoriesGridViewHolder(View view, Context context) {
        super(view);
        this.context = context;

        allViews(view);
    }

    private void allViews(View root) {
        tvTitle = root.findViewById(R.id.scategory_grid_title);

        thumbmail = root.findViewById(R.id.subcategory_grid_thumbmail);
    }

    //Modifications

    public void seTvTitle(String strValue) {
        this.tvTitle.setText(StringUtils.toUpperFirstChar(strValue));
    }

    public void setThumbmail(String imageName) {
        GlideComponent.simpleGlideComponent(this.context,
                UrlRestApiConfig.IMAGE_RESOURCE_URL + imageName, this.thumbmail);
    }

}
