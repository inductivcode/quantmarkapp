package com.inductivtechnologies.quantmarkapp.viewholders.category;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.components.GlideComponent;
import com.inductivtechnologies.quantmarkapp.configuration.UrlRestApiConfig;
import com.inductivtechnologies.quantmarkapp.design.DisplayLargeNumber;
import com.inductivtechnologies.quantmarkapp.utils.StringUtils;

/**
 * CategoryListRecyclerviewViewHolder : Implémente  l'affichage des éléments du recyclerview
 */

public class CategoryListViewHolder extends RecyclerView.ViewHolder{

    //Le context
    private final Context context;

    //Injections des vues
    private TextView tvContentSubcategory;
    private TextView tvContentCategory;
    private ImageView thumbmail;

    public CategoryListViewHolder(View view, Context context) {
        super(view);
        this.context = context;

        allViews(view);
    }

    //Les vues
    private void allViews(View root) {
        tvContentSubcategory = root.findViewById(R.id.category_list_content_subcategory);
        tvContentCategory = root.findViewById(R.id.category_list_content_category);

        thumbmail = root.findViewById(R.id.category_list_thumbmail);
    }

    //Modifications

    public void setTvContentSubcategory(long value) {
        if(value > 1){
            this.tvContentSubcategory.setText(context.getResources()
                    .getString(R.string.display_nb_article_plural,
                            DisplayLargeNumber.smartLargeNumber(value)));
        }else{
            this.tvContentSubcategory.setText(context.getResources()
                    .getString(R.string.display_nb_article_sin,
                            DisplayLargeNumber.smartLargeNumber(value)));
        }
    }

    public void setTvContentCategory(String value) {
        this.tvContentCategory.setText(StringUtils.toUpperFirstChar(value));
    }

    public void setThumbmail(String imageName) {
        GlideComponent.simpleGlideComponent(this.context,
                UrlRestApiConfig.IMAGE_RESOURCE_URL + imageName, this.thumbmail);
    }
}
