package com.inductivtechnologies.quantmarkapp.viewholders.others;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.utils.StringUtils;

/**
 * SelectItemRecyclerviewHolder : Recuperation et affichage des composants du recyclerview
 */
@SuppressWarnings({"FieldCanBeLocal", "unused"})
public class SelectItemRecyclerviewHolder extends RecyclerView.ViewHolder{

    //Le contexte
    private final Context context;

    private final TextView tvContentItem;

    public SelectItemRecyclerviewHolder(View view, Context context) {
        super(view);
        this.context = context;

        //On récupère les vues
        this.tvContentItem = view.findViewById(R.id.tv_recyclerview_select_item_content);
    }

    //Setters et getters

    public void setTvContentItem(String content) {
        this.tvContentItem.setText(StringUtils.toUpperFirstChar(content));
    }
}
