package com.inductivtechnologies.quantmarkapp.viewholders.cart;

import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.components.GlideComponent;
import com.inductivtechnologies.quantmarkapp.configuration.UrlRestApiConfig;
import com.inductivtechnologies.quantmarkapp.utils.CalculationUtils;
import com.inductivtechnologies.quantmarkapp.utils.StringUtils;

import java.text.NumberFormat;

/**
 * CartViewHolder
 */

public class CartViewHolder extends RecyclerView.ViewHolder{

    private final Context context;

    private ImageView thumbmail;
    private TextView tvTitle;
    private TextView tvIssuerName;
    private TextView tvQuantity;
    private TextView tvPrice;
    private TextView tvFinalPrice;
    private TextView tvReduction;

    private TextView tvTotal;

    private ImageButton btnDetails;
    private ImageButton btnMakeOrder;
    private ImageButton btnDelete;

    public CartViewHolder(Context context, View view) {
        super(view);
        this.context = context;

        allViews(view);
    }

    private void allViews(View root) {
        thumbmail = root.findViewById(R.id.thumbmail);

        tvTitle = root.findViewById(R.id.title);
        tvIssuerName = root.findViewById(R.id.issuer_name);
        tvQuantity = root.findViewById(R.id.quantity);
        tvPrice = root.findViewById(R.id.price);
        tvFinalPrice = root.findViewById(R.id.final_price);
        tvReduction = root.findViewById(R.id.reduction_notice);

        tvTotal = root.findViewById(R.id.total);

        btnDetails = root.findViewById(R.id.details);
        btnMakeOrder = root.findViewById(R.id.make_order);
        btnDelete = root.findViewById(R.id.delete);
    }

    //Getters et setters

    public void setIvThumbnail(String imageName) {
        GlideComponent.simpleGlideComponent(this.context,
                UrlRestApiConfig.IMAGE_RESOURCE_URL + imageName, thumbmail);
    }

    public void setTvTitle(String title) {
        this.tvTitle.setText(StringUtils.toUpperFirstChar(title));
    }

    public void setTvIssuerName(String value) {
        this.tvIssuerName.setText(StringUtils.toUpperFirstChar(value));
    }

    public void setTvQuantity(int quantity) {
        this.tvQuantity.setText(StringUtils.getSpannedText(context.getString(R.string.display_price_quantity, quantity)));
    }

    public void setTvPrice(double reductionPrice, double originalPrice) {
        if(CalculationUtils.isReduction(reductionPrice, originalPrice)){
            this.tvPrice.setVisibility(View.VISIBLE);
            this.tvPrice.setText(this.context.getString(R.string.display_price,
                    NumberFormat.getIntegerInstance().format(originalPrice)));
            this.tvPrice.setPaintFlags(this.tvPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }else{
            this.tvPrice.setVisibility(View.INVISIBLE);
        }
    }

    public void setTvFinalPrice(double finalPrice) {
        this.tvFinalPrice.setText(this.context.getString(R.string.display_price,
                NumberFormat.getIntegerInstance().format(finalPrice)));
    }

    public void setTvReduction(String value) {
        if(value.isEmpty()){
            this.tvReduction.setVisibility(View.GONE);
        }else{
            String strReduction = value + "%";
            tvReduction.setVisibility(View.VISIBLE);
            tvReduction.setText(context.getResources().getString(R.string.display_reduction_notice, strReduction));
        }
    }

    public void setTvTotal(double total) {
        this.tvTotal.setText(this.context.getString(R.string.display_price_total,
                NumberFormat.getIntegerInstance().format(total)));
    }

    public ImageButton getBtnDetails() {
        return btnDetails;
    }

    public ImageButton getBtnMakeOrder() {
        return btnMakeOrder;
    }

    public ImageButton getBtnDelete() {
        return btnDelete;
    }
}
