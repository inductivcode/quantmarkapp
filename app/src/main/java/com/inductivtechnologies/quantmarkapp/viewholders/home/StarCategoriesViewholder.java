package com.inductivtechnologies.quantmarkapp.viewholders.home;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.components.GlideComponent;
import com.inductivtechnologies.quantmarkapp.configuration.UrlRestApiConfig;
import com.inductivtechnologies.quantmarkapp.design.DisplayLargeNumber;
import com.inductivtechnologies.quantmarkapp.utils.StringUtils;

/**
 * StarCategoriesViewholder
 */

public class StarCategoriesViewholder extends RecyclerView.ViewHolder{

    //Le context
    private final Context context;

    //Injections des vues
    private TextView tvContent;
    private TextView tvSubcontent;
    private ImageView thumbmail;

    public StarCategoriesViewholder(View view, Context context) {
        super(view);
        this.context = context;

        allViews(view);
    }

    //Les vues
    private void allViews(View root) {
        tvContent = root.findViewById(R.id.content);
        tvSubcontent = root.findViewById(R.id.subcontent);

        thumbmail = root.findViewById(R.id.thumbmail);
    }

    //Modifications

    public void setTvSubcontent(long value) {
        if(value > 1){
            this.tvSubcontent.setText(context.getResources()
                    .getString(R.string.display_nb_article_plural,
                            DisplayLargeNumber.smartLargeNumber(value)));
        }else{
            this.tvSubcontent.setText(context.getResources()
                    .getString(R.string.display_nb_article_sin,
                            DisplayLargeNumber.smartLargeNumber(value)));
        }
    }

    public void setTvcontent(String value) {
        this.tvContent.setText(StringUtils.toUpperFirstChar(value));
    }

    public void setThumbmail(String imageName) {
        GlideComponent.simpleGlideComponentWithoutPlaceholder(this.context,
                UrlRestApiConfig.IMAGE_RESOURCE_URL + imageName, this.thumbmail);
    }
}
