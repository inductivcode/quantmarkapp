package com.inductivtechnologies.quantmarkapp.viewholders.category;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.design.DisplayLargeNumber;
import com.inductivtechnologies.quantmarkapp.utils.StringUtils;

/**
 * SubCategoriesGroupViewHolder
 */

public class SubCategoriesGroupGridViewHolder extends RecyclerView.ViewHolder{

    //Le context
    private final Context context;

    //Injections des vues
    private TextView title;
    private TextView subTitle;

    public SubCategoriesGroupGridViewHolder(View view, Context context) {
        super(view);
        this.context = context;

        allViews(view);
    }

    private void allViews(View root) {
        title = root.findViewById(R.id.subcategory_group_title);
        subTitle = root.findViewById(R.id.subcategory_group_content);
    }

    //Getters et setters

    public void setTitle(String strValue) {
        this.title.setText(StringUtils.toUpperFirstChar(strValue));
    }

    public void setSubTitle(long value) {
        if(value > 1){
            this.subTitle.setText(context.getResources()
                    .getString(R.string.display_nb_article_plural,
                            DisplayLargeNumber.smartLargeNumber(value)));
        }else{
            this.subTitle.setText(context.getResources()
                    .getString(R.string.display_nb_article_sin,
                            DisplayLargeNumber.smartLargeNumber(value)));
        }
    }
}
