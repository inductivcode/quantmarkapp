package com.inductivtechnologies.quantmarkapp.viewholders.profile;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.components.GlideComponent;
import com.inductivtechnologies.quantmarkapp.configuration.UrlRestApiConfig;
import com.inductivtechnologies.quantmarkapp.utils.MomentUtils;
import com.inductivtechnologies.quantmarkapp.utils.StringUtils;

import java.text.NumberFormat;
import java.util.Date;

/**
 * OrdersItemViewholder
 */
public class OrdersItemViewholder extends RecyclerView.ViewHolder {

    //Le contexte
    private final Context context;

    //Injections des vues

    private TextView dateOrder;
    private TextView titleOrder;
    private TextView issuerNameOrder;
    private TextView priceOrder;
    private TextView codeOrder;

    private ImageView thumbmailOrder;

    public OrdersItemViewholder(View view, Context context) {
        super(view);
        this.context = context;

        allViews(view);
    }

    //Les méthodes

    private void allViews(View rootView){
        dateOrder = rootView.findViewById(R.id.date_order);
        titleOrder = rootView.findViewById(R.id.title_order);
        issuerNameOrder = rootView.findViewById(R.id.issuer_name_order);
        priceOrder = rootView.findViewById(R.id.price_order);
        codeOrder = rootView.findViewById(R.id.code_order);

        thumbmailOrder = rootView.findViewById(R.id.order_thumbmail);
    }

    public void setTitleOrder(String txtValue) {
        this.titleOrder.setText(StringUtils.toUpperFirstChar(txtValue));
    }

    public void setIssuerNameOrder(String txtValue) {
        this.issuerNameOrder.setText(StringUtils.toUpperFirstChar(txtValue));
    }

    public void setPriceOrder(double price) {
        this.priceOrder.setText(this.context.getString(R.string.display_price,
                NumberFormat.getIntegerInstance().format(price)));
    }

    public void setCodeOrder(String txtValue) {
        this.codeOrder.setText(StringUtils.toUpperFirstChar(txtValue));
    }

    public void setThumbmailOrder(String imageName) {
        GlideComponent.simpleGlideComponent(this.context,
                UrlRestApiConfig.IMAGE_RESOURCE_URL + imageName, thumbmailOrder);
    }

    public void setDateOrder(Date date) {
        this.dateOrder.setText(MomentUtils.getRelativeDate(date));
    }
}
