package com.inductivtechnologies.quantmarkapp.viewholders.search;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.inductivtechnologies.quantmarkapp.R;

/**
 * SearchItemNoticeViewholder
 */
@SuppressWarnings({"FieldCanBeLocal", "unused"})
public class SearchItemNoticeViewholder extends RecyclerView.ViewHolder{

    //Le contexte
    private final Context context;

    private TextView content;

    public SearchItemNoticeViewholder(View view, Context context) {
        super(view);
        this.context = context;

        allViews(view);
    }

    //Les méthodes

    private void allViews(View rootView){
        content = rootView.findViewById(R.id.searchvalue);
    }

    public void setContent(String value) {
        this.content.setText(value);
    }
}
