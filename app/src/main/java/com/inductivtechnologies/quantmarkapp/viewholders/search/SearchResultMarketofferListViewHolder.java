package com.inductivtechnologies.quantmarkapp.viewholders.search;

import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.components.GlideComponent;
import com.inductivtechnologies.quantmarkapp.configuration.UrlRestApiConfig;
import com.inductivtechnologies.quantmarkapp.design.DisplayLargeNumber;
import com.inductivtechnologies.quantmarkapp.utils.CalculationUtils;
import com.inductivtechnologies.quantmarkapp.utils.StringUtils;

import java.text.NumberFormat;

/**
 * SearchResultMarketofferListViewHolder
 */
public class SearchResultMarketofferListViewHolder extends RecyclerView.ViewHolder{

    //Le context
    private final Context context;

    //Injections des vues
    private TextView tvPrice;
    private TextView tvFinalPrice;
    private TextView tvNbOrder;
    private TextView tvCode;
    private TextView tvIssuer;
    private TextView tvTitle;
    private ImageView ivThumbnail;

    private TextView tvReductionNotice;

    public SearchResultMarketofferListViewHolder(View view, Context context) {
        super(view);
        this.context = context;

        //Les bues
        finfAllViews(view);
    }

    private void finfAllViews(View rootView){
        tvPrice = rootView.findViewById(R.id.item_price);
        tvFinalPrice = rootView.findViewById(R.id.item_final_price);
        tvNbOrder = rootView.findViewById(R.id.nb_order);
        tvReductionNotice = rootView.findViewById(R.id.value_reduction);
        tvCode = rootView.findViewById(R.id.code);

        tvIssuer = rootView.findViewById(R.id.issuer);
        tvTitle = rootView.findViewById(R.id.title);

        ivThumbnail = rootView.findViewById(R.id.item_thumbnail);
    }

    //Getters et setters

    public void setTvFinalPrice(double finalPrice) {
        this.tvFinalPrice.setText(this.context.getString(R.string.display_price,
                NumberFormat.getIntegerInstance().format(finalPrice)));
    }

    public void setTvPrice(double reduction, double originalPrice) {
        if(CalculationUtils.isReduction(reduction, originalPrice)){
            this.tvPrice.setVisibility(View.VISIBLE);
            this.tvPrice.setText(this.context.getString(R.string.display_price,
                    NumberFormat.getIntegerInstance().format(originalPrice)));
            this.tvPrice.setPaintFlags(this.tvPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }else{
            this.tvPrice.setVisibility(View.INVISIBLE);
        }
    }

    public void setIvThumbnail(String imageName) {
        GlideComponent.simpleGlideComponent(this.context,
                UrlRestApiConfig.IMAGE_RESOURCE_URL + imageName, ivThumbnail);
    }

    public void setTvReductionNotice(String txtValue) {
        if(txtValue.isEmpty()){
            this.tvReductionNotice.setVisibility(View.GONE);
        }else{
            this.tvReductionNotice.setVisibility(View.VISIBLE);
            String reductionValue = "- " + txtValue + "%";
            this.tvReductionNotice.setText(reductionValue);
        }
    }

    public void setTvNbOrder(long nbOrder) {
        this.tvNbOrder.setText(DisplayLargeNumber.smartLargeNumber(nbOrder));
    }

    public void setTvCode(String code) {
        this.tvCode.setText(code);
    }

    public void setTvIssuer(String issuerName) {
        this.tvIssuer.setText(StringUtils.toUpperFirstChar(issuerName));
    }

    public void setTvTitle(String title) {
        this.tvTitle.setText(StringUtils.toUpperFirstChar(title));
    }

}
