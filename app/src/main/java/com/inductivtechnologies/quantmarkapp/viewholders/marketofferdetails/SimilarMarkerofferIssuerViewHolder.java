package com.inductivtechnologies.quantmarkapp.viewholders.marketofferdetails;

import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.components.GlideComponent;
import com.inductivtechnologies.quantmarkapp.configuration.UrlRestApiConfig;
import com.inductivtechnologies.quantmarkapp.utils.CalculationUtils;

import java.text.NumberFormat;

/**
 * SimilarOffersRecyclerviewViewHolder : Implémente l'affichage des éléments du recyclerview
 */
public class SimilarMarkerofferIssuerViewHolder extends RecyclerView.ViewHolder{

    //Le context
    private final Context context;

    private TextView tvPrice;
    private TextView tvFinalPrice;
    private TextView tvReductionNotice;

    private ImageView ivThumbnail;

    public SimilarMarkerofferIssuerViewHolder(View view, Context context) {
        super(view);
        this.context = context;

        //Les vues
        findAllViews(view);
    }

    private void findAllViews(View view){
        tvPrice = view.findViewById(R.id.similar_marketoffer_item_price);
        tvFinalPrice = view.findViewById(R.id.similar_marketoffer_item_final_price);
        tvReductionNotice = view.findViewById(R.id.similar_marketoffer_value_reduction);

        ivThumbnail = view.findViewById(R.id.similar_marketoffer_item_thumbnail);
    }

    public void setTvReductionNotice(String txtValue) {
        if(txtValue.isEmpty()){
            this.tvReductionNotice.setVisibility(View.GONE);
        }else{
            this.tvReductionNotice.setVisibility(View.VISIBLE);
            String reductionValue = "- " + txtValue + "%";
            this.tvReductionNotice.setText(reductionValue);
        }
    }

    public void setTvPrice(double reduction, double originalPrice) {
        if(CalculationUtils.isReduction(reduction, originalPrice)){
            this.tvPrice.setVisibility(View.VISIBLE);
            this.tvPrice.setText(this.context.getString(R.string.display_price,
                    NumberFormat.getIntegerInstance().format(originalPrice)));
            this.tvPrice.setPaintFlags(this.tvPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }else{
            this.tvPrice.setVisibility(View.INVISIBLE);
        }
    }

    public void setTvFinalPrice(double finalPrice) {
        this.tvFinalPrice.setText(this.context.getString(R.string.display_price,
                NumberFormat.getIntegerInstance().format(finalPrice)));
    }

    public void setIvThumbnail(String imageName) {
        GlideComponent.simpleGlideComponent(this.context,
                UrlRestApiConfig.IMAGE_RESOURCE_URL + imageName, ivThumbnail);
    }
}
