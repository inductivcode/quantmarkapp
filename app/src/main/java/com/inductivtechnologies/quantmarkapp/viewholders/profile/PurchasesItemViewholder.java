package com.inductivtechnologies.quantmarkapp.viewholders.profile;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.components.GlideComponent;
import com.inductivtechnologies.quantmarkapp.configuration.UrlRestApiConfig;
import com.inductivtechnologies.quantmarkapp.utils.MomentUtils;
import com.inductivtechnologies.quantmarkapp.utils.StringUtils;

import java.text.NumberFormat;
import java.util.Date;

/**
 * PurchasesItemViewholder : Affichage des achats
 */
public class PurchasesItemViewholder extends RecyclerView.ViewHolder{

    //Le contexte
    private final Context context;

    //Injections des vues

    private TextView datePurchase;
    private TextView titlePurchase;
    private TextView issuerNamePurchase;
    private TextView pricePurchase;
    private TextView codePurchase;

    private ImageView thumbmailPurchase;

    public PurchasesItemViewholder(View view, Context context) {
        super(view);
        this.context = context;

        allViews(view);
    }

    //Les méthodes

    private void allViews(View rootView){
        datePurchase = rootView.findViewById(R.id.date_purchase);
        titlePurchase = rootView.findViewById(R.id.title_purchase);
        issuerNamePurchase = rootView.findViewById(R.id.issuer_name_purchase);
        pricePurchase = rootView.findViewById(R.id.price_purchase);
        codePurchase = rootView.findViewById(R.id.code_purchase);

        thumbmailPurchase = rootView.findViewById(R.id.purchase_thumbmail);
    }

    //Setters et Getters

    public void setTitlePurchase(String txtValue) {
        this.titlePurchase.setText(StringUtils.toUpperFirstChar(txtValue));
    }

    public void setIssuerNamePurchase(String txtValue) {
        this.issuerNamePurchase.setText(StringUtils.toUpperFirstChar(txtValue));
    }

    public void setPricePurchase(double price) {
        this.pricePurchase.setText(this.context.getString(R.string.display_price,
                NumberFormat.getIntegerInstance().format(price)));
    }

    public void setCodePurchase(String txtValue) {
        this.codePurchase.setText(StringUtils.toUpperFirstChar(txtValue));
    }

    public void setThumbmailPurchase(String imageName) {
        GlideComponent.simpleGlideComponent(this.context,
                UrlRestApiConfig.IMAGE_RESOURCE_URL + imageName, thumbmailPurchase);
    }

    public void setDatePurchase(Date date) {
        this.datePurchase.setText(MomentUtils.getRelativeDate(date));
    }
}
