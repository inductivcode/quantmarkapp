package com.inductivtechnologies.quantmarkapp.viewholders.search;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.View;
import android.widget.TextView;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.design.DisplayLargeNumber;
import com.inductivtechnologies.quantmarkapp.utils.DesignUtils;
import com.inductivtechnologies.quantmarkapp.utils.StringUtils;

/**
 * SearchItemResult
 */
public class SearchItemResultViewholder extends RecyclerView.ViewHolder{

    //Le contexte
    private final Context context;

    private TextView title;
    private TextView countValue;

    public SearchItemResultViewholder(View view, Context context) {
        super(view);
        this.context = context;

        allViews(view);
    }

    //Les méthodes

    private void allViews(View rootView){
        title = rootView.findViewById(R.id.title);
        countValue = rootView.findViewById(R.id.count_content);
    }

    public void setTitle(String value, String searchValue) {
        Spannable spannable = new SpannableString(StringUtils.toUpperFirstChar(value));
        DesignUtils.setColorForPath(spannable, searchValue, Color.parseColor("#0277BD"));

        this.title.setText(spannable);
    }

    public void setCountValue(long count) {
        if(count > 1){
            this.countValue.setText(context
                    .getResources().getString(R.string.display_nb_article_plural,
                            DisplayLargeNumber.smartLargeNumber(count)));
        }else{
            this.countValue.setText(context
                    .getResources().getString(R.string.display_nb_article_sin,
                            DisplayLargeNumber.smartLargeNumber(count)));
        }
    }
}
