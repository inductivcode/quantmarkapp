package com.inductivtechnologies.quantmarkapp.viewholders.search;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.flexbox.FlexboxLayout;
import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.activities.search.SearchActivity;
import com.inductivtechnologies.quantmarkapp.activities.search.SearchResultActivity;
import com.inductivtechnologies.quantmarkapp.configuration.StringConfig;
import com.inductivtechnologies.quantmarkapp.models.entities.SearchTrace;
import com.inductivtechnologies.quantmarkapp.store.repositories.SearchTraceRepository;
import com.inductivtechnologies.quantmarkapp.utils.StringUtils;

import java.util.List;

/**
 * SearchItemContainerHistoryViewHolder
 */
public class SearchItemContainerHistoryViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    //Le contexte
    private final Context context;

    private FlexboxLayout containerSearchHistories;
    private TextView tvClearSearchHistory;

    private List<SearchTrace> searchHistories;
    private final SearchTraceRepository searchTraceRepo;

    public SearchItemContainerHistoryViewHolder(View itemView, Context context) {
        super(itemView);
        this.context = context;
        this.searchTraceRepo = new SearchTraceRepository(context);

        allViews(itemView);
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.clear_search_history :
                removePanelHistorySearch();
                break;
            default:

                int position = containerSearchHistories.indexOfChild(view);
                SearchTrace trace = this.searchHistories.get(position);

                //On lanec l'activité pour afficher les résultats
                Intent intent = new Intent(context, SearchResultActivity.class);
                //On ajoute les données
                intent.putExtra(StringConfig.INTENT_SEARCH_NB_ITEM, trace.getNbItems());
                intent.putExtra(StringConfig.INTENT_SEARCH_ID_HEX, trace.getIdHex());
                intent.putExtra(StringConfig.INTENT_SEARCH_LABEL, trace.getValue());
                intent.putExtra(StringConfig.INTENT_SEARCH_FILTER, trace.getSearchFilter());

                context.startActivity(intent);
        }
    }

    //Toutes les vues
    private void allViews(View root) {
        containerSearchHistories = root.findViewById(R.id.content_search_histories);
        tvClearSearchHistory = root.findViewById(R.id.clear_search_history);
    }

    private void removePanelHistorySearch(){
        //On vide la table historique de recherche dans la base de données
        searchTraceRepo.deleteAll();

        //Du côte de l'activité on vide l'historique
        //On retire le panel qui affiche l'historique
        //On notofie l'adaptateur
        ((SearchActivity) context).getSearchHistories().clear();
        ((SearchActivity) context).getSearchItems().remove(0);
        ((SearchActivity) context).getAdapter().notifyItemRemoved(
                ((SearchActivity) context).getSearchItems().size());

        searchHistories.clear();
        //On raffraichi la vue
        containerSearchHistories.removeAllViews();
    }

    private void addListenners(){
        int count = containerSearchHistories.getChildCount();
        for(int i = 0; i < count; i++){
            containerSearchHistories.getChildAt(i).setOnClickListener(this);
        }
        tvClearSearchHistory.setOnClickListener(this);
    }

    // Initialisation du recyclerview pour afficher l'historique de recherche
    public void initSearchistories(final List<SearchTrace> searchHistories) {
        //Historique 
        this.searchHistories = searchHistories;
        //On raffraichi la vue
        containerSearchHistories.removeAllViews();

        for(SearchTrace trace : searchHistories){
            LinearLayout layout = (LinearLayout) LayoutInflater.from(this.context)
                    .inflate(R.layout.search_history_item, containerSearchHistories, false);
            //Le contenu
            TextView tvContent = layout.findViewById(R.id.content);
            tvContent.setText(StringUtils.toUpperFirstChar(trace.getValue()));
            //on ajoute la vue
            containerSearchHistories.addView(layout);
        }
        //On ajoute les écouteurs
        addListenners();
    }
}
