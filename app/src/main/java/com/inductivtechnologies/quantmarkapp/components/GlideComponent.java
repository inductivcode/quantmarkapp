package com.inductivtechnologies.quantmarkapp.components;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.widget.ImageView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.design.CircleTransform;

/**
 * GlideComponent : Téléchargement des images depuis le serveur
 */
@SuppressWarnings("SameParameterValue")
public class GlideComponent {

    //Pour les messages dans le Android Log
    private static final String TAG = GlideComponent.class.getSimpleName();

    private static final RequestListener<String, GlideDrawable> glideListener = new RequestListener<String, GlideDrawable>() {
        @Override
        public boolean onException(Exception exception, String model,
                                   Target<GlideDrawable> target, boolean isFirstResource) {
            Log.e(TAG, "Le composant Glide n'a pas pu télécharger " + model);
            Log.e(TAG, exception.getMessage());
            return false;
        }

        @Override
        public boolean onResourceReady(GlideDrawable resource, String model,
                                       Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
            Log.i(TAG, "La ressource " + model + " a été téléchargée");
            return false;
        }
    };

    public static void simpleGlideComponent(Context context, String imagePath,
                                            ImageView imageviewTarget){

        TextDrawable textDrawable = TextDrawable.builder()
                .beginConfig()
                .useFont(Typeface.DEFAULT)
                .textColor(Color.parseColor("#E0E0E0"))
                .fontSize(30)
                .toUpperCase()
                .bold()
                .endConfig()
                .buildRect(context.getResources().getString(R.string.app_name),
                        Color.parseColor("#ffffff"));

        Glide.with(context)
                .load(imagePath)
                .listener(glideListener)
                .placeholder(textDrawable)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageviewTarget);
    }

    public static void glideComponentWithCustomPlaceholder(Context context, String imagePath, int sizePlaceHolder,
                                            ImageView imageviewTarget){

        TextDrawable textDrawable = TextDrawable.builder()
                .beginConfig()
                .useFont(Typeface.DEFAULT)
                .textColor(Color.parseColor("#E0E0E0"))
                .fontSize(sizePlaceHolder)
                .toUpperCase()
                .bold()
                .endConfig()
                .buildRect(context.getResources().getString(R.string.app_name),
                        Color.parseColor("#ffffff"));

        Glide.with(context)
                .load(imagePath)
                .listener(glideListener)
                .placeholder(textDrawable)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageviewTarget);
    }

    public static void simpleGlideComponentWithoutPlaceholder(Context context, String imagePath,
                                                              ImageView imageviewTarget){
        Glide.with(context)
                .load(imagePath)
                .listener(glideListener)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageviewTarget);
    }

    public static void initThumbmailProfile(final Context context, String imagePath,
                                           final ImageView imageviewTarget){
        Glide.with(context)
                .load(imagePath)
                .listener(glideListener)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .transform(new CircleTransform(context))
                .fitCenter()
                .into(imageviewTarget);
    }
}
