package com.inductivtechnologies.quantmarkapp.components;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.integration.volley.VolleyUrlLoader;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.module.GlideModule;
import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.application.ssl.ExtHttpClientStack;
import com.inductivtechnologies.quantmarkapp.application.ssl.SslHttpClient;
import com.inductivtechnologies.quantmarkapp.configuration.StringConfig;

import java.io.InputStream;

/**
 * SslVolleyGlideModule
 */
public class SslVolleyGlideModule implements GlideModule {

    @Override
    public void applyOptions(Context context, GlideBuilder builder) {

    }

    @Override
    public void registerComponents(Context context, Glide glide) {

        RequestQueue queue = Volley.newRequestQueue(context,
                new ExtHttpClientStack(new SslHttpClient(context.getResources()
                        .openRawResource(R.raw.keystore_quantmark_cm_com), StringConfig.SSL_KEYSTORE_PASSWORD)));

        glide.register(GlideUrl.class, InputStream.class, new VolleyUrlLoader.Factory(queue));
    }
}
