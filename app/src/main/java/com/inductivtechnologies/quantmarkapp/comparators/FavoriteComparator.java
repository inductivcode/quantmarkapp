package com.inductivtechnologies.quantmarkapp.comparators;

import com.inductivtechnologies.quantmarkapp.models.entities.Favorite;

import java.util.Comparator;

/**
 * FavoriteComparator
 */
public class FavoriteComparator implements Comparator<Favorite> {

    @Override
    public int compare(Favorite o1, Favorite o2) {
        return o1.getCreationDate() > o2.getCreationDate() ? -1 : (o1.getCreationDate() < o2.getCreationDate()) ? 1 : 0;
    }

}
