package com.inductivtechnologies.quantmarkapp.comparators;

import com.inductivtechnologies.quantmarkapp.models.entities.SearchTrace;

import java.util.Comparator;

/**
 * SearchTraceComparator
 */
public class SearchTraceComparator implements Comparator<SearchTrace> {

    @Override
    public int compare(SearchTrace o1, SearchTrace o2) {
        return o1.getCreationDate() > o2.getCreationDate() ? -1 : (o1.getCreationDate() < o2.getCreationDate()) ? 1 : 0;
    }
}
