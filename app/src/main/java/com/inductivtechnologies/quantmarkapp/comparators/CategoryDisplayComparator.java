package com.inductivtechnologies.quantmarkapp.comparators;

import com.inductivtechnologies.quantmarkapp.models.display.CategoryDisplay;

import java.util.Comparator;

/**
 * CategoryDisplayComparator
 */
public class CategoryDisplayComparator implements Comparator<CategoryDisplay> {

    @Override
    public int compare(CategoryDisplay o1, CategoryDisplay o2) {
        return o1.getNbOfMarketOffers() > o2.getNbOfMarketOffers() ? -1 : (o1.getNbOfMarketOffers() < o2.getNbOfMarketOffers()) ? 1 : 0;
    }

}
