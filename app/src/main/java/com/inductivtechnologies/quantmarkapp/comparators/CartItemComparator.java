package com.inductivtechnologies.quantmarkapp.comparators;

import com.inductivtechnologies.quantmarkapp.models.entities.CartItem;

import java.util.Comparator;

/**
 * CartItemComparator : Permet de trier les CartItem par date
 */
public class CartItemComparator implements Comparator<CartItem> {

    @Override
    public int compare(CartItem o1, CartItem o2) {
        return o1.getCreationDate() > o2.getCreationDate() ? -1 : (o1.getCreationDate() < o2.getCreationDate()) ? 1 : 0;
    }

}
