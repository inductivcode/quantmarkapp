package com.inductivtechnologies.quantmarkapp.comparators;

import com.inductivtechnologies.quantmarkapp.models.display.BuyDisplay;
import com.inductivtechnologies.quantmarkapp.models.utils.BuyDisplayCustom;

import java.util.Comparator;

/**
 * BuyComparator : Permet de trier les achats ou les commandes par date
 */
@SuppressWarnings({"WeakerAccess", "unused"})
public class BuyComparator implements Comparator<BuyDisplayCustom>{

    @Override
    public int compare(BuyDisplayCustom b1, BuyDisplayCustom b2) {
        return ((BuyDisplay) b1).getDate().getTime() > ((BuyDisplay) b2).getDate().getTime() ? -1 :
                (((BuyDisplay) b1).getDate().getTime() < ((BuyDisplay) b2).getDate().getTime()) ? 1 : 0;
    }

}
