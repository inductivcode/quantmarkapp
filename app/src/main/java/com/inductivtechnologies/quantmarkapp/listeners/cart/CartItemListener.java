package com.inductivtechnologies.quantmarkapp.listeners.cart;

import android.content.Intent;
import android.view.View;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.activities.cart.ResumeOrdersActivity;
import com.inductivtechnologies.quantmarkapp.activities.main.MainActivity;
import com.inductivtechnologies.quantmarkapp.activities.marketofferdetails.MarketofferDetails;
import com.inductivtechnologies.quantmarkapp.activities.account.UserLoginActivity;
import com.inductivtechnologies.quantmarkapp.adapters.cart.CartItemAdapter;
import com.inductivtechnologies.quantmarkapp.bottomsheets.BottomsheetDeliveryMethod;
import com.inductivtechnologies.quantmarkapp.configuration.StringConfig;
import com.inductivtechnologies.quantmarkapp.fragments.cart.CartFragment;
import com.inductivtechnologies.quantmarkapp.models.entities.CartItem;
import com.inductivtechnologies.quantmarkapp.store.repositories.CartItemRepository;
import com.inductivtechnologies.quantmarkapp.store.session.SessionManager;
import com.inductivtechnologies.quantmarkapp.utils.DesignUtils;
import com.inductivtechnologies.quantmarkapp.viewholders.cart.CartViewHolder;

import java.util.List;

/**
 * CartItemListener : Pour gérer les évènements de click sur les items du paniers
 */
@SuppressWarnings("CanBeFinal")
public class CartItemListener implements View.OnClickListener{

    //Le fragment
    private final CartFragment fragment;

    //La position de l'élément actuel
    private int position;
    //Les données
    private List<CartItem> cartitems;
    //Adaptateur
    private final CartItemAdapter adapter;
    //Le holder
    private final CartViewHolder viewHolder;

    // Repository
    private final CartItemRepository cartItemRepo;

    // Session
    private final SessionManager sessionManager;

    //Objet courrant
    private final CartItem cartItem;

    //Méthode de livraison
    private final BottomsheetDeliveryMethod bstDeliveryMethod;

    public CartItemListener(CartFragment fragment, CartItemAdapter adapter, List<CartItem> cartitems, int position, CartViewHolder viewHolder) {
        this.fragment = fragment;
        this.cartitems = cartitems;
        this.adapter = adapter;
        this.position = position;
        this.viewHolder = viewHolder;
        this.cartItem = cartitems.get(position);

        this.cartItemRepo = new CartItemRepository(fragment.getActivity());
        this.bstDeliveryMethod =  new BottomsheetDeliveryMethod(fragment.getActivity());
        this.sessionManager = new SessionManager(fragment.getActivity());

        setListener();
    }

    private void setListener() {
        viewHolder.getBtnDetails().setOnClickListener(this);
        viewHolder.getBtnMakeOrder().setOnClickListener(this);
        viewHolder.getBtnDelete().setOnClickListener(this);

        bstDeliveryMethod.getmBtnActionSave().setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                int deliveryMethod = bstDeliveryMethod.getDeliveryMethod();
                //On ferme
                bstDeliveryMethod.hide();

                if(deliveryMethod != -1){
                    Intent intent = new Intent(fragment.getActivity(), ResumeOrdersActivity.class);
                    intent.putExtra(StringConfig.INTENT_DELIVERY_METHOD, deliveryMethod);
                    intent.putExtra(StringConfig.INTENT_MARKETOFFER_HEX_ID, cartItem.getIdHexMarketOffer());
                    fragment.startActivity(intent);
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.details :
                //Détails de l'article
                Intent intentMarketOffer = new Intent(fragment.getActivity(), MarketofferDetails.class);
                intentMarketOffer.putExtra(StringConfig.INTENT_MARKETOFFER_HEX_ID, cartItem.getIdHexMarketOffer());
                fragment.startActivity(intentMarketOffer);

                break;
            case R.id.make_order :
                //Faire la commande juste pour l'article courant
                if(sessionManager.isLoggedIn()){
                    bstDeliveryMethod.show();
                }else{
                    //Connexion
                    Intent intent = new Intent(fragment.getActivity(), UserLoginActivity.class);
                    fragment.startActivity(intent);
                }

                break;
            case R.id.delete :
                //On le supprime dans la base de données interne
                long result = cartItemRepo.deleteCartitem(cartItem);
                if(result > 0){
                    cartitems.remove(position);
                    adapter.notifyItemRemoved(position);
                    adapter.notifyItemRangeChanged(position, cartitems.size());

                    fragment.setTvTotalCart();
                    fragment.getBtnOrder().setText(fragment.getContext().getResources()
                            .getString(R.string.text_display_label_order, cartitems.size()));

                    //Et on met à jour le badge
                    ((MainActivity) fragment.getActivity()).setBagde(cartitems.size());

                    //Si on supprime le dernier élémnet le panier est vide
                    //Alors on affiche le message panier vide
                    if(cartitems.size() == 0){
                        fragment.getRecyclerView().setVisibility(View.GONE);
                        fragment.llContainerEmptyCart.setVisibility(View.VISIBLE);
                    }

                }else{
                    DesignUtils.makeSnackbar(fragment.getActivity().getString(R.string.unknow_error_message),
                            ((MainActivity) fragment.getActivity()).getCoordinatorLayout(), fragment.getContext());
                }
                break;
        }
    }
}
