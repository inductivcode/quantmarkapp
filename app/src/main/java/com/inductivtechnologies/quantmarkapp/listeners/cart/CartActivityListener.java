package com.inductivtechnologies.quantmarkapp.listeners.cart;

import android.content.Intent;
import android.view.View;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.activities.cart.CartActivity;
import com.inductivtechnologies.quantmarkapp.activities.cart.ResumeOrdersActivity;
import com.inductivtechnologies.quantmarkapp.activities.marketofferdetails.MarketofferDetails;
import com.inductivtechnologies.quantmarkapp.activities.account.UserLoginActivity;
import com.inductivtechnologies.quantmarkapp.adapters.cart.CartActivityAdapter;
import com.inductivtechnologies.quantmarkapp.bottomsheets.BottomsheetDeliveryMethod;
import com.inductivtechnologies.quantmarkapp.configuration.StringConfig;
import com.inductivtechnologies.quantmarkapp.models.entities.CartItem;
import com.inductivtechnologies.quantmarkapp.store.repositories.CartItemRepository;
import com.inductivtechnologies.quantmarkapp.store.session.SessionManager;
import com.inductivtechnologies.quantmarkapp.utils.DesignUtils;
import com.inductivtechnologies.quantmarkapp.viewholders.cart.CartViewHolder;

import java.util.List;

/**
 * CartActivityListener: Pour gérer les évènements de click sur les items du paniers
 */
public class CartActivityListener implements View.OnClickListener{

    //Le context
    private final CartActivity context;

    //La position de l'élément actuel
    private final int position;
    //Les données
    private final List<CartItem> cartitems;
    //Adaptateur
    private final CartActivityAdapter adapter;
    //Le holder
    private final CartViewHolder viewHolder;

    // Repository
    private final CartItemRepository cartItemRepo;

    // Session
    private final SessionManager sessionManager;

    //Objet courrant
    private final CartItem cartItem;

    //Méthode de livraison
    private final BottomsheetDeliveryMethod bstDeliveryMethod;

    public CartActivityListener(CartActivity context, CartActivityAdapter adapter,
                            List<CartItem> cartitems, int position, CartViewHolder viewHolder) {
        this.context = context;
        this.cartitems = cartitems;
        this.adapter = adapter;
        this.position = position;
        this.viewHolder = viewHolder;
        this.cartItem = cartitems.get(position);

        this.cartItemRepo = new CartItemRepository(context);
        this.bstDeliveryMethod =  new BottomsheetDeliveryMethod(context);
        this.sessionManager = new SessionManager(context);

        setListener();
    }

    private void setListener() {
        viewHolder.getBtnDetails().setOnClickListener(this);
        viewHolder.getBtnMakeOrder().setOnClickListener(this);
        viewHolder.getBtnDelete().setOnClickListener(this);

        bstDeliveryMethod.getmBtnActionSave().setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                int deliveryMethod = bstDeliveryMethod.getDeliveryMethod();
                //On ferme
                bstDeliveryMethod.hide();

                if(deliveryMethod != -1){
                    Intent intent = new Intent(context, ResumeOrdersActivity.class);
                    intent.putExtra(StringConfig.INTENT_DELIVERY_METHOD, deliveryMethod);
                    intent.putExtra(StringConfig.INTENT_MARKETOFFER_HEX_ID, cartItem.getIdHexMarketOffer());
                    context.startActivity(intent);
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.details :
                //Détails de l'article
                Intent intentMarketOffer = new Intent(context, MarketofferDetails.class);
                intentMarketOffer.putExtra(StringConfig.INTENT_MARKETOFFER_HEX_ID, cartItem.getIdHexMarketOffer());
                context.startActivity(intentMarketOffer);

                break;
            case R.id.make_order :
                //Faire la commande juste pour l'article courant
                if(sessionManager.isLoggedIn()){
                    bstDeliveryMethod.show();
                }else{
                    //Connexion
                    Intent intent = new Intent(context, UserLoginActivity.class);
                    context.startActivity(intent);
                }

                break;
            case R.id.delete :
                //On le supprime dans la base de données interne
                long result = cartItemRepo.deleteCartitem(cartItem);
                if(result > 0){
                    cartitems.remove(position);
                    adapter.notifyItemRemoved(position);
                    adapter.notifyItemRangeChanged(position, cartitems.size());

                    context.setTvTotalCart();
                    context.getBtnOrder().setText(context.getResources()
                            .getString(R.string.text_display_label_order, cartitems.size()));

                    //Si on supprime le dernier élément le panier est vide
                    //Alors on affiche le message panier vide
                    if(cartitems.size() == 0){
                        context.getRecyclerView().setVisibility(View.GONE);
                        context.llContainerEmptyCart.setVisibility(View.VISIBLE);

                        context.getBtnClearCart().setVisibility(View.INVISIBLE);

                    }

                }else{
                    DesignUtils.makeSnackbar(context.getString(R.string.unknow_error_message),
                            context.getCoordinatorLayout(), context);
                }
                break;
        }
    }
}
