package com.inductivtechnologies.quantmarkapp.utils;

import android.content.Context;
import android.util.TypedValue;

import com.inductivtechnologies.quantmarkapp.configuration.NumberConfig;

/**
 * CalculationUtils : Quelques calculs
 */
public class CalculationUtils {

    // Retourne le prix réduit d'un produit si il y a une reduction
/*    public static double reducePrice(String reductionValue, double originalPrice){
        double reductionPrice = 0;

        if(!reductionValue.equals("")){
            reductionPrice = CalculationUtils.makeReductionPrice(Double.valueOf(reductionValue), originalPrice);
        }
        return reductionPrice;
    }*/

    //Retourne le prix réduit
//    private static double makeReductionPrice(double valueReduction, double originalPrice){
//        return originalPrice - ((originalPrice * valueReduction) / 100);
//    }

    //Pixel densité en pixel
    public static int dpToPx(Context context, float dp) {
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, context.getResources().getDisplayMetrics()));
    }

    //Calcule les frais de  livraison en fonction du montal total du panier
    public static double deliveryCost(double totalAmountOfCart) {
        double total = 0;

        if (totalAmountOfCart > 0.0 && totalAmountOfCart <= 10000.0)
            total = 1000.0;
        else if (totalAmountOfCart > 10000.0 && totalAmountOfCart <= 20000.0)
            total = 1500.0;
        else if (totalAmountOfCart > 20000.0 && totalAmountOfCart <= 30000.0)
            total = 2000.0;
        else if (totalAmountOfCart > 30000.0 && totalAmountOfCart <= 40000.0)
            total = 2500.0;
        else if (totalAmountOfCart > 40000.0 && totalAmountOfCart <= 50000.0)
            total = 3000.0;
        else if (totalAmountOfCart > 50000.0 && totalAmountOfCart <= 70000.0)
            total = 3500.0;
        else if (totalAmountOfCart > 70000.0 && totalAmountOfCart < 100000.0)
            total = 4000.0;
        else if (totalAmountOfCart >= 100000.0)
            total = 5000.0;

        return total;
    }

    //Converti le FCFA EN USD (ceci est une solution temporaire car le taux de change vari)
    public static double FcfaToEuro(double amount){
        return amount / NumberConfig.CURRENCIES_XAF_CHANGE;
    }

    public static boolean isReduction(double reduction, double originalPrice){
        return  reduction != originalPrice;
    }
}
