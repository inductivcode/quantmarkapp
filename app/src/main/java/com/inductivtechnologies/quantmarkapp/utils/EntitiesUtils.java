package com.inductivtechnologies.quantmarkapp.utils;

import com.inductivtechnologies.quantmarkapp.models.display.BuyDisplay;
import com.inductivtechnologies.quantmarkapp.models.display.CategoryDisplay;
import com.inductivtechnologies.quantmarkapp.models.display.MarketOfferDisplay;
import com.inductivtechnologies.quantmarkapp.models.entities.CartItem;
import com.inductivtechnologies.quantmarkapp.models.entities.Image;
import com.inductivtechnologies.quantmarkapp.models.entities.Term;

import org.bson.Document;
import org.bson.types.ObjectId;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * EntitiesUtils : Collection de méthodes utiles pour les entités
 */
@SuppressWarnings("SameParameterValue")
public class EntitiesUtils {

    // Crée un ObjectId à partir d'un objet JSON
    @SuppressWarnings("WeakerAccess")
    public static ObjectId getObjectIdByJSONObject(JSONObject jsonObject){
        ObjectId objectId = null;
        if(jsonObject != null){
            try{
                objectId = new ObjectId(jsonObject.getInt("timestamp"), jsonObject.getInt("machineIdentifier"),
                        (short)jsonObject.getInt("processIdentifier"), jsonObject.getInt("counter"));
            }catch (JSONException excJSON){
                excJSON.printStackTrace();
            }
        }
        return objectId;
    }

    // Représentation hexadécimale d'un objectId
    public static String getObjectIdHexByJSONObject(JSONObject jsonObject){
        return getObjectIdByJSONObject(jsonObject).toString();
    }

    // Crée une liste d'objets MarketOfferDisplay à partir d'un objet JSON Array
    public static List<MarketOfferDisplay> getMarketOffersDisplayByJSONArray(JSONArray jsonArray){
        List<MarketOfferDisplay> marketOffersDisplay = new ArrayList<>();
        if(jsonArray != null && jsonArray.length() != 0){
            try{
                for(int i = 0; i < jsonArray.length(); i++){
                    JSONObject jsonData = jsonArray.getJSONObject(i);

                    MarketOfferDisplay marketOfferDisplay = new MarketOfferDisplay(jsonData);
                    marketOffersDisplay.add(marketOfferDisplay);
                }
            }catch (JSONException excJSON){
                excJSON.printStackTrace();
            }
        }
        return marketOffersDisplay;
    }

    // Crée une liste d'objets CategoryDisplay à partir d'un objet JSON Array
    public static List<CategoryDisplay> getCategoryDisplayByJSONArray(JSONArray jsonArray){
        if(jsonArray != null && jsonArray.length() != 0){
            List<CategoryDisplay> categoriesDisplay = new ArrayList<>();

            try{
                for(int i = 0; i < jsonArray.length(); i++){
                    JSONObject jsonData = jsonArray.getJSONObject(i);
                    CategoryDisplay categoryDisplay = new CategoryDisplay(jsonData);
                    categoriesDisplay.add(categoryDisplay);
                }
            }catch (JSONException excJSON){
                excJSON.printStackTrace();
            }
            return categoriesDisplay;
        }
        return null;
    }

    // Crée un Document à partir d'un objet JSON
    public static Document getDocumentByJSONObject(JSONObject jsonObject){
        if(jsonObject != null){
            Map<String,Object> docMap = new HashMap<>();

            docMap.put("Couleur", jsonObject.optString("Couleur"));
            docMap.put("Taille", jsonObject.optString("Taille"));
            docMap.put("Marque", jsonObject.optString("Marque"));
            docMap.put("Size", jsonObject.optString("Size"));

            return new Document(docMap);
        }
        return null;
    }

    // Crée un tableau d'images à partir d'un objet JSON Array
    public static List<Image> getImagesByJSONArray(JSONArray jsonArray){
        if(jsonArray != null){
            List<Image> images = new ArrayList<>();
            try{
                for(int i = 0; i < jsonArray.length(); i++){
                    JSONObject jsonData = jsonArray.getJSONObject(i);
                    Image imageData = new Image(jsonData);
                    images.add(imageData);
                }
            }catch (JSONException excJSON){
                excJSON.printStackTrace();
            }
            return images;
        }
        return null;
    }

    // Crée une collection de String a partir d'un objet JSON Array
    public static List<String> getListOfStringByJSONArray(JSONArray jsonArray){
        if(jsonArray != null){
            List<String> strList = new ArrayList<>();
            try{
                for(int i = 0; i < jsonArray.length(); i++){
                    strList.add(jsonArray.getString(i));
                }
            }catch (JSONException excJSON){
                excJSON.printStackTrace();
            }
            return strList;
        }
        return null;
    }

    // Crée une liste d'objets Term à partir d'un objet JSON Array
    public static List<Term> getTermsByJSONArray(JSONArray jsonArray){
        if(jsonArray != null){
            List<Term> terms = new ArrayList<>();
            try{
                for(int i = 0; i < jsonArray.length(); i++){
                    JSONObject jsonData = jsonArray.getJSONObject(i);
                    Term term = new Term(jsonData);
                    terms.add(term);
                }
            }catch (JSONException excJSON){
                excJSON.printStackTrace();
            }
            return terms;
        }
        return null;
    }

    public static List<BuyDisplay> getBuyDisplayByJSONArray(JSONArray jsonArray){
        List<BuyDisplay> buysDisplay = new ArrayList<>();
        if(jsonArray != null && jsonArray.length() != 0){
            try{
                for(int i = 0; i < jsonArray.length(); i++){
                    JSONObject jsonData = jsonArray.getJSONObject(i);

                    BuyDisplay buyDisplay = new BuyDisplay(jsonData);
                    buysDisplay.add(buyDisplay);
                }
            }catch (JSONException excJSON){
                excJSON.printStackTrace();
            }
        }
        return buysDisplay;
    }

    //Vérifie si un document est completement vide ie si ses clés sont associées a des ojets vides
    public static boolean documentIsFullEmpty(Document document){
        int sizeDoc = document.size();
        int nbEmptyElement = 0;

        for(Map.Entry<String, Object> entry : document.entrySet()){
            String value = (String) entry.getValue();
            if(value == null || value.equals("")){
                nbEmptyElement ++;
            }
        }

        return (sizeDoc == nbEmptyElement);
    }

    // Vérifie si CartItem est dans la liste des éléments du panier
    public static int isContainCartItem(List<MarketOfferDisplay> marketOffers, CartItem cartitem){

        for(int i = 0; i < marketOffers.size(); i++){
            if(marketOffers.get(i).getMarketOffer().getIdHexMarketOffer().equals(cartitem.getIdHexMarketOffer())) return i;
        }

        return -1;
    }

    // Retourne le prix total en fonction de la quantité et de la réduction
/*    public static double getTotalPrice(double reduction, int quantity, double originalPrice, double delivered){
        if(reduction == 0){
            return (originalPrice * quantity) + delivered;
        }else{
            return (reduction * quantity) + delivered;
        }
    }*/

    // Retourne le prix total d'un élément du panier
    public static double getTotalPriceOfCartitem(int quantity, double price, double delivered){
        return (price * quantity) + delivered;
    }

}
