package com.inductivtechnologies.quantmarkapp.utils;

import com.inductivtechnologies.quantmarkapp.configuration.StringConfig;
import com.inductivtechnologies.quantmarkapp.models.display.BuyDisplay;
import com.inductivtechnologies.quantmarkapp.models.entities.CartItem;
import com.inductivtechnologies.quantmarkapp.models.entities.Category;
import com.inductivtechnologies.quantmarkapp.models.entities.Product;
import com.inductivtechnologies.quantmarkapp.models.entities.ProductType;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * LanguageDisolay
 */
@SuppressWarnings("WeakerAccess")
public class LanguageDisolay {

    public static final String FIELD_TITLE = "FIELD_TITLE";
    public static final String FIELD_DESCRIPTION = "FIELD_DESCRIPTION";
    public static final String FIELD_DETAIL = "FIELD_DETAIL";
    public static final String FIELD_COLOR = "FIELD_COLOR";
    public static final String FIELD_GROUP = "FIELD_GROUP";

    @SuppressWarnings("CanBeFinal")
    private static Map<String, Object> languageCollection = new HashMap<>();

    public static Map<String, Object> displayProduct(Product product){
        languageCollection.clear();

        switch(Locale.getDefault().getLanguage()){
            case StringConfig.DEFAULT_FR :
                languageCollection.put(FIELD_TITLE, product.getTitleFr());
                languageCollection.put(FIELD_DESCRIPTION, product.getDescriptionFr());
                languageCollection.put(FIELD_DETAIL, product.getDetailFr());
                languageCollection.put(FIELD_COLOR, StringConfig.STR_COLOR_FR);
                break;
            case StringConfig.DEFAULT_EN :
                languageCollection.put(FIELD_TITLE, product.getTitleEn());
                languageCollection.put(FIELD_DESCRIPTION, product.getDescriptionEn());
                languageCollection.put(FIELD_DETAIL, product.getDetailEn());
                languageCollection.put(FIELD_COLOR, StringConfig.STR_COLOR_EN);
                break;
            default :
                languageCollection.put(FIELD_TITLE, product.getTitleFr());
                languageCollection.put(FIELD_DESCRIPTION, product.getDescriptionFr());
                languageCollection.put(FIELD_DETAIL, product.getDetailFr());
                languageCollection.put(FIELD_COLOR, StringConfig.STR_COLOR_FR);
                break;
        }

        return languageCollection;
    }

    public static Map<String, Object> displayCartItem(CartItem cartitem){
        languageCollection.clear();

        switch(Locale.getDefault().getLanguage()){
            case StringConfig.DEFAULT_FR :
                languageCollection.put(FIELD_TITLE, cartitem.getTitleFr());
                break;
            case StringConfig.DEFAULT_EN :
                languageCollection.put(FIELD_TITLE, cartitem.getTitleEn());
                break;
            default :
                languageCollection.put(FIELD_TITLE, cartitem.getTitleFr());
                break;
        }

        return languageCollection;
    }

    public static Map<String, Object> displayBuy(BuyDisplay buy){
        languageCollection.clear();

        switch(Locale.getDefault().getLanguage()){
            case StringConfig.DEFAULT_FR :
                languageCollection.put(FIELD_TITLE, buy.getTitleFr());
                break;
            case StringConfig.DEFAULT_EN :
                languageCollection.put(FIELD_TITLE, buy.getTitleEn());
                break;
            default :
                languageCollection.put(FIELD_TITLE, buy.getTitleFr());
                break;
        }

        return languageCollection;
    }

    public static Map<String, Object> displayCategory(Category category){
        languageCollection.clear();

        switch(Locale.getDefault().getLanguage()){
            case StringConfig.DEFAULT_FR :
                languageCollection.put(FIELD_TITLE, category.getTitleFr());
                languageCollection.put(FIELD_DESCRIPTION, category.getDescriptionFr());
                break;
            case StringConfig.DEFAULT_EN :
                languageCollection.put(FIELD_TITLE, category.getTitleEn());
                languageCollection.put(FIELD_DESCRIPTION, category.getDescriptionFr());
                break;
            default :
                languageCollection.put(FIELD_TITLE, category.getTitleFr());
                languageCollection.put(FIELD_DESCRIPTION, category.getDescriptionFr());
                break;
        }

        return languageCollection;
    }

    public static Map<String, Object> displaySubCategory(ProductType subcategory){
        languageCollection.clear();

        switch(Locale.getDefault().getLanguage()){
            case StringConfig.DEFAULT_FR :
                languageCollection.put(FIELD_TITLE, subcategory.getTitleFr());
                languageCollection.put(FIELD_DESCRIPTION, subcategory.getDescriptionFr());
                languageCollection.put(FIELD_GROUP, subcategory.getGroupeFr());
                break;
            case StringConfig.DEFAULT_EN :
                languageCollection.put(FIELD_TITLE, subcategory.getTitleEn());
                languageCollection.put(FIELD_DESCRIPTION, subcategory.getDescriptionFr());
                languageCollection.put(FIELD_GROUP, subcategory.getGroupeEn());
                break;
            default :
                languageCollection.put(FIELD_TITLE, subcategory.getTitleFr());
                languageCollection.put(FIELD_DESCRIPTION, subcategory.getDescriptionFr());
                languageCollection.put(FIELD_GROUP, subcategory.getGroupeFr());
                break;
        }

        return languageCollection;
    }
}
