package com.inductivtechnologies.quantmarkapp.utils;

import com.inductivtechnologies.quantmarkapp.design.DisplayLargeNumber;
import com.inductivtechnologies.quantmarkapp.models.display.BuyDisplay;
import com.inductivtechnologies.quantmarkapp.models.display.MarketOfferDisplay;
import com.inductivtechnologies.quantmarkapp.models.entities.CartItem;
import com.inductivtechnologies.quantmarkapp.viewholders.cart.CartViewHolder;
import com.inductivtechnologies.quantmarkapp.viewholders.category.SubCategoryMarketofferGridViewHolder;
import com.inductivtechnologies.quantmarkapp.viewholders.category.SubCategoryMarketofferListViewHolder;
import com.inductivtechnologies.quantmarkapp.viewholders.favorite.FavoriteItemViewHolder;
import com.inductivtechnologies.quantmarkapp.viewholders.home.HomeViewmoreMarketoffersItemViewholder;
import com.inductivtechnologies.quantmarkapp.viewholders.home.LowCostOffersViewholder;
import com.inductivtechnologies.quantmarkapp.viewholders.home.PromotionOffersViewholder;
import com.inductivtechnologies.quantmarkapp.viewholders.home.RandomOffersItemViewholder;
import com.inductivtechnologies.quantmarkapp.viewholders.marketofferdetails.IssuerProductsItemGridViewHolder;
import com.inductivtechnologies.quantmarkapp.viewholders.marketofferdetails.IssuerProductsItemListViewHolder;
import com.inductivtechnologies.quantmarkapp.viewholders.marketofferdetails.SimilarMarkerofferIssuerViewHolder;
import com.inductivtechnologies.quantmarkapp.viewholders.profile.OrdersItemViewholder;
import com.inductivtechnologies.quantmarkapp.viewholders.profile.PurchasesItemViewholder;
import com.inductivtechnologies.quantmarkapp.viewholders.search.SearchResultMarketofferGridViewHolder;
import com.inductivtechnologies.quantmarkapp.viewholders.search.SearchResultMarketofferListViewHolder;

/**
 * BindDataIntoViewholder : Utilitaire permet d'ajouter des données à des viewholder
 */
public class BindDataIntoViewholder {

    public static void addRandomOffers(MarketOfferDisplay marketOfferDisplay,
                                                 RandomOffersItemViewholder viewHolder) {

        double originalPrice = marketOfferDisplay.getMarketOffer().getProduct().getOriginalprice();
        double reductionPrice = marketOfferDisplay.getMarketOffer().getPrice();

        //Nombre de favori
        viewHolder.setTvNbFavorite(DisplayLargeNumber.smartLargeNumber(marketOfferDisplay.getNbOrder()));
        //Image
        viewHolder.setIvThumbnail(marketOfferDisplay.getMarketOffer().getProduct().getImage());

        //Prix réduit
        viewHolder.setTvPrice(reductionPrice, originalPrice);
        //Prix final
        viewHolder.setTvFinalPrice(reductionPrice);
        //Reduction notice
        viewHolder.setTvReductionNotice(marketOfferDisplay.getMarketOffer().getCoupon().getValue());
    }

    public static void addPurchasesData(BuyDisplay buyDisplay,
                                        PurchasesItemViewholder viewholder){
        double reductionPrice = buyDisplay.getReductionPrice();

        //Image
        viewholder.setThumbmailPurchase(buyDisplay.getImageName());

        //Code
        viewholder.setCodePurchase(buyDisplay.getCode());

        //Nom du vendeur
        viewholder.setIssuerNamePurchase(buyDisplay.getIssuerName());

        //Titre
        viewholder.setTitlePurchase(StringUtils
                .toUpperFirstChar((String) LanguageDisolay
                        .displayBuy(buyDisplay).get(LanguageDisolay.FIELD_TITLE)));

        //Prix
        viewholder.setPricePurchase(reductionPrice);

        //Date
        viewholder.setDatePurchase(buyDisplay.getDate());
    }

    public static void addOrdersData(BuyDisplay buyDisplay,
                                     OrdersItemViewholder viewholder){
        double reductionPrice = buyDisplay.getReductionPrice();

        //Image
        viewholder.setThumbmailOrder(buyDisplay.getImageName());

        //Code
        viewholder.setCodeOrder(buyDisplay.getCode());

        //Nom du vendeur
        viewholder.setIssuerNameOrder(buyDisplay.getIssuerName());

        //Titre
         viewholder.setTitleOrder(StringUtils
                .toUpperFirstChar((String) LanguageDisolay
                        .displayBuy(buyDisplay).get(LanguageDisolay.FIELD_TITLE)));

        //Prix
        viewholder.setPriceOrder(reductionPrice);

        //Date
        viewholder.setDateOrder(buyDisplay.getDate());
    }

    public static void addMarketofferOfGridSubcategory(MarketOfferDisplay marketOfferDisplay,
                                                       SubCategoryMarketofferGridViewHolder viewHolder) {
        double originalPrice = marketOfferDisplay.getMarketOffer().getProduct().getOriginalprice();
        double reductionPrice = marketOfferDisplay.getMarketOffer().getPrice();

        //Image
        viewHolder.setIvThumbnail(marketOfferDisplay.getMarketOffer().getProduct().getImage());
        //nombre de commandes
        viewHolder.setTvNbOrder(marketOfferDisplay.getNbOrder());

        //Prix réduit
        viewHolder.setTvPrice(reductionPrice, originalPrice);
        //Prix final
        viewHolder.setTvFinalPrice(reductionPrice);
        //Reduction notice
        viewHolder.setTvReductionNotice(marketOfferDisplay.getMarketOffer().getCoupon().getValue());
    }

    public static void addLowCostOffers(MarketOfferDisplay marketOfferDisplay,
                                        LowCostOffersViewholder viewHolder) {
        double originalPrice = marketOfferDisplay.getMarketOffer().getProduct().getOriginalprice();
        double reductionPrice = marketOfferDisplay.getMarketOffer().getPrice();

        //Image
        viewHolder.setIvThumbnail(marketOfferDisplay.getMarketOffer().getProduct().getImage());

        //Prix réduit
        viewHolder.setTvPrice(reductionPrice, originalPrice);
        //Prix final
        viewHolder.setTvFinalPrice(reductionPrice);
    }

    public static void addPromotionOffers(MarketOfferDisplay marketOfferDisplay,
                                          PromotionOffersViewholder viewHolder) {
        double originalPrice = marketOfferDisplay.getMarketOffer().getProduct().getOriginalprice();
        double reductionPrice = marketOfferDisplay.getMarketOffer().getPrice();

        //Image
        viewHolder.setIvThumbnail(marketOfferDisplay.getMarketOffer().getProduct().getImage());

        //Prix réduit
        viewHolder.setTvPrice(reductionPrice, originalPrice);
        //Prix final
        viewHolder.setTvFinalPrice(reductionPrice);
        //Reduction
        viewHolder.setTvReductionValue(marketOfferDisplay.getMarketOffer().getCoupon().getValue());
    }

    public static void addHomeViewMoreOffers(MarketOfferDisplay marketOfferDisplay,
                                              HomeViewmoreMarketoffersItemViewholder viewHolder) {

        double originalPrice = marketOfferDisplay.getMarketOffer().getProduct().getOriginalprice();
        double reductionPrice = marketOfferDisplay.getMarketOffer().getPrice();

        //Nombre de favori
        viewHolder.setTvNbFavorite(DisplayLargeNumber.smartLargeNumber(marketOfferDisplay.getNbOrder()));
        //Image
        viewHolder.setIvThumbnail(marketOfferDisplay.getMarketOffer().getProduct().getImage());

        //Prix réduit
        viewHolder.setTvPrice(reductionPrice, originalPrice);
        //Prix final
        viewHolder.setTvFinalPrice(reductionPrice);
        //Reduction notice
        viewHolder.setTvReductionNotice(marketOfferDisplay.getMarketOffer().getCoupon().getValue());
    }


    public static void addMarketofferOfListSubcategory(MarketOfferDisplay marketOfferDisplay,
                                                       SubCategoryMarketofferListViewHolder viewHolder) {
        double originalPrice = marketOfferDisplay.getMarketOffer().getProduct().getOriginalprice();
        double reductionPrice = marketOfferDisplay.getMarketOffer().getPrice();

        //Image
        viewHolder.setIvThumbnail(marketOfferDisplay.getMarketOffer().getProduct().getImage());
        //nombre de commandes
        viewHolder.setTvNbOrder(marketOfferDisplay.getNbOrder());

        //Prix réduit
        viewHolder.setTvPrice(reductionPrice, originalPrice);
        //Prix final
        viewHolder.setTvFinalPrice(reductionPrice);
        //Reduction notice
        viewHolder.setTvReductionNotice(marketOfferDisplay.getMarketOffer().getCoupon().getValue());

        //Code
        viewHolder.setTvCode(marketOfferDisplay.getMarketOffer().getProduct().getProductCode());

        //Issuer
        viewHolder.setTvIssuer(marketOfferDisplay.getMarketOffer().getProduct().getIssuer().getCompleteName());

        //Title
        viewHolder.setTvTitle(StringUtils
                .toUpperFirstChar((String) LanguageDisolay
                        .displayProduct(marketOfferDisplay
                                .getMarketOffer().getProduct()).get(LanguageDisolay.FIELD_TITLE)));
    }

    public static void addSimilarMarketofferToLayout(MarketOfferDisplay marketOfferDisplay,
                                                     SimilarMarkerofferIssuerViewHolder viewHolder){
        double originalPrice = marketOfferDisplay.getMarketOffer().getProduct().getOriginalprice();
        double reductionPrice = marketOfferDisplay.getMarketOffer().getPrice();

        //Image
        viewHolder.setIvThumbnail(marketOfferDisplay.getMarketOffer().getProduct().getImage());

        //Prix réduit
        viewHolder.setTvPrice(reductionPrice, originalPrice);
        //Prix final
        viewHolder.setTvFinalPrice(reductionPrice);
        //Reduction notice
        viewHolder.setTvReductionNotice(marketOfferDisplay.getMarketOffer().getCoupon().getValue());
    }

    public static void addCartItemToLayout(CartItem cartitem,
                                           CartViewHolder viewHolder) {

        //Image
        viewHolder.setIvThumbnail(cartitem.getImage());
        //Nom du vendeur
        viewHolder.setTvIssuerName(cartitem.getIssuerName());
        //Title
        viewHolder.setTvTitle(StringUtils
                .toUpperFirstChar((String) LanguageDisolay
                        .displayCartItem(cartitem).get(LanguageDisolay.FIELD_TITLE)));
        //Quantité
        viewHolder.setTvQuantity(cartitem.getQuantity());
        //Prix réduit
        viewHolder.setTvPrice(cartitem.getReductionPrice(), cartitem.getSinglePrice());
        //Prix final
        viewHolder.setTvFinalPrice(cartitem.getReductionPrice());
        //Reduction notice
        viewHolder.setTvReduction(cartitem.getReductionValue());

        //Total
        viewHolder.setTvTotal(cartitem.getTotalPrice());
    }

    public static void addIssuerMarketofferOfList(MarketOfferDisplay marketOfferDisplay,
                                                  IssuerProductsItemListViewHolder viewHolder) {
        double originalPrice = marketOfferDisplay.getMarketOffer().getProduct().getOriginalprice();
        double reductionPrice = marketOfferDisplay.getMarketOffer().getPrice();

        //Image
        viewHolder.setIvThumbnail(marketOfferDisplay.getMarketOffer().getProduct().getImage());
        //nombre de commandes
        viewHolder.setTvNbOrder(marketOfferDisplay.getNbOrder());

        //Prix réduit
        viewHolder.setTvPrice(reductionPrice, originalPrice);
        //Prix final
        viewHolder.setTvFinalPrice(reductionPrice);
        //Reduction notice
        viewHolder.setTvReductionNotice(marketOfferDisplay.getMarketOffer().getCoupon().getValue());

        //Code
        viewHolder.setTvCode(marketOfferDisplay.getMarketOffer().getProduct().getProductCode());

        //Issuer
        viewHolder.setTvIssuer(marketOfferDisplay.getMarketOffer().getProduct().getIssuer().getCompleteName());

        //Title
        viewHolder.setTvTitle(StringUtils
                .toUpperFirstChar((String) LanguageDisolay
                        .displayProduct(marketOfferDisplay
                                .getMarketOffer().getProduct()).get(LanguageDisolay.FIELD_TITLE)));
    }

    public static void addIssuerMarketofferOfGrid(MarketOfferDisplay marketOfferDisplay,
                                                  IssuerProductsItemGridViewHolder viewHolder) {
        double originalPrice = marketOfferDisplay.getMarketOffer().getProduct().getOriginalprice();
        double reductionPrice = marketOfferDisplay.getMarketOffer().getPrice();

        //Image
        viewHolder.setIvThumbnail(marketOfferDisplay.getMarketOffer().getProduct().getImage());
        //nombre de commandes
        viewHolder.setTvNbOrder(marketOfferDisplay.getNbOrder());

        //Prix réduit
        viewHolder.setTvPrice(reductionPrice, originalPrice);
        //Prix final
        viewHolder.setTvFinalPrice(reductionPrice);
        //Reduction notice
        viewHolder.setTvReductionNotice(marketOfferDisplay.getMarketOffer().getCoupon().getValue());
    }

    public static void addMarketofferOfGridSearchResult(MarketOfferDisplay marketOfferDisplay,
                                                        SearchResultMarketofferGridViewHolder viewHolder) {
        double originalPrice = marketOfferDisplay.getMarketOffer().getProduct().getOriginalprice();
        double reductionPrice = marketOfferDisplay.getMarketOffer().getPrice();

        //Image
        viewHolder.setIvThumbnail(marketOfferDisplay.getMarketOffer().getProduct().getImage());
        //nombre de commandes
        viewHolder.setTvNbOrder(marketOfferDisplay.getNbOrder());

        //Prix réduit
        viewHolder.setTvPrice(reductionPrice, originalPrice);
        //Prix final
        viewHolder.setTvFinalPrice(reductionPrice);
        //Reduction notice
        viewHolder.setTvReductionNotice(marketOfferDisplay.getMarketOffer().getCoupon().getValue());
    }

    public static void addMarketofferOfListSearchResult(MarketOfferDisplay marketOfferDisplay,
                                                        SearchResultMarketofferListViewHolder viewHolder) {
        double originalPrice = marketOfferDisplay.getMarketOffer().getProduct().getOriginalprice();
        double reductionPrice = marketOfferDisplay.getMarketOffer().getPrice();

        //Image
        viewHolder.setIvThumbnail(marketOfferDisplay.getMarketOffer().getProduct().getImage());
        //nombre de commandes
        viewHolder.setTvNbOrder(marketOfferDisplay.getNbOrder());

        //Prix réduit
        viewHolder.setTvPrice(reductionPrice, originalPrice);
        //Prix final
        viewHolder.setTvFinalPrice(reductionPrice);
        //Reduction notice
        viewHolder.setTvReductionNotice(marketOfferDisplay.getMarketOffer().getCoupon().getValue());

        //Code
        viewHolder.setTvCode(marketOfferDisplay.getMarketOffer().getProduct().getProductCode());

        //Issuer
        viewHolder.setTvIssuer(marketOfferDisplay.getMarketOffer().getProduct().getIssuer().getCompleteName());

        //Title
        viewHolder.setTvTitle(StringUtils
                .toUpperFirstChar((String) LanguageDisolay
                        .displayProduct(marketOfferDisplay
                                .getMarketOffer().getProduct()).get(LanguageDisolay.FIELD_TITLE)));
    }

    public static void addFavoriteItems(MarketOfferDisplay marketOfferDisplay,
                                                       FavoriteItemViewHolder viewHolder) {
        double originalPrice = marketOfferDisplay.getMarketOffer().getProduct().getOriginalprice();
        double reductionPrice = marketOfferDisplay.getMarketOffer().getPrice();

        //Image
        viewHolder.setIvThumbnail(marketOfferDisplay.getMarketOffer().getProduct().getImage());
        //nombre de commandes
        viewHolder.setTvNbOrder(marketOfferDisplay.getNbOrder());

        //Prix réduit
        viewHolder.setTvPrice(reductionPrice, originalPrice);
        //Prix final
        viewHolder.setTvFinalPrice(reductionPrice);
        //Reduction notice
        viewHolder.setTvReductionNotice(marketOfferDisplay.getMarketOffer().getCoupon().getValue());
    }
}
