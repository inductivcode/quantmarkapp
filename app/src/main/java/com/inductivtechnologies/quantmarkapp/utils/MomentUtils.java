package com.inductivtechnologies.quantmarkapp.utils;

import android.text.format.DateUtils;

import org.joda.time.DateTime;

import java.util.Date;

/**
 * MomentUtils : Utilitaire pour les dates
 */
public class MomentUtils {

    //Date relative Ex : il y a 2 secondes
    public static CharSequence getRelativeDate(Date date){
        long now = System.currentTimeMillis();
        return DateUtils.getRelativeTimeSpanString(date.getTime(),
                now, DateUtils.DAY_IN_MILLIS);
    }

    //Nombre de jour dans un mois donné
    public static int howManyDaysInMonth(int year, int month){
        DateTime currentDate = new DateTime(year, month, 1, 0, 0);
        return currentDate.dayOfMonth().getMaximumValue();
    }
}
