package com.inductivtechnologies.quantmarkapp.utils;

import android.os.Build;
import android.telephony.PhoneNumberUtils;
import android.text.Html;
import android.text.Spanned;

/**
 * StringUtils : Utilitaire pour les chaines de caractères
 */
@SuppressWarnings("deprecation")
public class StringUtils {

    //Met le premier caractère en majuscule
    public static  String toUpperFirstChar(String string){
        if(string.equals("")) return "";
        return string.substring(0, 1).toUpperCase() + string.substring(1).toLowerCase();
    }

    //Masque le mot de passe
 /*   public static String maskPassword(String password){
        int passwordLenght = password.length();
        StringBuilder mask = new StringBuilder();
        for(int i= 0; i < passwordLenght; i++){
            mask.append("*");
        }

        return mask.toString();
    }*/

    //Retourne la position dune chaine dans un tableau
    public static int getPositionOfStringInArray(String[] source, String search){
        for(int i = 0; i < source.length; i++){
            if(source[i].equalsIgnoreCase(search)) return i;
        }
        return -1;
    }

    //Pour utiliser le html dans les chaines de caractères
    public static Spanned getSpannedText(String text) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT);
        } else {
            return Html.fromHtml(text);
        }
    }

    //Affiche un numéro de téléphone suivant le format Francais
    public static String displayPhoneNumber(String phoneNUmber){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            return PhoneNumberUtils.formatNumber(phoneNUmber, "FR");
        } else {
            return PhoneNumberUtils.formatNumber(phoneNUmber);
        }
    }
}