package com.inductivtechnologies.quantmarkapp.utils;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.util.Patterns;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.configuration.StringPatterns;

/**
 * FormsUtils : Validation des formulaires
 */
public class FormsUtils {

    private final Context context;

    public FormsUtils(Context context) {
        this.context = context;
    }

    public boolean validateSimpleInput(TextInputLayout inputLayout, String inputValue){
        if (inputValue.isEmpty()) {
            inputLayout.setError(context.getResources().getString(R.string.error_empty_simple_input));
            return false;
        } else {
            if(inputValue.length() < 3){
                inputLayout.setError(context.getResources().getString(R.string.error_short_simple_input));
                return false;
            }else{
                inputLayout.setErrorEnabled(false);
            }
        }

        return true;
    }

    public boolean validatePassword(TextInputLayout inputLayout, String inputValue){
        if(inputValue.isEmpty()){
            inputLayout.setError(context.getResources().getString(R.string.error_password_input));
            return false;
        }else{
            if(inputValue.length() < 6){
                inputLayout.setError(context.getResources().getString(R.string.error_short_password_input));
                return false;
            }else{
                inputLayout.setErrorEnabled(false);
            }
        }
        return true;
    }

    public boolean validatePasswordUpdate(TextInputLayout inputLayout, String inputValue, String passwordOld){
        if(!inputValue.equals(passwordOld)){
            inputLayout.setError(context.getResources().getString(R.string.label_password_not_match));
            return false;
        }
        return true;
    }

    public boolean validateLogin(TextInputLayout inputLayout, String inputValue){
        if(inputValue.isEmpty()){
            inputLayout.setError(context.getResources().getString(R.string.error_login_input));
            return false;
        }else{
            if(inputValue.length() < 6){
                inputLayout.setError(context.getResources().getString(R.string.error_short_login_input));
                return false;
            }else{
                inputLayout.setErrorEnabled(false);
            }
        }
        return true;
    }

    public boolean validateEmail(TextInputLayout inputLayout, String inputValue){
        if (!inputValue.isEmpty() && Patterns.EMAIL_ADDRESS.matcher(inputValue).matches()) {
            inputLayout.setErrorEnabled(false);
        } else {
            inputLayout.setError(context.getResources().getString(R.string.error_email_input));
            return false;
        }

        return true;
    }


    public boolean validatePhoneNumber(TextInputLayout inputLayout, String inputValue){

        if(inputValue.isEmpty()){
            inputLayout.setError(context.getResources().getString(R.string.error_phone_input));
            return false;
        }

        if(inputValue.length() == 9) {
            inputLayout.setErrorEnabled(false);
            return true;
        }

        inputLayout.setError(context.getResources().getString(R.string.error_phone_input));
        return false;
    }


    public boolean validatePhoneNumberWithRegex(TextInputLayout inputLayout, String inputValue){

        if (!inputValue.matches(StringPatterns.CMR_PHONE_PATTERN)) {
            inputLayout.setError(context.getResources().getString(R.string.error_phone_input));
            return false;
        }

        inputLayout.setErrorEnabled(false);
        return true;
    }

    public boolean validatePoBox(TextInputLayout inputLayout, String inputValue){
        if (inputValue.isEmpty()) {
            inputLayout.setError(context.getResources().getString(R.string.error_empty_simple_input));
            return false;
        } else {
            if(inputValue.length() < 3){
                inputLayout.setError(context.getResources().getString(R.string.error_short_simple_input));
                return false;
            }else{
                inputLayout.setErrorEnabled(false);
            }
        }

        return true;
    }

}
