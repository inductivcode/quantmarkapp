package com.inductivtechnologies.quantmarkapp.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.inductivtechnologies.quantmarkapp.R;

/**
 * DesignUtils
 */
public class DesignUtils {

    private static Snackbar snackbar;

    //Fabrique un snackbar
    public static void makeSnackbar(String text, View coordinatorLayout, Context context){
        snackbar = Snackbar.make(coordinatorLayout,
                text,
                Snackbar.LENGTH_LONG)
                .setAction(context.getResources().getString(R.string.snackbar_close), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        snackbar.dismiss();
                    }
                });
        snackbar.setActionTextColor(context.getResources().getColor(R.color.color_white));
        snackbar.getView().setBackgroundColor(context.getResources().getColor(R.color.color_snackbar));
        TextView textView = snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
        textView.setMaxLines(10);
        snackbar.show();
    }

    //Associe un drapeau à la fenêtre d'une activité
    @SuppressWarnings("SameParameterValue")
    private static void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window window = activity.getWindow();
        WindowManager.LayoutParams winParams = window.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        window.setAttributes(winParams);
    }

    //Barre de statut transparente
    public static void makeFullTransparentStatutBar(Activity activity){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setWindowFlag(activity, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            activity.getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
    }

    //Modifie une sous chaine contenu dans une chaine
    public static void setColorForPath(Spannable spannable, String subString, int color) {
        int indexOfSubString = spannable.toString().toLowerCase().indexOf(subString.toLowerCase());
        if (indexOfSubString == -1) {
            return;
        }

        final ForegroundColorSpan spanColor = new ForegroundColorSpan(color);
        final StyleSpan boldSpan = new StyleSpan(android.graphics.Typeface.BOLD);

        spannable.setSpan(spanColor, indexOfSubString,
                indexOfSubString + subString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannable.setSpan(boldSpan, indexOfSubString,
                indexOfSubString + subString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
    }
}
