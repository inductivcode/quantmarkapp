package com.inductivtechnologies.quantmarkapp.fragments.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dgreenhalgh.android.simpleitemdecoration.linear.StartOffsetItemDecoration;
import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.activities.category.SubCategoryMarketofferActivity;
import com.inductivtechnologies.quantmarkapp.activities.main.HomeViewMoreMarketoffersActivity;
import com.inductivtechnologies.quantmarkapp.activities.main.MainActivity;
import com.inductivtechnologies.quantmarkapp.activities.marketofferdetails.MarketofferDetails;
import com.inductivtechnologies.quantmarkapp.adapters.home.LowCostOfferAdapter;
import com.inductivtechnologies.quantmarkapp.adapters.home.PromotionOfferAdapter;
import com.inductivtechnologies.quantmarkapp.adapters.home.RandomOffersAdapter;
import com.inductivtechnologies.quantmarkapp.adapters.home.StarCategoryAdapter;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.callbacks.HttpCallback;
import com.inductivtechnologies.quantmarkapp.configuration.StringConfig;
import com.inductivtechnologies.quantmarkapp.design.DisplayLargeNumber;
import com.inductivtechnologies.quantmarkapp.design.GridSpacingItemDecoration;
import com.inductivtechnologies.quantmarkapp.httptask.home.HomeTask;
import com.inductivtechnologies.quantmarkapp.listeners.others.RecyclerTouchListener;
import com.inductivtechnologies.quantmarkapp.models.display.MarketOfferDisplay;
import com.inductivtechnologies.quantmarkapp.models.display.ProductTypeDisplay;
import com.inductivtechnologies.quantmarkapp.models.http.HttpResponseObject;
import com.inductivtechnologies.quantmarkapp.utils.CalculationUtils;
import com.inductivtechnologies.quantmarkapp.utils.DesignUtils;
import com.inductivtechnologies.quantmarkapp.utils.LanguageDisolay;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * HomeFragment : Affichage des offres sur l'interface principale
 */
public class HomeFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener,
        View.OnClickListener, HttpCallback {

    //Tag utilisé pour annuler la requête
    public static final String TASK_HOME_FRAGMENT = "TASK_HOME_FRAGMENT";

    //Les vues
    private SwipeRefreshLayout swipeRefreshLayout;
    private NestedScrollView mainScroll;

    //Les vues
    private RecyclerView recyclerViewStarSubcategories;
    private RecyclerView recyclerViewLowCostOffers;
    private RecyclerView recyclerViewPromotionOffers;
    private RecyclerView recyclerViewRandomOffers;
    //Adaptateurs
    private StarCategoryAdapter starSubcategoryadapter;
    private LowCostOfferAdapter lowCostAdapter;
    private PromotionOfferAdapter promotionAdapter;
    private RandomOffersAdapter randomAdapter;

    private GridSpacingItemDecoration itemDecoration;
    private StaggeredGridLayoutManager randomManager;

    //Volley instance singleton
    private AppController appController;

    //Les données
    private List<MarketOfferDisplay> lowCostOffers;
    private List<ProductTypeDisplay> subcategoriesDisplay;
    private List<MarketOfferDisplay> promotionOffersDisplay;
    private List<MarketOfferDisplay> randomOffersDisplay;

    //Pour la requête HTTP
    private HomeTask homeTask;

    //Instance unique du fragment
    public static HomeFragment newInstance() {
        Bundle args = new Bundle();

        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Initialisation des objets
        initObjects();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }

        View view = inflater.inflate(R.layout.fragment_home, container, false);

        //Les vues
        findAllViews(view);

        //On ajoute les ecouteurs
        addListenesr();

        //Init
        initRecyclerviewStarSubcategories();
        initRecyclerviewLowCostOffers();
        initRecyclerviewPromotionOffers();
        initRecyclerviewRandomOffers();

        launchHttpRequest();

        return view;
    }

    @Override
    public void onDestroyView() {
        // On annule la requête
        this.appController.cancelPendingRequests(TASK_HOME_FRAGMENT);

        super.onDestroyView();
    }

    @Override
    public void onRefresh() {
        //Task
        lowCostOffers.clear();
        subcategoriesDisplay.clear();
        promotionOffersDisplay.clear();
        randomOffersDisplay.clear();

        //On relance la requête
        homeTask.makeRequest();
    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(getActivity(), HomeViewMoreMarketoffersActivity.class);
        switch(view.getId()){
            case R.id.low_cost_view_more :
                intent.putExtra(StringConfig.INTENT_HOME_TITLE,
                        getResources().getString(R.string.home_title_low_cost_offers));

                startActivity(intent);
                break;
            case R.id.promotion_view_more :
                intent.putExtra(StringConfig.INTENT_HOME_TITLE,
                        getResources().getString(R.string.home_title_promotion_offers));

                startActivity(intent);
                break;
        }
    }

    //Méthodes

    //On recupère les vues
    private void findAllViews(View rootView){
        swipeRefreshLayout = rootView.findViewById(R.id.home_swipe_refresh_layout);
        //On modofie la couleur du progress
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));

        mainScroll = rootView.findViewById(R.id.home_nestedscroll_layout);

        TextView tvLoadPromotionOffers = rootView.findViewById(R.id.promotion_view_more);
        tvLoadPromotionOffers.setOnClickListener(this);
        TextView tvLoadLowCostOffers = rootView.findViewById(R.id.low_cost_view_more);
        tvLoadLowCostOffers.setOnClickListener(this);

        recyclerViewStarSubcategories = rootView.findViewById(R.id.start_categories_recycler_view);
        recyclerViewLowCostOffers = rootView.findViewById(R.id.low_cost_offers_recycler_view);
        recyclerViewPromotionOffers = rootView.findViewById(R.id.promotion_offers_recycler_view);
        recyclerViewRandomOffers = rootView.findViewById(R.id.random_offers_recycler_view);
    }

    //Les écouteurs

    private void addListenesr(){
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    //Init des objets

    private void initObjects(){
        appController = AppController.getInstance();

        lowCostOffers = new ArrayList<>();
        subcategoriesDisplay = new ArrayList<>();
        promotionOffersDisplay = new ArrayList<>();
        randomOffersDisplay = new ArrayList<>();

        //Item decoration
        itemDecoration = new GridSpacingItemDecoration(3,
                CalculationUtils.dpToPx(getActivity(), 6), true);

        randomManager = new StaggeredGridLayoutManager(3,
                StaggeredGridLayoutManager.VERTICAL);

        starSubcategoryadapter = new StarCategoryAdapter(getActivity(),
                subcategoriesDisplay);
        lowCostAdapter = new LowCostOfferAdapter(getActivity(),
                lowCostOffers);
        promotionAdapter = new PromotionOfferAdapter(getActivity(),
                promotionOffersDisplay);
        randomAdapter = new RandomOffersAdapter(getActivity(),
                randomOffersDisplay);

        homeTask = new HomeTask(this, appController);
    }

    //Initialisation des recyclerview

    private void initRecyclerviewStarSubcategories(){
        recyclerViewStarSubcategories.setLayoutManager(new LinearLayoutManager(getActivity(),
                LinearLayoutManager.HORIZONTAL, false));
        recyclerViewStarSubcategories.setAdapter(starSubcategoryadapter);

        recyclerViewStarSubcategories.setItemAnimator(new DefaultItemAnimator());
        recyclerViewStarSubcategories.addItemDecoration(new StartOffsetItemDecoration(22));

        recyclerViewStarSubcategories.addOnItemTouchListener(new RecyclerTouchListener(getActivity(),
                recyclerViewStarSubcategories, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                ProductTypeDisplay subcategoryDisplay = subcategoriesDisplay.get(position);
                long nbMarkets = subcategoryDisplay.getNbOfMarketOffer();

                String title = (String) LanguageDisolay
                        .displaySubCategory(subcategoryDisplay.getSubCategory())
                        .get(LanguageDisolay.FIELD_TITLE);

                String subtitle;
                if(nbMarkets > 1){
                    subtitle = getResources().getString(R.string.display_nb_article_plural,
                            DisplayLargeNumber.smartLargeNumber(nbMarkets));
                }else{
                    subtitle = getResources().getString(R.string.display_nb_article_sin,
                            DisplayLargeNumber.smartLargeNumber(nbMarkets));
                }

                String idHexSubCategory = subcategoryDisplay.getSubCategory().getIdHexType();
                String imageName = subcategoryDisplay.getSubCategory().getImage();

                // On passe à l'activité suivante
                Intent intent = new Intent(getActivity(), SubCategoryMarketofferActivity.class);
                intent.putExtra(StringConfig.INTENT_SUBCATEGORY_TITLE, title);
                intent.putExtra(StringConfig.INTENT_SUBCATEGORY_SUBTITLE, subtitle);
                intent.putExtra(StringConfig.INTENT_SUBCATEGORY_IMAGE_NAME, imageName);
                intent.putExtra(StringConfig.INTENT_SUBCATEGORY_HEX_ID, idHexSubCategory);
                intent.putExtra(StringConfig.INTENT_SUBCATEGORY_NB_MARKETOFFER, nbMarkets);

                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));
    }

    private void initRecyclerviewLowCostOffers(){
        recyclerViewLowCostOffers.setLayoutManager(new LinearLayoutManager(getActivity(),
                LinearLayoutManager.HORIZONTAL, false));

        recyclerViewLowCostOffers.setAdapter(lowCostAdapter);

        recyclerViewLowCostOffers.setItemAnimator(new DefaultItemAnimator());
        recyclerViewLowCostOffers.addItemDecoration(new StartOffsetItemDecoration(22));

        recyclerViewLowCostOffers.addOnItemTouchListener(new RecyclerTouchListener(getActivity(),
                recyclerViewLowCostOffers, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                MarketOfferDisplay marketOfferDisplay = lowCostOffers.get(position);

                Intent intent = new Intent(getActivity(), MarketofferDetails.class);
                intent.putExtra(StringConfig.INTENT_MARKETOFFER_HEX_ID,
                        marketOfferDisplay.getMarketOffer().getIdHexMarketOffer());

                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));
    }

    private void initRecyclerviewPromotionOffers(){
        recyclerViewPromotionOffers.setLayoutManager(new LinearLayoutManager(getActivity(),
                LinearLayoutManager.HORIZONTAL, false));

        recyclerViewPromotionOffers.setAdapter(promotionAdapter);

        recyclerViewPromotionOffers.setItemAnimator(new DefaultItemAnimator());
        recyclerViewPromotionOffers.addItemDecoration(new StartOffsetItemDecoration(22));

        recyclerViewPromotionOffers.addOnItemTouchListener(new RecyclerTouchListener(getActivity(),
                recyclerViewPromotionOffers, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                MarketOfferDisplay marketOfferDisplay = promotionOffersDisplay.get(position);

                Intent intent = new Intent(getActivity(), MarketofferDetails.class);
                intent.putExtra(StringConfig.INTENT_MARKETOFFER_HEX_ID,
                        marketOfferDisplay.getMarketOffer().getIdHexMarketOffer());

                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));
    }

    private void initRecyclerviewRandomOffers(){
        //On désactive le scroll
        recyclerViewRandomOffers.setNestedScrollingEnabled(false);
        //Lié à la taille des éléments
        recyclerViewRandomOffers.setHasFixedSize(true);

        recyclerViewRandomOffers.setLayoutManager(randomManager);

        recyclerViewRandomOffers.setAdapter(randomAdapter);

        recyclerViewRandomOffers.setItemAnimator(new DefaultItemAnimator());
        recyclerViewRandomOffers.addItemDecoration(itemDecoration);

        recyclerViewRandomOffers.addOnItemTouchListener(new RecyclerTouchListener(getActivity(),
                recyclerViewRandomOffers, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                MarketOfferDisplay marketOfferDisplay = randomOffersDisplay.get(position);

                Intent intent = new Intent(getActivity(), MarketofferDetails.class);
                intent.putExtra(StringConfig.INTENT_MARKETOFFER_HEX_ID,
                        marketOfferDisplay.getMarketOffer().getIdHexMarketOffer());

                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));
    }

    //Getters et setters

    // La tâche asynchrone

    //Lance une requête asynchrone  HTTP
    private void launchHttpRequest(){
        homeTask.makeRequest();
    }

    @Override
    public void beforeExecute(){
        //Le progress
        updateProgress(true);
    }

    @Override
    public void afterExecute(JSONObject response){
        //On recupère les résultats
        getBestSubcategories(response);
        getLowCostOffers(response);
        getPromotionOffers(response);
        getRandomOffers(response);

        //On affiche tout
        mainScroll.setVisibility(View.VISIBLE);

        //Le progress
        updateProgress(false);
    }

    @Override
    public void whenErrors(){
        //Le progress
        updateProgress(false);

        //Le snackbar
        makeSnackbar();
    }

    private void getBestSubcategories(JSONObject response) {
        try {
            JSONArray arraySucategoriesDisplay = response
                    .getJSONObject(HttpResponseObject.OBJ_VALUE_STR)
                    .getJSONArray(HttpResponseObject.SMART_SUBCATEGORIES_STR);

            if(arraySucategoriesDisplay.length() != 0){
                for(int i = 0; i < arraySucategoriesDisplay.length(); i++){
                    ProductTypeDisplay productTypeDisplay = new
                            ProductTypeDisplay(arraySucategoriesDisplay.getJSONObject(i));

                    this.subcategoriesDisplay.add(productTypeDisplay);
                }

                //Notification des adaptateurs
                starSubcategoryadapter.notifyDataSetChanged();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    
    private void getLowCostOffers(JSONObject response) {
        try {
            JSONArray lowCostOffers = response
                    .getJSONObject(HttpResponseObject.OBJ_VALUE_STR)
                    .getJSONArray(HttpResponseObject.LOW_COST_MARKETOFFERS_STR);

            if(lowCostOffers.length() != 0){
                for(int i = 0; i < lowCostOffers.length(); i++){
                    MarketOfferDisplay lowCostDisplay = new
                            MarketOfferDisplay(lowCostOffers.getJSONObject(i));

                    this.lowCostOffers.add(lowCostDisplay);
                }

                //Notification des adaptateurs
                this.lowCostAdapter.notifyDataSetChanged();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    
    private void getPromotionOffers(JSONObject response) {
        try {
            JSONArray promotionOffers = response
                    .getJSONObject(HttpResponseObject.OBJ_VALUE_STR)
                    .getJSONArray(HttpResponseObject.PROMOTION_MARKETOFFERS_STR);

            if(promotionOffers.length() != 0){
                for(int i = 0; i < promotionOffers.length(); i++){
                    MarketOfferDisplay lowCostDisplay = new
                            MarketOfferDisplay(promotionOffers.getJSONObject(i));

                    promotionOffersDisplay.add(lowCostDisplay);
                }

                //Notification des adaptateurs
                promotionAdapter.notifyDataSetChanged();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    
    private void getRandomOffers(JSONObject response) {
        try {
            JSONArray randomOffers = response
                    .getJSONObject(HttpResponseObject.OBJ_VALUE_STR)
                    .getJSONArray(HttpResponseObject.RANDOM_MARKETOFFERS_STR);

            if(randomOffers.length() != 0){
                for(int i = 0; i < randomOffers.length(); i++){
                    MarketOfferDisplay lowCostDisplay = new
                            MarketOfferDisplay(randomOffers.getJSONObject(i));

                    this.randomOffersDisplay.add(lowCostDisplay);
                }

                //Notification des adaptateurs
                this.randomAdapter.notifyDataSetChanged();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void updateProgress(boolean visibility){
        if(swipeRefreshLayout != null){
            swipeRefreshLayout.setRefreshing(visibility);
        }
    }

    private void makeSnackbar(){
        DesignUtils.makeSnackbar(getContext().getString(R.string.network_message_error),
                ((MainActivity) getActivity()).getCoordinatorLayout(), getContext());
    }
}
