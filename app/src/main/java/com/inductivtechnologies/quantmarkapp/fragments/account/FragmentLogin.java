package com.inductivtechnologies.quantmarkapp.fragments.account;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.activities.account.UserLoginActivity;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.bottomsheets.BottomSheetProgress;
import com.inductivtechnologies.quantmarkapp.callbacks.HttpCallback;
import com.inductivtechnologies.quantmarkapp.httptask.account.LoginTask;
import com.inductivtechnologies.quantmarkapp.models.entities.Token;
import com.inductivtechnologies.quantmarkapp.models.entities.User;
import com.inductivtechnologies.quantmarkapp.models.http.HttpResponseObject;
import com.inductivtechnologies.quantmarkapp.models.http.NameValuePair;
import com.inductivtechnologies.quantmarkapp.store.repositories.AddressRepository;
import com.inductivtechnologies.quantmarkapp.store.repositories.PhoneRepository;
import com.inductivtechnologies.quantmarkapp.store.repositories.TokenRepository;
import com.inductivtechnologies.quantmarkapp.store.repositories.UserRepository;
import com.inductivtechnologies.quantmarkapp.store.session.SessionManager;
import com.inductivtechnologies.quantmarkapp.utils.DesignUtils;

import org.json.JSONObject;

/**
 * FragmentLogin : Pour la connexion
 */
public class FragmentLogin extends Fragment implements View.OnClickListener, HttpCallback {

    public static final String LOGIN_TASK = "LOGIN_TASK";

    private TextInputEditText etLogin;
    private TextInputEditText etpassword;

    //Dialog
    private BottomSheetProgress progress;

    //Controlleur
    private AppController appController;
    //Session manager
    private SessionManager sessionManager;

    //Repositories
    private TokenRepository tokenRepo;
    private UserRepository userRepo;
    private AddressRepository addressRepo;
    private PhoneRepository phoneRepo;

    //Instance unique du fragment
    public static FragmentLogin newInstance() {
        Bundle args = new Bundle();

        FragmentLogin fragment = new FragmentLogin();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Initialisation des objets
        initObjects();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }

        View view = inflater.inflate(R.layout.fragment_login, container, false);

        //Les vues
        findAllViews(view);

        return view;
    }

    @Override
    public void onDestroyView() {
        // On annule la requête
        this.appController.cancelPendingRequests(LOGIN_TASK);

        super.onDestroyView();
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.btn_login :
                makeLogin();
                break;
        }
    }

    //Les méthodes

    private void initObjects() {
        //Instance singleton du controlleur volley
        appController = AppController.getInstance();
        //Session
        this.sessionManager = new SessionManager(getActivity());

        //Progress
        this.progress = new BottomSheetProgress(getActivity());

        this.tokenRepo = new TokenRepository(getActivity());
        this.userRepo = new UserRepository(getActivity());
        this.addressRepo = new AddressRepository(getActivity());
        this.phoneRepo = new PhoneRepository(getActivity());
    }

    private void findAllViews(View root){
        Button btnLogin = root.findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(this);

        //TextInputLayout inputLayoutLogin = root.findViewById(R.id.tl_login);
        etLogin = root.findViewById(R.id.login);
        //TextInputLayout inputLayoutPassword = root.findViewById(R.id.tl_password);
        etpassword = root.findViewById(R.id.password);
    }

    //Task Http

    private void makeLogin(){
        String login = etLogin.getText().toString().trim();
        String password = etpassword.getText().toString().trim();

        if(!login.isEmpty() && !password.isEmpty()){

            NameValuePair paramValues = new NameValuePair();
            paramValues.getParamsMap().put(User.FIELD_NAME_LOGIN, login);
            paramValues.getParamsMap().put(User.FIELD_NAME_PASSWORD, password);

            new LoginTask(this, appController).makeRequest(paramValues);
        }
    }

    private void updateProgress(boolean isVisible){
        if(isVisible){
            if(progress != null)
                progress.show(getResources().getString(R.string.login_progress_message));
        }else{
            if(progress != null)
                progress.hide();
        }
    }

    private void makeSnackbar(String message){
        DesignUtils.makeSnackbar(message,
                ((UserLoginActivity) getActivity()).getCoordinatorLayout(), getContext());
    }

    @Override
    public void beforeExecute() {
        updateProgress(true);
    }

    @Override
    public void afterExecute(JSONObject response) {
        //Vérification
        final int statusCode = response.optInt(HttpResponseObject.CODE_STR);
        switch(statusCode){
            case HttpResponseObject.SUCCESS_CODE :
                //succès login
                //On recupère les données
                HttpResponseObject responseObject = new HttpResponseObject(response);
                responseObject.addUserLoginResults(response);

                //Token
                Token token = (Token) responseObject.getObjectValue().get(HttpResponseObject.TOKEN_STR);
                //Buyer
                User user = (User) responseObject.getObjectValue().get(HttpResponseObject.BUYER_STR);
                user.setToken(token);

                // On insère dans la base de données
                addressRepo.insertAddress(user.getAddress());
                phoneRepo.insertPhone(user.getPhone());
                tokenRepo.insertToken(user.getToken());
                userRepo.insertUser(user);

                //On ouvre la session de connexion
                sessionManager.createLoginSession();

                //On termine l'activité
                getActivity().setResult(Activity.RESULT_OK,
                        getActivity().getIntent());
                getActivity().finish();

                break;
            case HttpResponseObject.LOGIN_NO_MATCH_ERROR_CODE :
                makeSnackbar(getResources().getString(R.string.profile_label_login_not_match));
                break;
            case HttpResponseObject.PASSWORD_NO_MATCH_ERROR_CODE :
                makeSnackbar(getResources().getString(R.string.profile_label_login_not_match));
                break;
            default :
                makeSnackbar(getResources().getString(R.string.label_unknow_error_message));
                break;
        }

        updateProgress(false);
    }

    @Override
    public void whenErrors() {
        updateProgress(false);
        //Le snackbar
        makeSnackbar(getResources().getString(R.string.network_message_error));
    }
}
