package com.inductivtechnologies.quantmarkapp.fragments.category;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.activities.category.SubCategoriesActivity;
import com.inductivtechnologies.quantmarkapp.adapters.category.CategoryGridAdapter;
import com.inductivtechnologies.quantmarkapp.adapters.category.CategoryListAdapter;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.configuration.StringConfig;
import com.inductivtechnologies.quantmarkapp.design.DisplayLargeNumber;
import com.inductivtechnologies.quantmarkapp.design.GridSpacingItemDecoration;
import com.inductivtechnologies.quantmarkapp.httptask.category.CategoryTask;
import com.inductivtechnologies.quantmarkapp.listeners.others.RecyclerTouchListener;
import com.inductivtechnologies.quantmarkapp.models.display.CategoryDisplay;
import com.inductivtechnologies.quantmarkapp.utils.CalculationUtils;
import com.inductivtechnologies.quantmarkapp.utils.LanguageDisolay;

import java.util.ArrayList;
import java.util.List;

/**
 * CategoryFragment : Affichage des produits par catégorie
 */
public class CategoryFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener{

    public static final String CATEGORY_TASK = "CATEGORY_TASK";

    //Les vues
    private SwipeRefreshLayout swipeRefreshLayout;

    private ImageButton btnListDisplay;
    private ImageButton btnGridDisplay;

    // Recycler view
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private GridSpacingItemDecoration itemDecoration;

    private LinearLayoutManager linearManager;
    private GridLayoutManager gridManager;
    private CategoryGridAdapter gridAdapter;
    private CategoryListAdapter listAdapter;

    //Les categories
    private List<CategoryDisplay> categories;

    //Volley instance singleton
    private AppController appController;

    private CategoryTask categoryTask;

    //Indique le mode d'affichage
    private boolean isListDisplay = true;

    //Instance unique du fragment
    public static CategoryFragment newInstance() {
        Bundle args = new Bundle();

        CategoryFragment fragment = new CategoryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Objets init
        initObjects();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }

        View view = inflater.inflate(R.layout.fragment_category, container, false);

        //Les vues
        allViews(view);

        initRecyclerViewCategoryItem();

        launchTask();

        return view;
    }

    @Override
    public void onDestroyView() {
        // On annule la requête
        this.appController.cancelPendingRequests(CATEGORY_TASK);

        super.onDestroyView();
    }

    @Override
    public void onClick(View view){
        switch (view.getId()){
            case R.id.btn_category_list_display :
                if(!isListDisplay){
                    isListDisplay = true;
                    btnGridDisplay.setImageDrawable(getResources().getDrawable(R.drawable.ic_action_display_grid_unselected));
                    btnListDisplay.setImageDrawable(getResources().getDrawable(R.drawable.ic_action_display_list_selected));

                    changeRecyclerViewConfiguration();
                }
                break;
            case R.id.btn_category_grid_display :
                if(isListDisplay){
                    isListDisplay = false;
                    btnGridDisplay.setImageDrawable(getResources().getDrawable(R.drawable.ic_action_display_grid_selected));
                    btnListDisplay.setImageDrawable(getResources().getDrawable(R.drawable.ic_action_display_list_unselected));

                    changeRecyclerViewConfiguration();
                }
                break;
        }
    }

    @Override
    public void onRefresh() {
        //Task
        categories.clear();
        launchTask();
    }

    //Les méthodes

    private void launchTask(){
        categoryTask.makeRequest();
    }

    private void allViews(View view) {
        swipeRefreshLayout = view.findViewById(R.id.category_swipe_refresh);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
        swipeRefreshLayout.setOnRefreshListener(this);

        btnListDisplay = view.findViewById(R.id.btn_category_list_display);
        btnListDisplay.setOnClickListener(this);
        btnGridDisplay = view.findViewById(R.id.btn_category_grid_display);
        btnGridDisplay.setOnClickListener(this);

        recyclerView = view.findViewById(R.id.categories_items__recycler_view);
    }

    // Initialisation du recyclerview avec les données recues
    private void initRecyclerViewCategoryItem(){
        layoutManager = linearManager;
        recyclerView.setLayoutManager(layoutManager);

        adapter = listAdapter;
        recyclerView.setAdapter(adapter);

        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(),
                recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                long nbMarketOffers = categories.get(position).getNbOfMarketOffers();

                String subtitle;
                String title = (String) LanguageDisolay
                        .displayCategory(categories.get(position).getCategory()).get(LanguageDisolay.FIELD_TITLE);

                if(nbMarketOffers > 1){
                    subtitle = getResources().getString(R.string.display_nb_article_plural,
                            DisplayLargeNumber.smartLargeNumber(nbMarketOffers));
                }else{
                    subtitle = getResources().getString(R.string.display_nb_article_sin,
                            DisplayLargeNumber.smartLargeNumber(nbMarketOffers));
                }

                String idHexCategory = categories.get(position).getCategory().getIdHexCategory();
                String imageName = categories.get(position).getCategory().getImage();

                // On passe à l'activité suivante
                Intent intent = new Intent(getActivity(), SubCategoriesActivity.class);
                intent.putExtra(StringConfig.INTENT_CATEGORY_TITLE, title);
                intent.putExtra(StringConfig.INTENT_CATEGORY_SUBTITLE, subtitle);
                intent.putExtra(StringConfig.INTENT_CATEGORY_IMAGE_NAME, imageName);
                intent.putExtra(StringConfig.INTENT_CATEGORY_HEX_ID, idHexCategory);
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));
    }

    //Affichage en grid
    private void changeRecyclerViewConfiguration(){
        if(recyclerView.getLayoutManager() instanceof GridLayoutManager){
            layoutManager = linearManager;
            recyclerView.removeItemDecoration(itemDecoration);
        }else{
            layoutManager = gridManager;
            recyclerView.addItemDecoration(itemDecoration);
        }

        if(recyclerView.getAdapter() instanceof CategoryGridAdapter){
            adapter = listAdapter;
        }else{
            adapter = gridAdapter;
        }

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    //Init objects
    private void initObjects(){
        //Volley
        this.appController = AppController.getInstance();

        //Data
        this.categories = new ArrayList<>();

        //Task
        this.categoryTask = new CategoryTask(this, appController);

        //Item decoration
        this.itemDecoration = new GridSpacingItemDecoration(2, CalculationUtils.dpToPx(getContext(), 4), true);

        linearManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        gridManager = new GridLayoutManager(getContext(), 2);

        gridAdapter = new CategoryGridAdapter(getContext(), this.categories);
        listAdapter = new CategoryListAdapter(getContext(), this.categories);
    }

    //Getters et setters

    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return swipeRefreshLayout;
    }

    public RecyclerView.Adapter getAdapter() {
        return adapter;
    }

    public List<CategoryDisplay> getCategories() {
        return categories;
    }
}
