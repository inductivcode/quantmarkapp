package com.inductivtechnologies.quantmarkapp.fragments.favorite;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.activities.main.MainActivity;
import com.inductivtechnologies.quantmarkapp.activities.marketofferdetails.MarketofferDetails;
import com.inductivtechnologies.quantmarkapp.adapters.favorite.FavoriteAdapter;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.components.GlideComponent;
import com.inductivtechnologies.quantmarkapp.configuration.StringConfig;
import com.inductivtechnologies.quantmarkapp.configuration.UrlRestApiConfig;
import com.inductivtechnologies.quantmarkapp.design.GridSpacingItemDecoration;
import com.inductivtechnologies.quantmarkapp.httptask.favorite.FavoriteTask;
import com.inductivtechnologies.quantmarkapp.listeners.others.RecyclerTouchListener;
import com.inductivtechnologies.quantmarkapp.models.display.MarketOfferDisplay;
import com.inductivtechnologies.quantmarkapp.models.entities.Favorite;
import com.inductivtechnologies.quantmarkapp.models.utils.MarketOfferDisplayCustom;
import com.inductivtechnologies.quantmarkapp.store.repositories.FavoriteRepository;
import com.inductivtechnologies.quantmarkapp.utils.CalculationUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * FavoriteFragment
 */
@SuppressWarnings("ConstantConditions")
public class FavoriteFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener{

    public static final String FAVORITES_TASK = "FAVORITES_TASK";

    //Les vues

    private SwipeRefreshLayout swipeRefreshLayout;

    public LinearLayout llContainerEmpty;

    // Recycler view
    private RecyclerView recyclerView;
    private FavoriteAdapter adapter;
    private GridLayoutManager layoutManager;
    private GridSpacingItemDecoration itemDecoration;

    //Instance volley
    private AppController appController;

    //Les favoris
    private List<MarketOfferDisplayCustom> favorites;

    //Task
    private FavoriteTask favoriteTask;

    //Repo
    private FavoriteRepository favoriteRepo;

    //Instance unique du fragment
    public static FavoriteFragment newInstance() {
        Bundle args = new Bundle();

        FavoriteFragment fragment = new FavoriteFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Objets init
        initObjects();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }

        View view = inflater.inflate(R.layout.fragment_favorite, container, false);

        //Les vues
        findAllViews(view);

        //Recyclerview
        initRecyclerView();

        return view;
    }

    @Override
    public void onResume(){
        super.onResume();

        favorites.clear();
        launchTask();
    }

    @Override
    public void onDestroyView() {
        // On annule la requête
        this.appController.cancelPendingRequests(FAVORITES_TASK);

        super.onDestroyView();
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.btn_add_favorite :
                ((MainActivity) getActivity()).getMainTablayout().getTabAt(0).select();
                break;
        }
    }

    @Override
    public void onRefresh() {
        favorites.clear();
        launchTask();
    }

    //Les méthodes

    //Construit la liste des ids des favoris contenus dans le panier
    private ArrayList<String> buildIdsHexList(){
        List<Favorite> favorites = favoriteRepo.getAllFavorites();
        ArrayList<String> idsHex = new ArrayList<>();

        if(favorites.size() != 0) {
            for (Favorite favorite : favorites) {
                idsHex.add(favorite.getIdHexMarketOffer());
            }
        }

        return idsHex;
    }

    private void launchTask() {
        ArrayList<String> params = buildIdsHexList();
        if(params.size() != 0){
            favoriteTask.makeRequest(params);
        }else{
            swipeRefreshLayout.setRefreshing(false);

            if(favorites.size() != 0){
                favorites.clear();
                adapter.notifyDataSetChanged();
            }
            //On affiche le message
            llContainerEmpty.setVisibility(View.VISIBLE);
            //On cache le recyclerview
            recyclerView.setVisibility(View.GONE);
        }
    }

    //Init objects
    private void initObjects(){
        //Volley
        this.appController = AppController.getInstance();

        //Data
        this.favorites = new ArrayList<>();

        //Item decoration
        itemDecoration = new GridSpacingItemDecoration(2,
                CalculationUtils.dpToPx(getActivity(), 2), true);

        //Adaptateurs
        this.adapter = new FavoriteAdapter(getActivity(), this.favorites);

        //Layout managers
        this.layoutManager = new GridLayoutManager(getActivity(), 2);

        //Task
        this.favoriteTask = new FavoriteTask(this, appController);

        //Repo
        this.favoriteRepo = new FavoriteRepository(getContext());
    }

    private void findAllViews(View view) {
        ImageView ivEmptyFavorites = view.findViewById(R.id.iv_empty_favorite);
        GlideComponent.simpleGlideComponentWithoutPlaceholder(getContext(),
                UrlRestApiConfig.IMAGE_RESOURCE_URL + StringConfig.DEFAULT_EMPTY_FAVORITE, ivEmptyFavorites);
        llContainerEmpty = view.findViewById(R.id.container_empty_favorite);
        Button btnAddFavorites = view.findViewById(R.id.btn_add_favorite);
        btnAddFavorites.setOnClickListener(this);

        recyclerView = view.findViewById(R.id.favorites_recycler_view);
        swipeRefreshLayout = view.findViewById(R.id.favorite_swipe_refresh);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
        swipeRefreshLayout.setOnRefreshListener(this);
    }

    // Initialisation du recyclerview avec les données recues
    private void initRecyclerView(){
        //Layout manager
        recyclerView.setLayoutManager(layoutManager);
        //Adapter
        recyclerView.setAdapter(adapter);

        //Design
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(itemDecoration);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                MarketOfferDisplayCustom item = favorites.get(position);

                if(item instanceof MarketOfferDisplay){
                    MarketOfferDisplay marketOfferDisplay = (MarketOfferDisplay) item;

                    Intent intent = new Intent(getActivity(), MarketofferDetails.class);
                    intent.putExtra(StringConfig.INTENT_MARKETOFFER_HEX_ID,
                            marketOfferDisplay.getMarketOffer().getIdHexMarketOffer());
                    startActivity(intent);
                }
            }

            @Override
            public void onLongClick(View view, int position) {
            }

        }));
    }

    //Getters et setters

    public RecyclerView getRecyclerView() {
        return recyclerView;
    }

    public FavoriteAdapter getAdapter() {
        return adapter;
    }

    public List<MarketOfferDisplayCustom> getFavorites() {
        return favorites;
    }

    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return swipeRefreshLayout;
    }
}
