package com.inductivtechnologies.quantmarkapp.fragments.cart;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.activities.cart.ResumeOrdersActivity;
import com.inductivtechnologies.quantmarkapp.activities.main.MainActivity;
import com.inductivtechnologies.quantmarkapp.activities.account.UserLoginActivity;
import com.inductivtechnologies.quantmarkapp.adapters.cart.CartItemAdapter;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.bottomsheets.BottomsheetDeliveryMethod;
import com.inductivtechnologies.quantmarkapp.comparators.CartItemComparator;
import com.inductivtechnologies.quantmarkapp.components.GlideComponent;
import com.inductivtechnologies.quantmarkapp.configuration.StringConfig;
import com.inductivtechnologies.quantmarkapp.configuration.UrlRestApiConfig;
import com.inductivtechnologies.quantmarkapp.httptask.cart.CartTask;
import com.inductivtechnologies.quantmarkapp.models.display.MarketOfferDisplay;
import com.inductivtechnologies.quantmarkapp.models.entities.CartItem;
import com.inductivtechnologies.quantmarkapp.store.repositories.CartItemRepository;
import com.inductivtechnologies.quantmarkapp.store.session.SessionManager;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * CartFragment : Affichage des éléments du panier
 */
@SuppressWarnings("ConstantConditions")
public class CartFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener{

    public static final String CARTITEMS_TASK = "CARTITEMS_TASK";

    private Button btnOrder;
    private TextView tvTotal;

    public LinearLayout llContainerEmptyCart;

    // Recycler view
    private RecyclerView recyclerView;
    private CartItemAdapter adapter;

    private SwipeRefreshLayout swipeRefreshLayout;

    //Les données
    private List<CartItem> cartitems;

    //Volley instance singleton
    private AppController appController;
    //Repo
    private CartItemRepository cartRepo;
    // Session
    private SessionManager sessionManager;

    //Le task
    private CartTask cartTask;

    //Pour choisir la méthode de livraison
    private BottomsheetDeliveryMethod bstDeliveryMethod;

    //Méthode de livraison
    private int deliveryMethod;

    //Instance unique du fragment
    public static CartFragment newInstance() {
        Bundle args = new Bundle();

        CartFragment fragment = new CartFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Objets init
        initObjects();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }

        View view = inflater.inflate(R.layout.fragment_cart, container, false);

        //Les vues
        findAllViews(view);

        //Recyclerview
        initRecyclerView();

        return view;
    }

    @Override
    public void onResume(){
        super.onResume();

        //Init
        initCart();
    }

    @Override
    public void onDestroyView() {
        // On annule la requête
        this.appController.cancelPendingRequests(CARTITEMS_TASK);

        super.onDestroyView();
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.btn_buy_now_cart :
                if(sessionManager.isLoggedIn()){
                    bstDeliveryMethod.show();
                }else{
                    //Connxion
                    Intent intent = new Intent(getActivity(), UserLoginActivity.class);
                    startActivity(intent);
                }
                break;
            case R.id.btn_start_shopping :
                ((MainActivity) getActivity()).getMainTablayout().getTabAt(0).select();
                break;
        }
    }

    @Override
    public void onRefresh() {
        //Task
        initCart();
    }

    //Les méthodes

    //Construit la liste des ids des markets offers contenus dans le panier
    private ArrayList<String> buildIdsHexList(){
        List<CartItem> cartitems = cartRepo.getAllCartitems();
        ArrayList<String> idsHex = new ArrayList<>();

        if(cartitems.size() != 0) {
            for (CartItem cartitem : cartitems) {
                idsHex.add(cartitem.getIdHexMarketOffer());
            }
        }

        return idsHex;
    }

    private void findAllViews(View view) {
        ImageView ivEmptyCart = view.findViewById(R.id.iv_empty_cart);
        GlideComponent.simpleGlideComponentWithoutPlaceholder(getContext(),
                UrlRestApiConfig.IMAGE_RESOURCE_URL + StringConfig.DEFAULT_EMPTY_CART, ivEmptyCart);
        llContainerEmptyCart = view.findViewById(R.id.container_empty_cart);
        Button btnStartShopping = view.findViewById(R.id.btn_start_shopping);
        btnStartShopping.setOnClickListener(this);

        recyclerView = view.findViewById(R.id.cartitems__recycler_view);
        swipeRefreshLayout = view.findViewById(R.id.cartitem_swipe_refresh);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
        swipeRefreshLayout.setOnRefreshListener(this);

        btnOrder = view.findViewById(R.id.btn_buy_now_cart);
        btnOrder.setOnClickListener(this);
        btnOrder.setEnabled(false);
        btnOrder.setText(getResources().getString(R.string.text_display_label_order, cartitems.size()));
        tvTotal = view.findViewById(R.id.tv_total_cart);
    }

    // On raffraichi les données
    public  List<CartItem> refreshCartItems(final List<MarketOfferDisplay> marketOffersDisplay){
        List<CartItem> cartItemsFinal = cartRepo.refreshData(marketOffersDisplay);

        Collections.sort(cartItemsFinal, new CartItemComparator());
        return cartItemsFinal;
    }

    //Initialisation du panier
    private void initCart(){
        ArrayList<String> params = buildIdsHexList();
        if(params.size() != 0){
            cartTask.makeRequest(params);
        }else{
            swipeRefreshLayout.setRefreshing(false);

            if(cartitems.size() != 0){
                cartitems.clear();
                adapter.notifyDataSetChanged();

                //Le total
                setTvTotalCart();
            }
            //On affiche le message
            llContainerEmptyCart.setVisibility(View.VISIBLE);
            //On cache le recyclerview
            recyclerView.setVisibility(View.GONE);
            //On modifie le message du bouton
            getBtnOrder().setEnabled(false);
            btnOrder.setText(getResources()
                    .getString(R.string.text_display_label_order, cartitems.size()));
        }
    }

    // Initialisation du recyclerview avec les données recues
    private void initRecyclerView(){
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new CartItemAdapter(this, cartitems);
        recyclerView.setAdapter(adapter);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    //Init objects
    private void initObjects(){
        //Volley
        this.appController = AppController.getInstance();

        //La session
        sessionManager = new SessionManager(getContext());

        //Data
        this.cartitems = new ArrayList<>();

        //Task
        this.cartTask = new CartTask(this, appController);

        //Repo
        cartRepo = new CartItemRepository(getContext());

        bstDeliveryMethod = new BottomsheetDeliveryMethod(getActivity());
        bstDeliveryMethod.getmBtnActionSave().setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                deliveryMethod = bstDeliveryMethod.getDeliveryMethod();
                //On ferme
                bstDeliveryMethod.hide();

                if(deliveryMethod != -1){
                  Intent intent = new Intent(getActivity(), ResumeOrdersActivity.class);
                    intent.putExtra(StringConfig.INTENT_DELIVERY_METHOD, deliveryMethod);
                    intent.putExtra(StringConfig.INTENT_MARKETOFFER_HEX_ID, "");
                    startActivity(intent);
                }
            }
        });
    }

    public void setTvTotalCart() {
        double total = 0;
        for(CartItem cartitem : cartitems){
            total = total + cartitem.getTotalPrice();
        }

        if(total == 0){
            btnOrder.setEnabled(false);
        }else{
            btnOrder.setEnabled(true);
        }
        this.tvTotal.setText(getResources().getString(R.string.display_price, NumberFormat.getIntegerInstance().format(total)));
    }

    //Getters et setters

    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return swipeRefreshLayout;
    }

    public CartItemAdapter getAdapter() {
        return adapter;
    }

    public List<CartItem> getCartitems() {
        return cartitems;
    }

    public Button getBtnOrder() {
        return btnOrder;
    }

    public RecyclerView getRecyclerView() {
        return recyclerView;
    }
}
