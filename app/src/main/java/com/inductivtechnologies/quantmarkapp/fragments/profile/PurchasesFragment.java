package com.inductivtechnologies.quantmarkapp.fragments.profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.activities.profile.PurchaseDetailsActivity;
import com.inductivtechnologies.quantmarkapp.adapters.profile.PurchasesAdapter;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.components.GlideComponent;
import com.inductivtechnologies.quantmarkapp.configuration.NumberConfig;
import com.inductivtechnologies.quantmarkapp.configuration.StringConfig;
import com.inductivtechnologies.quantmarkapp.configuration.UrlRestApiConfig;
import com.inductivtechnologies.quantmarkapp.httptask.profile.PurchasesBuyerTask;
import com.inductivtechnologies.quantmarkapp.listeners.others.RecyclerTouchListener;
import com.inductivtechnologies.quantmarkapp.models.display.BuyDisplay;
import com.inductivtechnologies.quantmarkapp.models.display.LoadMoreDisplay;
import com.inductivtechnologies.quantmarkapp.models.utils.BuyDisplayCustom;
import com.inductivtechnologies.quantmarkapp.models.entities.User;
import com.inductivtechnologies.quantmarkapp.store.repositories.UserRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * ProfilePurchasesFragment : Affichage des achats de l'utilisateur
 */
public class PurchasesFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener,
    View.OnClickListener{

    public static final String PROFILE_PURCHASES_TASK = "PROFILE_PURCHASES_TASK";

    //Les vues
    private SwipeRefreshLayout swipeRefreshLayout;

    private LinearLayout containerEmpty;
    private ImageView ivEmpty;

    private RecyclerView recyclerview;

    private PurchasesAdapter adapter;

    //Controller
    private AppController appController;

    //Task
    private PurchasesBuyerTask purchaseTask;

    //Repositories
    private UserRepository userRepository;

    //Utilisateur
    private User currentUser = null;

    //Offset lors de l'interrogation de la base de données
    private int range = 0;

    //Les données
    private List<BuyDisplayCustom> purchaseItems;

    //Instance unique du fragment
    public static PurchasesFragment newInstance() {
        Bundle args = new Bundle();

        PurchasesFragment fragment = new PurchasesFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Init objets
        initObjects();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }

        View view = inflater.inflate(R.layout.fragment_profile_purchases, container, false);

        //Les vues
        allViews(view);

        //La liste des achats
        initRecyclerView(purchaseItems);

        //Task
        launchTask();

        return view;
    }

    @Override
    public void onDestroyView() {
        // On annule la requête
        this.appController.cancelPendingRequests(PROFILE_PURCHASES_TASK);

        super.onDestroyView();
    }

    @Override
    public void onClick(View view) {
    }

    @Override
    public void onRefresh() {
        purchaseItems.clear();
        range = 0;
        launchTask();
    }

    //Les méthodes

    private void launchTask() {
        if(currentUser != null){
            purchaseTask.makeRequest(currentUser, NumberConfig.NB_WIDTH_DATA, range);
        }
    }

    private void onLoadMoreData(){
        purchaseItems.remove(purchaseItems.size() - 1);
        adapter.notifyItemRemoved(purchaseItems.size());

        adapter.setWithLoadMore(false);

        //On charge les données de nouveau
        launchTask();
    }

    //Utilisée pour savoir si les données ont été totalemnt chargées et quand ajouter le load more
    public void loadMoreAsyn(boolean dataIsComplete){
        if(!dataIsComplete){
            purchaseItems.add(new LoadMoreDisplay());
            adapter.setWithLoadMore(true);
        }
    }

    private void initObjects() {
        //Repository
        userRepository = new UserRepository(getContext());

        currentUser = getCurrenUser();

        purchaseItems = new ArrayList<>();

        //Instance singleton du controlleur volley
        appController = AppController.getInstance();

        //Task
        purchaseTask = new PurchasesBuyerTask(this, appController);
    }

    //Utilisateur courant
    private User getCurrenUser(){
        List<User> users = userRepository.getAllUsers();
        if(users != null && !users.isEmpty()) return users.get(0);
        return null;
    }

    private void allViews(View root){
        swipeRefreshLayout = root.findViewById(R.id.profile_purchases_swipe_refresh_layout);
        //On modofie la couleur du progress
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
        swipeRefreshLayout.setOnRefreshListener(this);

        recyclerview = root.findViewById(R.id.purchases_recycler_view);

        containerEmpty = root.findViewById(R.id.container_empty_purchases);
        ivEmpty = root.findViewById(R.id.iv_empty_purchases);
    }

    // Initialisation du recyclerview avec les données recues
    private void initRecyclerView(final List<BuyDisplayCustom> purchaseItems){
        //Configuration
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        recyclerview.setLayoutManager(layoutManager);

        // Set adapter
        adapter = new PurchasesAdapter(getActivity(), purchaseItems);
        recyclerview.setAdapter(adapter);

        recyclerview.setItemAnimator(new DefaultItemAnimator());

        recyclerview.addOnItemTouchListener(new RecyclerTouchListener(getContext(),
                recyclerview, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                BuyDisplayCustom buyDisplayCustom = purchaseItems.get(position);

                if(buyDisplayCustom instanceof BuyDisplay){
                    BuyDisplay purchase = (BuyDisplay) buyDisplayCustom;

                    Intent intent = new Intent(getActivity(), PurchaseDetailsActivity.class);
                    intent.putExtra(StringConfig.INTENT_BUY_HEX_ID, purchase.getIdHexBuy());
                    startActivity(intent);
                }else{
                    onLoadMoreData();
                }
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));
    }

    //Getters et setters

    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return swipeRefreshLayout;
    }

    public void displayContainerEmpty(boolean isVisible) {
        if(isVisible){
            GlideComponent.initThumbmailProfile(getContext(),
                    UrlRestApiConfig.IMAGE_RESOURCE_URL + StringConfig.DEFAULT_EMPTY_PURCHASES, ivEmpty);
            containerEmpty.setVisibility(View.VISIBLE);
        }else{
            containerEmpty.setVisibility(View.GONE);
        }
    }

    public void setRange() {
        this.range += 10;
    }

    public RecyclerView getRecyclerview() {
        return recyclerview;
    }

    public PurchasesAdapter getAdapter() {
        return adapter;
    }

    public List<BuyDisplayCustom> getPurchaseItems() {
        return purchaseItems;
    }
}
