package com.inductivtechnologies.quantmarkapp.fragments.account;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.activities.account.UserLoginActivity;
import com.inductivtechnologies.quantmarkapp.activities.account.ValidateAccountActivity;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.bottomsheets.BottomSheetProgress;
import com.inductivtechnologies.quantmarkapp.callbacks.HttpCallback;
import com.inductivtechnologies.quantmarkapp.configuration.StringConfig;
import com.inductivtechnologies.quantmarkapp.httptask.account.SignupTask;
import com.inductivtechnologies.quantmarkapp.models.entities.Address;
import com.inductivtechnologies.quantmarkapp.models.entities.Phone;
import com.inductivtechnologies.quantmarkapp.models.entities.Token;
import com.inductivtechnologies.quantmarkapp.models.entities.User;
import com.inductivtechnologies.quantmarkapp.models.http.HttpResponseObject;
import com.inductivtechnologies.quantmarkapp.store.repositories.AddressRepository;
import com.inductivtechnologies.quantmarkapp.store.repositories.PhoneRepository;
import com.inductivtechnologies.quantmarkapp.store.repositories.TokenRepository;
import com.inductivtechnologies.quantmarkapp.store.repositories.UserRepository;
import com.inductivtechnologies.quantmarkapp.store.session.SessionManager;
import com.inductivtechnologies.quantmarkapp.utils.DesignUtils;
import com.inductivtechnologies.quantmarkapp.utils.FormsUtils;
import com.inductivtechnologies.quantmarkapp.utils.StringUtils;

import org.json.JSONObject;
import java.util.Date;

/**
 * FragmentSignup : Pour l'inscription
 */
public class FragmentSignup extends Fragment implements View.OnClickListener, HttpCallback{

    public static final String SIGNUP_TASK = "SIGNUP_TASK";
    public static final String INTENT_VALIDATION_ACCOUNT_REQUEST = "INTENT_VALIDATION_ACCOUNT_REQUEST";

    private TextInputLayout inputLayoutLastname;
    private TextInputEditText etLastname;
    private TextInputLayout inputLayoutFirstname;
    private TextInputEditText etFirstname;
    private TextInputLayout inputLayoutEmail;
    private TextInputEditText etEmail;
    private TextInputLayout inputLayoutLogin;
    private TextInputEditText etLogin;
    private TextInputLayout inputLayoutPassword;
    private TextInputEditText etpassword;
    private TextInputLayout inputLayoutPhone;
    private TextInputEditText etPhone;

    //Controlleur
    private AppController appController;
    //Session manager
    private SessionManager sessionManager;

    //Repositories
    private TokenRepository tokenRepo;
    private UserRepository userRepo;
    private AddressRepository addressRepo;
    private PhoneRepository phoneRepo;

    //Dialog
    private BottomSheetProgress progress;

    private FormsUtils formsUtils;

    //Instance unique du fragment
    public static FragmentSignup newInstance() {
        Bundle args = new Bundle();

        FragmentSignup fragment = new FragmentSignup();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Initialisation des objets
        initObjects();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }

        View view = inflater.inflate(R.layout.fragment_signup, container, false);

        //Les vues
        findAllViews(view);

        return view;
    }

    @Override
    public void onDestroyView() {
        // On annule la requête
        this.appController.cancelPendingRequests(SIGNUP_TASK);

        super.onDestroyView();
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.btn_signup :
                makeSignup();
                break;
        }
    }

    //Les méthodes

    private void initObjects() {
        //Instance singleton du controlleur volley
        appController = AppController.getInstance();

        this.tokenRepo = new TokenRepository(getActivity());
        this.userRepo = new UserRepository(getActivity());
        this.addressRepo = new AddressRepository(getActivity());
        this.phoneRepo = new PhoneRepository(getActivity());

        this.sessionManager = new SessionManager(getActivity());

        //Progress
        this.progress = new BottomSheetProgress(getActivity());

        formsUtils = new FormsUtils(getActivity().getApplicationContext());
    }

    //Les vues
    private void findAllViews(View root){
        Button btnSignup = root.findViewById(R.id.btn_signup);
        btnSignup.setOnClickListener(this);

        inputLayoutLastname = root.findViewById(R.id.tl_lastname);
        etLastname = root.findViewById(R.id.lastname);
        etLastname.addTextChangedListener(new SignupTextWatcher(etLastname));

        inputLayoutFirstname = root.findViewById(R.id.tl_firstname);
        etFirstname = root.findViewById(R.id.firstname);
        etFirstname.addTextChangedListener(new SignupTextWatcher(etFirstname));

        inputLayoutEmail = root.findViewById(R.id.tl_email);
        etEmail = root.findViewById(R.id.email);
        etEmail.addTextChangedListener(new SignupTextWatcher(etEmail));

        inputLayoutLogin = root.findViewById(R.id.tl_login);
        etLogin = root.findViewById(R.id.login);
        etLogin.addTextChangedListener(new SignupTextWatcher(etLogin));

        inputLayoutPassword = root.findViewById(R.id.tl_password);
        etpassword = root.findViewById(R.id.password);
        etpassword.addTextChangedListener(new SignupTextWatcher(etpassword));

        inputLayoutPhone = root.findViewById(R.id.tl_phone);
        etPhone = root.findViewById(R.id.phone);
        etPhone.addTextChangedListener(new SignupTextWatcher(etPhone));
    }

    //Task Http

    private void makeSignup() {
        String lastname = etLastname.getText().toString().trim();
        String firstname = etFirstname.getText().toString().trim();
        String email = etEmail.getText().toString().trim();
        String password = etpassword.getText().toString().trim();
        String login = etLogin.getText().toString().trim();
        String phone = etPhone.getText().toString().trim();

        boolean validLastName = formsUtils.validateSimpleInput(inputLayoutLastname, lastname);
        boolean validFirstname = formsUtils.validateSimpleInput(inputLayoutFirstname, firstname);
        boolean validEmail = formsUtils.validateEmail(inputLayoutEmail, email);
        boolean validLogin = formsUtils.validateLogin(inputLayoutLogin, login);
        boolean validPassword = formsUtils.validatePassword(inputLayoutPassword, password);
        boolean validPhone = formsUtils.validatePhoneNumberWithRegex(inputLayoutPhone, phone);

        if(validLastName && validFirstname && validEmail && validLogin && validPassword && validPhone){
            User user = new User();

            //Adresse
            Address address = new Address();
            address.setEmail(email);
            address.setCountry(StringConfig.getDefaultArrayCountries(getActivity().getApplicationContext())[0]);
            address.setCity(StringConfig.cities[0]);
            user.setAddress(address);

            user.setPhone(new Phone("Portable", phone));

            user.setLogin(login);
            user.setPassword(password);
            user.setName(StringUtils.toUpperFirstChar(lastname));
            user.setFirstName(StringUtils.toUpperFirstChar(firstname));
            user.setImage("");
            user.setBirthDate(null);

            user.setCreationDate(new Date());
            user.setSexe(null);

            //Task
            new SignupTask(this, appController).makeRequest(user);
        }
    }

    private void updateProgress(boolean isVisible){
        if(isVisible){
            if(progress != null)
                progress.show(getResources().getString(R.string.signup_progress_message));
        }else{
            if(progress != null)
                progress.hide();
        }
    }

    private void makeSnackbar(String message){
        DesignUtils.makeSnackbar(message,
                ((UserLoginActivity) getActivity()).getCoordinatorLayout(), getContext());
    }

    @Override
    public void beforeExecute() {
        updateProgress(true);
    }

    @Override
    public void afterExecute(JSONObject response) {
        //Vérification
        final int statusCode = response.optInt(HttpResponseObject.CODE_STR);

        switch(statusCode) {
            case HttpResponseObject.SUCCESS_CODE:
                HttpResponseObject responseObject = new HttpResponseObject(response);
                responseObject.addUserSignupResults(response);

                //On enregistre l'utilisateur et son token
                //Token
                Token token = (Token) responseObject.getObjectValue().get(HttpResponseObject.TOKEN_STR);
                //Buyer
                User user = (User) responseObject.getObjectValue().get(HttpResponseObject.BUYER_STR);
                user.setToken(token);

                //Code de validation
                String code = (String) responseObject.getObjectValue().get(HttpResponseObject.VALIDATION_CODE_STR);

                Log.i("TAG", code);

                // On insère dans la base de données
                addressRepo.insertAddress(user.getAddress());
                phoneRepo.insertPhone(user.getPhone());
                tokenRepo.insertToken(user.getToken());
                userRepo.insertUser(user);

                //On ouvre la session de connexion
                sessionManager.createLoginSession();
                sessionManager.addValidationCode(code);

                //On termine l'activité
                getActivity().finish();

                //Validation du compte
                Intent intent = new Intent(getActivity(), ValidateAccountActivity.class);
                intent.putExtra(INTENT_VALIDATION_ACCOUNT_REQUEST, 120);
                startActivity(intent);

                break;
            case HttpResponseObject.UNKNOW_USER_ERROR_CODE:
                makeSnackbar(getResources().getString(R.string.unknow_user_error_message));
                break;
            case HttpResponseObject.LOGIN_USER_EXIST_ERROR_CODE:
                makeSnackbar(getActivity().getResources().getString(R.string.display_message_login_exist));
                break;
            case HttpResponseObject.EMAIL_USER_EXIST_ERROR_CODE:
                makeSnackbar(getResources().getString(R.string.display_message_email_exist));
                break;
        }

        updateProgress(false);
    }

    @Override
    public void whenErrors() {
    }

    private class SignupTextWatcher implements TextWatcher {

        private final View view;

        private SignupTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.lastname:
                    formsUtils.validateSimpleInput(inputLayoutLastname, etLastname.getText().toString().trim());
                    break;
                case R.id.firstname:
                    formsUtils.validateSimpleInput(inputLayoutFirstname, etFirstname.getText().toString().trim());
                    break;
                case R.id.email:
                    formsUtils.validateEmail(inputLayoutEmail, etEmail.getText().toString().trim());
                    break;
                case R.id.login:
                    formsUtils.validateLogin(inputLayoutLogin, etLogin.getText().toString().trim());
                    break;
                case R.id.password:
                    formsUtils.validatePassword(inputLayoutPassword, etpassword.getText().toString().trim());
                    break;
                case R.id.phone:
                    formsUtils.validatePhoneNumberWithRegex(inputLayoutPhone, etPhone.getText().toString().trim());
                    break;
            }
        }
    }
}