package com.inductivtechnologies.quantmarkapp.fragments.profile;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.activities.account.ValidateAccountActivity;
import com.inductivtechnologies.quantmarkapp.activities.profile.ProfileUpdateSectionAddressActivity;
import com.inductivtechnologies.quantmarkapp.activities.profile.ProfileUpdateSectionConfActivity;
import com.inductivtechnologies.quantmarkapp.activities.profile.ProfileUpdateSectionEmailActivity;
import com.inductivtechnologies.quantmarkapp.activities.profile.ProfileUpdateSectionIdentificationActivity;
import com.inductivtechnologies.quantmarkapp.activities.profile.UserProfileActivity;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.bottomsheets.BottomsheetDate;
import com.inductivtechnologies.quantmarkapp.bottomsheets.BottomsheetGender;
import com.inductivtechnologies.quantmarkapp.components.GlideComponent;
import com.inductivtechnologies.quantmarkapp.configuration.StringConfig;
import com.inductivtechnologies.quantmarkapp.configuration.UrlRestApiConfig;
import com.inductivtechnologies.quantmarkapp.design.DisplayLargeNumber;
import com.inductivtechnologies.quantmarkapp.httptask.profile.BuyerPropertiesTask;
import com.inductivtechnologies.quantmarkapp.httptask.account.LogoutTask;
import com.inductivtechnologies.quantmarkapp.httptask.profile.UpdateProfileSectionCommonTask;
import com.inductivtechnologies.quantmarkapp.models.display.PropertiesNumber;
import com.inductivtechnologies.quantmarkapp.models.entities.User;
import com.inductivtechnologies.quantmarkapp.store.repositories.UserRepository;
import com.inductivtechnologies.quantmarkapp.utils.DesignUtils;
import com.inductivtechnologies.quantmarkapp.utils.MomentUtils;
import com.inductivtechnologies.quantmarkapp.utils.StringUtils;

import java.util.Date;
import java.util.List;

/**
 * ProfileInfoFragment : Affichage des informations de l'utilisateur
 */
public class ProfileInfoFragment extends Fragment implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener{

    private final static int UPDATE_CONF_SECTION_INTENT_REQUESTCODE = 30;

    public static final String PROFILE_PROPERTIES_TASK = "PROFILE_PROPERTIES_TASK";
    public static final String PROFILE_LOGOUT_TASK = "PROFILE_LOGOUT_TASK";
    public static final String PROFILE_UPDATE_COMMON_SECTION_TASK = "PROFILE_UPDATE_COMMON_SECTION_TASK";

    //Les vues
    private SwipeRefreshLayout swipeRefreshLayout;

    private ImageView ivThumbmail;

    private TextView tvQuantityPurchases;
    private TextView tvQuantityOrders;
    private TextView tvQuantityPoints;
    private TextView tvLabelQuantityPurchases;
    private TextView tvLabelQuantityOrders;
    private TextView tvLabelQuantityPoints;

    private TextView tvHeaderFirstname;
    private TextView tvHeaderLastname;
    private TextView tvEmail;

    //Section identification
    private TextView tvLastname;
    private TextView tvFirstname;

    //Email et numero
    private TextView tvEmailP;
    private TextView tvPhone;
    private LinearLayout llContainetValidateEmail;

    //Section date et genre
    private TextView tvGender;
    private TextView tvBirthdate;

    //Section adresse
    private TextView tvCountry;
    private TextView tvCity;
    private TextView tvDistrict;
    private TextView tvPobox;

    //Conf section
    private TextView tvLogin;

    //Controller
    private AppController appController;

    //Task
    private BuyerPropertiesTask buyerPropertiesTask;

    //Repositories
    private UserRepository userRepository;

    //Dialog gender
    private BottomsheetGender bshGender;
    //Dialog date
    private BottomsheetDate bsdDate;

    //Instance unique du fragment
    public static ProfileInfoFragment newInstance() {
        Bundle args = new Bundle();

        ProfileInfoFragment fragment = new ProfileInfoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Init des objets
        initObjects();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }

        View view = inflater.inflate(R.layout.fragment_profile_info, container, false);

        // Les vues
        findAllViews(view);

        launchTask();

        return view;
    }

    @Override
    public void onDestroyView() {
        // On annule la requête
        this.appController.cancelPendingRequests(PROFILE_PROPERTIES_TASK);
        this.appController.cancelPendingRequests(PROFILE_UPDATE_COMMON_SECTION_TASK);

        super.onDestroyView();
    }

    @Override
    public void onResume(){
        super.onResume();;

        //Init globale
        initProfile();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch(requestCode){
            case UPDATE_CONF_SECTION_INTENT_REQUESTCODE :

                if(resultCode == Activity.RESULT_OK) {
                    DesignUtils.makeSnackbar(getContext().getString(R.string.update_login_password_message),
                            ((UserProfileActivity) getActivity()).getCoordinatorLayout(), getContext());
                }

                break;
        }
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        User currentUser = getCurrenUser();

        switch(view.getId()){
            case R.id.btn_profile_logout :
                buildDialog(currentUser).show();
                break;
            case R.id.btn_profile_edit_identification_section :
                intent = new Intent(getContext(), ProfileUpdateSectionIdentificationActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_profile_edit_email_section :
                intent = new Intent(getContext(), ProfileUpdateSectionEmailActivity.class);
                startActivity(intent);
                break;
            case R.id.btn_profile_edit_confidentiality_section :
                intent = new Intent(getContext(), ProfileUpdateSectionConfActivity.class);
                startActivityForResult(intent, UPDATE_CONF_SECTION_INTENT_REQUESTCODE);
                break;
            case R.id.btn_profile_edit_address_section :
                intent = new Intent(getContext(), ProfileUpdateSectionAddressActivity.class);
                startActivity(intent);
                break;
            case R.id.container_gender_value :
                bshGender.show();
                break;
            case R.id.container_birthdate_value :

                if(currentUser != null){
                    bsdDate.show(currentUser.getBirthDate() == null ? new Date() : currentUser.getBirthDate());
                }

                break;
            case R.id.tv_email_validation :
                startActivity(new Intent(getActivity(), ValidateAccountActivity.class));
                break;
        }
    }

    @Override
    public void onRefresh() {
        launchTask();
    }

    //Les méthodes

    private void launchTask(){
        User currentUser = getCurrenUser();

        if(currentUser != null){
            buyerPropertiesTask.makeRequest(currentUser.getToken().getTokenValue(), currentUser.getObjectIdHexUser());
        }
    }

    private void initObjects() {
        //Repository
        userRepository = new UserRepository(getContext());

        //Instance singleton du controlleur volley
        appController = AppController.getInstance();

        //Task
        buyerPropertiesTask = new BuyerPropertiesTask(this, appController);

        bshGender = new BottomsheetGender(getContext());
        bshGender.getmBtnActionSave().setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(bshGender.getGenderValue() != null && !bshGender.getGenderValue().isEmpty()){
                    //On ferme
                    bshGender.hide();

                    //On modifie l'utilisateur
                    User currentUser = getCurrenUser();
                    if(currentUser != null && !currentUser.getSexe().equalsIgnoreCase(bshGender.getGenderValue())){
                        currentUser.setSexe(bshGender.getGenderValue());

                        //On lance le task
                        new UpdateProfileSectionCommonTask(ProfileInfoFragment.this, appController).makeRequest(currentUser);
                    }
                }
            }
        });

        bsdDate = new BottomsheetDate(getContext());
        bsdDate.getBtnActionSave().setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //La date selectionnée
                Date birthdDate = bsdDate.getDateSelect();
                //La date actuelle
                Date now = new Date();

                if(birthdDate.getTime() > now.getTime()){
                    Toast.makeText(getActivity(), getResources().getString(R.string.invalid_date_message),
                            Toast.LENGTH_SHORT).show();
                }else{
                    //On ferme
                    bsdDate.hide();

                    //On modifie l'utilisateur
                    User currentUser = getCurrenUser();
                    if(currentUser != null){
                        if(currentUser.getBirthDate() != null){
                            if(birthdDate.getTime() != currentUser.getBirthDate().getTime()){
                                currentUser.setBirthDate(birthdDate);

                                //On lance le task
                                new UpdateProfileSectionCommonTask(ProfileInfoFragment.this,
                                        appController).makeRequest(currentUser);
                            }
                        }else{
                            currentUser.setBirthDate(birthdDate);

                            //On lance le task
                            new UpdateProfileSectionCommonTask(ProfileInfoFragment.this,
                                    appController).makeRequest(currentUser);
                        }
                    }

                }
            }
        });
    }

    //Utilisateur courant
    private User getCurrenUser(){
        List<User> users = userRepository.getAllUsers();
        if(users != null && !users.isEmpty()) return users.get(0);
        return null;
    }

    private void findAllViews(View rootView){
        swipeRefreshLayout = rootView.findViewById(R.id.profile_info_swipe_refresh_layout);
        //On modofie la couleur du progress
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
        swipeRefreshLayout.setOnRefreshListener(this);

        ivThumbmail = rootView.findViewById(R.id.profile_thumbmail);

        tvQuantityPurchases = rootView.findViewById(R.id.profile_quantity_purchase);
        tvQuantityOrders = rootView.findViewById(R.id.profile_quantity_order);
        tvQuantityPoints = rootView.findViewById(R.id.profile_quantity_point);
        tvLabelQuantityPurchases = rootView.findViewById(R.id.profile_label_quantity_purchase);
        tvLabelQuantityOrders = rootView.findViewById(R.id.profile_label_quantity_order);
        tvLabelQuantityPoints = rootView.findViewById(R.id.profile_label_quantity_point);

        tvHeaderFirstname = rootView.findViewById(R.id.profile_firstname);
        tvHeaderLastname = rootView.findViewById(R.id.profile_lastname);
        tvEmail = rootView.findViewById(R.id.profile_user_email);

        tvLastname = rootView.findViewById(R.id.tv_profile_name_value);
        tvFirstname = rootView.findViewById(R.id.tv_profile_firstname_value);
        ImageButton btnSetIdenSection = rootView.findViewById(R.id.btn_profile_edit_identification_section);
        btnSetIdenSection.setOnClickListener(this);

        tvEmailP = rootView.findViewById(R.id.tv_profile_email_value);
        tvPhone = rootView.findViewById(R.id.tv_profile_phone_value);
        ImageButton btnSetEmailSection = rootView.findViewById(R.id.btn_profile_edit_email_section);
        btnSetEmailSection.setOnClickListener(this);

        llContainetValidateEmail = rootView.findViewById(R.id.container_notice_email_validation);
        TextView tvValidateEmail = rootView.findViewById(R.id.tv_email_validation);
        tvValidateEmail.setOnClickListener(this);

        tvGender = rootView.findViewById(R.id.tv_profile_gender_value);
        tvBirthdate = rootView.findViewById(R.id.tv_profile_birthdate_value);
        LinearLayout llContainerGender = rootView.findViewById(R.id.container_gender_value);
        llContainerGender.setOnClickListener(this);
        LinearLayout llContainerBirthdate = rootView.findViewById(R.id.container_birthdate_value);
        llContainerBirthdate.setOnClickListener(this);

        tvCountry = rootView.findViewById(R.id.tv_profile_country_value);
        tvCity = rootView.findViewById(R.id.tv_profile_city_value);
        tvDistrict = rootView.findViewById(R.id.tv_profile_district_value);
        tvPobox = rootView.findViewById(R.id.tv_profile_pobox_value);
        ImageButton btnSetAddressSection = rootView.findViewById(R.id.btn_profile_edit_address_section);
        btnSetAddressSection.setOnClickListener(this);

        tvLogin = rootView.findViewById(R.id.tv_profile_login_value);
        ImageButton btnSetConfSection = rootView.findViewById(R.id.btn_profile_edit_confidentiality_section);
        btnSetConfSection.setOnClickListener(this);

        Button btnLogout = rootView.findViewById(R.id.btn_profile_logout);
        btnLogout.setOnClickListener(this);
    }

    private void initProfile(){
        User currentUser = getCurrenUser();

        if(currentUser != null){
            //Initialise l'image de profile
            initThumbmailProfile(currentUser.getImage(),
                    currentUser.getFirstName().substring(0,1) + currentUser.getName().substring(0,1));

            //Le nom et l'email
            initNameAndEmail(currentUser.getName(), currentUser.getFirstName(), currentUser.getAddress().getEmail());

            //Section identification
            iniIdentificationSection(currentUser.getName(), currentUser.getFirstName());

            initEmailPhoneSection(currentUser.getAddress().getEmail(),
                    currentUser.getPhone().getNumber(), currentUser.getAddress().isValide());

            //Date et sex
            initGenderDateSection(currentUser.getSexe(), currentUser.getBirthDate());

            //Adresse
            initAddressSection(currentUser.getAddress().getCountry(), currentUser.getAddress().getCity(),
                    currentUser.getAddress().getStreet(), currentUser.getAddress().getPoBox());

            //Conf section
            initConfSection(currentUser.getLogin());
        }
    }

    //Initialise l'image de profile
    private void initThumbmailProfile(String imageName, String name) {
        if(imageName == null || imageName.isEmpty()){
            TextDrawable textDrawable = TextDrawable.builder()
                    .beginConfig()
                    .withBorder(1)
                    .useFont(Typeface.DEFAULT)
                    .textColor(Color.parseColor("#FFFFFF"))
                    .fontSize(44)
                    .toUpperCase()
                    .bold()
                    .endConfig()
                    .buildRound(name, Color.parseColor("#0277BD"));

            ivThumbmail.setImageDrawable(textDrawable);
        }else{
            GlideComponent.initThumbmailProfile(getContext(),
                    UrlRestApiConfig.IMAGE_RESOURCE_URL + imageName, ivThumbmail);
        }
    }

    public void initQuantitySection(PropertiesNumber userProperties){

        long qPurchase = userProperties.getNbOfPurchases();
        long qOrders = userProperties.getNbOfOrders();
        long qPoint = userProperties.getNbPoints();

        tvLabelQuantityPurchases.setText(qPurchase > 1 ? getResources().getString(R.string.profile_text_purchases_plural):
        getResources().getString(R.string.profile_text_purchases_sin));
        tvQuantityPurchases.setText(DisplayLargeNumber.smartLargeNumber(qPurchase));

        tvLabelQuantityOrders.setText(qOrders > 1 ? getResources().getString(R.string.profile_text_order_plural):
                getResources().getString(R.string.profile_text_order_sin));
        tvQuantityOrders.setText(DisplayLargeNumber.smartLargeNumber(qOrders));

        tvLabelQuantityPoints.setText(qPoint > 1 ? getResources().getString(R.string.profile_text_points_plural):
                getResources().getString(R.string.profile_text_points_sin));
        tvQuantityPoints.setText(DisplayLargeNumber.smartLargeNumber(qPoint));
    }

    private void initNameAndEmail(String lastname, String firstname, String email){
        if(lastname.contains(" ")){
            String[] lastnameParts = lastname.split(" ");
            tvHeaderLastname.setText(StringUtils.toUpperFirstChar(lastnameParts[0]));
        }else{
            tvHeaderLastname.setText(StringUtils.toUpperFirstChar(lastname));
        }

        if(firstname.contains(" ")){
            String[] firtsnameParts = firstname.split(" ");
            tvHeaderFirstname.setText(StringUtils.toUpperFirstChar(firtsnameParts[0]));
        }else{
            tvHeaderFirstname.setText(StringUtils.toUpperFirstChar(firstname));
        }

        tvEmail.setText(email);
    }

    private void iniIdentificationSection(String name, String firstname){
        tvLastname.setText(StringUtils.toUpperFirstChar(name));
        tvFirstname.setText(StringUtils.toUpperFirstChar(firstname));
    }

    private void initEmailPhoneSection(String email, String phone, boolean emailValidate){
        tvEmailP.setText(email);
        if(emailValidate){
            llContainetValidateEmail.setVisibility(View.GONE);
        }else{
            llContainetValidateEmail.setVisibility(View.VISIBLE);
        }

        String phoneStr;
        if(phone.isEmpty()){
            tvPhone.setText(getResources().getString(R.string.nothing_message));
        }else{
            if(phone.length() == 9){
                phoneStr = StringConfig.NB_IND_PHONE_CMR + "" + phone;
                tvPhone.setText(StringUtils.displayPhoneNumber(phoneStr));
            }else{
                tvPhone.setText(StringUtils.displayPhoneNumber(phone));
            }
        }
    }

    public void initGenderDateSection(String gender, Date birthdate){
        if(gender.isEmpty()){
            tvGender.setText(getResources().getString(R.string.nothing_message));
        }else{
            int position = StringUtils.getPositionOfStringInArray(StringConfig.getDefaultArrayGender(getContext()), gender);
            if(position != -1){
                tvGender.setText(StringConfig.getLocaleArrayGender(getContext())[position]);
            }else{
                tvGender.setText(gender);
            }
        }

        if(birthdate == null){
            tvBirthdate.setText(getResources().getString(R.string.nothing_message));
        }else{
            tvBirthdate.setText(MomentUtils.getRelativeDate(birthdate));
        }
    }

    private void initAddressSection(String country, String city, String district, String pobox){
        //Country
        if(country.isEmpty()){
            tvCountry.setText(getResources().getString(R.string.nothing_message));
        }else{
            int position = StringUtils.getPositionOfStringInArray(StringConfig.getDefaultArrayCountries(getContext()), country);
            if(position != -1){
                tvCountry.setText(StringConfig.getLocaleArrayCountries(getContext())[position]);
            }else{
                tvCountry.setText(country);
            }
        }

        //City
        if(city.isEmpty()){
            tvCity.setText(getResources().getString(R.string.nothing_message));
        }else{
            tvCity.setText(city);
        }

        //District
        if(district.isEmpty()){
            tvDistrict.setText(getResources().getString(R.string.nothing_message));
        }else{
            tvDistrict.setText(StringUtils.toUpperFirstChar(district));
        }

        //Pobox
        if(pobox.isEmpty()){
            tvPobox.setText(getResources().getString(R.string.nothing_message));
        }else{
            tvPobox.setText(pobox);
        }
    }

    private void initConfSection(String login){
        tvLogin.setText(login);
    }

    private AlertDialog buildDialog(final User currentUser){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.label_profile_logout));
        builder.setMessage(getString(R.string.profil_dialog_logout_message));

        String positiveText = getString(android.R.string.ok);
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // On annule la commande
                        dialog.dismiss();
                        if(currentUser != null){
                            new LogoutTask((UserProfileActivity) getActivity(), appController).makeRequest(currentUser);
                        }
                    }
                });

        String negativeText = getString(android.R.string.cancel);
        builder.setNegativeButton(negativeText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //On ferme tout simplement
                        dialog.dismiss();
                    }
                });

        return builder.create();
    }

    //Getters et setters

    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return swipeRefreshLayout;
    }
}
