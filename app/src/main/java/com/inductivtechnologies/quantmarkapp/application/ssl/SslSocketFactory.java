package com.inductivtechnologies.quantmarkapp.application.ssl;

import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import khandroid.ext.apache.http.conn.ssl.SSLSocketFactory;

/**
 * SslSocketFactory
 */
class SslSocketFactory extends SSLSocketFactory {

    public SslSocketFactory(InputStream keyStore, String keyStorePassword) throws GeneralSecurityException {
        super(createSSLContext(keyStore, keyStorePassword), STRICT_HOSTNAME_VERIFIER);
    }


    private static SSLContext createSSLContext(InputStream keyStore, String keyStorePassword) throws GeneralSecurityException {
        SSLContext sslcontext;
        try {
            sslcontext = SSLContext.getInstance("TLS");
            sslcontext.init(null, new TrustManager[] { new SsX509TrustManager(keyStore, keyStorePassword) }, null);
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException("Echec initialisation de l'objet SSLContext", e);
        } catch (KeyManagementException e) {
            throw new IllegalStateException("Echec initialisation de l'objet SSLContext", e);
        }

        return sslcontext;
    }
}    