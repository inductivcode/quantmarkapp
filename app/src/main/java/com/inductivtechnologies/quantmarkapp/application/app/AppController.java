package com.inductivtechnologies.quantmarkapp.application.app;

import android.app.Application;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.application.ssl.ExtHttpClientStack;
import com.inductivtechnologies.quantmarkapp.application.ssl.SslHttpClient;
import com.inductivtechnologies.quantmarkapp.configuration.StringConfig;

import java.io.InputStream;

/**
 * AppController : Controlleur global de l'application
 */
@SuppressWarnings({"FieldCanBeLocal", "unused"})
public class AppController extends Application {

    private static final String TAG = AppController.class.getSimpleName();

    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    private InputStream keyStore;

    private static AppController singleInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        singleInstance = this;
        keyStore = getApplicationContext().getResources().openRawResource(R.raw.keystore_quantmark_cm_com);
    }

    public static synchronized AppController getInstance() {
        return singleInstance;
    }

    private RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext(), new ExtHttpClientStack(new SslHttpClient(keyStore, StringConfig.SSL_KEYSTORE_PASSWORD)));
            //mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return mRequestQueue;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue,
                    new LruBitmapCache());
        }
        return this.mImageLoader;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
}
