package com.inductivtechnologies.quantmarkapp.bottomsheets;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.utils.MomentUtils;

import java.util.Calendar;
import java.util.Date;

import biz.kasual.materialnumberpicker.MaterialNumberPicker;

/**
 * BottomsheetDate : Permet de sélectionner la date de naissance de l'utilisateur
 */
@SuppressWarnings({"SameParameterValue", "ConstantConditions"})
public class BottomsheetDate implements NumberPicker.OnValueChangeListener{

    // Le conteneur secondaire
    private final Dialog dialog;

    // Le conteneur principale
    //private Window window;

    /// Le layout emglobant
    private final View layoutView;

    // Composants graphiques
    private MaterialNumberPicker npDay;
    private MaterialNumberPicker npMonth;
    private MaterialNumberPicker npYear;

    private ImageButton btnActionSave;

    private int dayValue;
    private int monthValue;
    private int yearValue;

    private int npCurrentDay;

    private final String[] strArrayMonths;

    @SuppressLint("InflateParams")
    public BottomsheetDate(Context context){

        this.strArrayMonths = context.getResources()
                .getStringArray(R.array.months_array);

        this.layoutView = LayoutInflater
                .from(context)
                .inflate(R.layout.content_bottomsheet_date, null);

        this.dialog = new Dialog(context, R.style.AppTheme_DialogSheetProgress);
        this.dialog.setContentView(this.layoutView);
        this.dialog.setCancelable(true);
        this.dialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        this.dialog.getWindow().setGravity(Gravity.BOTTOM);

        // Find views
        findViews();

        // Add listeners
        addListeners();
    }

    //Override

    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
        switch(picker.getId()){
            case R.id.np_date_day_value:
                
                dayValue = newVal;
                
                break;
            case R.id.np_date_month_value:
                
                npDay.setMaxValue(MomentUtils.howManyDaysInMonth(npCurrentDay, newVal));
                dayValue = npDay.getValue();
                monthValue = newVal;
                
                break;
            case R.id.np_date_year_value:
                
                yearValue = newVal;
                
                break;
        }
    }

    // Methodes

    private void findViews() {
        this.npDay = this.layoutView.findViewById(R.id.np_date_day_value);
        this.npMonth = this.layoutView.findViewById(R.id.np_date_month_value);
        this.npYear = this.layoutView.findViewById(R.id.np_date_year_value);

        this.btnActionSave = this.layoutView.findViewById(R.id.btn_action_save);
    }

    private void addListeners() {
        npDay.setOnValueChangedListener(this);
        npMonth.setOnValueChangedListener(this);
        npYear.setOnValueChangedListener(this);
    }

    //Jour
    private void initNumberPickerDay(int min, int max, int current){
        npDay.setMinValue(min);
        npDay.setMaxValue(max);
        npDay.setValue(current);
        npDay.setWrapSelectorWheel(true);

        npDay.setFormatter(new NumberPicker.Formatter() {

            @SuppressLint("DefaultLocale")
            @Override
            public String format(int value) {
                if (value >= 10) return String.valueOf(value);
                return String.format("0%d", value);
            }
        });

        dayValue = npDay.getValue();
    }

    //Mois
    private void initNumberPickerMonth(int min, int max, int current){
        npMonth.setMinValue(min);
        npMonth.setMaxValue(max);
        npMonth.setValue(current);
        npMonth.setWrapSelectorWheel(true);

        npMonth.setFormatter(new NumberPicker.Formatter() {
            @Override
            public String format(int value) {
                return strArrayMonths[value - 1];
            }
        });

        monthValue = npMonth.getValue();
    }

    //Annee
    private void initNumberPickerYear(int min, int max, int current){
        npYear.setMinValue(min);
        npYear.setMaxValue(max);
        npYear.setValue(current);

        yearValue = npYear.getValue();
    }

    public Date getDateSelect() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, yearValue);
        calendar.set(Calendar.MONTH, monthValue - 1);
        calendar.set(Calendar.DAY_OF_MONTH, dayValue);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        return calendar.getTime();
    }

    //Ouvre
    public void show(Date currentDate){
        if(currentDate != null){
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(currentDate);
            this.npCurrentDay = calendar.get(Calendar.DAY_OF_MONTH);
            int npCurrentMonth = calendar.get(Calendar.MONTH);
            int npCurrentYear = calendar.get(Calendar.YEAR);
            
            //Number picker init
            initNumberPickerDay(1, MomentUtils.howManyDaysInMonth(npCurrentDay, npCurrentMonth), npCurrentDay);
            initNumberPickerMonth(1, 12, npCurrentMonth + 1);
            initNumberPickerYear(1900, 2018, npCurrentYear);
        }
        dialog.show();
    }

    //Ferme
    public void hide(){
        dialog.dismiss();
    }

    public ImageButton getBtnActionSave() {
        return btnActionSave;
    }
}
