package com.inductivtechnologies.quantmarkapp.bottomsheets;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.inductivtechnologies.quantmarkapp.R;

/**
 * BottomsheetCustom
 */
public class BottomsheetCustom {

    // Le conteneur secondaire
    private final Dialog dialog;

    /// Le layout emglobant
    private final View layoutView;

    // Composants graphiques
    private TextView tvContent;
    private Button btnDialog;

    // Construsteur
    @SuppressLint("InflateParams")
    public BottomsheetCustom(Context context){

        Activity activity = (Activity) context;

        // Inflatation de la vue du dialog
        this.layoutView = activity.getLayoutInflater()
                .inflate(R.layout.content_bottomsheet_custom, null);

        // Création du dialog avec un thème
        this.dialog = new Dialog(context, R.style.AppTheme_DialogSheetProgress);
        this.dialog.setContentView(layoutView);
        this.dialog.setCancelable(false);

        // On recupère une instance de la fenetre et on positionne le dialog
        Window window = dialog.getWindow();
        assert window != null;
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        window.setGravity(Gravity.CENTER);

        // On recupere les vues
        findViews();
    }

    private void findViews(){
        this.tvContent = layoutView.findViewById(R.id.content);
        this.btnDialog = layoutView.findViewById(R.id.btn_dialog);
    }

    public void show(){
        //On affiche
        dialog.show();
    }

    public void hide(){
        dialog.dismiss();
    }

    public void setContent(String content){
        this.tvContent.setText(content);
    }

    public void setButtonTitle(String title){
        this.btnDialog.setText(title);
    }

    //Getters et setters

    public Button getBtnDialog() {
        return btnDialog;
    }


}
