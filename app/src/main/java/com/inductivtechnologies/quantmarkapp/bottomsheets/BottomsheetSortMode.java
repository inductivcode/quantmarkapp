package com.inductivtechnologies.quantmarkapp.bottomsheets;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.configuration.NumberConfig;

/**
 * BottomsheetSortMode : Permet de choisir le mode de trie des produits
 */
@SuppressWarnings("ConstantConditions")
public class BottomsheetSortMode implements View.OnClickListener{

    //Le context
    private final Context context;

    // Le conteneur secondaire
    private final Dialog dialog;

    // Le conteneur principale
    //private Window window;

    /// Le layout emglobant
    private final View layoutView;

    private ImageButton btnActionSave;
    private LinearLayout llContainerAction;

    //Le mode de trie
    private int sortMode;

    //Les données
    private final String[] sortModesText;
    private final int[] sortIconDescription = {
            R.drawable.ic_action_best_sort,
            R.drawable.ic_action_price_ascending,
            R.drawable.ic_action_price_descending,
            R.drawable.ic_action_recent_sort
    };

    @SuppressLint("InflateParams")
    public BottomsheetSortMode(Context context){
        this.context = context;
        this.sortMode = NumberConfig.NB_DEFAULT_SORT;
        this.layoutView = LayoutInflater.from(this.context)
                .inflate(R.layout.content_bottomsheet_sort_mode, null);
        sortModesText = context.getResources().getStringArray(R.array.sort_mode_array);

        this.dialog = new Dialog(this.context, R.style.AppTheme_DialogSheetProgress);
        this.dialog.setContentView(this.layoutView);
        this.dialog.setCancelable(true);
        this.dialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        this.dialog.getWindow().setGravity(Gravity.BOTTOM);

        // Find views
        findViews();

        //Construction
        buildBottomsheet();

        // Add listeners
        addListeners();
    }

    //Override

    @Override
    public void onClick(View view) {
        //On récupère la vue
        RelativeLayout clickLayout = (RelativeLayout) view;

        //On affiche l'image de l'élément nouvellement sélectionné
        ImageView ivCheck = clickLayout.findViewById(R.id.sort_ic_check);
        ivCheck.setVisibility(View.VISIBLE);

        //On affiche l'image des autres
        hideOtherCheckImage(llContainerAction.indexOfChild(clickLayout));

        //La valeur sélectionnée
        sortMode = llContainerAction.indexOfChild(clickLayout);
    }

    //Les méthodes

    //Peuple le dialog
    private  void buildBottomsheet(){
        for(int i= 0; i < sortModesText.length; i++){
            View view = LayoutInflater.from(this.context)
                    .inflate(R.layout.layout_sort_mode_item, llContainerAction, false);

            ImageView iconDescription = view.findViewById(R.id.sort_ic_description);
            iconDescription.setImageDrawable(context.getResources().getDrawable(sortIconDescription[i]));

            ImageView iconCheck = view.findViewById(R.id.sort_ic_check);
            if(i == 0){
                iconCheck.setVisibility(View.VISIBLE);
            }

            TextView textContent = view.findViewById(R.id.sort_text_value);
            textContent.setText(sortModesText[i]);

            //On ajoute
            llContainerAction.addView(view);
        }
    }

    //On récupère les vues
    private void findViews() {
        this.btnActionSave = this.layoutView.findViewById(R.id.bsd_sort_mode_btn_action_save);
        this.llContainerAction = this.layoutView.findViewById(R.id.bsd_sort_mode_container_action);
    }

    //On ajoute un écouteur sur tous les éléments qui seront cliqués
    private void addListeners() {
        int count = llContainerAction.getChildCount();
        for(int i = 0; i < count; i++){
            llContainerAction.getChildAt(i).setOnClickListener(this);
        }
    }

    //Cache l'image des panel sauf celui à la position <<positionExclude>>
    private  void hideOtherCheckImage(int positionExclude){
        int count = llContainerAction.getChildCount();

        for(int i = 0; i < count; i++){
            if(i != positionExclude){
                RelativeLayout clickLayout = (RelativeLayout) llContainerAction.getChildAt(i);
                ImageView ivCheck = clickLayout.findViewById(R.id.sort_ic_check);
                ivCheck.setVisibility(View.INVISIBLE);
            }
        }
    }

    //Ouvre
    public void show(){
        dialog.show();
    }

    //Ferme
    public void hide(){
        dialog.dismiss();
    }

    //Getters et Setters

    public ImageButton getmBtnActionSave() {
        return btnActionSave;
    }

    public int getSortMode() {
        switch(sortMode){
            case NumberConfig.NB_DEFAULT_SORT :
                return sortMode;
            case NumberConfig.NB_ASCENDING_PRICE_SORT :
                return sortMode;
            case NumberConfig.NB_DESCENDING_PRICE_SORT :
                return sortMode;
            case NumberConfig.NB_NEW_ARTICLES :
                return sortMode;
            default :
                return NumberConfig.NB_DEFAULT_SORT;
        }
    }
}
