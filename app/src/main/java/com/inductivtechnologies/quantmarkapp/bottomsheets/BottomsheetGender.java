package com.inductivtechnologies.quantmarkapp.bottomsheets;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.inductivtechnologies.quantmarkapp.R;

/**
 * BottomsheetGender.
 */
@SuppressWarnings("ConstantConditions")
public class BottomsheetGender implements View.OnClickListener {

    //Le context
    private final Context context;

    // Le conteneur secondaire
    private final Dialog dialog;

    // Le conteneur principale
    //private Window window;

    /// Le layout emglobant
    private final View layoutView;

    private ImageButton btnActionSave;
    private LinearLayout llContainerAction;

    //Le mode de trie
    private String genderValue;

    //Les données
    private final String[] genderStrings;
    private final String[] defaultGenderStrings;


    private final int[] genderIconDescription = {
            R.drawable.ic_action_gender_male,
            R.drawable.ic_action_gender_female
    };


    @SuppressLint("InflateParams")
    public BottomsheetGender(Context context){
        this.context = context;
        this.layoutView = LayoutInflater.from(this.context).inflate(R.layout.content_bottomsheet_gender, null);

        this.genderStrings = context.getResources().getStringArray(R.array.gender_array_display);
        defaultGenderStrings =  context.getResources().getStringArray(R.array.gender_array);

        this.dialog = new Dialog(this.context, R.style.AppTheme_DialogSheetProgress);
        this.dialog.setContentView(this.layoutView);
        this.dialog.setCancelable(true);
        this.dialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        this.dialog.getWindow().setGravity(Gravity.BOTTOM);

        // Find views
        findViews();

        //Construction
        buildBottomsheet();

        // Add listeners
        addListeners();
    }

    //Override

    @Override
    public void onClick(View view) {
        //On récupère la vue
        RelativeLayout clickLayout = (RelativeLayout) view;

        //On affiche l'image de l'élément nouvellement sélectionné
        ImageView ivCheck = clickLayout.findViewById(R.id.gender_ic_check);
        ivCheck.setVisibility(View.VISIBLE);

        //On affiche l'image des autres
        hideOtherCheckImage(llContainerAction.indexOfChild(clickLayout));

        //La valeur sélectionnée
        genderValue = defaultGenderStrings[llContainerAction.indexOfChild(clickLayout)];
    }

    //Les méthodes

    //Peuple le dialog
    private  void buildBottomsheet(){
        for(int i= 0; i < genderStrings.length; i++){
            View view = LayoutInflater.from(this.context).
                    inflate(R.layout.layout_gender_item, llContainerAction, false);

            ImageView iconDescription = view.findViewById(R.id.gender_ic_description);
            iconDescription.setImageDrawable(context.getResources().getDrawable(genderIconDescription[i]));

            TextView textContent = view.findViewById(R.id.gender_text_value);
            textContent.setText(genderStrings[i]);

            //On ajoute
            llContainerAction.addView(view);
        }
    }

    //On récupère les vues
    private void findViews() {
        this.btnActionSave = this.layoutView.findViewById(R.id.bsd_gender_btn_action_save);
        this.llContainerAction = this.layoutView.findViewById(R.id.bsd_gender_container_action);
    }

    //On ajoute un écouteur sur tous les éléments qui seront cliqués
    private void addListeners() {
        int count = llContainerAction.getChildCount();
        for(int i = 0; i < count; i++){
            llContainerAction.getChildAt(i).setOnClickListener(this);
        }
    }

    //Cache l'image des panel sauf celui à la position <<positionExclude>>
    private  void hideOtherCheckImage(int positionExclude){
        int count = llContainerAction.getChildCount();

        for(int i = 0; i < count; i++){
            if(i != positionExclude){
                RelativeLayout clickLayout = (RelativeLayout) llContainerAction.getChildAt(i);
                ImageView ivCheck = clickLayout.findViewById(R.id.gender_ic_check);
                ivCheck.setVisibility(View.INVISIBLE);
            }
        }
    }

    //Ouvre
    public void show(){
        dialog.show();
    }

    //Ferme
    public void hide(){
        dialog.dismiss();
    }

    //Getters et Setters

    public ImageButton getmBtnActionSave() {
        return btnActionSave;
    }

    public String getGenderValue() {
        return genderValue;
    }

}
