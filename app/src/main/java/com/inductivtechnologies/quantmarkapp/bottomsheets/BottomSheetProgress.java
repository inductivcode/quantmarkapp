package com.inductivtechnologies.quantmarkapp.bottomsheets;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.inductivtechnologies.quantmarkapp.R;

/**
 * BottomSheetProgress : Implémentation d'un progress dialog personnalisé
 */
public class BottomSheetProgress {

    // Le conteneur secondaire
    private final Dialog mDialog;

    /// Le layout emglobant
    private final View mLayoutView;

    // Composants graphiques
    private TextView tvTextContent;

    // Construsteur
    @SuppressLint("InflateParams")
    public BottomSheetProgress(Context context){

        Activity activity = (Activity) context;

        // Inflatation de la vue du dialog
        mLayoutView = activity.getLayoutInflater().inflate(R.layout.content_bottomsheet_progress, null);

        // Création du dialog avec un thème
        this.mDialog = new Dialog(context, R.style.AppTheme_DialogSheetProgress);
        this.mDialog.setContentView(mLayoutView);
        this.mDialog.setCancelable(false);

        // On recupère une instance de la fenetre et on positionne le dialog
        Window mWindow = mDialog.getWindow();
        assert mWindow != null;
        mWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        mWindow.setLayout(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        mWindow.setGravity(Gravity.CENTER);

        // On recupere les vues
        findViews();
    }

    private void findViews(){
        this.tvTextContent = mLayoutView.findViewById(R.id.bst_text_progress);
    }

    public void show(String content){
        this.tvTextContent.setText(content);

        //On affiche
        mDialog.show();
    }

    public void hide(){
        mDialog.dismiss();
    }

}
