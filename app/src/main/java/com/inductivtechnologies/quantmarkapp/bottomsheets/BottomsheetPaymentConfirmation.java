package com.inductivtechnologies.quantmarkapp.bottomsheets;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.inductivtechnologies.quantmarkapp.R;

/**
 * BottomsheetPaymentConfirmation : Boite de dialogue qui propose à l'utilisateur de confirmer le payement
 */
@SuppressWarnings("ConstantConditions")
public class BottomsheetPaymentConfirmation {

    //Le context
    private final Context context;

    // Le conteneur secondaire
    private final Dialog mDialog;

    // Le conteneur principale
    //private Window mWindow;

    /// Le layout emglobant
    private final View layoutView;

    private ImageButton mBtnActionSave;

    private TextView tvSubtotal;
    private TextView tvDeliveryAmount;
    private TextView tvTotalAmount;
    private TextView tvDeviseTotalAmount;

    private ImageView thumbmail;

    @SuppressLint("InflateParams")
    public BottomsheetPaymentConfirmation(Context context){
        this.context = context;
        this.layoutView = LayoutInflater.from(this.context) .inflate(R.layout.content_bottomsheet_confirm_payment, null);

        this.mDialog = new Dialog(this.context, R.style.AppTheme_DialogSheetProgress);
        this.mDialog.setContentView(this.layoutView);
        this.mDialog.setCancelable(true);
        this.mDialog.getWindow().setLayout(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        this.mDialog.getWindow().setGravity(Gravity.BOTTOM);

        // Find views
        findViews();
    }

    //Les méthodes

    //On récupère les vues
    private void findViews() {
        this.mBtnActionSave = this.layoutView.findViewById(R.id.bsh_confirm_payment_btn_action_save);

        this.tvSubtotal  = this.layoutView.findViewById(R.id.tv_bsh_confirm_payment_section_subtotal);
        this.tvDeliveryAmount  = this.layoutView.findViewById(R.id.tv_bsh_confirm_payment_section_delevery_amount);
        this.tvTotalAmount  = this.layoutView.findViewById(R.id.tv_bsh_confirm_payment_section_total_amount);
        this.tvDeviseTotalAmount  = this.layoutView.findViewById(R.id.tv_bsh_confirm_payment_section_total_amount_usd);

        this.thumbmail = this.layoutView.findViewById(R.id.tv_bsh_confirm_payment_thumbmail);
    }

    //Ouvre
    public void open(){
        mDialog.show();
    }

    //Ferme
    public void close(){
        mDialog.dismiss();
    }

    //Getters et Setters

    public void setThumbmail(int resourceId) {
        this.thumbmail.setImageDrawable(context.getResources().getDrawable(resourceId));
    }

    public ImageButton getmBtnActionSave() {
        return mBtnActionSave;
    }

    public void setTvDeviseTotalAmount(double value) {
        this.tvDeviseTotalAmount.setText(context.getResources().getString(R.string.display_price_devise, value));
    }

    public void setTvSubtotal(String value) {
        this.tvSubtotal.setText(context.getResources().getString(R.string.display_price, value));
    }

    public void setTvDeliveryAmount(String value) {
        this.tvDeliveryAmount.setText(context.getResources().getString(R.string.display_price, value));
    }

    public void setTvTotalAmount(String value) {
        this.tvTotalAmount.setText(context.getResources().getString(R.string.display_price, value));
    }

}
