package com.inductivtechnologies.quantmarkapp.activities.profile;

import android.content.DialogInterface;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.components.GlideComponent;
import com.inductivtechnologies.quantmarkapp.configuration.StringConfig;
import com.inductivtechnologies.quantmarkapp.configuration.UrlRestApiConfig;
import com.inductivtechnologies.quantmarkapp.design.DisplayLargeNumber;
import com.inductivtechnologies.quantmarkapp.httptask.profile.CancelOrderTask;
import com.inductivtechnologies.quantmarkapp.httptask.profile.OrderDetailsTask;
import com.inductivtechnologies.quantmarkapp.models.entities.Buy;
import com.inductivtechnologies.quantmarkapp.models.entities.User;
import com.inductivtechnologies.quantmarkapp.store.repositories.UserRepository;
import com.inductivtechnologies.quantmarkapp.utils.CalculationUtils;
import com.inductivtechnologies.quantmarkapp.utils.LanguageDisolay;
import com.inductivtechnologies.quantmarkapp.utils.MomentUtils;
import com.inductivtechnologies.quantmarkapp.utils.StringUtils;

import java.text.NumberFormat;
import java.util.List;

/**
 * OrderDetailsActivity
 */
@SuppressWarnings("ConstantConditions")
public class OrderDetailsActivity extends AppCompatActivity implements View.OnClickListener ,
        SwipeRefreshLayout.OnRefreshListener{

    ///Pour annuler les requêtes
    public static final String ORDER_DETAILS_TASK = "ORDER_DETAILS_TASK";
    public static final String CANCEL_ORDER_TASK = "CANCEL_ORDER_TASK";

    private CoordinatorLayout coordinator;

    private SwipeRefreshLayout swipeRefreshLayout;

    private LinearLayout containerorder;

    //Section details
    private ImageView ivThumbmail;
    private TextView tvTitle;
    private TextView tvDescription;
    private Button btnCancelOrder;

    private TextView tvPrice;
    private TextView tvReductionPrice;
    private TextView tvCode;
    private TextView tvStatut;
    private TextView tvDate;
    private TextView tvPoint;

    //Section vendeur
    private TextView tvIssuerProfile;
    private TextView tvName;
    private TextView tvLocation;
    private TextView tvCountArticle;

    //Section livraison
    private TextView tvDeliveryMethod;
    private TextView tvPayementMethod;

    //SEction résumé
    private TextView tvQuantity;
    private TextView tvCost;
    private TextView tvSubtoital;
    private TextView tvTotal;

    //Controller
    private AppController appController;

    //Repositories
    private UserRepository userRepository;

    //Task
    private OrderDetailsTask orderDetailsTask;

    private String[] defaultDeliveryMethod;
    private String[] localeDeliveryMethod;
    private String[] defaultPaymentMethod;
    private String[] localePaymentMethod;

    //Données
    private Buy order;
    private int issuerNbArticles;

    //Identifiant
    private String idHexBuy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_order);

        Toolbar toolbar = findViewById(R.id.order_details_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        idHexBuy = getIntent().getStringExtra(StringConfig.INTENT_BUY_HEX_ID);

        //Objets
        initObjects();

        //Les vues
        findAllViews();

        //Task
        launchTask();
    }

    @Override
    public void onDestroy() {
        //Annulation des requêtes
        this.appController.cancelPendingRequests(ORDER_DETAILS_TASK);

        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.order_details_toolbar_back :
                finish();
                break;
            case R.id.btn_cancel_order :
                buildDialog().show();
                break;
        }
    }

    @Override
    public void onRefresh() {
        launchTask();
    }

    //Les méthodes

    public void globalInit(){
        containerorder.setVisibility(View.VISIBLE);

        //Section details
        initSectionDetails();
        initBtnCancelOrder();

        //Section issuer
        initSectionIssuer();

        //Section livraison
        initSectionDelivery();

        //Section résumé
        initSectionSummary();
    }

    private void launchTask(){
        User currentUser = getCurrenUser();

        if(currentUser != null){
            orderDetailsTask.makeRequest(currentUser, idHexBuy);
        }
    }

    //Utilisateur courant
    private User getCurrenUser(){
        List<User> users = userRepository.getAllUsers();
        if(users != null && !users.isEmpty()) return users.get(0);
        return null;
    }

    //Initialisation des objets
    private void initObjects() {
        //Repository
        userRepository = new UserRepository(getApplicationContext());

        //Instance singleton du controlleur volley
        this.appController = AppController.getInstance();

        issuerNbArticles = 0;

        //Task
        orderDetailsTask = new OrderDetailsTask(this, appController);

        defaultDeliveryMethod = getResources().getStringArray(R.array.default_delivery_method_array);
        localeDeliveryMethod = getResources().getStringArray(R.array.delivery_method_array);

        defaultPaymentMethod = getResources().getStringArray(R.array.default_payment_method_array);
        localePaymentMethod = getResources().getStringArray(R.array.payment_method_array);
    }

    private void findAllViews(){
        coordinator = findViewById(R.id.order_details_coordinator);
        ImageButton btnBack = findViewById(R.id.order_details_toolbar_back);
        btnBack.setOnClickListener(this);

        swipeRefreshLayout = findViewById(R.id.order_details_swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
        swipeRefreshLayout.setOnRefreshListener(this);

        containerorder = findViewById(R.id.container_order_details);

        //Section details
        ivThumbmail = findViewById(R.id.order_thumbmail);
        tvTitle = findViewById(R.id.order_title);
        tvDescription = findViewById(R.id.order_description);

        btnCancelOrder = findViewById(R.id.btn_cancel_order);
        btnCancelOrder.setOnClickListener(this);

        tvPrice = findViewById(R.id.order_price);
        tvReductionPrice = findViewById(R.id.order_reduction_price);
        tvCode = findViewById(R.id.order_code);
        tvStatut = findViewById(R.id.order_status);
        tvDate = findViewById(R.id.order_date);
        tvPoint = findViewById(R.id.order_point);

        //Section vendeur
        tvIssuerProfile = findViewById(R.id.order_issuer);
        tvName = findViewById(R.id.order_issuer_name);
        tvLocation = findViewById(R.id.order_issuer_address);
        tvCountArticle = findViewById(R.id.tv_issuer_nb_articles);

        //Section livraison
        tvPayementMethod = findViewById(R.id.order_payment_mode);
        tvDeliveryMethod = findViewById(R.id.order_delivery_address);

        //Section résumé
        tvQuantity = findViewById(R.id.order_quantity);
        tvSubtoital = findViewById(R.id.order_shipping_subtotal);
        tvCost = findViewById(R.id.order_shipping_cost);
        tvTotal = findViewById(R.id.order_total);
    }

    private void initSectionDetails(){
        //Image
        GlideComponent.simpleGlideComponent(getApplicationContext(),
                UrlRestApiConfig.IMAGE_RESOURCE_URL + order.getMarketOffer().getProduct().getImage(), ivThumbmail);
        //Titre
        tvTitle.setText(StringUtils
                .toUpperFirstChar((String) LanguageDisolay
                        .displayProduct(order.getMarketOffer().getProduct()).get(LanguageDisolay.FIELD_TITLE)));
        //Description
        tvDescription.setText(StringUtils
                .toUpperFirstChar((String) LanguageDisolay
                        .displayProduct(order.getMarketOffer().getProduct()).get(LanguageDisolay.FIELD_DESCRIPTION)));

        //Price
        tvPrice.setText(getResources().getString(R.string.display_price,
                NumberFormat.getIntegerInstance().format(order.getMarketOffer().getPrice())));

        if(order.getMarketOffer().getPrice() == order.getMarketOffer().getProduct().getOriginalprice()){
            tvReductionPrice.setVisibility(View.GONE);
        }else{
            tvReductionPrice.setVisibility(View.VISIBLE);
            tvReductionPrice.setText(getResources().getString(R.string.display_price,
                    NumberFormat.getIntegerInstance()
                            .format(order.getMarketOffer().getProduct().getOriginalprice())));
        }

        tvReductionPrice.setPaintFlags(tvReductionPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        //Statut
        tvStatut.setText(getResources().getString(R.string.full_statut));
        //Code
        tvCode.setText(order.getCode());
        //Date //
        tvDate.setText(MomentUtils.getRelativeDate(order.getPurchaseDate()));
        //Point
        String point = "+" + DisplayLargeNumber.smartLargeNumber(order.getPoint());
        tvPoint.setText(point);
    }

    private void initBtnCancelOrder() {
        int positionPay = StringUtils
                .getPositionOfStringInArray(defaultPaymentMethod, order.getPaymentType());

        if(positionPay != 2 && positionPay != 3){
            btnCancelOrder.setVisibility(View.VISIBLE);
        }else{
            btnCancelOrder.setVisibility(View.GONE);
        }
    }

    private void initSectionIssuer(){
        //Profile ind
        String[] words = order.getMarketOffer().getProduct().getIssuer().getCompleteName().split(" ");
        String contentProfile;
        if(words.length >= 2){
            contentProfile = words[0].substring(0,1).toUpperCase() + "" + words[1].substring(0, 1).toUpperCase();
        }else{
            contentProfile =  words[0].substring(0,1).toUpperCase();
        }
        tvIssuerProfile.setText(contentProfile);

        //Nom
        tvName.setText(StringUtils.toUpperFirstChar(order.getMarketOffer().getProduct().getIssuer().getCompleteName()));

        //location
        String location = order.getMarketOffer().getProduct().getIssuer().getAddress().getCity() + " " +
                order.getMarketOffer().getProduct().getIssuer().getAddress().getStreet();
        tvLocation.setText(StringUtils.toUpperFirstChar(location));

        //Nombre d'article actifs
        if(issuerNbArticles != 0){
            tvCountArticle.setVisibility(View.VISIBLE);
            tvCountArticle.setText(getResources()
                    .getQuantityString(R.plurals.display_nb_articles_plural, issuerNbArticles, (int) issuerNbArticles));
        }else{
            tvCountArticle.setVisibility(View.INVISIBLE);
        }
    }

    private void initSectionDelivery(){
        StringBuilder address = new StringBuilder();

        int positionDel = StringUtils.getPositionOfStringInArray(defaultDeliveryMethod, order.getDeliverPlace());
        String deliveryPlace = positionDel != -1 ? localeDeliveryMethod[positionDel] : order.getDeliverPlace();

        address.append(order.getNameAlt()).append(" ").append(order.getFirstNameAlt()).append("\n");
        address.append(order.getEmailAlt()).append("\n");
        if(order.getTelAlt().length() == 9){
            address.append(StringUtils.displayPhoneNumber(StringConfig.NB_IND_PHONE_CMR + order.getTelAlt()));
        }else{
            address.append(order.getTelAlt());
        }

        if(address.toString().isEmpty()){
            tvDeliveryMethod.setText(deliveryPlace);
        }else{
            deliveryPlace = deliveryPlace + "\n\n" + address.toString();
            tvDeliveryMethod.setText(deliveryPlace);
        }

        int positionPay = StringUtils.getPositionOfStringInArray(defaultPaymentMethod, order.getPaymentType());
        String paymentType = positionPay != -1 ? localePaymentMethod[positionPay] : order.getPaymentType();
        tvPayementMethod.setText(StringUtils.toUpperFirstChar(paymentType));
    }

    private void initSectionSummary(){
        double deliveryAmount = 0;
        double subTotal = order.getMarketOffer().getPrice() * order.getQuantity();

        String quantity = "x " + DisplayLargeNumber.smartLargeNumber(order.getQuantity());
        tvQuantity.setText(quantity);

        int positionDel = StringUtils.getPositionOfStringInArray(defaultDeliveryMethod,
                order.getDeliverPlace());
        if(positionDel != 1){
            deliveryAmount = CalculationUtils.deliveryCost(subTotal);
        }

        tvSubtoital.setText(getResources().getString(R.string.display_price,
                NumberFormat.getIntegerInstance().format(subTotal)));

        tvCost.setText(getResources().getString(R.string.display_price,
                NumberFormat.getIntegerInstance().format(deliveryAmount)));

        tvTotal.setText(getResources().getString(R.string.display_price,
                NumberFormat.getIntegerInstance().format(order.getTotalBuy() + deliveryAmount)));
    }

    private AlertDialog buildDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(OrderDetailsActivity.this);
        builder.setTitle(getString(R.string.label_cancel_order));
        builder.setMessage(getString(R.string.text_cancel_order_message));

        String positiveText = getString(android.R.string.ok);
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // On annule la commande
                        dialog.dismiss();
                        User currentUser = getCurrenUser();
                        if(currentUser != null){
                            new CancelOrderTask(OrderDetailsActivity.this, appController)
                                    .makeRequest(currentUser, idHexBuy);
                        }

                    }
                });

        String negativeText = getString(android.R.string.cancel);
        builder.setNegativeButton(negativeText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //On ferme tout simplement
                        dialog.dismiss();
                    }
                });

        return builder.create();
    }

    public CoordinatorLayout getCoordinator() {
        return coordinator;
    }

    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return swipeRefreshLayout;
    }

    public void setPurchase(Buy order) {
        this.order = order;
    }

    public void setIssuerNbArticles(int issuerNbArticles) {
        this.issuerNbArticles = issuerNbArticles;
    }
}
