package com.inductivtechnologies.quantmarkapp.activities.profile;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.components.GlideComponent;
import com.inductivtechnologies.quantmarkapp.configuration.StringConfig;
import com.inductivtechnologies.quantmarkapp.configuration.UrlRestApiConfig;
import com.inductivtechnologies.quantmarkapp.design.DisplayLargeNumber;
import com.inductivtechnologies.quantmarkapp.httptask.profile.PurchaseDetailsTask;
import com.inductivtechnologies.quantmarkapp.models.entities.Buy;
import com.inductivtechnologies.quantmarkapp.models.entities.User;
import com.inductivtechnologies.quantmarkapp.store.repositories.UserRepository;
import com.inductivtechnologies.quantmarkapp.utils.CalculationUtils;
import com.inductivtechnologies.quantmarkapp.utils.LanguageDisolay;
import com.inductivtechnologies.quantmarkapp.utils.MomentUtils;
import com.inductivtechnologies.quantmarkapp.utils.StringUtils;

import java.text.NumberFormat;
import java.util.List;

/**
 *PurchaseDetailsActivity : Affiche les détails d'une activité
 */
@SuppressWarnings("ConstantConditions")
public class PurchaseDetailsActivity extends AppCompatActivity implements View.OnClickListener ,
        SwipeRefreshLayout.OnRefreshListener{

    ///Pour annuler les requêtes
    public static final String PURCHASE_DETAILS_TASK = "PURCHASE_DETAILS_TASK";

    private CoordinatorLayout coordinator;

    private SwipeRefreshLayout swipeRefreshLayout;

    private LinearLayout containerPurchase;

    //Section details
    private ImageView ivThumbmail;
    private TextView tvTitle;
    private TextView tvDescription;
    private TextView tvPrice;
    private TextView tvCode;
    private TextView tvStatut;
    private TextView tvDate;
    private TextView tvPoint;

    //Section vendeur
    private TextView tvIssuerProfile;
    private TextView tvName;
    private TextView tvLocation;
    private TextView tvCountArticle;

    //Section livraison
    private TextView tvDeliveryMethod;
    private TextView tvPayementMethod;

    //SEction résumé
    private TextView tvQuantity;
    private TextView tvCost;
    private TextView tvSubtoital;
    private TextView tvTotal;

    //Controller
    private AppController appController;

    //Repositories
    private UserRepository userRepository;

    //Task
    private PurchaseDetailsTask purchaseDetailsTask;

    private String[] defaultDeliveryMethod;
    private String[] localeDeliveryMethod;
    private String[] defaultPaymentMethod;
    private String[] localePaymentMethod;

    //Données
    private Buy purchase;
    private int issuerNbArticles;

    //Identifiant
    private String idHexBuy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_purchase);

        Toolbar toolbar = findViewById(R.id.purchase_details_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        idHexBuy = getIntent().getStringExtra(StringConfig.INTENT_BUY_HEX_ID);

        //Objets
        initObjects();

        //Les vues
        findAllViews();

        //Task
        launchTask();
    }

    @Override
    public void onDestroy() {
        //Annulation des requêtes
        this.appController.cancelPendingRequests(PURCHASE_DETAILS_TASK);

        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.purchase_details_toolbar_back :
                finish();
                break;
        }
    }

    @Override
    public void onRefresh() {
        launchTask();
    }

    //Les méthodes

    public void globalInit(){
        containerPurchase.setVisibility(View.VISIBLE);

        //Section details
        initSectionDetails();

        //Section issuer
        initSectionIssuer();

        //Section livraison
        initSectionDelivery();

        //Section résumé
        initSectionSummary();
    }

    private void launchTask(){
        User currentUser = getCurrenUser();

        if(currentUser != null){
            purchaseDetailsTask.makeRequest(currentUser, idHexBuy);
        }
    }

    //Utilisateur courant
    private User getCurrenUser(){
        List<User> users = userRepository.getAllUsers();
        if(users != null && !users.isEmpty()) return users.get(0);
        return null;
    }

    //Initialisation des objets
    private void initObjects() {
        //Repository
        userRepository = new UserRepository(getApplicationContext());

        //Instance singleton du controlleur volley
        this.appController = AppController.getInstance();

        issuerNbArticles = 0;

        //Task
        purchaseDetailsTask = new PurchaseDetailsTask(this, appController);

        defaultDeliveryMethod = getResources().getStringArray(R.array.default_delivery_method_array);
        localeDeliveryMethod = getResources().getStringArray(R.array.delivery_method_array);

        defaultPaymentMethod = getResources().getStringArray(R.array.default_payment_method_array);
        localePaymentMethod = getResources().getStringArray(R.array.payment_method_array);
    }

    private void findAllViews(){
        coordinator = findViewById(R.id.purchase_details_coordinator);
        ImageButton btnBack = findViewById(R.id.purchase_details_toolbar_back);
        btnBack.setOnClickListener(this);

        swipeRefreshLayout = findViewById(R.id.purchase_details_swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
        swipeRefreshLayout.setOnRefreshListener(this);

        containerPurchase = findViewById(R.id.container_purchase_details);

        //Section details
        ivThumbmail = findViewById(R.id.purchase_thumbmail);
        tvTitle = findViewById(R.id.purchase_title);
        tvDescription = findViewById(R.id.purchase_description);
        tvPrice = findViewById(R.id.purchase_price);
        tvCode = findViewById(R.id.purchase_code);
        tvStatut = findViewById(R.id.purchase_status);
        tvDate = findViewById(R.id.purchase_date);
        tvPoint = findViewById(R.id.purchase_point);

        //Section vendeur
        tvIssuerProfile = findViewById(R.id.purchase_issuer);
        tvName = findViewById(R.id.purchase_issuer_name);
        tvLocation = findViewById(R.id.purchase_issuer_address);
        tvCountArticle = findViewById(R.id.tv_offerard_issuer_display_nb_articles);

        //Section livraison
        tvPayementMethod = findViewById(R.id.purchase_payment_mode);
        tvDeliveryMethod = findViewById(R.id.purchase_delivery_address);

        //Section résumé
        tvQuantity = findViewById(R.id.purchase_quantity);
        tvSubtoital = findViewById(R.id.purchase_shipping_subtotal);
        tvCost = findViewById(R.id.purchase_shipping_cost);
        tvTotal = findViewById(R.id.purchase_total);
    }

    private void initSectionDetails(){
        //Image
        GlideComponent.simpleGlideComponent(getApplicationContext(),
                UrlRestApiConfig.IMAGE_RESOURCE_URL + purchase.getMarketOffer().getProduct().getImage(), ivThumbmail);
        //Titre
        tvTitle.setText(StringUtils
                .toUpperFirstChar((String) LanguageDisolay
                        .displayProduct(purchase.getMarketOffer().getProduct()).get(LanguageDisolay.FIELD_TITLE)));
        //Description
        tvDescription.setText(StringUtils
                .toUpperFirstChar((String) LanguageDisolay
                        .displayProduct(purchase.getMarketOffer().getProduct()).get(LanguageDisolay.FIELD_DESCRIPTION)));

       //Price
        tvPrice.setText(getResources().getString(R.string.display_price,
                NumberFormat.getIntegerInstance().format(purchase.getMarketOffer().getPrice())));

        //Statut
        tvStatut.setText(getResources().getString(R.string.full_statut));
        //Code
        tvCode.setText(purchase.getCode());
        //Date //
        tvDate.setText(MomentUtils.getRelativeDate(purchase.getPurchaseDate()));
        //Point
        String point = "+" + DisplayLargeNumber.smartLargeNumber(purchase.getPoint());
        tvPoint.setText(point);
    }

    private void initSectionIssuer(){
        //Profile ind
        String[] words = purchase.getMarketOffer().getProduct().getIssuer().getCompleteName().split(" ");
        String contentProfile;
        if(words.length >= 2){
            contentProfile = words[0].substring(0,1).toUpperCase() + "" + words[1].substring(0, 1).toUpperCase();
        }else{
            contentProfile =  words[0].substring(0,1).toUpperCase();
        }
        tvIssuerProfile.setText(contentProfile);

        //Nom
        tvName.setText(StringUtils.toUpperFirstChar(purchase.getMarketOffer().getProduct().getIssuer().getCompleteName()));

        //location
        String location = purchase.getMarketOffer().getProduct().getIssuer().getAddress().getCity() + " " +
                purchase.getMarketOffer().getProduct().getIssuer().getAddress().getStreet();
        tvLocation.setText(StringUtils.toUpperFirstChar(location));

        //Nombre d'article actifs
        if(issuerNbArticles != 0){
            tvCountArticle.setVisibility(View.VISIBLE);
            tvCountArticle.setText(getResources()
                    .getQuantityString(R.plurals.display_nb_articles_plural, issuerNbArticles, (int) issuerNbArticles));
        }else{
            tvCountArticle.setVisibility(View.INVISIBLE);
        }
    }

    private void initSectionDelivery(){
        StringBuilder address = new StringBuilder();

        int positionDel = StringUtils.getPositionOfStringInArray(defaultDeliveryMethod, purchase.getDeliverPlace());
        String deliveryPlace = positionDel != -1 ? localeDeliveryMethod[positionDel] : purchase.getDeliverPlace();

        address.append(purchase.getNameAlt()).append(" ").append(purchase.getFirstNameAlt()).append("\n");
        address.append(purchase.getEmailAlt()).append("\n");
        if(purchase.getTelAlt().length() == 9){
            address.append(StringUtils.displayPhoneNumber(StringConfig.NB_IND_PHONE_CMR + purchase.getTelAlt()));
        }else{
            address.append(purchase.getTelAlt());
        }

        if(address.toString().isEmpty()){
            tvDeliveryMethod.setText(deliveryPlace);
        }else{
            deliveryPlace = deliveryPlace + "\n\n" + address.toString();
            tvDeliveryMethod.setText(deliveryPlace);
        }

        int positionPay = StringUtils.getPositionOfStringInArray(defaultPaymentMethod, purchase.getPaymentType());
        String paymentType = positionPay != -1 ? localePaymentMethod[positionPay] : purchase.getPaymentType();
        tvPayementMethod.setText(StringUtils.toUpperFirstChar(paymentType));
    }

    private void initSectionSummary(){
        double deliveryAmount = 0;
        double subTotal = purchase.getMarketOffer().getPrice() * purchase.getQuantity();

        String quantity = "x " + DisplayLargeNumber.smartLargeNumber(purchase.getQuantity());
        tvQuantity.setText(quantity);

        int positionDel = StringUtils.getPositionOfStringInArray(defaultDeliveryMethod,
                purchase.getDeliverPlace());
        if(positionDel != 1){
            deliveryAmount = CalculationUtils.deliveryCost(subTotal);
        }

        tvSubtoital.setText(getResources().getString(R.string.display_price,
                NumberFormat.getIntegerInstance().format(subTotal)));

        tvCost.setText(getResources().getString(R.string.display_price,
                NumberFormat.getIntegerInstance().format(deliveryAmount)));

        tvTotal.setText(getResources().getString(R.string.display_price,
                NumberFormat.getIntegerInstance().format(purchase.getTotalBuy() + deliveryAmount)));
    }

    //Getters et setters

    public CoordinatorLayout getCoordinator() {
        return coordinator;
    }

    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return swipeRefreshLayout;
    }

    public void setPurchase(Buy purchase) {
        this.purchase = purchase;
    }

    public void setIssuerNbArticles(int issuerNbArticles) {
        this.issuerNbArticles = issuerNbArticles;
    }
}
