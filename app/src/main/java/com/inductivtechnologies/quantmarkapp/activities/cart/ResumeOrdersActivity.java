package com.inductivtechnologies.quantmarkapp.activities.cart;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.activities.others.AddAddressActivity;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.bottomsheets.BottomsheetPaymentConfirmation;
import com.inductivtechnologies.quantmarkapp.configuration.NumberConfig;
import com.inductivtechnologies.quantmarkapp.configuration.PaypalConfig;
import com.inductivtechnologies.quantmarkapp.configuration.StringConfig;
import com.inductivtechnologies.quantmarkapp.httptask.cart.PaypalPaymentTask;
import com.inductivtechnologies.quantmarkapp.httptask.cart.SaveOrderTask;
import com.inductivtechnologies.quantmarkapp.models.entities.Address;
import com.inductivtechnologies.quantmarkapp.models.entities.CartItem;
import com.inductivtechnologies.quantmarkapp.models.entities.User;
import com.inductivtechnologies.quantmarkapp.models.utils.Order;
import com.inductivtechnologies.quantmarkapp.store.repositories.CartItemRepository;
import com.inductivtechnologies.quantmarkapp.store.repositories.UserRepository;
import com.inductivtechnologies.quantmarkapp.utils.CalculationUtils;
import com.inductivtechnologies.quantmarkapp.utils.DesignUtils;
import com.inductivtechnologies.quantmarkapp.utils.StringUtils;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalItem;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalPaymentDetails;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * ResumeOrderActivity
 */
@SuppressWarnings({"ConstantConditions", "unchecked"})
public class ResumeOrdersActivity extends AppCompatActivity implements View.OnClickListener{

    private static final int ADD_ADDRESS_REQUEST = 200;

    public static final String SAVE_ORDERS_TASK = "SAVE_ORDERS_TASK";
    public static final String SAVE_ORDERS_WITH_PAYPAL_PAYMENT_TASK = "SAVE_ORDERS_WITH_PAYPAL_PAYMENT_TASK";

    //Pour envoyer les données à l'intent de payement paypal
    private static final int PAYPAL_REQUEST_CODE = 400;

// --Commented out by Inspection START (02/03/18 14:21):
//    //Pour envoyer les données à l'intent de payement paypal
//    public static final int INTENT_PAYPAL_REQUEST_CODE = 300;
// --Commented out by Inspection STOP (02/03/18 14:21)

    //Les vues
    private CoordinatorLayout coordinatorLayout;

    private RelativeLayout rlContainerDeliveryPlace;
    private TextView tvDeliveryPlaceValue;

    //Conteneur ajouter une nouvelle adresse
    private RelativeLayout rlContainerAddAddress;

    //Pour sélectionner la méthode de payement
    private LinearLayout llContainerAction;
    //Payement au magasin ou à la livraison
    //Ces deux textview permettent de personnaliser le message en fonction de la méthode de payement (Magasin, marche)
    private TextView tvPaymentFirstMethodLabel;
    private TextView tvPaymentFirstMethodNotice;

    //Pour afficher l'adresse de livraison
    private LinearLayout llContainerDisplayAddress;
    private TextView tvDeliveryAddressFullNameDest;
    private TextView tvDeliveryAddressCity;
    private TextView tvDeliveryAddressDistrict;
    private TextView tvDeliveryAddressPhone;
    private TextView tvDeliveryAddressEmail;

    //Pour la section résumé de la commande
    private TextView tvSubtotal;
    private TextView tvDeliveryCost;
    private TextView tvTotalAmount;
    private TextView tvSectionResumeOrderHeader;

    private TextView bottomTvTotalPrice;
    private Button saveOrder;

    //Ressources
    private String[] deliveryMethods;
    private String[] defaultDeliveryMethods;
    private String[] paymentMethods;

    //Les valuer importantes recues de toutes modifications du formulaire
    //Renseigne sur le mode de payement
    private int paymentMethod = -1;
    private String paymentType;
    //Mode de livraison
    private int deliveryMethod;

    //L'adresse de livraison et le destinataire
    private String lastname;
    private String firstname;
    private String phone;
    private String city ;
    private String district;
    private String email;

    //Total à payer et frais de livraison
    private double subtotal = 0;
    private double deliveryAmount = 0;
    private double totalAmount= 0;

    //Controlleur
    private AppController appController;

    //Repositories
    private UserRepository userRepository;

    //Les éléments à acheter
    private List<CartItem> cartitems;

    //Les items paypal
    private List<PayPalItem> paypalItems;

    private String idMarketoffer;

    //Utilisateur courrant
    private User currentUser = null;

    //Dialogue confirmation du payement en ligne
    private BottomsheetPaymentConfirmation paymentConfirmDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resume_orders);

        //Toolbar
        Toolbar toolbar = findViewById(R.id.resume_orders_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //Initialisation des objets
        initObjects();

        // Find views
        findViews();

        //Initialisation de la première section du layout
        iniHeaderContainer();

        //Initialisation de la section <RESUME DE LA COMMANDE> du layout de cette activité
        initResumeOrderSection();

        // Add listeners
        addListeners();
    }

    @Override
    public void onDestroy() {
        //On arrete le service paypal
        stopService(new Intent(this, PayPalService.class));

        // On annule la requête
        switch (paymentMethod){
            case NumberConfig.NB_STORE_OR_PLACE_PAYMENT :
                this.appController.cancelPendingRequests(SAVE_ORDERS_TASK);
                break;
            case NumberConfig.NB_PAYPAL_PAYMENT :
                this.appController.cancelPendingRequests(SAVE_ORDERS_WITH_PAYPAL_PAYMENT_TASK);
                break;
        }

        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch(view.getId()){
            case R.id.resume_orders_add_address :

                intent = new Intent(this, AddAddressActivity.class);
                intent.putExtra(StringConfig.STR_RESULT_ADD_NEW_ADDRESS, putIntentValues());
                startActivityForResult(intent , ADD_ADDRESS_REQUEST);

                break;
            case R.id.btn_close_resume_orders :
                finish();
                break;
            case R.id.resume_orders_container_display_delivery_address :

                intent = new Intent(this, AddAddressActivity.class);
                intent.putExtra(StringConfig.STR_RESULT_ADD_NEW_ADDRESS, putIntentValues());
                startActivityForResult(intent , ADD_ADDRESS_REQUEST);

                break;
            case R.id.btn_resume_orders :
                switch(paymentMethod){
                    case NumberConfig.NB_STORE_OR_PLACE_PAYMENT :
                        // On enregistre la commande
                        List<Order> orders = listOfOrders();

                        //On lance la requête
                        new SaveOrderTask(this, appController).makeRequest(currentUser, orders);
                        break;
                    case NumberConfig.NB_MTN_PAYMENT :
                        Toast.makeText(this, "MTN", Toast.LENGTH_SHORT).show();
                        break;
                    case NumberConfig.NB_PAYPAL_PAYMENT :

                        //On modifie le dialog et on ouvre
                        paymentConfirmDialog.setTvSubtotal(NumberFormat.getIntegerInstance().format(subtotal));
                        paymentConfirmDialog.setTvDeliveryAmount(NumberFormat.getIntegerInstance().format(deliveryAmount));
                        paymentConfirmDialog.setTvTotalAmount(NumberFormat.getIntegerInstance().format(totalAmount));

                        double totalDeviseAmount = CalculationUtils.FcfaToEuro(totalAmount);
                        paymentConfirmDialog.setTvDeviseTotalAmount(totalDeviseAmount);

                        paymentConfirmDialog.setThumbmail(R.drawable.ic_paypal);

                        //On Ouvre
                        paymentConfirmDialog.open();

                        break;
                }
                break;
            default:
                if(deliveryMethod == NumberConfig.NB_STORE_DELIVERY || deliveryMethod == NumberConfig.NB_MOKOLO_DELIVERY ||
                        deliveryMethod == NumberConfig.NB_CENTRAL_MARKET_DELIVERY ){
                    saveOrder.setEnabled(true);
                }else{
                    if(!district.isEmpty()) saveOrder.setEnabled(true);
                }

                //On récupère la vue cliquée
                RelativeLayout clickLayoutContainer = (RelativeLayout) view;

                //On affiche l'image de l'élément nouvellement sélectionné
                ImageView ivCheck = (ImageView) clickLayoutContainer.getChildAt(1);
                ivCheck.setVisibility(View.VISIBLE);

                //On affiche l'image des autres
                hideOtherCheckImage(llContainerAction.indexOfChild(clickLayoutContainer));

                //La valuer sélectionnée
                paymentMethod = llContainerAction.indexOfChild(clickLayoutContainer);

                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        switch (requestCode){
            case ADD_ADDRESS_REQUEST :
                if(resultCode == Activity.RESULT_OK){
                    getIntentValues(intent);
                    setDisplayAddressViews();

                    //Lorsqu'on entre une nouvelle adresse si la méthode de payement
                    //a déjà été choisi on rend le bouton save cliquable
                    if(paymentMethod != -1) saveOrder.setEnabled(true);
                }
                break;
            case PAYPAL_REQUEST_CODE :
                if (resultCode == Activity.RESULT_OK) {
                    PaymentConfirmation confirm = intent.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);

                    if (confirm != null) {
                        try {
                            //Id du payement
                            String paymentId = confirm.toJSONObject().getJSONObject("response").getString("id");

                            //Objet json contenant le informations relatives au client
                            JSONObject paymentDetails = confirm.getPayment().toJSONObject();

                            // On enregistre la commande
                            List<Order> orders = listOfOrders();

                            //On lance la requête
                            new PaypalPaymentTask(this, appController)
                                    .makeRequest(currentUser, orders, paymentId, paymentDetails);

                        } catch (JSONException e) {
                            finish();

                            this.paypalItems.clear();

                            DesignUtils.makeSnackbar(getResources().getString(R.string.network_message_error),
                                    getCoordinatorLayout(), getApplicationContext());
                        }
                    }
                } else if (resultCode == Activity.RESULT_CANCELED) {
                    finish();

                    this.paypalItems.clear();

                    DesignUtils.makeSnackbar(getResources().getString(R.string.label_paypal_cancel),
                            getCoordinatorLayout(), getApplicationContext());
                } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                    finish();

                    this.paypalItems.clear();

                    DesignUtils.makeSnackbar(getResources().getString(R.string.label_paypal_error_message),
                            getCoordinatorLayout(), getApplicationContext());
                }
                break;
        }
    }

    //Les méthodes

    private void initObjects() {
        //Mode livraison
        deliveryMethod = getIntent().getIntExtra(StringConfig.INTENT_DELIVERY_METHOD, 0);

        //Ressources
        deliveryMethods = getResources().getStringArray(R.array.delivery_method_array);
        defaultDeliveryMethods = getResources().getStringArray(R.array.default_delivery_method_array);
        paymentMethods = getResources().getStringArray(R.array.payment_method_array_db);

        //Instance singleton du controlleur volley
        appController = AppController.getInstance();

        //Repository simple user
        userRepository = new UserRepository(this);
        //Cart item repo
        CartItemRepository cartItemRepository = new CartItemRepository(this);

        this.paymentConfirmDialog =  new BottomsheetPaymentConfirmation(this);

        idMarketoffer = getIntent().getStringExtra(StringConfig.INTENT_MARKETOFFER_HEX_ID);
        if(idMarketoffer.isEmpty()){
            cartitems = cartItemRepository.getAllCartitems();
        }else{
            cartitems = new ArrayList<>();
            cartitems.add(cartItemRepository.findCartitemByIdMarketOffer(idMarketoffer));
        }

        //Items paypal
        paypalItems = new ArrayList<>();

        //Utilisateur connecté
        getCurrenUser();

        //On fait quelques initialisations afin de transmettre les valeurs
        lastname = currentUser.getName();
        firstname = currentUser.getFirstName();
        email = currentUser.getAddress().getEmail();
        phone = currentUser.getPhone().getNumber();
        city = currentUser.getAddress().getCity();
        district = currentUser.getAddress().getStreet();
    }

    //Liste des articles de la commande
    //C'est cette liste qui sera transmise au serveur
    private List<Order> listOfOrders(){
        List<Order> orders = new ArrayList<>();

        for(CartItem cartitem : cartitems) {
            Order order = new Order();

            order.setNameAlt(lastname);
            order.setFirstNameAlt(firstname);
            order.setEmailAlt(email);
            order.setTelAlt(phone);

            order.setQuantity(cartitem.getQuantity());
            order.setCompleted(true);
            order.setCompletedBuyer(false);
            order.setPurchaseDate(new Date(System.currentTimeMillis()));
            order.setCompleteDate(null);
            order.setObjectIdHexmarketOffer(cartitem.getIdHexMarketOffer());
            order.setIdHexbuyer(currentUser.getObjectIdHexUser());

            if (deliveryMethod == NumberConfig.NB_HOME_DELIVERY) {
                order.setToBeDelivered(true);
                order.setDeliverPlace(district);
            }

            if (deliveryMethod == NumberConfig.NB_CENTRAL_MARKET_DELIVERY || deliveryMethod == NumberConfig.NB_MOKOLO_DELIVERY) {
                order.setToBeDelivered(true);
                String value = tvDeliveryPlaceValue.getText().toString();

                int position = StringUtils.getPositionOfStringInArray(deliveryMethods, value);
                value = position == -1 ? value.trim() : defaultDeliveryMethods[position];
                order.setDeliverPlace(value.trim());
            }

            if (deliveryMethod == NumberConfig.NB_STORE_DELIVERY) {
                order.setToBeDelivered(false);
                order.setDeliverPlace(defaultDeliveryMethods[0]);
            }

            order.setPaymentType(getPaymentType());
            order.setTotalBuy(cartitem.getTotalPrice());

            orders.add(order);
        }
        return orders;
    }

    //Traitement selon le mode de payement
    private String getPaymentType(){
        switch(paymentMethod){
            case NumberConfig.NB_STORE_OR_PLACE_PAYMENT :
                if(deliveryMethod == NumberConfig.NB_STORE_DELIVERY) {paymentType = paymentMethods[0];}
                else{
                    paymentType = paymentMethods[1];
                }
                break;
            case NumberConfig.NB_MTN_PAYMENT :
                paymentType = paymentMethods[2];
                break;
            case NumberConfig.NB_PAYPAL_PAYMENT :
                paymentType = paymentMethods[3];
                break;
        }

        return paymentType;
    }

    private void getCurrenUser(){
        List<User> users = userRepository.getAllUsers();
        if(users != null && !users.isEmpty()) currentUser =  users.get(0);
    }

    //Modification des textviews qui affichent l'adresse de livraison
    private void setDisplayAddressViews(){
        //On cache l'affichage ajouter une nouvelle adresse
        rlContainerAddAddress.setVisibility(View.GONE);
        llContainerDisplayAddress.setVisibility(View.VISIBLE);

        //Nom
        String fullName = StringUtils.toUpperFirstChar(lastname) + " " + StringUtils.toUpperFirstChar(firstname);
        tvDeliveryAddressFullNameDest.setText(fullName);

        //Ville par défaut
        tvDeliveryAddressCity.setText(StringUtils.toUpperFirstChar(city));

        //Quartier choisi
        tvDeliveryAddressDistrict.setText(StringUtils.toUpperFirstChar(district.toLowerCase()));

        //Téléphone
        if(phone.length() == 9){
            String phoneStr = StringConfig.NB_IND_PHONE_CMR + "" + phone;
            tvDeliveryAddressPhone.setText(StringUtils.displayPhoneNumber(phoneStr));
        }else{
            String phoneStr = "00" + phone;
            tvDeliveryAddressPhone.setText(StringUtils.displayPhoneNumber(phoneStr));
        }

        //Email
        tvDeliveryAddressEmail.setText(email);
    }

    //Initialisation des containers succeptibles d'etre cahés
    private void iniHeaderContainer(){
        switch(deliveryMethod){
            case NumberConfig.NB_STORE_DELIVERY :
                rlContainerDeliveryPlace.setVisibility(View.VISIBLE);
                rlContainerAddAddress.setVisibility(View.GONE);

                //On ajoute les valuers
                tvDeliveryPlaceValue.setText(String.valueOf(deliveryMethods[deliveryMethod]));
                tvPaymentFirstMethodLabel.setText(getResources().getString(R.string.label_resume_orders_store_payment));
                tvPaymentFirstMethodNotice.setText(getResources().getString(R.string.label_resume_orders_store_payment_notice));
                break;
            case NumberConfig.NB_HOME_DELIVERY :
                rlContainerDeliveryPlace.setVisibility(View.GONE);
                rlContainerAddAddress.setVisibility(View.VISIBLE);

                tvPaymentFirstMethodLabel.setText(getResources().getString(R.string.label_resume_orders_delivery_payment));
                tvPaymentFirstMethodNotice.setText(getResources().getString(R.string.label_resume_orders_delivery_payment_notice));
                break;
            case NumberConfig.NB_MOKOLO_DELIVERY :
                rlContainerDeliveryPlace.setVisibility(View.VISIBLE);
                rlContainerAddAddress.setVisibility(View.GONE);

                //On ajoute les valuers
                tvDeliveryPlaceValue.setText(String.valueOf(deliveryMethods[deliveryMethod]));
                tvPaymentFirstMethodLabel.setText(getResources().getString(R.string.label_resume_orders_delivery_payment));
                tvPaymentFirstMethodNotice.setText(getResources().getString(R.string.label_resume_orders_delivery_payment_notice));
                break;
            case NumberConfig.NB_CENTRAL_MARKET_DELIVERY :
                rlContainerDeliveryPlace.setVisibility(View.VISIBLE);
                rlContainerAddAddress.setVisibility(View.GONE);

                //On ajoute les valuers
                tvDeliveryPlaceValue.setText(String.valueOf(deliveryMethods[deliveryMethod]));
                tvPaymentFirstMethodLabel.setText(getResources().getString(R.string.label_resume_orders_delivery_payment));
                tvPaymentFirstMethodNotice.setText(getResources().getString(R.string.label_resume_orders_delivery_payment_notice));
                break;
        }
    }

    //Inititialisation de la section <RESUME DE LA COMMANDE> du layout de cette activité
    private void initResumeOrderSection() {
        //Il suffit de récuperer les élémnets du paniers et d'appliquer la formule
        // montant total = livraison + somme des prix des paniers
        //Puis afficher

        //On calcule le sous total
        for(CartItem cartitem : cartitems){
            subtotal = subtotal + cartitem.getTotalPrice();
        }

        //On calcule les frais de livraison
        if(deliveryMethod == NumberConfig.NB_HOME_DELIVERY){
            deliveryAmount = CalculationUtils.deliveryCost(subtotal);
        }

        //Total à payer
        totalAmount = deliveryAmount + subtotal;

        //On met à jour l'affichage
        tvSectionResumeOrderHeader.setText(getResources().getQuantityString(R.plurals.resume_order_section_header_plural, cartitems.size(), cartitems.size()));
        tvSubtotal.setText(getResources().getString(R.string.display_price, NumberFormat.getIntegerInstance().format(subtotal)));
        tvDeliveryCost.setText(getResources().getString(R.string.display_price, NumberFormat.getIntegerInstance().format(deliveryAmount)));
        tvTotalAmount.setText(getResources().getString(R.string.display_price, NumberFormat.getIntegerInstance().format(totalAmount)));
        bottomTvTotalPrice.setText(getResources().getString(R.string.display_price, NumberFormat.getIntegerInstance().format(totalAmount)));
    }

    //On récupère les vues
    private void findViews() {
        this.coordinatorLayout = findViewById(R.id.resume_orders_coordinator);
        ImageButton btnClose = findViewById(R.id.btn_close_resume_orders);
        btnClose.setOnClickListener(this);

        this.llContainerAction = findViewById(R.id.resume_orders_container_choice_payment_method);
        this.tvPaymentFirstMethodLabel = findViewById(R.id.resume_orders_label_first_payment_method);
        this.tvPaymentFirstMethodNotice = findViewById(R.id.resume_orders_notice_first_payment_method);

        this.llContainerDisplayAddress = findViewById(R.id.resume_orders_container_display_delivery_address);
        this.llContainerDisplayAddress.setOnClickListener(this);
        this.tvDeliveryAddressFullNameDest = findViewById(R.id.resume_orders_full_name);
        this.tvDeliveryAddressCity = findViewById(R.id.resume_orders_full_city);
        this.tvDeliveryAddressDistrict = findViewById(R.id.resume_orders_full_district);
        this.tvDeliveryAddressPhone = findViewById(R.id.resume_orders_full_phone);
        this.tvDeliveryAddressEmail = findViewById(R.id.resume_orders_full_email);

        this.rlContainerDeliveryPlace = findViewById(R.id.resume_orders_delivery_place);
        this.tvDeliveryPlaceValue = findViewById(R.id.resume_orders_delivery_place_value);

        this.rlContainerAddAddress = findViewById(R.id.resume_orders_add_address);
        this.rlContainerAddAddress.setOnClickListener(this);

        this.tvSectionResumeOrderHeader = findViewById(R.id.tv_resume_order_section_title);
        this.tvSubtotal = findViewById(R.id.tv_resume_order_section_subtotal);
        this.tvDeliveryCost = findViewById(R.id.tv_resume_order_section_delevery_amount);
        this.tvTotalAmount = findViewById(R.id.tv_resume_order_section_total_amount);

        this.bottomTvTotalPrice = findViewById(R.id.tv_total_price_resume_orders);
        this.saveOrder = findViewById(R.id.btn_resume_orders);
        this.saveOrder.setEnabled(false);
        this.saveOrder.setOnClickListener(this);
    }

    //Utilitaires

    //On ajoute un écouteur sur tous les éléments qui seront cliqués
    private void addListeners() {
        int count = llContainerAction.getChildCount();
        for(int i = 0; i < count; i++){
            llContainerAction.getChildAt(i).setOnClickListener(this);
        }

        this.paymentConfirmDialog.getmBtnActionSave().setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                paymentConfirmDialog.close();

                PayPalConfiguration paypalConfig = getPaypalConfiguration();
                startPaypalService(paypalConfig);

                //On lance le payement
                launchPayPalPayment(paypalConfig);
            }
        });
    }

    //Cache l'image des panel sauf celui à la position <<positionExclude>>
    private void hideOtherCheckImage(int positionExclude){
        int count = llContainerAction.getChildCount();

        for(int i = 0; i < count; i++){
            if(i != positionExclude){
                RelativeLayout clickLayoutContainer = (RelativeLayout) llContainerAction.getChildAt(i);

                ImageView ivCheck = (ImageView) clickLayoutContainer.getChildAt(1);
                ivCheck.setVisibility(View.INVISIBLE);
            }
        }
    }

    //Recupère les valeurs de l'intent
    private void getIntentValues(Intent intent){
        HashMap<String, String> activityParams = (HashMap<String, String>) intent.getSerializableExtra(StringConfig.STR_RESULT_ADD_NEW_ADDRESS);

        // On récupère les valeurs
        lastname = activityParams.get(User.FIELD_NAME_NAME);
        firstname = activityParams.get(User.FIELD_NAME_FIRSTNAME);
        city = activityParams.get(Address.FIELD_NAME_CITY);
        district = activityParams.get(Address.FIELD_NAME_DISTRICT);

        String phoneInter = activityParams.get(User.FIELD_NAME_FOREIGN_PHONE);
        if(phoneInter.length() == 9){
            phone = phoneInter;
        }else{
            phone = phoneInter.substring(2);
        }

        district = activityParams.get(Address.FIELD_NAME_DISTRICT);
        email = activityParams.get(Address.FIELD_NAME_EMAIL);
    }

    //Construit les valeurs à ajouter à l'intent
    private HashMap<String, String> putIntentValues(){
        HashMap<String, String> activityParams = new HashMap<>();

        activityParams.put(User.FIELD_NAME_NAME, lastname);
        activityParams.put(User.FIELD_NAME_FIRSTNAME, firstname);
        activityParams.put(User.FIELD_NAME_FOREIGN_PHONE, phone);
        activityParams.put(Address.FIELD_NAME_CITY, city);
        activityParams.put(Address.FIELD_NAME_DISTRICT, district);
        activityParams.put(Address.FIELD_NAME_EMAIL, email);

        return activityParams;
    }

    //Paypal

    //Config de l'objet paypal
    private PayPalConfiguration getPaypalConfiguration(){
        return  new PayPalConfiguration().environment(PaypalConfig.PAYPAL_LIVE_ENVIRONMENT)
                .clientId(PaypalConfig.PAYPAL_CLIENT_ID);
    }

    //Demarrage du service paypal
    private void startPaypalService(PayPalConfiguration PaypalConfig){
        Intent intent = new Intent(this, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, PaypalConfig);
        startService(intent);
    }

    //Initialisation du payement
    private PayPalPayment prepareFinalPayment() {

        for(CartItem cartitem : cartitems){
            BigDecimal singlePrice = new BigDecimal(CalculationUtils.FcfaToEuro(cartitem.getReductionPrice()));

            PayPalItem item = new PayPalItem(StringUtils.toUpperFirstChar(cartitem.getTitleFr()), cartitem.getQuantity(),
                    singlePrice.setScale(2, BigDecimal.ROUND_HALF_UP),
                    PaypalConfig.DEFAULT_EUR_CURRENCY, cartitem.getCode());

            this.paypalItems.add(item);
        }

        PayPalItem[] items = new PayPalItem[this.paypalItems.size()];
        items = this.paypalItems.toArray(items);

        // Sous total
        BigDecimal paypalSubtotal = PayPalItem.getItemTotal(items);

        // Frais de livraison
        BigDecimal shipping = new BigDecimal(CalculationUtils.FcfaToEuro(deliveryAmount));

        // Taxes hein
        BigDecimal tax = new BigDecimal("0.0");

        PayPalPaymentDetails paymentDetails = new PayPalPaymentDetails(shipping, paypalSubtotal, tax);

        BigDecimal amount = paypalSubtotal.add(shipping).add(tax);

        String messagePayment = "Payement de  Mr/Mme " +
                StringUtils.toUpperFirstChar(currentUser.getName()) + " " +
                StringUtils.toUpperFirstChar(currentUser.getFirstName()) + " correspondant au montant de : " +
                getResources().getString(R.string.display_price, NumberFormat.getIntegerInstance().format(totalAmount));

        PayPalPayment payment = new PayPalPayment(
                amount,
                PaypalConfig.DEFAULT_EUR_CURRENCY,
                messagePayment,
                PaypalConfig.PAYMENT_INTENT);

        payment.items(items);
        payment.paymentDetails(paymentDetails);

        return payment;
    }

    // Lancement du payement
    private void launchPayPalPayment(PayPalConfiguration paypalConfig) {
        PayPalPayment allBuyThings = prepareFinalPayment();

        Intent intent = new Intent(ResumeOrdersActivity.this, PaymentActivity.class);

        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, paypalConfig);

        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, allBuyThings);

        startActivityForResult(intent, PAYPAL_REQUEST_CODE);
    }

    //Getters et setters

    public CoordinatorLayout getCoordinatorLayout() {
        return coordinatorLayout;
    }

    public List<PayPalItem> getPaypalItems() {
        return paypalItems;
    }

    public String getIdMarketoffer() {
        return idMarketoffer;
    }
}
