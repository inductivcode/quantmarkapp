package com.inductivtechnologies.quantmarkapp.activities.search;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ImageButton;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.activities.account.UserLoginActivity;
import com.inductivtechnologies.quantmarkapp.activities.profile.UserProfileActivity;
import com.inductivtechnologies.quantmarkapp.adapters.search.SearchAdapter;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.bottomsheets.BottomsheetSearchFilter;
import com.inductivtechnologies.quantmarkapp.configuration.StringConfig;
import com.inductivtechnologies.quantmarkapp.design.GridSpacingItemDecoration;
import com.inductivtechnologies.quantmarkapp.httptask.search.SearchTask;
import com.inductivtechnologies.quantmarkapp.listeners.others.RecyclerTouchListener;
import com.inductivtechnologies.quantmarkapp.models.display.SearchHistoryDisplay;
import com.inductivtechnologies.quantmarkapp.models.display.SearchItemDisplay;
import com.inductivtechnologies.quantmarkapp.models.entities.SearchTrace;
import com.inductivtechnologies.quantmarkapp.models.utils.SearchItemCustom;
import com.inductivtechnologies.quantmarkapp.store.repositories.SearchTraceRepository;
import com.inductivtechnologies.quantmarkapp.store.session.SessionManager;
import com.inductivtechnologies.quantmarkapp.utils.CalculationUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * SearchActivity
 */
@SuppressWarnings("ConstantConditions")
public class SearchActivity extends AppCompatActivity implements View.OnClickListener{

    public static final String SEARCH_ITEM_TASK = "SEARCH_ITEM_TASK";

    //Les vues
    private CoordinatorLayout coordinatorLayout;

    // Recycler view
    private RecyclerView recyclerView;
    private SearchAdapter adapter;
    private GridLayoutManager layoutManager;
    private GridSpacingItemDecoration itemDecoration;

    // Pour la recherche
    private TextInputEditText inputSearch;

    //Dialog qui permet d'ajouter un filtre pour la recherche
    private BottomsheetSearchFilter bshSearchFilter;

    //La session
    private SessionManager sessionManager;

    //Volley instance singleton
    private AppController appController;

    //Task
    private SearchTask searchTask;

    //Les données
    private List<SearchItemCustom> searchItems;

    //historique de recherche
    private List<SearchTrace> searchHistories;

    //Repositoire
    private SearchTraceRepository searchTraceRepository;

    //Mode de trie
    private int searchFilterValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        //Toolbar
        Toolbar toolbar = findViewById(R.id.search_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //Initialisation des objets
        initObjects();

        ///Les vues
        findAllViews();

        //Les écouteurs
        addListeners();

        //Init du recyclerview
        initRecyclerView();
    }

    @Override
    protected void onResume() {
        super.onResume();

        //Historique de recherches
        addPanelHistorySearch();
    }

    @Override
    public void onDestroy() {
        // On annule la requête
        this.appController.cancelPendingRequests(SEARCH_ITEM_TASK);

        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.btn_close_search :
                finish();
                break;
            case R.id.search_btn_display_profile :
                if(sessionManager.isLoggedIn()){
                    startActivity(new Intent(this, UserProfileActivity.class));
                }else{
                    startActivity(new Intent(this, UserLoginActivity.class));
                }
                break;
            case R.id.search_btn_filter :
                bshSearchFilter.show();
                break;
            case R.id.bsd_search_filter_btn_action_save :
                bshSearchFilter.hide();
                //On modifie le filtre de la recherche
                searchFilterValue = bshSearchFilter.getSearchFilterValue();
        }
    }

    //Les méthodes

    //Initialise les objets
    private void initObjects() {
        sessionManager = new SessionManager(getApplicationContext());

        //Pour choisir le mode de trie
        bshSearchFilter = new BottomsheetSearchFilter(this);
        searchFilterValue = bshSearchFilter.getSearchFilterValue();

        //Les données
        searchItems = new ArrayList<>();

        //Volley
        this.appController = AppController.getInstance();

        //Task
        searchTask = new SearchTask(this, appController);

        //Repo
        searchTraceRepository = new SearchTraceRepository(getApplicationContext());

        //Adapter
        adapter = new SearchAdapter(this, searchItems, "");

        //Item decoration
        this.itemDecoration = new GridSpacingItemDecoration(1,
                CalculationUtils.dpToPx(getApplicationContext(), 4), true);

        layoutManager = new GridLayoutManager(this, 2);
    }

    //Les écouteurs
    private void addListeners() {
        bshSearchFilter.getmBtnActionSave().setOnClickListener(this);
    }

    private void findAllViews() {
        coordinatorLayout = findViewById(R.id.search_coordinator);

        ImageButton btnBack = findViewById(R.id.btn_close_search);
        btnBack.setOnClickListener(this);

        ImageButton btnDisplayProfile = findViewById(R.id.search_btn_display_profile);
        btnDisplayProfile.setOnClickListener(this);

        ImageButton btnSearchFilter = findViewById(R.id.search_btn_filter);
        btnSearchFilter.setOnClickListener(this);

        //Recycler view
        recyclerView = findViewById(R.id.recyclerview_search_item);

        inputSearch = findViewById(R.id.input_search);
        //On ajoute le listenner
        inputSearch.addTextChangedListener(new SearchActivity.SearchItemTextWatcher(inputSearch));
    }

    // Initialisation du recyclerview avec les données recues
    private void initRecyclerView() {
        //Manager
        recyclerView.setLayoutManager(layoutManager);

        //Adapter
        recyclerView.setAdapter(adapter);

        //Item decoration
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        //Item decoration
        recyclerView.addItemDecoration(itemDecoration);

        //Permet de modifier la disposition du layout manager en fonction de l'élément à inserer dans le recyclerview
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return position == 0 ? layoutManager.getSpanCount() : 1;
            }
        });

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this,
                recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                SearchItemCustom searchItemCustom = searchItems.get(position);

                if(searchItemCustom instanceof SearchItemDisplay){
                    SearchItemDisplay searchItem = (SearchItemDisplay) searchItemCustom;

                    //On garde une trace
                    SearchTrace searchTrace = new SearchTrace();
                    searchTrace.setValue(searchItem.getItemTitle());
                    searchTrace.setIdHex(searchItem.getIdHex());
                    searchTrace.setNbItems(searchItem.getItemNbResult());
                    searchTrace.setSearchFilter(searchFilterValue);
                    //On insère
                    searchTraceRepository.insertSearchItem(searchTrace);

                    //On lanec l'activité pour afficher les résultats
                    Intent intent = new Intent(SearchActivity.this, SearchResultActivity.class);
                    //On ajoute les données
                    intent.putExtra(StringConfig.INTENT_SEARCH_NB_ITEM, searchItem.getItemNbResult());
                    intent.putExtra(StringConfig.INTENT_SEARCH_ID_HEX, searchItem.getIdHex());
                    intent.putExtra(StringConfig.INTENT_SEARCH_LABEL, searchItem.getItemTitle());
                    intent.putExtra(StringConfig.INTENT_SEARCH_FILTER, searchFilterValue);

                    startActivity(intent);
                }
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));
    }

    private void addPanelHistorySearch(){
        //On ajoute l'historique des recherches
        searchHistories = searchTraceRepository.getAllSearchTrace();
        if(searchHistories != null && searchHistories.size() != 0){
            if(searchItems.size() == 0){
                searchItems.add(new SearchHistoryDisplay(searchHistories));
            }
        }
    }

    //Getters et setters

    public CoordinatorLayout getCoordinatorLayout() {
        return coordinatorLayout;
    }

    public List<SearchItemCustom> getSearchItems() {
        return searchItems;
    }

    public SearchAdapter getAdapter() {
        return adapter;
    }

    public List<SearchTrace> getSearchHistories() {
        return searchHistories;
    }

    //Classes internes

    private class SearchItemTextWatcher implements TextWatcher {

        private final View view;

        private SearchItemTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.input_search:
                    //On annule toues les reqoêtes précedentes
                    appController.cancelPendingRequests(SEARCH_ITEM_TASK);

                    String searchValue = inputSearch.getText().toString().trim();

                    if(searchValue.isEmpty()){
                        //On vide tout
                        searchItems.clear();
                        ///On ajoute le panel pour l'historique
                        addPanelHistorySearch();
                        //On notifie l'adaptateur
                        adapter.notifyDataSetChanged();
                    }else{
                        searchTask.makeRequest(searchValue, searchFilterValue);
                    }

                    break;
            }
        }
    }
}
