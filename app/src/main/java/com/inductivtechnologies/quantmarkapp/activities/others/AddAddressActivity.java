package com.inductivtechnologies.quantmarkapp.activities.others;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.configuration.NumberConfig;
import com.inductivtechnologies.quantmarkapp.configuration.StringConfig;
import com.inductivtechnologies.quantmarkapp.models.entities.Address;
import com.inductivtechnologies.quantmarkapp.models.entities.User;
import com.inductivtechnologies.quantmarkapp.utils.FormsUtils;
import com.inductivtechnologies.quantmarkapp.utils.StringUtils;

import java.util.HashMap;

/**
 * AddAddressActivity Pour ajouter une adresse de livraison
 */
@SuppressWarnings({"ConstantConditions", "unchecked"})
public class AddAddressActivity extends AppCompatActivity implements View.OnClickListener{

    ///Pour annuler les requêtes
    //public static String DISTRICT_TASK = "DISTRICT_TASK";

    private static final int INTENT_DISTRICT_REQUEST = 500;
    private static final int INTENT_CITY_REQUEST = 600;

    //Les vues

    private TextInputLayout inputLayoutLastname;
    private TextInputEditText lastnameText;
    private TextInputLayout inputLayoutFirstname;
    private TextInputEditText firstnameText;
    private TextInputLayout inputLayoutEmail;
    private TextInputEditText emailText;
    private TextInputLayout inputLayoutPhone;
    private TextInputEditText phonenumberText;

    private TextView tvCityValue;
    private TextView tvDistrictValue;

    //Controller
    private AppController appController;

    private FormsUtils formsUtils;

    //Les valuers enregistrées
    private String lastname;
    private String firstname;
    private String phone;
    private String city ;
    private String district;
    private String email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address);

        //Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar_add_address);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //On récupère les vues
        findAllViews();

        //Objets
        initObjects();

        //On recupère les valeurs
        getIntentValues(getIntent());

        //Init
        initAllViews();
    }


    @Override
    public void onDestroy() {
        //Annulation des requêtes
        this.appController.cancelPendingRequests(
                SelectRecyclerviewItemActivity.LOAD_DATA_TASK);

        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        Intent intent =  new Intent(this, SelectRecyclerviewItemActivity.class);

        switch(view.getId()){
            case R.id.btn_close_add_address :
                finish();
                break;
            case R.id.btn_validation_add_address :
                validateForm();
                break;
            case R.id.container_add_address_city :

                intent.putExtra(StringConfig.UPDATE_ADDR_INTENT_EXTRA, NumberConfig.NB_LOAD_CITY);
                startActivityForResult(intent, INTENT_CITY_REQUEST);

                break;
            case R.id.container_tv_add_address_district :

                intent.putExtra(StringConfig.UPDATE_ADDR_INTENT_EXTRA, NumberConfig.NB_LOAD_DISTRICT);
                startActivityForResult(intent, INTENT_DISTRICT_REQUEST);

                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        switch (requestCode){
            case INTENT_CITY_REQUEST :
                if(resultCode == Activity.RESULT_OK){
                    city = intent.getStringExtra(StringConfig.STR_SELECT_DATA);
                    tvCityValue.setText(StringUtils.toUpperFirstChar(city));
                }
                break;
            case INTENT_DISTRICT_REQUEST :
                if(resultCode == Activity.RESULT_OK){
                    district = intent.getStringExtra(StringConfig.STR_SELECT_DATA);
                    tvDistrictValue.setText(StringUtils.toUpperFirstChar(district));
                }
                break;
        }
    }

    //Les méthodes

    private void findAllViews() {
        ImageButton btnBack = findViewById(R.id.btn_close_add_address);
        btnBack.setOnClickListener(this);
        ImageButton btnSave = findViewById(R.id.btn_validation_add_address);
        btnSave.setOnClickListener(this);

        inputLayoutLastname = findViewById(R.id.tl_lastname);
        lastnameText = findViewById(R.id.lastname);
        lastnameText.addTextChangedListener(new AddAddressTextWatcher(lastnameText));

        inputLayoutFirstname = findViewById(R.id.tl_firstname);
        firstnameText = findViewById(R.id.firstname);
        firstnameText.addTextChangedListener(new AddAddressTextWatcher(firstnameText));

        inputLayoutEmail = findViewById(R.id.tl_email);
        emailText = findViewById(R.id.email);
        emailText.addTextChangedListener(new AddAddressTextWatcher(emailText));

        inputLayoutPhone = findViewById(R.id.tl_phonenumber);
        phonenumberText = findViewById(R.id.phonenumber);
        phonenumberText.addTextChangedListener(new AddAddressTextWatcher(phonenumberText));

        RelativeLayout containerCity = findViewById(R.id.container_add_address_city);
        containerCity.setOnClickListener(this);
        tvCityValue = findViewById(R.id.tv_add_address_city_value);

        RelativeLayout containerDistrict = findViewById(R.id.container_tv_add_address_district);
        containerDistrict.setOnClickListener(this);
        tvDistrictValue = findViewById(R.id.tv_add_address_district_value);
    }

    //Initialisation des objets
    private void initObjects() {

        //Instance singleton du controlleur volley
        appController = AppController.getInstance();

        formsUtils = new FormsUtils(getApplicationContext());
    }

    private void initAllViews(){
        lastnameText.setText(lastname);
        firstnameText.setText(firstname);
        phonenumberText.setText(phone);
        emailText.setText(email);

        if(!city.isEmpty()){
            tvCityValue.setText(city);
        }else{
            tvCityValue.setText(getResources().getString(R.string.select_value_message));
        }

        if(!district.isEmpty()){
            tvDistrictValue.setText(district);
        }else{
            tvDistrictValue.setText(getResources().getString(R.string.select_value_message));
        }
    }

    //Validation du formulaire
    private void validateForm() {
        lastname = lastnameText.getText().toString().trim();
        firstname = firstnameText.getText().toString().trim();
        email = emailText.getText().toString().trim();
        phone = phonenumberText.getText().toString().trim();

        district = tvDistrictValue.getText().toString().trim();
        city = tvCityValue.getText().toString().trim();

        boolean validLastName = formsUtils.validateSimpleInput(inputLayoutLastname, lastname);
        boolean validFirstname = formsUtils.validateSimpleInput(inputLayoutFirstname, firstname);
        boolean validEmail = formsUtils.validateEmail(inputLayoutEmail, email);
        boolean validPhone = formsUtils.validatePhoneNumber(inputLayoutPhone, phone);

        //On valide les valeurs avant de les envoyer
        if(validLastName &&
                validFirstname &&
                validPhone &&
                validEmail &&
                !city.equals(getResources().getString(R.string.select_value_message)) &&
                !district.equals(getResources().getString(R.string.select_value_message))){

            setResult(Activity.RESULT_OK, getIntent());
            getIntent().putExtra(StringConfig.STR_RESULT_ADD_NEW_ADDRESS, putIntentValues());
            finish();
        }
    }

    private void getIntentValues(Intent intent){
        HashMap<String, String> activityParams = (HashMap<String, String>) intent.getSerializableExtra(StringConfig.STR_RESULT_ADD_NEW_ADDRESS);

        // On récupère les valeurs
        lastname = activityParams.get(User.FIELD_NAME_NAME);
        firstname = activityParams.get(User.FIELD_NAME_FIRSTNAME);
        city = activityParams.get(Address.FIELD_NAME_CITY);
        district = activityParams.get(Address.FIELD_NAME_DISTRICT);

        String phoneInter = activityParams.get(User.FIELD_NAME_FOREIGN_PHONE);
        if(!phoneInter.isEmpty()){
            if(phoneInter.length() == 9){
                phone = phoneInter;
            }else{
                phone = phoneInter.substring(2);
            }
        }

        district = activityParams.get(Address.FIELD_NAME_DISTRICT);
        email = activityParams.get(Address.FIELD_NAME_EMAIL);
    }

    private HashMap<String, String> putIntentValues(){
        HashMap<String, String> activityParams = new HashMap<>();

        activityParams.put(User.FIELD_NAME_NAME, lastname);
        activityParams.put(User.FIELD_NAME_FIRSTNAME, firstname);
        activityParams.put(User.FIELD_NAME_FOREIGN_PHONE, phone);
        activityParams.put(Address.FIELD_NAME_CITY, city);
        activityParams.put(Address.FIELD_NAME_DISTRICT, district);
        activityParams.put(Address.FIELD_NAME_EMAIL, email);

        return activityParams;
    }

    private class AddAddressTextWatcher implements TextWatcher {

        private final View view;

        private AddAddressTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.lastname:
                    formsUtils.validateSimpleInput(inputLayoutLastname, lastnameText.getText().toString().trim());
                    break;
                case R.id.firstname:
                    formsUtils.validateSimpleInput(inputLayoutFirstname, firstnameText.getText().toString().trim());
                    break;
                case R.id.email:
                    formsUtils.validateEmail(inputLayoutEmail, emailText.getText().toString().trim());
                    break;
                case R.id.phonenumber:
                    formsUtils.validatePhoneNumber(inputLayoutPhone, phonenumberText.getText().toString().trim());
                    break;
            }
        }
    }
}

