package com.inductivtechnologies.quantmarkapp.activities.profile;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.httptask.profile.UpdateProfileSectionConfTask;
import com.inductivtechnologies.quantmarkapp.models.entities.User;
import com.inductivtechnologies.quantmarkapp.store.repositories.UserRepository;
import com.inductivtechnologies.quantmarkapp.utils.FormsUtils;

import java.util.List;

/**
 * ProfileUpdateSectionAddressActivity : Pour mettre à jour la section confidentialité du profil de l'utilisateur
 */
@SuppressWarnings("ConstantConditions")
public class ProfileUpdateSectionConfActivity extends AppCompatActivity implements View.OnClickListener{

    public static final String UPDATE_CONF_TASK = "UPDATE_CONF_TASK";

    //Les vues
    private CoordinatorLayout coordinatorLayout;

    private TextInputLayout inputLayoutLogin;
    private TextInputEditText etLogin;
    private TextInputLayout inputLayoutPasswordOld;
    private TextInputEditText etNewPassword;
    private TextInputLayout inputLayoutPasswordNew;
    private TextInputEditText etActualPassword;

    //Controlleur
    private AppController appController;

    //Task
    private UpdateProfileSectionConfTask updateTask;

    //Repositoires
    private UserRepository userRepository;

    private User currentUser = null;

    private FormsUtils formsUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_prodile_section_conf);

        //Toolbar
        Toolbar toolbar = findViewById(R.id.update_section_conf_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        initObjects();

        //Toutes les vues
        allViews();

        globalInit();
    }

    @Override
    public void onDestroy() {
        // On annule la requête
        this.appController.cancelPendingRequests(UPDATE_CONF_TASK);

        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.btn_close_update_section_conf :
                finish();
                break;
            case R.id.btn_validation_update_section_conf :
                validateFormUpdate();
                break;
        }
    }

    // Les méthodes

    //Utilisateur courant
    private User getCurrenUser(){
        List<User> users = userRepository.getAllUsers();
        if(users != null && !users.isEmpty()) return users.get(0);
        return null;
    }

    private void initObjects() {
        //Repository
        userRepository = new UserRepository(getApplicationContext());

        currentUser = getCurrenUser();

        //Instance singleton du controlleur volley
        appController = AppController.getInstance();

        //Task
        updateTask = new UpdateProfileSectionConfTask(this, appController);

        formsUtils= new FormsUtils(getApplicationContext());
    }

    private void globalInit(){
        if(currentUser != null){
            initLogin(currentUser.getLogin());
        }
    }

    private void initLogin(String login){
        etLogin.setText(login);
    }

    private void allViews(){
        coordinatorLayout = findViewById(R.id.update_section_conf_coordinator);

        ImageButton btnClose = findViewById(R.id.btn_close_update_section_conf);
        btnClose.setOnClickListener(this);
        ImageButton btnSave = findViewById(R.id.btn_validation_update_section_conf);
        btnSave.setOnClickListener(this);

        inputLayoutLogin = findViewById(R.id.layout_login_update);
        etLogin = findViewById(R.id.edittext_login_update);
        inputLayoutPasswordOld = findViewById(R.id.layout_password_old);
        etActualPassword = findViewById(R.id.edittext_actual_password_update);
        inputLayoutPasswordNew = findViewById(R.id.layout_password_new);
        etNewPassword = findViewById(R.id.edittext_new_password_update);
    }

    //Validation du formulaire
    private void validateFormUpdate() {
        String login = etLogin.getText().toString().trim();
        String oldPassword = etActualPassword.getText().toString().trim();
        String newPassword = etNewPassword.getText().toString().trim();

        boolean validateLogin = formsUtils.validateLogin(inputLayoutLogin, login);
        boolean validateOldPassword = formsUtils.validatePasswordUpdate(inputLayoutPasswordOld,
                oldPassword, currentUser.getPassword());
        boolean validateNewPassword = formsUtils.validatePassword(inputLayoutPasswordNew, newPassword);

        if(validateLogin && validateOldPassword && validateNewPassword){
            currentUser.setLogin(login);
            currentUser.setPassword(newPassword);
            //Task
            updateTask.makeRequest(currentUser);
        }
    }

    //Getters et setters

    public CoordinatorLayout getCoordinatorLayout() {
        return this.coordinatorLayout;
    }
}
