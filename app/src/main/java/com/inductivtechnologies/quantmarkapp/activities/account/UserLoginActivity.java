package com.inductivtechnologies.quantmarkapp.activities.account;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.fragments.account.FragmentLogin;
import com.inductivtechnologies.quantmarkapp.fragments.account.FragmentSignup;

import java.util.ArrayList;
import java.util.List;

/**
 * LoginActivity : Activité de connexion
 */
@SuppressWarnings("ConstantConditions")
public class UserLoginActivity extends AppCompatActivity{

    //Les vues
    private CoordinatorLayout coordinatorLayout;

    //View page
    private ViewPager viewPager;

    private LinearLayout llContainerTitles;

    private HandleTitlesClick handleTitlesClick;

    //Les fragments
    private FragmentLogin loginFragment;
    private FragmentSignup signupFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_login);

        //Toolbar
        Toolbar toolbar = findViewById(R.id.user_login_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //Les objets
        initObjects();

        //Les vues
        findAllViews();

        //Les écouteurs
        addListeners();
    }

    //Les objets
    private void initObjects() {
        handleTitlesClick = new HandleTitlesClick();

        loginFragment = FragmentLogin.newInstance();
        signupFragment = FragmentSignup.newInstance();
    }

    //Les méthodes

    //Initialisation du viewpager
    private void initViewPager(ViewPager viewPager) {
        ViewpagerAdapter adapter = new ViewpagerAdapter(getSupportFragmentManager());
        adapter.addFragment(loginFragment);
        adapter.addFragment(signupFragment);

        viewPager.setAdapter(adapter);
    }

    //Toutes es vues
    private void findAllViews(){
        llContainerTitles = findViewById(R.id.container_titles);

        viewPager = findViewById(R.id.user_login_viewpager);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);
        initViewPager(viewPager);

        coordinatorLayout = findViewById(R.id.login_coordinator_layout);
    }

    //Les écouteurs
    private void addListeners(){
        for(int i = 0; i < llContainerTitles.getChildCount(); i++){
            llContainerTitles.getChildAt(i).setOnClickListener(handleTitlesClick);
        }
    }

    //Getters et setters

    public CoordinatorLayout getCoordinatorLayout() {
        return coordinatorLayout;
    }

    private class HandleTitlesClick implements View.OnClickListener{

        private void hideIndicator(int positionExclude){
            int count = llContainerTitles.getChildCount();

            for(int i = 0; i < count; i++){
                if(i != positionExclude){
                    LinearLayout clickLayout = (LinearLayout) llContainerTitles.getChildAt(i);
                    clickLayout.getChildAt(1).setVisibility(View.INVISIBLE);
                }
            }
        }

        @Override
        public void onClick(View view) {
            //On récupère la vue
            LinearLayout clickLayout = (LinearLayout) view;

            //On affiche son indicateur
            clickLayout.getChildAt(1).setVisibility(View.VISIBLE);

            //On cache les indicators des autres
            hideIndicator(llContainerTitles.indexOfChild(clickLayout));

            //On modifie le view pager
            viewPager.setCurrentItem(llContainerTitles.indexOfChild(clickLayout));
        }

    }

    public class ViewpagerAdapter extends FragmentPagerAdapter {

        private final List<Fragment> mFragmentList = new ArrayList<>();

        public ViewpagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        private void addFragment(Fragment fragment) {
            mFragmentList.add(fragment);
        }
    }

    //  page change listener
    private final ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            //On affiche son indicateur
           LinearLayout container = (LinearLayout) llContainerTitles.getChildAt(position);
            container.getChildAt(1).setVisibility(View.VISIBLE);

            //On cache les indicators des autres
            handleTitlesClick.hideIndicator(llContainerTitles.indexOfChild(container));
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        @Override
        public void onPageScrollStateChanged(int arg0) {
        }
    };

}
