package com.inductivtechnologies.quantmarkapp.activities.about;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.utils.StringUtils;

/**
 * AboutAppActivity
 * Permet de récupérer et afficher la version de l'application
 */
@SuppressWarnings("ConstantConditions")
public class AboutAppActivity extends AppCompatActivity implements View.OnClickListener{

    /**
     * tvAppVersion
     * Vue dans laquelle la version de l'application est affichée
     */
    private TextView tvAppVersion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        //Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar_about_app);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //Les vues
        findAllViews();

        tvAppVersion.setText(StringUtils
                .getSpannedText(getResources()
                        .getString(R.string.display_app_version, getVersionInfo())));
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.btn_close_about_app :
                finish();
                break;
        }
    }

    /**
     * findAllViews
     * Récupère toutes les vues
     */
    private void findAllViews() {
        ImageButton btnBack = findViewById(R.id.btn_close_about_app);
        btnBack.setOnClickListener(this);

        tvAppVersion = findViewById(R.id.app_version);
    }

    /**
     * getVersionInfo
     * Fournit la version de l'application sous forme de chaine de caractères
     * @return La version de l'application
     */
    private String getVersionInfo() {
        String versionName = "";

        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            versionName = packageInfo.versionName;

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return versionName;
    }
}
