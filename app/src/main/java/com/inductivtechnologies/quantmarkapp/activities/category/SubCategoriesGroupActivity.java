package com.inductivtechnologies.quantmarkapp.activities.category;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.activities.cart.CartActivity;
import com.inductivtechnologies.quantmarkapp.activities.account.UserLoginActivity;
import com.inductivtechnologies.quantmarkapp.activities.profile.UserProfileActivity;
import com.inductivtechnologies.quantmarkapp.adapters.category.SubCategoryGridAdapter;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.components.GlideComponent;
import com.inductivtechnologies.quantmarkapp.configuration.StringConfig;
import com.inductivtechnologies.quantmarkapp.configuration.UrlRestApiConfig;
import com.inductivtechnologies.quantmarkapp.design.DisplayLargeNumber;
import com.inductivtechnologies.quantmarkapp.design.GridSpacingItemDecoration;
import com.inductivtechnologies.quantmarkapp.design.OvalTextView;
import com.inductivtechnologies.quantmarkapp.httptask.category.SubCategoryGroupTask;
import com.inductivtechnologies.quantmarkapp.listeners.others.RecyclerTouchListener;
import com.inductivtechnologies.quantmarkapp.models.display.ProductTypeDisplay;
import com.inductivtechnologies.quantmarkapp.models.utils.ProductTypeGroup;
import com.inductivtechnologies.quantmarkapp.models.utils.ProductTypeGroupCustom;
import com.inductivtechnologies.quantmarkapp.store.repositories.CartItemRepository;
import com.inductivtechnologies.quantmarkapp.store.session.SessionManager;
import com.inductivtechnologies.quantmarkapp.utils.CalculationUtils;
import com.inductivtechnologies.quantmarkapp.utils.DesignUtils;
import com.inductivtechnologies.quantmarkapp.utils.LanguageDisolay;
import com.inductivtechnologies.quantmarkapp.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * SubCategoriesGroupActivity
 */
@SuppressWarnings({"ConstantConditions", "unused"})
public class SubCategoriesGroupActivity extends AppCompatActivity implements View.OnClickListener , SwipeRefreshLayout.OnRefreshListener{

    ///Pour annuler les requêtes
    public static final String SUB_CATEGORY_GROUP_TASK = "SUB_CATEGORY_GROUP_TASK";

    private CoordinatorLayout coordinator;

    private AppBarLayout appBar;

    private SwipeRefreshLayout swipeRefreshLayout;

    private CollapsingToolbarLayout collapsedToolbar;
    private ImageView thumbmailCategory;

    private LinearLayout containerTitleToolbar;

    private TextView collapsingSubtitleSubcategory;
    private TextView collapsingTitleSubcategory;

    private TextView toolbarSubtitleSubcategory;
    private TextView toolbarTitleSubcategory;

    private OvalTextView tvBadge;

    //Controller
    private AppController appController;

    //Task
    private SubCategoryGroupTask subCategoryGroupTask;

    private RecyclerView recyclerView;
    private SubCategoryGridAdapter adapter;
    private GridSpacingItemDecoration gridSpacingDecation;

    //Les données
    private List<ProductTypeGroupCustom> subCategories;

    private ArrayList<String> idsHexSubcategory ;
    //La session
    private SessionManager sessionManager;

    private CartItemRepository cartRepo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subcategories_group);

        Toolbar toolbar = findViewById(R.id.subcategories_group_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //Barre de statut transparente
        DesignUtils.makeFullTransparentStatutBar(this);

        String title = getIntent().getStringExtra(StringConfig.INTENT_SUBCATEGORY_TITLE);
        String subtitle = getIntent().getStringExtra(StringConfig.INTENT_SUBCATEGORY_SUBTITLE);
        String imageName = getIntent().getStringExtra(StringConfig.INTENT_SUBCATEGORY_IMAGE_NAME);
        idsHexSubcategory = getIntent().getStringArrayListExtra(StringConfig.INTENT_SUBCATEGORY_HEX_ID);

        //Objets
        initObjects();

        //Les vues
        findAllViews();

        //Collapsing
        initCollapsingToolbar();

        setAppbarLayout(subtitle, title, imageName);

        initRecyclerViewSubcategoryGroupItem();

        //Task
        launchTask();
    }

    @Override
    public void onDestroy() {
        //Annulation des requêtes
        this.appController.cancelPendingRequests(SUB_CATEGORY_GROUP_TASK);

        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();

        //Le badge
        setBadge(cartRepo.getAllCartitems().size());
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.btn_close_subcategories_group :
                finish();
                break;
            case R.id.container_badge :
                //On ouvre le panier
                startActivity(new Intent(SubCategoriesGroupActivity.this, CartActivity.class));
                break;
            case R.id.btn_display_profile :
                if(sessionManager.isLoggedIn()){
                    startActivity(new Intent(this, UserProfileActivity.class));
                }else{
                    startActivity(new Intent(this, UserLoginActivity.class));
                }
                break;
        }
    }

    @Override
    public void onRefresh() {
        subCategories.clear();
        launchTask();
    }

    //Les méthodes

    private void launchTask(){
        subCategoryGroupTask.makeRequest(idsHexSubcategory);
    }

    //Initialisation des objets
    private void initObjects() {
        //Instance singleton du controlleur volley
        this.appController = AppController.getInstance();

        sessionManager = new SessionManager(getApplicationContext());

        //Les données
        this.subCategories = new ArrayList<>();

        //Task
        subCategoryGroupTask = new SubCategoryGroupTask(this, appController);

        cartRepo = new CartItemRepository(getApplicationContext());

        //Grid decoration
        gridSpacingDecation = new GridSpacingItemDecoration(3, CalculationUtils.dpToPx(this, 2), true);
    }

    private void findAllViews(){
        coordinator = findViewById(R.id.subcategories_group_coordinator);

        appBar = findViewById(R.id.appbar_toolbar_subcategories_group);

        collapsedToolbar = findViewById(R.id.collapsing_toolbar_subcategories_group);
        thumbmailCategory = findViewById(R.id.thumbmail_subcategories_group);

        containerTitleToolbar = findViewById(R.id.container_toolbar_title_subcategories_group);
        //LinearLayout containerTitleCollapsing = findViewById(R.id.containet_collapsing_title_subcategories_group);

        collapsingSubtitleSubcategory = findViewById(R.id.tv_collapsing_subtitle_subcategories_group);
        collapsingTitleSubcategory = findViewById(R.id.tv_collapsing_title_subcategories_group);

        toolbarSubtitleSubcategory = findViewById(R.id.tv_toolbar_subtitle_subcategories_group);
        toolbarTitleSubcategory = findViewById(R.id.tv_toolbar_title_subcategories_group);

        ImageButton btnBack = findViewById(R.id.btn_close_subcategories_group);
        btnBack.setOnClickListener(this);

        //Le badge
        RelativeLayout containerBadge = findViewById(R.id.container_badge);
        containerBadge.setOnClickListener(this);
        tvBadge = findViewById(R.id.tv_bagde);
        tvBadge.setSolidColor("#ffffff");

        swipeRefreshLayout = findViewById(R.id.subcategories_group_swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
        swipeRefreshLayout.setOnRefreshListener(this);

        recyclerView = findViewById(R.id.subcategories_group_items_recycler_view);

        ImageButton btnDisplayProfile = findViewById(R.id.btn_display_profile);
        btnDisplayProfile.setOnClickListener(this);
    }

    //Initialise le collapsed
    private void initCollapsingToolbar() {
        collapsedToolbar.setTitle(" ");
        appBar.setExpanded(true);

        appBar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    containerTitleToolbar.setAlpha(1f);
                    isShow = true;
                } else if (isShow) {
                    containerTitleToolbar.setAlpha(0);
                    isShow = false;
                }
            }
        });
    }

    //Initialise le appbar
    private void setAppbarLayout(String subTitle, String title, String imageName){
        collapsingSubtitleSubcategory.setText(StringUtils.toUpperFirstChar(subTitle));
        collapsingTitleSubcategory.setText(StringUtils.toUpperFirstChar(title));

        toolbarSubtitleSubcategory.setText(StringUtils.toUpperFirstChar(subTitle));
        toolbarTitleSubcategory.setText(StringUtils.toUpperFirstChar(title));

        GlideComponent.simpleGlideComponentWithoutPlaceholder(this,
                UrlRestApiConfig.IMAGE_RESOURCE_URL + imageName, thumbmailCategory);
    }

    // Initialisation du recyclerview avec les données recues
    private void initRecyclerViewSubcategoryGroupItem(){
        GridLayoutManager layoutManager = new GridLayoutManager(this, 3);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new SubCategoryGridAdapter(this, subCategories);
        recyclerView.setAdapter(adapter);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(gridSpacingDecation);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this,
                recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                ProductTypeGroupCustom productTypeGroupCustom = subCategories.get(position);

                if(productTypeGroupCustom instanceof ProductTypeGroup){
                    ProductTypeGroup productTypeGroup = (ProductTypeGroup) productTypeGroupCustom;
                }else{
                    ProductTypeDisplay productTypeDisplay = (ProductTypeDisplay) productTypeGroupCustom;
                    long nbMarketSubCategory = productTypeDisplay.getNbOfMarketOffer();

                    String title = (String) LanguageDisolay
                            .displaySubCategory(productTypeDisplay.getSubCategory()).get(LanguageDisolay.FIELD_TITLE);

                    String subtitle;
                    if(nbMarketSubCategory > 1){
                        subtitle = getResources().getString(R.string.display_nb_article_plural,
                                DisplayLargeNumber.smartLargeNumber(nbMarketSubCategory));
                    }else{
                        subtitle = getResources().getString(R.string.display_nb_article_sin,
                                DisplayLargeNumber.smartLargeNumber(nbMarketSubCategory));
                    }

                    String idHexSubCategory = productTypeDisplay.getSubCategory().getIdHexType();
                    String imageName = productTypeDisplay.getSubCategory().getImage();

                    // On passe à l'activité suivante
                    Intent intent = new Intent(SubCategoriesGroupActivity.this, SubCategoryMarketofferActivity.class);
                    intent.putExtra(StringConfig.INTENT_SUBCATEGORY_TITLE, title);
                    intent.putExtra(StringConfig.INTENT_SUBCATEGORY_SUBTITLE, subtitle);
                    intent.putExtra(StringConfig.INTENT_SUBCATEGORY_IMAGE_NAME, imageName);
                    intent.putExtra(StringConfig.INTENT_SUBCATEGORY_HEX_ID, idHexSubCategory);
                    intent.putExtra(StringConfig.INTENT_SUBCATEGORY_NB_MARKETOFFER, nbMarketSubCategory);
                    startActivity(intent);
                }
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));
    }

    //Met à jour le badge
    private void setBadge(long count){
        tvBadge.setText(DisplayLargeNumber.smartLargeNumber(count));
    }

    //Getters et setters

    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return swipeRefreshLayout;
    }

    public List<ProductTypeGroupCustom> getSubCategories() {
        return subCategories;
    }

    public SubCategoryGridAdapter getAdapter() {
        return adapter;
    }

    public CoordinatorLayout getCoordinatorLayout() {
        return coordinator;
    }
}
