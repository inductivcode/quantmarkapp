package com.inductivtechnologies.quantmarkapp.activities.category;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.activities.cart.CartActivity;
import com.inductivtechnologies.quantmarkapp.activities.marketofferdetails.MarketofferDetails;
import com.inductivtechnologies.quantmarkapp.activities.account.UserLoginActivity;
import com.inductivtechnologies.quantmarkapp.activities.profile.UserProfileActivity;
import com.inductivtechnologies.quantmarkapp.adapters.category.SubCategoryMarketofferGridAdapter;
import com.inductivtechnologies.quantmarkapp.adapters.category.SubCategoryMarketofferListAdapter;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.bottomsheets.BottomsheetSortMode;
import com.inductivtechnologies.quantmarkapp.callbacks.OnLoadMoreListener;
import com.inductivtechnologies.quantmarkapp.components.GlideComponent;
import com.inductivtechnologies.quantmarkapp.configuration.NumberConfig;
import com.inductivtechnologies.quantmarkapp.configuration.StringConfig;
import com.inductivtechnologies.quantmarkapp.configuration.UrlRestApiConfig;
import com.inductivtechnologies.quantmarkapp.design.DisplayLargeNumber;
import com.inductivtechnologies.quantmarkapp.design.GridSpacingItemDecoration;
import com.inductivtechnologies.quantmarkapp.design.OvalTextView;
import com.inductivtechnologies.quantmarkapp.httptask.category.SubCategoryMarketofferTask;
import com.inductivtechnologies.quantmarkapp.listeners.others.RecyclerTouchListener;
import com.inductivtechnologies.quantmarkapp.models.display.LoadMoreMarketOfferDisplay;
import com.inductivtechnologies.quantmarkapp.models.display.MarketOfferDisplay;
import com.inductivtechnologies.quantmarkapp.models.utils.MarketOfferDisplayCustom;
import com.inductivtechnologies.quantmarkapp.store.repositories.CartItemRepository;
import com.inductivtechnologies.quantmarkapp.store.session.SessionManager;
import com.inductivtechnologies.quantmarkapp.utils.CalculationUtils;
import com.inductivtechnologies.quantmarkapp.utils.DesignUtils;
import com.inductivtechnologies.quantmarkapp.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * SubCategoryMarketofferActivity : Affiche les produits d'une sous-catégorie
 */
@SuppressWarnings("ConstantConditions")
public class SubCategoryMarketofferActivity extends AppCompatActivity
        implements View.OnClickListener , SwipeRefreshLayout.OnRefreshListener{

    ///Pour annuler les requêtes
    public static final String SUB_CATEGORY_MARKETOFFER_TASK = "SUB_CATEGORY_MARKETOFFER_TASK";

    private CoordinatorLayout coordinator;

    private AppBarLayout appBar;

    private SwipeRefreshLayout swipeRefreshLayout;

    private CollapsingToolbarLayout collapsedToolbar;
    private ImageView thumbmailCategory;

    private LinearLayout containerTitleToolbar;

    private TextView collapsingSubtitleSubcategory;
    private TextView collapsingTitleSubcategory;

    private TextView toolbarSubtitleSubcategory;
    private TextView toolbarTitleSubcategory;

    private TextView sortValue;

    private OvalTextView tvBadge;

    private ImageButton btnListDisplay;
    private ImageButton btnGridDisplay;

    //Controller
    private AppController appController;
    //La session
    private SessionManager sessionManager;

    //Task
    private SubCategoryMarketofferTask httpTask;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter recyclerviewAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private GridSpacingItemDecoration itemDecoration;

    private GridLayoutManager gridLayoutManager;
    private SubCategoryMarketofferGridAdapter gridAdapter;
    private LinearLayoutManager linearLayoutManager;
    private SubCategoryMarketofferListAdapter listAdapter;
    //Les données
    private List<MarketOfferDisplayCustom> marketOffers;

    private String idHexSubcategory ;
    //Nombre total d'article pour la sous catégorie
    private long nbMarketOffers;
    //Offset lors de l'interrogation de la base de données
    private int range = 0;
    //Mode de trie
    private int sortMode;
    //Pour iniquer le mode d'affichage
    private boolean isGridDisplay = true;

    //Dialog qui permet de choisir le mode de trie
    private BottomsheetSortMode bshSortMode;

    private String[] strSortMode;

    private CartItemRepository cartRepo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subcategory_marketoffer);

        Toolbar toolbar = findViewById(R.id.subcategories_toolbar_marketoffer);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //Barre de statut transparente
        DesignUtils.makeFullTransparentStatutBar(this);

        String title = getIntent().getStringExtra(StringConfig.INTENT_SUBCATEGORY_TITLE);
        String subtitle = getIntent().getStringExtra(StringConfig.INTENT_SUBCATEGORY_SUBTITLE);
        String imageName = getIntent().getStringExtra(StringConfig.INTENT_SUBCATEGORY_IMAGE_NAME);
        nbMarketOffers = getIntent().getLongExtra(StringConfig.INTENT_SUBCATEGORY_NB_MARKETOFFER, 0);
        idHexSubcategory = getIntent().getStringExtra(StringConfig.INTENT_SUBCATEGORY_HEX_ID);

        //Objets
        initObjects();

        //Les vues
        findAllViews();

        //Les écouteurs
        addListeners();

        //Collapsing
        initCollapsingToolbar();

        setAppbarLayout(subtitle, title, imageName);

        setSortValueView();

        initRecyclerView();

        //Task
        launchTask();
    }

    @Override
    public void onDestroy() {
        //Annulation des requêtes
        this.appController.cancelPendingRequests(SUB_CATEGORY_MARKETOFFER_TASK);

        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();

        //Le badge
        setBadge(cartRepo.getAllCartitems().size());
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.btn_close_subcategories_marketoffer :
                finish();
                break;
            case R.id.subcategory_marketoffer_container_sort_value :
                bshSortMode.show();
                break;
            case R.id.bsd_sort_mode_btn_action_save :
                bshSortMode.hide();

                //On modifie le mode de trie
                sortMode = bshSortMode.getSortMode();

                //On raffraichi avec le task
                onRefresh();

                //On raffraichi
                setSortValueView();

                break;
            case R.id.btn_subcategory_marketoffer_list_display :
                if(isGridDisplay){
                    isGridDisplay = false;
                    btnGridDisplay.setImageDrawable(getResources().getDrawable(R.drawable.ic_action_display_grid_unselected));
                    btnListDisplay.setImageDrawable(getResources().getDrawable(R.drawable.ic_action_display_list_selected));

                    listConfiguration();
                }
                break;
            case R.id.btn_subcategory_marketoffer_grid_display :
                if(!isGridDisplay){
                    isGridDisplay = true;
                    btnGridDisplay.setImageDrawable(getResources().getDrawable(R.drawable.ic_action_display_grid_selected));
                    btnListDisplay.setImageDrawable(getResources().getDrawable(R.drawable.ic_action_display_list_unselected));

                    gridConfiguration();
                }
                break;
            case R.id.container_badge :
                //On ouvre le panier
                startActivity(new Intent(SubCategoryMarketofferActivity.this, CartActivity.class));
                break;
            case R.id.btn_display_profile :
                if(sessionManager.isLoggedIn()){
                    startActivity(new Intent(this, UserProfileActivity.class));
                }else{
                    startActivity(new Intent(this, UserLoginActivity.class));
                }
                break;
        }
    }

    @Override
    public void onRefresh() {
        marketOffers.clear();
        range = 0;
        launchTask();
    }

    //Les méthodes

    private void launchTask(){
        httpTask.makeRequest(idHexSubcategory, NumberConfig.NB_WIDTH_DATA, range, sortMode);
    }

    private void onLoadMoreData(){
        marketOffers.remove(marketOffers.size() - 1);
        recyclerviewAdapter.notifyItemRemoved(marketOffers.size());

        if(recyclerviewAdapter instanceof SubCategoryMarketofferGridAdapter){
            ((SubCategoryMarketofferGridAdapter) recyclerviewAdapter).setWithLoadMore(false);
        }else{
            ((SubCategoryMarketofferListAdapter) recyclerviewAdapter).setWithLoadMore(false);
        }

        //On charge les données de nouveau
        launchTask();
    }

    //Utilisée pour savoir si les données ont été totalemnt chargées et quand ajouter le load more
    public void loadMoreAsyn(){
        if(marketOffers.size() < nbMarketOffers){
            marketOffers.add(new LoadMoreMarketOfferDisplay());
            if(recyclerviewAdapter instanceof SubCategoryMarketofferGridAdapter){
                ((SubCategoryMarketofferGridAdapter)recyclerviewAdapter).setWithLoadMore(true);
            }else{
                ((SubCategoryMarketofferListAdapter)recyclerviewAdapter).setWithLoadMore(true);
            }
        }
    }

    //Initialisation des objets
    private void initObjects() {
        //Instance singleton du controlleur volley
        this.appController = AppController.getInstance();

        sessionManager = new SessionManager(getApplicationContext());

        //Pour choisir le mode de trie
        bshSortMode = new BottomsheetSortMode(this);
        sortMode = bshSortMode.getSortMode();

        //Les données
        this.marketOffers = new ArrayList<>();

        //Item decoration
        itemDecoration = new GridSpacingItemDecoration(2,
                CalculationUtils.dpToPx(this, 2), true);

        //Adaptateurs
        gridAdapter = new SubCategoryMarketofferGridAdapter(this, this.marketOffers);
        listAdapter = new SubCategoryMarketofferListAdapter(this, this.marketOffers);

        //Layout managers
        gridLayoutManager = new GridLayoutManager(this, 2);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        //Task
        httpTask = new SubCategoryMarketofferTask(this, appController);

        strSortMode = getResources().getStringArray(R.array.sort_mode_array);

        cartRepo = new CartItemRepository(getApplicationContext());
    }

    //Listeners
    private void addListeners() {
        bshSortMode.getmBtnActionSave().setOnClickListener(this);
    }

    private void findAllViews(){
        coordinator = findViewById(R.id.subcategories_marketoffer_coordinator);

        appBar = findViewById(R.id.appbar_toolbar_subcategories_marketoffer);

        collapsedToolbar = findViewById(R.id.collapsing_toolbar_subcategories_marketoffer);
        thumbmailCategory = findViewById(R.id.thumbmail_subcategories_marketoffer);

        containerTitleToolbar = findViewById(R.id.container_toolbar_title_subcategories_marketoffer);
        //LinearLayout containerTitleCollapsing = findViewById(R.id.containet_collapsing_title_subcategories_marketoffer);

        collapsingSubtitleSubcategory = findViewById(R.id.tv_collapsing_subtitle_subcategories_marketoffer);
        collapsingTitleSubcategory = findViewById(R.id.tv_collapsing_title_subcategories_marketoffer);

        toolbarSubtitleSubcategory = findViewById(R.id.tv_toolbar_subtitle_subcategories_marketoffer);
        toolbarTitleSubcategory = findViewById(R.id.tv_toolbar_title_subcategories_marketoffer);

        ImageButton btnBack = findViewById(R.id.btn_close_subcategories_marketoffer);
        btnBack.setOnClickListener(this);

        //Le badge
        RelativeLayout containerBadge = findViewById(R.id.container_badge);
        containerBadge.setOnClickListener(this);
        tvBadge = findViewById(R.id.tv_bagde);
        tvBadge.setSolidColor("#ffffff");

        swipeRefreshLayout = findViewById(R.id.subcategories_marketoffer_swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
        swipeRefreshLayout.setOnRefreshListener(this);

        recyclerView = findViewById(R.id.subcategories_marketoffer_items_recycler_view);

        LinearLayout containerSortValue = findViewById(R.id.subcategory_marketoffer_container_sort_value);
        containerSortValue.setOnClickListener(this);
        sortValue = findViewById(R.id.subcategory_marketoffer_sort_value);

        btnListDisplay = findViewById(R.id.btn_subcategory_marketoffer_list_display);
        btnListDisplay.setOnClickListener(this);
        btnGridDisplay = findViewById(R.id.btn_subcategory_marketoffer_grid_display);
        btnGridDisplay.setOnClickListener(this);

        ImageButton btnDisplayProfile = findViewById(R.id.btn_display_profile);
        btnDisplayProfile.setOnClickListener(this);
    }

    //Modifie le label du mode de trie
    private void setSortValueView(){
        sortValue.setText(StringUtils.getSpannedText(getResources().getString(R.string.text_sort_value, strSortMode[sortMode])));
    }

    //Initialise le collapsed
    private void initCollapsingToolbar() {
        collapsedToolbar.setTitle(" ");
        appBar.setExpanded(true);

        appBar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    containerTitleToolbar.setAlpha(1f);
                    isShow = true;
                } else if (isShow) {
                    containerTitleToolbar.setAlpha(0);
                    isShow = false;
                }
            }
        });
    }

    //Initialise le appbar
    private void setAppbarLayout(String subTitle, String title, String imageName){
        collapsingSubtitleSubcategory.setText(StringUtils.toUpperFirstChar(subTitle));
        collapsingTitleSubcategory.setText(StringUtils.toUpperFirstChar(title));

        toolbarSubtitleSubcategory.setText(StringUtils.toUpperFirstChar(subTitle));
        toolbarTitleSubcategory.setText(StringUtils.toUpperFirstChar(title));

        GlideComponent.simpleGlideComponentWithoutPlaceholder(this,
                UrlRestApiConfig.IMAGE_RESOURCE_URL + imageName, thumbmailCategory);
    }

    // Initialisation du recyclerview avec les données recues
    private void initRecyclerView(){
        gridConfiguration();
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this,
                recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                MarketOfferDisplayCustom item = marketOffers.get(position);

                if(item instanceof MarketOfferDisplay){
                    MarketOfferDisplay marketOfferDisplay = (MarketOfferDisplay) item;

                    Intent intent = new Intent(SubCategoryMarketofferActivity.this, MarketofferDetails.class);
                    intent.putExtra(StringConfig.INTENT_MARKETOFFER_HEX_ID, marketOfferDisplay.getMarketOffer().getIdHexMarketOffer());
                    startActivity(intent);
                }else{
                    onLoadMoreData();
                }
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));
    }

    //Affichage en grid
    private void gridConfiguration(){
        layoutManager = gridLayoutManager;
        recyclerviewAdapter = gridAdapter;

        //Permet de modifier la disposition du layout manager en fonction de l'élément à inserer dans le recyclerview
        ((GridLayoutManager) layoutManager).setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return ((SubCategoryMarketofferGridAdapter) recyclerviewAdapter).isPositionLoadMore(position) ? ((GridLayoutManager) layoutManager).getSpanCount() : 1;
            }
        });

        ((SubCategoryMarketofferGridAdapter) recyclerviewAdapter).setLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                onLoadMoreData();
            }
        });

        //Manager
        recyclerView.setLayoutManager(layoutManager);
        //On ajoute la decoration pour l'affichage en grille
        recyclerView.addItemDecoration(itemDecoration);
        //On attache l'adaptateur
        recyclerView.setAdapter(recyclerviewAdapter);
    }

    //Affichage en list
    private void listConfiguration(){
        layoutManager = linearLayoutManager;
        recyclerviewAdapter = listAdapter;

        ((SubCategoryMarketofferListAdapter) recyclerviewAdapter).setLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                onLoadMoreData();
            }
        });

        //On retire la decoration pour l'affichage en grille
        recyclerView.removeItemDecoration(itemDecoration);
        //Manager
        recyclerView.setLayoutManager(layoutManager);
        //On attache l'adaptateur
        recyclerView.setAdapter(recyclerviewAdapter);
    }

    //Met à jour le badge
    private void setBadge(long count){
        tvBadge.setText(DisplayLargeNumber.smartLargeNumber(count));
    }

    //Getters et setters

    public void setRange() {
        this.range += 10;
    }

    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return swipeRefreshLayout;
    }

    public CoordinatorLayout getCoordinatorLayout() {
        return coordinator;
    }

    public List<MarketOfferDisplayCustom> getMarketOffers() {
        return marketOffers;
    }

    public RecyclerView.Adapter getRecyclerviewAdapter() {
        return recyclerviewAdapter;
    }
}
