package com.inductivtechnologies.quantmarkapp.activities.profile;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.activities.cart.CartActivity;
import com.inductivtechnologies.quantmarkapp.adapters.profile.ProfileViewpagerAdapter;
import com.inductivtechnologies.quantmarkapp.design.DisplayLargeNumber;
import com.inductivtechnologies.quantmarkapp.design.OvalTextView;
import com.inductivtechnologies.quantmarkapp.fragments.profile.OrdersFragment;
import com.inductivtechnologies.quantmarkapp.fragments.profile.ProfileInfoFragment;
import com.inductivtechnologies.quantmarkapp.fragments.profile.PurchasesFragment;
import com.inductivtechnologies.quantmarkapp.store.repositories.CartItemRepository;

/**
 * UserProfileActivity : Profil de l'utilisateur
 */
@SuppressWarnings("ConstantConditions")
public class UserProfileActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener, View.OnClickListener {

    //Gestionnaire de fragment
    private FragmentManager fragmentManager;

    //Les vues
    private CoordinatorLayout coordinatorLayout;

    //Titre
    private TextView fragmentTitle;

    private OvalTextView tvBadge;

    //Le tablayout
    private TabLayout profileTablayout;

    //Les icones du tablayout (seléctionnées)
    private final int[] selectedIcons = {
            R.drawable.ic_tabitem_profile_info_selected,
            R.drawable.ic_tabitem_profile_purchase_selected,
            R.drawable.ic_tabitem_profile_orders_selected
    };
    //Les icones du tablayout (non seléctionnées)
    private final int[] unselectedIcons = {
            R.drawable.ic_tabitem_profile_info_unselected,
            R.drawable.ic_tabitem_profile_purchase_unselected,
            R.drawable.ic_tabitem_profile_orders_unselected
    };

    //Les titres des fragments
    private final int[] fragmentTitles = {
            R.string.profile_title,
            R.string.purchases_title,
            R.string.orders_title,
    };

    private CartItemRepository cartRepo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        //Toolbar
        Toolbar toolbar = findViewById(R.id.profile_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //Initialisation des objets
        initObjects();

        ///Les vues
        findAllViews();
    }

    //Override

    @Override
    public void onResume() {
        super.onResume();

        //Le badge
        setBadge(cartRepo.getAllCartitems().size());
    }

    @Override
    public void onBackPressed() {
        if (profileTablayout.getSelectedTabPosition() != 0) {
            profileTablayout.getTabAt(0).select();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        int position = tab.getPosition();
        ImageView icon = tab.getCustomView().findViewById(R.id.tabitem_icon);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            icon.setImageDrawable(getResources().getDrawable(selectedIcons[position], null));
        }else{
            icon.setImageDrawable(getResources().getDrawable(selectedIcons[position]));
        }

        fragmentTitle.setText(getResources().getString(fragmentTitles[position]));
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
        int position = tab.getPosition();
        ImageView icon = tab.getCustomView().findViewById(R.id.tabitem_icon);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            icon.setImageDrawable(getResources().getDrawable(unselectedIcons[position], null));
        }else{
            icon.setImageDrawable(getResources().getDrawable(unselectedIcons[position]));
        }
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.profile_toolbar_back :
                finish();
                break;
            case R.id.container_badge :
                //On ouvre le panier
                startActivity(new Intent(UserProfileActivity.this, CartActivity.class));
                break;
        }
    }

    //Méthodes

    private void initObjects() {
        //Gestionnaire de fragment
        fragmentManager = getSupportFragmentManager();

        cartRepo = new CartItemRepository(getApplicationContext());
    }

    // On recupère les vues
    private void findAllViews(){
        coordinatorLayout = findViewById(R.id.profile_coordinator);

        //Le badge
        RelativeLayout containerBadge = findViewById(R.id.container_badge);
        containerBadge.setOnClickListener(this);
        tvBadge = findViewById(R.id.tv_bagde);
        tvBadge.setSolidColor("#ffffff");
        tvBadge.setStrokeWidth(1);
        tvBadge.setStrokeColor("#0277BD");

        //Viewpager
        ViewPager profileViewPager = findViewById(R.id.profile_viewpager);
        initViewPager(profileViewPager);

        //Tablayout
        profileTablayout = findViewById(R.id.profile_tablayout);
        profileTablayout.setupWithViewPager(profileViewPager);
        initTablayout();
        profileTablayout.addOnTabSelectedListener(this);

        //Titre
        fragmentTitle = findViewById(R.id.profile_toolbar_title);
        ImageButton btnClose = findViewById(R.id.profile_toolbar_back);
        btnClose.setOnClickListener(this);
    }

    //Initialisation du tablayout
    @SuppressLint("InflateParams")
    private void initTablayout(){
        View tabitemContent;
        ImageView icon;

        for(int i= 0; i < selectedIcons.length; i++){
            tabitemContent = LayoutInflater.from(this).inflate(R.layout.custom_tab_item_main, null);
            icon = tabitemContent.findViewById(R.id.tabitem_icon);

            if(i == 0){
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                    icon.setImageDrawable(getResources().getDrawable(selectedIcons[i], null));
                }else{
                    icon.setImageDrawable(getResources().getDrawable(selectedIcons[i]));
                }
            }else{
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                    icon.setImageDrawable(getResources().getDrawable(unselectedIcons[i], null));
                }else{
                    icon.setImageDrawable(getResources().getDrawable(unselectedIcons[i]));
                }
            }
            profileTablayout.getTabAt(i).setCustomView(tabitemContent);
        }
    }

    //Initialisation du viewpager
    private void initViewPager(ViewPager viewPager) {
        ProfileViewpagerAdapter adapter = new ProfileViewpagerAdapter(fragmentManager);
        adapter.addFragment(ProfileInfoFragment.newInstance());
        adapter.addFragment(PurchasesFragment.newInstance());
        adapter.addFragment(OrdersFragment.newInstance());

        viewPager.setAdapter(adapter);
    }

    //Met à jour le badge
    private void setBadge(long count){
        tvBadge.setText(DisplayLargeNumber.smartLargeNumber(count));
    }

    //Getters et setters

    public CoordinatorLayout getCoordinatorLayout() {
        return this.coordinatorLayout;
    }
}
