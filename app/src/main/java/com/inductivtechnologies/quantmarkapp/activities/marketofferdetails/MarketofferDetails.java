package com.inductivtechnologies.quantmarkapp.activities.marketofferdetails;

import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.flexbox.FlexboxLayout;
import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.activities.cart.CartActivity;
import com.inductivtechnologies.quantmarkapp.adapters.marketofferdetails.MarketofferViewpagerAdapter;
import com.inductivtechnologies.quantmarkapp.adapters.marketofferdetails.SimilarMarketofferIssuerAdapter;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.configuration.StringConfig;
import com.inductivtechnologies.quantmarkapp.design.DisplayLargeNumber;
import com.inductivtechnologies.quantmarkapp.design.GridSpacingItemDecoration;
import com.inductivtechnologies.quantmarkapp.design.OvalTextView;
import com.inductivtechnologies.quantmarkapp.httptask.marketofferdetails.AddFavoriteTask;
import com.inductivtechnologies.quantmarkapp.httptask.marketofferdetails.DeleteFavoriteTask;
import com.inductivtechnologies.quantmarkapp.httptask.marketofferdetails.MarketOfferDetailsTask;
import com.inductivtechnologies.quantmarkapp.listeners.others.RecyclerTouchListener;
import com.inductivtechnologies.quantmarkapp.models.display.MarketOfferDisplay;
import com.inductivtechnologies.quantmarkapp.models.entities.CartItem;
import com.inductivtechnologies.quantmarkapp.models.entities.Favorite;
import com.inductivtechnologies.quantmarkapp.store.repositories.CartItemRepository;
import com.inductivtechnologies.quantmarkapp.store.repositories.FavoriteRepository;
import com.inductivtechnologies.quantmarkapp.utils.CalculationUtils;
import com.inductivtechnologies.quantmarkapp.utils.DesignUtils;
import com.inductivtechnologies.quantmarkapp.utils.EntitiesUtils;
import com.inductivtechnologies.quantmarkapp.utils.LanguageDisolay;
import com.inductivtechnologies.quantmarkapp.utils.StringUtils;

import org.bson.Document;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * MarketofferDetails : Affiche les détails d'une offre
 */
@SuppressWarnings("ConstantConditions")
public class MarketofferDetails extends AppCompatActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    public static final String MARKETOFFER_DETAILS_TASK = "MARKETOFFER_DETAILS_TASK";
    public static final String ADD_FAVORITE_TASK = "ADD_FAVORITE_TASK";
    public static final String REMOVE_FAVORITE_TASK = "REMOVE_FAVORITE_TASK";

    private CoordinatorLayout coordinatorLayout;
    private Toolbar toolbar;
    private TextView tvCount;

    private OvalTextView tvBadge;

    private SwipeRefreshLayout swipeRefreshLayout;
    private LinearLayout llContainerMain;

    //En-tête
    private TextView tvTitle;
    private TextView tvFinalPrice;
    private TextView tvPrice;
    private TextView tvReductionNotice;

    private TextView tvQuantityCart;

    private ImageButton btnAddFavorite;
    private TextView tvFavorite;

    //Section description
    private TextView tvDescription;
    private ImageButton btnDisplayDescription;

    //Section couleur
    private LinearLayout llContainerColorEx;
    private FlexboxLayout llContainerColorIn;

    //Section taille
    private LinearLayout llContainerLenghtEx;
    private FlexboxLayout llContainerLenghtIn;
    private TextView tvLabelLenght;

    //Section vendeur
    private TextView tvIssuerProfile;
    private TextView tvName;
    private TextView tvLocation;
    private TextView tvCountArticle;

    //Section détails
    private LinearLayout llContainerDetails;
    private TextView tvDetails;

    private RecyclerView recyclerView;
    private LinearLayout llContainerRecyclerview;
    private TextView tvViewMore;

    //Viewpager
    private ViewPager viewPager;

    //Controller
    private AppController appController;

    //Repositories
    private FavoriteRepository favoriteRepository;

    //Repo
    private CartItemRepository cartItemRepo;

    //Task
    private MarketOfferDetailsTask marketofferDetailsTask;
    private AddFavoriteTask addFavoriteTask;
    private DeleteFavoriteTask deleteFavoriteTask;

    //les données
    private String marketOfferHexId;
    private MarketOfferDisplay marketOfferDisplay;
    private int similarCount;

    private Favorite currentFavorite;

    private int quantityCart;
    private boolean quantityChange;
    private boolean isDescriptionExtended;

    //  page change listener
    private final ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            displayAssociateInfo(position);
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        @Override
        public void onPageScrollStateChanged(int arg0) {
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marketoffer_details);

        //Full transparent statusbar
        DesignUtils.makeFullTransparentStatutBar(this);

        toolbar = findViewById(R.id.marketoffer_details_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        marketOfferHexId = getIntent().getStringExtra(StringConfig.INTENT_MARKETOFFER_HEX_ID);

        //Objets
        initObjects();

        //Les vues
        findAllViews();

        //Task
        launchTask();
    }

    @Override
    public void onResume() {
        super.onResume();

        //Le badge
        setBadge(cartItemRepo.getAllCartitems().size());
    }

    @Override
    public void onDestroy() {
        //Annulation des requêtes
        this.appController.cancelPendingRequests(MARKETOFFER_DETAILS_TASK);
        this.appController.cancelPendingRequests(MARKETOFFER_DETAILS_TASK);
        this.appController.cancelPendingRequests(REMOVE_FAVORITE_TASK);

        super.onDestroy();
    }

    @Override
    public void onRefresh() {
        launchTask();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.marketoffer_details_btn_close:
                finish();
                break;
            case R.id.container_badge:
                //On ouvre le panier
                startActivity(new Intent(MarketofferDetails.this, CartActivity.class));
                break;
            case R.id.marketoffer_details_btn_display_description:
                if (!isDescriptionExtended) {
                    isDescriptionExtended = true;
                    btnDisplayDescription.setImageDrawable(getResources().getDrawable(R.drawable.ic_action_read_minus_text));

                    tvDescription.setMaxLines(500);
                } else {
                    isDescriptionExtended = false;
                    btnDisplayDescription.setImageDrawable(getResources().getDrawable(R.drawable.ic_action_read_more_text));

                    tvDescription.setMaxLines(4);
                }
                break;
            case R.id.btn_add_cart:
                addToCart();
                break;
            case R.id.btn_add_favorite:
                addToFavorite();
                break;
            case R.id.btn_decrement_quantity:
                removeQuantity();
                break;
            case R.id.btn_increment_quantity:
                addQuantity();
                break;
            case R.id.tv_view_more_issuer_marketoffer:
                Intent intent = new Intent(this, IssuerProductActivity.class);
                intent.putExtra(StringConfig.INTENT_ISSUER_NB_MARKET_MARKETOFFERS, similarCount);
                intent.putExtra(StringConfig.INTENT_ISSUER_COMPLETE_NAME, marketOfferDisplay
                        .getMarketOffer().getProduct().getIssuer().getCompleteName());
                intent.putExtra(StringConfig.INTENT_ISSUER_HEX_ID, marketOfferDisplay
                        .getMarketOffer().getProduct().getIssuer().getObjectIdHexUser());

                startActivity(intent);
                break;
            case R.id.btn_call_seller:
                String phoneNumber = marketOfferDisplay.getMarketOffer().getProduct().getIssuer().getPhone().getNumber();

                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:" + phoneNumber));

                startActivity(callIntent);

                break;
        }
    }

    //Les méthodes

    //Initialisation des objets
    private void initObjects() {
        //Repository
        favoriteRepository = new FavoriteRepository(getApplicationContext());

        //Instance singleton du controlleur volley
        this.appController = AppController.getInstance();

        //Task
        marketofferDetailsTask = new MarketOfferDetailsTask(this, appController);
        addFavoriteTask = new AddFavoriteTask(this, appController);
        deleteFavoriteTask = new DeleteFavoriteTask(this, appController);

        // Repositories
        cartItemRepo = new CartItemRepository(getApplicationContext());

        CartItem cartitemExist = cartItemRepo.findCartitemByIdMarketOffer(marketOfferHexId);
        if(cartitemExist != null){
            quantityCart = cartitemExist.getQuantity();
        }else{
            quantityCart = 1;
        }
    }


    private void launchTask(){
        marketofferDetailsTask.makeRequest(marketOfferHexId);
    }

    public void globalInit(){
        llContainerMain.setVisibility(View.VISIBLE);

        //En-tête
        initHeaderSection();
        initBtnAddFavorite();

        //Section description
        initSectionDescription();

        //Section couleur
        initSectionColor();

        //Section couleur
        initSectionLenght();

        //Section vendeur
        initSectionIssuer();

        //Section details
        initSectionDetails();
    }

    private void initHeaderSection(){
        double originalPrice = marketOfferDisplay.getMarketOffer().getProduct().getOriginalprice();
        double reductionPrice = marketOfferDisplay.getMarketOffer().getPrice();

        //Titre
        tvTitle.setText(StringUtils
                .toUpperFirstChar((String) LanguageDisolay
                        .displayProduct(marketOfferDisplay
                                .getMarketOffer().getProduct()).get(LanguageDisolay.FIELD_TITLE)));

        //Prix reduit
        if(reductionPrice == originalPrice){
            tvFinalPrice.setText(getResources().getString(R.string.display_price, NumberFormat.getIntegerInstance().format(originalPrice)));
            tvPrice.setVisibility(View.GONE);
            tvReductionNotice.setVisibility(View.GONE);
        }else{
            tvFinalPrice.setText(getResources().getString(R.string.display_price, NumberFormat.getIntegerInstance().format(reductionPrice)));

            tvPrice.setVisibility(View.VISIBLE);
            tvPrice.setText(getResources().getString(R.string.display_price, NumberFormat.getIntegerInstance().format(originalPrice)));
            tvPrice.setPaintFlags(this.tvPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            String strReduction = marketOfferDisplay.getMarketOffer().getCoupon().getValue() + "%";
            tvReductionNotice.setVisibility(View.VISIBLE);
            tvReductionNotice.setText(getResources().getString(R.string.display_reduction_notice, strReduction));
        }
    }

    private void initSectionDescription(){
        tvDescription.setText(StringUtils
                .toUpperFirstChar((String) LanguageDisolay
                        .displayProduct(marketOfferDisplay
                                .getMarketOffer().getProduct()).get(LanguageDisolay.FIELD_DESCRIPTION)));
    }

    private void initSectionColor(){
        Document details = marketOfferDisplay.getMarketOffer().getProduct().getDetailFr();

        if(details == null || details.isEmpty() || EntitiesUtils.documentIsFullEmpty(details)) {
            llContainerColorEx.setVisibility(View.GONE);
        }else{
            if(details.containsKey("Couleur")){
                try{
                    String strColor = "#" + details.get("Couleur");
                    //int color = Color.parseColor(strColor);

                    OvalTextView panelColor = new OvalTextView(this);
                    panelColor.setWidth(CalculationUtils.dpToPx(this,
                            getResources().getDimension(R.dimen.marketofferer_details_dimen_18)));
                    panelColor.setHeight(CalculationUtils.dpToPx(this,
                            getResources().getDimension(R.dimen.marketofferer_details_dimen_18)));
                    panelColor.setSolidColor(strColor);

                    llContainerColorEx.setVisibility(View.VISIBLE);
                    llContainerColorIn.removeAllViews();
                    llContainerColorIn.addView(panelColor);
                }catch (IllegalArgumentException error){
                    llContainerColorEx.setVisibility(View.GONE);
                }
            }else{
                llContainerColorEx.setVisibility(View.GONE);
            }
        }
    }

    private void initSectionLenght(){
        List<String> tailles = marketOfferDisplay.getMarketOffer().getProduct().getTailles();

        if(tailles == null || tailles.size() == 0){
            llContainerLenghtEx.setVisibility(View.GONE);
        }else{

            TextView textLenght = (TextView) getLayoutInflater()
                    .inflate(R.layout.layout_length, llContainerColorIn, false);
            //textLenght.setBackground(getResources().getDrawable(R.drawable.layout_with_stroke));

            if(tailles.size() > 1){
                tvLabelLenght.setText(getResources().getString(R.string.marketoffer_details_label_lenght_plural));
            }else{
                tvLabelLenght.setText(getResources().getString(R.string.marketoffer_details_label_lenght_sin));
            }

            if(tailles.size() > 0){
                llContainerLenghtEx.setVisibility(View.VISIBLE);
                llContainerLenghtIn.removeAllViews();

                for(String taille : tailles){
                    if(!taille.equals("")){
                        textLenght.setText(taille);
                        llContainerLenghtIn.addView(textLenght);
                    }
                }
            }else{
                llContainerLenghtEx.setVisibility(View.GONE);
            }
        }
    }

    private void initSectionIssuer(){
        //Profile ind
        String[] words = marketOfferDisplay.getMarketOffer().getProduct().getIssuer().getCompleteName().split(" ");
        String contentProfile;
        if(words.length >= 2){
            contentProfile = words[0].substring(0,1).toUpperCase() + "" + words[1].substring(0, 1).toUpperCase();
        }else{
            contentProfile =  words[0].substring(0,1).toUpperCase();
        }
        tvIssuerProfile.setText(contentProfile);

        //Nom
        tvName.setText(StringUtils.toUpperFirstChar(marketOfferDisplay.getMarketOffer().
                getProduct().getIssuer().getCompleteName()));

        //location
        String location = marketOfferDisplay.getMarketOffer().getProduct().getIssuer().getAddress().getCity() + " " +
                marketOfferDisplay.getMarketOffer().getProduct().getIssuer().getAddress().getStreet();
        tvLocation.setText(StringUtils.toUpperFirstChar(location));

        //Nombre d'article actifs
        if(similarCount != 0){
            tvCountArticle.setVisibility(View.VISIBLE);
            tvCountArticle.setText(getResources().getQuantityString(R.plurals.display_nb_articles_plural, similarCount, similarCount));
        }else{
            tvCountArticle.setVisibility(View.INVISIBLE);
        }
    }

    private void initSectionDetails(){
        Document details = marketOfferDisplay.getMarketOffer().getProduct().getDetailFr();

        //Si le document est vide on cache tout
        if(details == null){
            llContainerDetails.setVisibility(View.GONE);
        }else{
            if(details.containsKey("Couleur")){
                details.remove("Couleur");
                if(details.isEmpty() || EntitiesUtils.documentIsFullEmpty(details)){
                    llContainerDetails.setVisibility(View.GONE);
                }else{
                    llContainerDetails.setVisibility(View.VISIBLE);

                    StringBuilder content = new StringBuilder();

                    for(Map.Entry<String, Object> entry : details.entrySet()){
                        String key = entry.getKey();
                        String value = (String) entry.getValue();

                        //On affiche tout sauf la couleur
                        if(!value.equals("") && !key.equalsIgnoreCase("Couleur")){
                            String inter = key + " : " + value;
                            content.append(inter).append("\n");
                        }
                    }
                    tvDetails.setText(content.toString());
                }
            }
        }
    }

    private void initBtnAddFavorite(){
        currentFavorite = favoriteRepository.findFavorite(marketOfferHexId);

        if(currentFavorite != null){
            btnAddFavorite.setImageDrawable(getResources().getDrawable(R.drawable.ic_action_add_favorite_success));
            tvFavorite.setText(getResources().getString(R.string.marketoffer_details_label_delete_favorite));
        }else{
            btnAddFavorite.setImageDrawable(getResources().getDrawable(R.drawable.ic_action_add_favorite));
            tvFavorite.setText(getResources().getString(R.string.marketoffer_details_label_add_favorite));
        }
    }

    public void initViewpager(){
        viewPager = findViewById(R.id.marketoffer_details_viewpager);

        ArrayList<String> images = new ArrayList<>();
        images.add(marketOfferDisplay.getMarketOffer().getProduct().getImage());

        MarketofferViewpagerAdapter viewpagerAdapter = new MarketofferViewpagerAdapter(this, images);
        viewPager.setAdapter(viewpagerAdapter);

        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);

        //viewPager.setPageTransformer(true, new DepthPageTransformer());

        int selectedPosition = 0;
        setCurrentItem(selectedPosition);
        displayAssociateInfo(selectedPosition);
    }

    private void findAllViews(){
        llContainerMain = findViewById(R.id.marketoffer_details_container_main);

        ImageButton btnAddCart = findViewById(R.id.btn_add_cart);
        btnAddCart.setOnClickListener(this);

        btnAddFavorite = findViewById(R.id.btn_add_favorite);
        btnAddFavorite.setOnClickListener(this);

        ImageButton btnCallSeller = findViewById(R.id.btn_call_seller);
        btnCallSeller.setOnClickListener(this);

        tvFavorite = findViewById(R.id.tv_favorite);

        //Le badge
        RelativeLayout containerBadge = findViewById(R.id.container_badge);
        containerBadge.setOnClickListener(this);
        tvBadge = findViewById(R.id.tv_bagde);
        tvBadge.setSolidColor("#ffffff");

        coordinatorLayout = findViewById(R.id.marketoffer_details_coordinator);
        toolbar = findViewById(R.id.marketoffer_details_toolbar);
        ImageButton btnBack = findViewById(R.id.marketoffer_details_btn_close);
        btnBack.setOnClickListener(this);

        tvCount = findViewById(R.id.tv_marketoffer_details_count_slide);

        swipeRefreshLayout = findViewById(R.id.marketoffer_details_swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
        swipeRefreshLayout.setOnRefreshListener(this);

        //En-tête
        tvTitle = findViewById(R.id.marketoffer_details_title);
        tvFinalPrice = findViewById(R.id.marketoffer_details_final_price);
        tvPrice = findViewById(R.id.marketoffer_details_price);
        tvReductionNotice = findViewById(R.id.marketoffer_details_reduction_notice);

        //Section description
        tvDescription = findViewById(R.id.marketoffer_details_description);
        btnDisplayDescription = findViewById(R.id.marketoffer_details_btn_display_description);
        btnDisplayDescription.setOnClickListener(this);

        //Section couleur
        llContainerColorEx = findViewById(R.id.marketoffer_details_container_color);
        llContainerColorIn = findViewById(R.id.marketoffer_details_container_colors);
        //Section taille
        llContainerLenghtEx = findViewById(R.id.marketoffer_details_container_lenght);
        llContainerLenghtIn = findViewById(R.id.marketoffer_details_container_lenghts);
        tvLabelLenght = findViewById(R.id.marketoffer_details_label_lenght);

        //Section vendeur
        tvIssuerProfile = findViewById(R.id.marketoffer_details_issuer_ind);
        tvName = findViewById(R.id.marketoffer_details_issuer_name);
        tvLocation = findViewById(R.id.marketoffer_details_issuer_address);
        tvCountArticle = findViewById(R.id.marketoffer_details_tv_issuer_display_nb_articles);

        //Section détails
        llContainerDetails = findViewById(R.id.marketoffer_details_container_others_details);
        tvDetails = findViewById(R.id.marketoffer_details_others_details);

        recyclerView = findViewById(R.id.marketoffer_details_display_recyclerview);
        llContainerRecyclerview = findViewById(R.id.marketoffer_details_container_recyclerview);
        tvViewMore = findViewById(R.id.tv_view_more_issuer_marketoffer);
        tvViewMore.setOnClickListener(this);

        ImageButton btnAddQuantity = findViewById(R.id.btn_increment_quantity);
        btnAddQuantity.setOnClickListener(this);
        ImageButton btnRemoveQuantity = findViewById(R.id.btn_decrement_quantity);
        btnRemoveQuantity.setOnClickListener(this);
        tvQuantityCart = findViewById(R.id.marketoffer_details_quantity);
        tvQuantityCart.setText(String.valueOf(quantityCart));
    }

    private void addQuantity(){
        quantityChange = true;
        int quantity = marketOfferDisplay.getMarketOffer().getProduct().getQuantity();

        if(quantity == 0){
            DesignUtils.makeSnackbar(getResources().getString(R.string.label_quantity_full), coordinatorLayout, getApplicationContext());
        }else{
           if((quantityCart + 1) > quantity){
               DesignUtils.makeSnackbar(getResources().getString(R.string.label_quantity_full), coordinatorLayout, getApplicationContext());
           }else{
               quantityCart += 1;
               tvQuantityCart.setText(String.valueOf(quantityCart));
           }
        }
    }

    private void removeQuantity(){
        quantityChange = true;
        int quantity = marketOfferDisplay.getMarketOffer().getProduct().getQuantity();

        if(quantity == 0){
            DesignUtils.makeSnackbar(getResources().getString(R.string.label_quantity_full), coordinatorLayout, getApplicationContext());
        }else{
            if((quantityCart - 1) > 0) {
                quantityCart -= 1;
                tvQuantityCart.setText(String.valueOf(quantityCart));
            }
        }
    }

    // Ajoute au panier
    private void addToCart(){
        if(marketOfferDisplay.getMarketOffer().getProduct().getQuantity() == 0){
            DesignUtils.makeSnackbar(getResources().getString(R.string.label_quantity_full), coordinatorLayout, getApplicationContext());
        }else{
            final CartItem cartitemUpdated = cartItemRepo.findCartitemByIdMarketOffer(marketOfferHexId);
            if(cartitemUpdated != null){
                int quantityUpdate;
                int quantity = marketOfferDisplay.getMarketOffer().getProduct().getQuantity();

                //Quantité à mettre à jour
                if(quantityChange){
                    quantityUpdate = quantityCart;
                }else{
                    quantityUpdate = cartitemUpdated.getQuantity() + 1;
                }

                if(quantityUpdate > quantity){
                    DesignUtils.makeSnackbar(getResources().getString(R.string.cart_quantity_unsuccess_add_cart), coordinatorLayout, getApplicationContext());
                }else{
                    //On modifie l'indicateur de quantité
                    tvQuantityCart.setText(String.valueOf(quantityUpdate));
                    //Nouveau total
                    double totalPriceUpdated = EntitiesUtils.getTotalPriceOfCartitem(quantityUpdate, cartitemUpdated.getReductionPrice(), 0);

                    // On modifie les propriétés
                    cartitemUpdated.setQuantity(quantityUpdate);
                    cartitemUpdated.setTotalPrice(totalPriceUpdated);

                    //MAJ
                    long result = cartItemRepo.updateCartItem(cartitemUpdated);
                    if(result > 0){
                        DesignUtils.makeSnackbar(getResources().getString(R.string.cart_success_add_cart), coordinatorLayout, getApplicationContext());
                    }else{
                        DesignUtils.makeSnackbar(getResources().getString(R.string.cart_unsuccess_add_cart), coordinatorLayout, getApplicationContext());
                    }
                }
            }else{
                double originalPrice = marketOfferDisplay.getMarketOffer().getProduct().getOriginalprice();
                double reductionPrice = marketOfferDisplay.getMarketOffer().getPrice();

                // On crée un cartItem
                CartItem cartItem = new CartItem();
                //Id
                cartItem.setIdHexMarketOffer(marketOfferDisplay.getMarketOffer().getIdHexMarketOffer());

                //Titre
                cartItem.setTitleFr(marketOfferDisplay.getMarketOffer().getProduct().getTitleFr());
                cartItem.setTitleEn(marketOfferDisplay.getMarketOffer().getProduct().getTitleEn());
                //Image
                cartItem.setImage(marketOfferDisplay.getMarketOffer().getProduct().getImage());
                //Issuername
                cartItem.setIssuerName(marketOfferDisplay.getMarketOffer().getProduct().getIssuer().getCompleteName());
                //Reduction
                cartItem.setReductionValue(marketOfferDisplay.getMarketOffer().getCoupon().getValue());
                //Quantité
                cartItem.setQuantity(quantityCart);
                //code
                cartItem.setCode(marketOfferDisplay.getMarketOffer().getProduct().getProductCode());

                //Prix unitaire
                cartItem.setReductionPrice(reductionPrice);
                cartItem.setSinglePrice(originalPrice);

                // Add data
                double totalPrice = EntitiesUtils.getTotalPriceOfCartitem(quantityCart, reductionPrice, 0);
                //Total
                cartItem.setTotalPrice(totalPrice);

                long result = cartItemRepo.insertCartItem(cartItem);
                if(result > 0){
                    //On met à jour le badge
                    setBadge(cartItemRepo.getAllCartitems().size());
                    DesignUtils.makeSnackbar(getResources().getString(R.string.cart_success_add_cart), coordinatorLayout, getApplicationContext());
                }else{
                    DesignUtils.makeSnackbar(getResources().getString(R.string.cart_unsuccess_add_cart), coordinatorLayout, getApplicationContext());
                }
            }
        }
    }

    private void addToFavorite(){
        currentFavorite = favoriteRepository.findFavorite(marketOfferHexId);

        if(currentFavorite == null){
            addFavoriteTask.makeRequest(marketOfferHexId);
        }else{
            deleteFavoriteTask.makeRequest(marketOfferHexId);
        }
    }

    //Recyclerview
    public void initRecyclerView(List<MarketOfferDisplay> marketoffers){
        recyclerView.setNestedScrollingEnabled(false);
        //Lié à la taille des éléments
        recyclerView.setHasFixedSize(true);

        if(marketoffers == null || marketoffers.size() == 0){
            llContainerRecyclerview.setVisibility(View.GONE);
        }else{
            llContainerRecyclerview.setVisibility(View.VISIBLE);
            if(marketoffers.size() < similarCount){
                tvViewMore.setVisibility(View.VISIBLE);
            }else {
                tvViewMore.setVisibility(View.GONE);
            }

            StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(3,
                    StaggeredGridLayoutManager.VERTICAL);
            recyclerView.setLayoutManager(layoutManager);

            SimilarMarketofferIssuerAdapter adapter = new SimilarMarketofferIssuerAdapter(this, marketoffers);
            recyclerView.setAdapter(adapter);

            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.addItemDecoration(new GridSpacingItemDecoration(3,
                    CalculationUtils.dpToPx(this, 6), true));

            recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this,recyclerView,
                    new RecyclerTouchListener.ClickListener() {
                        @Override
                        public void onClick(View view, int position) {
                        }

                        @Override
                        public void onLongClick(View view, int position) {
                        }
                    }));
        }
    }

    // Lors du changement du viewpager on modifie les elements graphiques
    private void displayAssociateInfo(int position) {
        tvCount.setText(getResources()
                .getString(R.string.display_count_item, position + 1,
                        marketOfferDisplay.getMarketOffer().getProduct().getImageList().size()));
    }

    // Retourne l'item courant du viewpager
    private void setCurrentItem(int position) {
        viewPager.setCurrentItem(position, false);
    }

    //Met à jour le badge
    private void setBadge(long count){
        tvBadge.setText(DisplayLargeNumber.smartLargeNumber(count));
    }

    //Getters et setters

    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return swipeRefreshLayout;
    }

    public CoordinatorLayout getCoordinatorLayout() {
        return coordinatorLayout;
    }

    public void setSimilarCount(int similarCount) {
        this.similarCount = similarCount;
    }

    public void setMarketOfferDisplay(MarketOfferDisplay marketOfferDisplay) {
        this.marketOfferDisplay = marketOfferDisplay;
    }

    public ImageButton getBtnAddFavorite() {
        return btnAddFavorite;
    }

    public TextView getTvFavorite() {
        return tvFavorite;
    }
}
