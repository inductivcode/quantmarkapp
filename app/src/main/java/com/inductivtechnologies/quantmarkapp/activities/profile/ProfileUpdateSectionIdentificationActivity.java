package com.inductivtechnologies.quantmarkapp.activities.profile;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ImageButton;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.httptask.profile.UpdateProfileSectionIdentificationTask;
import com.inductivtechnologies.quantmarkapp.models.entities.User;
import com.inductivtechnologies.quantmarkapp.store.repositories.UserRepository;
import com.inductivtechnologies.quantmarkapp.utils.FormsUtils;
import com.inductivtechnologies.quantmarkapp.utils.StringUtils;

import java.util.List;

/**
 * ProfileUpdateSectionIdentificationActivity : Pour mettre à jour la section identification du profile de l'utilisateur
 */
@SuppressWarnings("ConstantConditions")
public class ProfileUpdateSectionIdentificationActivity extends AppCompatActivity implements View.OnClickListener{

    public static final String UPDATE_IND_TASK = "UPDATE_IND_TASK";

    //Les vues
    private CoordinatorLayout coordinatorLayout;

    private TextInputLayout inputLayoutLastname;
    private TextInputEditText etLastname;
    private TextInputLayout inputLayoutFirstname;
    private TextInputEditText etFirstname;

    //Controlleur
    private AppController appController;

    private UpdateProfileSectionIdentificationTask updateTask;

    //Repositoires
    private UserRepository userRepository;

    private  User currentUser = null;

    private FormsUtils formsUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_prodile_section_identification);

        //Toolbar
        Toolbar toolbar = findViewById(R.id.update_section_iden_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //Init
        initObjects();

        //Toutes les vues
        allViews();

        globalInit();
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.btn_close_update_section_iden :
                finish();
                break;
            case R.id.btn_validation_update_section_iden :
                validateFormUpdate();
                break;
        }
    }

    @Override
    public void onDestroy() {
        // On annule la requête
        this.appController.cancelPendingRequests(UPDATE_IND_TASK);

        super.onDestroy();
    }

    // Les méthodes

    //Utilisateur courant
    private User getCurrenUser(){
        List<User> users = userRepository.getAllUsers();
        if(users != null && !users.isEmpty()) return users.get(0);
        return null;
    }

    private void initObjects() {
        //Repository
        userRepository = new UserRepository(getApplicationContext());

        currentUser = getCurrenUser();

        //Instance singleton du controlleur volley
        appController = AppController.getInstance();

        //Task
        updateTask = new UpdateProfileSectionIdentificationTask(this, appController);

        formsUtils = new FormsUtils(getApplicationContext());
    }

    private void globalInit(){
        if(currentUser != null){
            initFamilyName(currentUser.getName());
            intFirstname(currentUser.getFirstName());
        }
    }

    private void initFamilyName(String familyName){
        etLastname.setText(StringUtils.toUpperFirstChar(familyName));
    }

    private void intFirstname(String firstname){
        etFirstname.setText(StringUtils.toUpperFirstChar(firstname));
    }

    private void allViews(){
        coordinatorLayout = findViewById(R.id.update_section_iden_coordinator);

        ImageButton btnClose = findViewById(R.id.btn_close_update_section_iden);
        btnClose.setOnClickListener(this);
        ImageButton btnSave = findViewById(R.id.btn_validation_update_section_iden);
        btnSave.setOnClickListener(this);

        inputLayoutLastname = findViewById(R.id.input_layout_lastname_update);
        etLastname = findViewById(R.id.edittext_lastname_update);
        etLastname.addTextChangedListener(new UpdateTextWatcher(etLastname));
        inputLayoutFirstname = findViewById(R.id.input_layout_firstname_update);
        etFirstname = findViewById(R.id.edittext_firstname_update);
        etFirstname.addTextChangedListener(new UpdateTextWatcher(etFirstname));
    }

    //Validation du formulaire
    private void validateFormUpdate() {
        String lastname = etLastname.getText().toString().trim();
        String firstname = etFirstname.getText().toString().trim();

        boolean validLastName = formsUtils.validateSimpleInput(inputLayoutLastname, lastname);
        boolean validFirstname = formsUtils.validateSimpleInput(inputLayoutFirstname, firstname);

        if(validLastName && validFirstname){
            if(currentUser != null) {
                currentUser.setName(lastname);
                currentUser.setFirstName(firstname);

                //Task
                updateTask.makeRequest(currentUser);
            }
        }
    }

    //Getters et setters

    public CoordinatorLayout getCoordinatorLayout() {
        return this.coordinatorLayout;
    }

    //Classes internes

    private class UpdateTextWatcher implements TextWatcher {

        private final View view;

        private UpdateTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.edittext_lastname_update:
                    formsUtils.validateSimpleInput(inputLayoutLastname, etLastname.getText().toString().trim());
                    break;
                case R.id.edittext_firstname_update:
                    formsUtils.validateSimpleInput(inputLayoutFirstname, etFirstname.getText().toString().trim());
                    break;
            }
        }
    }
}
