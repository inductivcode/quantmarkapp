package com.inductivtechnologies.quantmarkapp.activities.marketofferdetails;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.adapters.marketofferdetails.IssuerProductsItemGridAdapter;
import com.inductivtechnologies.quantmarkapp.adapters.marketofferdetails.IssuerProductsItemListAdapter;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.bottomsheets.BottomsheetSortMode;
import com.inductivtechnologies.quantmarkapp.configuration.NumberConfig;
import com.inductivtechnologies.quantmarkapp.configuration.StringConfig;
import com.inductivtechnologies.quantmarkapp.design.GridSpacingItemDecoration;
import com.inductivtechnologies.quantmarkapp.httptask.marketofferdetails.LoadIssuerMarketOffersTask;
import com.inductivtechnologies.quantmarkapp.listeners.others.RecyclerTouchListener;
import com.inductivtechnologies.quantmarkapp.models.display.LoadMoreMarketOfferDisplay;
import com.inductivtechnologies.quantmarkapp.models.display.MarketOfferDisplay;
import com.inductivtechnologies.quantmarkapp.models.utils.MarketOfferDisplayCustom;
import com.inductivtechnologies.quantmarkapp.utils.CalculationUtils;
import com.inductivtechnologies.quantmarkapp.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * IssuerProductActivity
 */
@SuppressWarnings("ConstantConditions")
public class IssuerProductActivity extends AppCompatActivity implements
        SwipeRefreshLayout.OnRefreshListener, View.OnClickListener{

    public static final String ISSUER_MARKETOFFERS_TASK = "ISSUER_MARKETOFFERS_TASK";

    //Les vues
    private CoordinatorLayout coordinatorLayout;

    private TextView title;
    private TextView subTitle;

    private SwipeRefreshLayout swipeRefreshLayout;

    // Recycler view
    private RecyclerView recyclerView;
    private RecyclerView.Adapter recyclerviewAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private GridSpacingItemDecoration itemDecoration;

    private GridLayoutManager gridLayoutManager;
    private LinearLayoutManager linearLayoutManager;
    private IssuerProductsItemGridAdapter gridAdapter;
    private IssuerProductsItemListAdapter listAdapter;

    private TextView sortValue;

    private ImageButton btnListDisplay;
    private ImageButton btnGridDisplay;

    //Nombre total d'article pour le vendeur courrant
    private int nbMarketOffers;
    //Nom du vendeur
    private String issuerCompleteName;
    //Id du vendeur
    private String idHexIssuer;
    //Offset lors de l'interrogation de la base de données
    private int range = 0;

    //Le données
    private List<MarketOfferDisplayCustom> marketoffers;

    //Controller
    private AppController appController;

    //Task
    private LoadIssuerMarketOffersTask issuerMarketoffersTask;

    //Dialog qui permet de choisir le mode de trie
    private BottomsheetSortMode bshSortMode;

    //Mode de trie
    private int sortMode;

    //Pour iniquer le mode d'affichage
    private boolean isGridDisplay = true;

    //Pour afficher le mode de trie
    private String[] strSortMode;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_issuer_products);

        //Toolbar
        Toolbar toolbar = findViewById(R.id.issuer_product_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //INitialisation des objets
        initObjects();

        //Les vues
        findAllViews();

        //Initialisation de la barre des outils
        setToolbar();

        //Ecouteurs
        addListeners();

        setSortValueView();

        //Recyclerview
        initRecyclerView();

        //Task
        launchTask();
    }

    //Ovveride

    @Override
    public void onDestroy() {
        //Annulation des requêtes
        this.appController.cancelPendingRequests(ISSUER_MARKETOFFERS_TASK);

        super.onDestroy();
    }

    @Override
    public void onRefresh() {
        // Raffraichi l'interface de nouveau
        marketoffers.clear();
        range = 0;
        launchTask();
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.bsd_sort_mode_btn_action_save :
                bshSortMode.hide();

                //On modifie le mode de trie
                sortMode = bshSortMode.getSortMode();

                //On raffraichi avec le task
                onRefresh();

                //On raffraichi
                setSortValueView();
                break;
            case R.id.btn_close_issuer_product :
                finish();
                break;
            case R.id.issuer_products_container_sort_value :
                bshSortMode.show();
                break;
            case R.id.issuer_products_btn_display_list :
                if(isGridDisplay){
                    isGridDisplay = false;
                    btnGridDisplay.setImageDrawable(getResources().getDrawable(R.drawable.ic_action_display_grid_unselected));
                    btnListDisplay.setImageDrawable(getResources().getDrawable(R.drawable.ic_action_display_list_selected));

                    listConfiguration();
                }
                break;
            case R.id.issuer_products_btn_display_grid :
                if(!isGridDisplay){
                    isGridDisplay = true;
                    btnGridDisplay.setImageDrawable(getResources().getDrawable(R.drawable.ic_action_display_grid_selected));
                    btnListDisplay.setImageDrawable(getResources().getDrawable(R.drawable.ic_action_display_list_unselected));

                    gridConfiguration();
                }
                break;
        }
    }

    //Les méthodes

    private void launchTask(){
        issuerMarketoffersTask.makeRequest(idHexIssuer, NumberConfig.NB_WIDTH_DATA, range, sortMode);
    }

    //Chargement externe
    private void onLoadMoreData(){
        marketoffers.remove(marketoffers.size() - 1);
        recyclerviewAdapter.notifyItemRemoved(marketoffers.size());

        if(recyclerviewAdapter instanceof IssuerProductsItemGridAdapter){
            ((IssuerProductsItemGridAdapter) recyclerviewAdapter).setWithLoadMore(false);
        }else{
            ((IssuerProductsItemListAdapter) recyclerviewAdapter).setWithLoadMore(false);
        }

        //On charge les données de nouveau
        launchTask();
    }

    //Utilisée pour savoir si les données ont été totalemnt chargées et quand ajouter le load more
    public void loadMoreAsyn(){
        if(marketoffers.size() < nbMarketOffers){
            marketoffers.add(new LoadMoreMarketOfferDisplay());
            if(recyclerviewAdapter instanceof IssuerProductsItemGridAdapter){
                ((IssuerProductsItemGridAdapter)recyclerviewAdapter).setWithLoadMore(true);
            }else{
                ((IssuerProductsItemListAdapter)recyclerviewAdapter).setWithLoadMore(true);
            }
        }
    }

    // Initialisation du recyclerview avec les données recues
    private void initRecyclerView() {
        gridConfiguration();
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this,
                recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                MarketOfferDisplayCustom item = marketoffers.get(position);

                if(item instanceof MarketOfferDisplay){
                    MarketOfferDisplay marketOfferDisplay = (MarketOfferDisplay) marketoffers.get(position);

                    // On passe à l'activité suivante
                    Intent intent = new Intent(IssuerProductActivity.this, MarketofferDetails.class);
                    intent.putExtra(StringConfig.INTENT_MARKETOFFER_HEX_ID,
                            marketOfferDisplay.getMarketOffer().getIdHexMarketOffer());
                    startActivity(intent);
                }else{
                    onLoadMoreData();
                }
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));
    }

    //Affichage en grid
    private void gridConfiguration(){
        layoutManager = gridLayoutManager;
        recyclerviewAdapter = gridAdapter;

        //Permet de modifier la disposition du layout manager en fonction de l'élément à inserer dans le recyclerview
        ((GridLayoutManager) layoutManager).setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return ((IssuerProductsItemGridAdapter) recyclerviewAdapter)
                        .isPositionLoadMore(position) ? ((GridLayoutManager) layoutManager).getSpanCount() : 1;
            }
        });

        ((IssuerProductsItemGridAdapter) recyclerviewAdapter)
                .setLoadMoreListener(new IssuerProductsItemGridAdapter.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                onLoadMoreData();
            }
        });

        //Manager
        recyclerView.setLayoutManager(layoutManager);
        //On ajoute la decoration pour l'affichage en grille
        recyclerView.addItemDecoration(itemDecoration);
        //On attache l'adaptateur
        recyclerView.setAdapter(recyclerviewAdapter);
    }

    //Affichage en list
    private void listConfiguration(){
        layoutManager = linearLayoutManager;
        recyclerviewAdapter = listAdapter;

        ((IssuerProductsItemListAdapter) recyclerviewAdapter).
                setLoadMoreListener(new IssuerProductsItemListAdapter.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                onLoadMoreData();
            }
        });

        //On retire la decoration pour l'affichage en grille
        recyclerView.removeItemDecoration(itemDecoration);
        //Manager
        recyclerView.setLayoutManager(layoutManager);
        //On attache l'adaptateur
        recyclerView.setAdapter(recyclerviewAdapter);
    }

    private void initObjects() {
        //Les valeurs contenues dans l'intent
        nbMarketOffers = getIntent().getIntExtra(StringConfig.INTENT_ISSUER_NB_MARKET_MARKETOFFERS, 0);
        issuerCompleteName = getIntent().getStringExtra(StringConfig.INTENT_ISSUER_COMPLETE_NAME);
        idHexIssuer =  getIntent().getStringExtra(StringConfig.INTENT_ISSUER_HEX_ID);

        //On initialise les données
        marketoffers = new ArrayList<>();

        //Instance singleton du controlleur volley
        appController = AppController.getInstance();

        //Task objet
        issuerMarketoffersTask = new LoadIssuerMarketOffersTask(this, appController);

        //Pour choisir le mode de trie
        bshSortMode = new BottomsheetSortMode(this);
        sortMode = bshSortMode.getSortMode();

        //Item decoration
        itemDecoration = new GridSpacingItemDecoration(2,
                CalculationUtils.dpToPx(this, 6), true);

        gridAdapter = new IssuerProductsItemGridAdapter(this, this.marketoffers);
        listAdapter = new IssuerProductsItemListAdapter(this, this.marketoffers);

        gridLayoutManager = new GridLayoutManager(this, 2);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,
                false);

        strSortMode = getResources().getStringArray(R.array.sort_mode_array);
    }

    //Listeners
    private void addListeners() {
        bshSortMode.getmBtnActionSave().setOnClickListener(this);
    }

    private void findAllViews(){
        coordinatorLayout = findViewById(R.id.issuer_product_coordinator);

        title = findViewById(R.id.issuer_product_title);
        subTitle = findViewById(R.id.issuer_product_subtitle);

        ImageButton btnBack = findViewById(R.id.btn_close_issuer_product);
        btnBack.setOnClickListener(this);

        swipeRefreshLayout = findViewById(R.id.issuer_product_swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
        swipeRefreshLayout.setOnRefreshListener(this);

        recyclerView = findViewById(R.id.issuer_products_items_recycler_view);

        LinearLayout llContainerSortValue = findViewById(R.id.issuer_products_container_sort_value);
        llContainerSortValue.setOnClickListener(this);
        sortValue = findViewById(R.id.issuer_products_sort_value);

        btnListDisplay = findViewById(R.id.issuer_products_btn_display_list);
        btnListDisplay.setOnClickListener(this);
        btnGridDisplay = findViewById(R.id.issuer_products_btn_display_grid);
        btnGridDisplay.setOnClickListener(this);
    }

    //Modifie le label du mode de trie
    private void setSortValueView() {
        sortValue.setText(StringUtils.getSpannedText(getResources()
                .getString(R.string.text_sort_value, strSortMode[sortMode])));
    }

    private void setToolbar(){
        //Le titre
        title.setText(StringUtils.toUpperFirstChar(issuerCompleteName));
        //Le sous titre
        subTitle.setText(getResources()
                .getQuantityString(R.plurals.display_nb_articles_plural, nbMarketOffers, (int) nbMarketOffers));
    }

    //Getters et setters

    public CoordinatorLayout getCoordinatorLayout() {
        return coordinatorLayout;
    }

    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return swipeRefreshLayout;
    }

    public List<MarketOfferDisplayCustom> getMarketOffers() {
        return marketoffers;
    }

    public RecyclerView.Adapter getRecyclerviewAdapter() {
        return recyclerviewAdapter;
    }

    public void setRange() {
        this.range += 10;
    }
}
