package com.inductivtechnologies.quantmarkapp.activities.profile;

import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.httptask.profile.UpdateProfileSectionEmailPhoneTask;
import com.inductivtechnologies.quantmarkapp.models.entities.User;
import com.inductivtechnologies.quantmarkapp.store.repositories.UserRepository;
import com.inductivtechnologies.quantmarkapp.utils.FormsUtils;

import java.util.List;

/**
 * ProfileUpdateSectionEmailActivity : Pour mettre à jour la section email et téléphone de l'utilisateur
 */
@SuppressWarnings("ConstantConditions")
public class ProfileUpdateSectionEmailActivity extends AppCompatActivity implements View.OnClickListener{

    public static final String UPDATE_EMAIL_PHONE_TASK = "UPDATE_EMAIL_PHONE_TASK";

    //Les vues
    private CoordinatorLayout coordinatorLayout;

    private TextInputLayout inputLayoutEmail;
    private TextInputEditText etEmail;
    private TextInputLayout inputLayoutPhoneNUmber;
    private TextInputEditText etPhone;

    //Controlleur
    private AppController appController;

    private UpdateProfileSectionEmailPhoneTask updateTask;

    //Repositoires
    private UserRepository userRepository;

    private User currentUser = null;

    private FormsUtils formUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_prodile_section_email_phone);

        //Toolbar
        Toolbar toolbar = findViewById(R.id.update_section_ep_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        initObjects();

        //Toutes les vues
        allViews();

        globalInit();
    }

    @Override
    public void onDestroy() {
        // On annule la requête
        this.appController.cancelPendingRequests(UPDATE_EMAIL_PHONE_TASK);

        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.btn_close_update_section_ep :
                finish();
                break;
            case R.id.btn_validation_update_section_ep :
                validateFormUpdate();
                break;
        }
    }

    // Les méthodes

    //Utilisateur courant
    private User getCurrenUser(){
        List<User> users = userRepository.getAllUsers();
        if(users != null && !users.isEmpty()) return users.get(0);
        return null;
    }

    private void initObjects() {
        //Repository
        userRepository = new UserRepository(getApplicationContext());

        currentUser = getCurrenUser();

        //Instance singleton du controlleur volley
        appController = AppController.getInstance();

        //Task
        updateTask = new UpdateProfileSectionEmailPhoneTask(this, appController);

        formUtils = new FormsUtils(getApplicationContext());
    }

    private void globalInit(){
        if(currentUser != null){
            initEmail(currentUser.getAddress().getEmail());
            initPhone(currentUser.getPhone().getNumber());
        }
    }

    private void initEmail(String email){
        etEmail.setText(email);
    }

    private void initPhone(String phone){
        etPhone.setText(phone);
    }

    //Validation du formulaire
    private void validateFormUpdate() {
        String email = etEmail.getText().toString().trim();
        String phone = etPhone.getText().toString().trim();

        boolean validatePhone = formUtils.validatePhoneNumberWithRegex(inputLayoutPhoneNUmber, phone);
        boolean validatePEmail = formUtils.validateEmail(inputLayoutEmail, email);

        if(validatePhone && validatePEmail){
            if(currentUser != null) {
                //Adresse
                currentUser.getAddress().setEmail(email);
                //phone
                currentUser.getPhone().setNumber(phone);

                //Task
                updateTask.makeRequest(currentUser);
            }
        }
    }

    private void allViews(){
        coordinatorLayout = findViewById(R.id.update_email_phone_coordinator);

        ImageButton btnClose = findViewById(R.id.btn_close_update_section_ep);
        btnClose.setOnClickListener(this);
        ImageButton btnSave = findViewById(R.id.btn_validation_update_section_ep);
        btnSave.setOnClickListener(this);

        inputLayoutEmail = findViewById(R.id.input_layout_email_update);
        etEmail = findViewById(R.id.edittext_email_update);
        inputLayoutPhoneNUmber = findViewById(R.id.input_layout_update_phone);
        etPhone = findViewById(R.id.edittext_update_phone);
    }

    //Getters et setters

    public CoordinatorLayout getCoordinatorLayout() {
        return this.coordinatorLayout;
    }
}
