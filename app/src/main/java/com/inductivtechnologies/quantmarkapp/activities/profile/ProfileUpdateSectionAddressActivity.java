package com.inductivtechnologies.quantmarkapp.activities.profile;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.activities.others.SelectRecyclerviewItemActivity;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.configuration.NumberConfig;
import com.inductivtechnologies.quantmarkapp.configuration.StringConfig;
import com.inductivtechnologies.quantmarkapp.httptask.profile.UpdateProfileSectionAddrTask;
import com.inductivtechnologies.quantmarkapp.models.entities.User;
import com.inductivtechnologies.quantmarkapp.store.repositories.UserRepository;
import com.inductivtechnologies.quantmarkapp.utils.FormsUtils;
import com.inductivtechnologies.quantmarkapp.utils.StringUtils;

import java.util.List;

/**
 * ProfileUpdateSectionAddressActivity : Pour mettre à jour la section identification du profile de l'utilisateur
 */
@SuppressWarnings("ConstantConditions")
public class ProfileUpdateSectionAddressActivity extends AppCompatActivity implements View.OnClickListener{

    public static final String UPDATE_ADDR_TASK = "UPDATE_ADDR_TASK";

    private final int UPDATE_ADDR_INTENT_REQUEST = 20;

    //Les vues
    private CoordinatorLayout coordinatorLayout;

    private TextView tvCountry;
    private TextView tvCity;
    private TextView tvDistrict;

    private TextInputLayout layoutInputPobox;
    private TextInputEditText etpobox;

    //Controlleur
    private AppController appController;

    //Repositoires
    private UserRepository userRepository;

    //Task
    private UpdateProfileSectionAddrTask updateTask;

    //Formulaire
    private FormsUtils formUtils;

    private int choiceValue;

    private  User currentUser = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_prodile_section_address);

        //Toolbar
        Toolbar toolbar = findViewById(R.id.update_section_address_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //Init des objets
        initObjects();

        //Toutes les vues
        allViews();

        globalInit();
    }

    @Override
    public void onDestroy() {
        // On annule la requête
        this.appController.cancelPendingRequests(UPDATE_ADDR_TASK);

        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        Intent intent =  new Intent(this, SelectRecyclerviewItemActivity.class);

        switch(view.getId()){
            case R.id.btn_close_update_section_address :
                finish();
                break;
            case R.id.btn_validation_update_section_address :
                validateFormUpdate();
                break;
            case R.id.container_tv_update_country :
                choiceValue = NumberConfig.NB_LOAD_COUNTRIES;

                intent.putExtra(StringConfig.UPDATE_ADDR_INTENT_EXTRA, NumberConfig.NB_LOAD_COUNTRIES);
                startActivityForResult(intent, UPDATE_ADDR_INTENT_REQUEST);
                break;
            case R.id.container_tv_update_city :
                choiceValue = NumberConfig.NB_LOAD_CITY;

                intent.putExtra(StringConfig.UPDATE_ADDR_INTENT_EXTRA, NumberConfig.NB_LOAD_CITY);
                startActivityForResult(intent, UPDATE_ADDR_INTENT_REQUEST);
                break;
            case R.id.container_tv_update_district :
                choiceValue = NumberConfig.NB_LOAD_DISTRICT;

                intent.putExtra(StringConfig.UPDATE_ADDR_INTENT_EXTRA, NumberConfig.NB_LOAD_DISTRICT);
                startActivityForResult(intent, UPDATE_ADDR_INTENT_REQUEST);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if(requestCode == UPDATE_ADDR_INTENT_REQUEST){
            if(resultCode == Activity.RESULT_OK) {
                switch(choiceValue){
                    case NumberConfig.NB_LOAD_COUNTRIES :
                        initCountry(intent.getStringExtra(StringConfig.STR_SELECT_DATA));
                        break;
                    case NumberConfig.NB_LOAD_CITY :
                        initCity(intent.getStringExtra(StringConfig.STR_SELECT_DATA));
                        break;
                    case NumberConfig.NB_LOAD_DISTRICT :
                        initDistrict(intent.getStringExtra(StringConfig.STR_SELECT_DATA));
                        break;
                }
            }
        }
    }

    // Les méthodes

    private void initObjects() {
        //Repository
        userRepository = new UserRepository(getApplicationContext());

        currentUser = getCurrenUser();

        //Instance singleton du controlleur volley
        appController = AppController.getInstance();

        //Task
        updateTask = new UpdateProfileSectionAddrTask(this, appController);

        formUtils = new FormsUtils(getApplicationContext());
    }

    //Utilisateur courant
    private User getCurrenUser(){
        List<User> users = userRepository.getAllUsers();
        if(users != null && !users.isEmpty()) return users.get(0);
        return null;
    }

    private void globalInit(){
        if(currentUser != null){
            initCountry(currentUser.getAddress().getCountry());
            initCity(currentUser.getAddress().getCity());
            initDistrict(currentUser.getAddress().getStreet());
            initPobox(currentUser.getAddress().getPoBox());
        }
    }

    private void initCountry(String country){
        //Country
        if(country.isEmpty()){
            tvCountry.setText("");
        }else{
            int position = StringUtils.getPositionOfStringInArray(StringConfig.getDefaultArrayCountries(getApplicationContext()), country);
            if(position != -1){
                tvCountry.setText(StringConfig.getLocaleArrayCountries(getApplicationContext())[position]);
            }else{
                tvCountry.setText(StringUtils.toUpperFirstChar(country));
            }
        }
    }

    private void initCity(String city){
        //City
        tvCity.setText(StringUtils.toUpperFirstChar(city));
    }

    private void initDistrict(String district){
        if(!district.isEmpty()){
            //District
            tvDistrict.setText(StringUtils.toUpperFirstChar(district));
        }else{
            //District
            tvDistrict.setText(getResources().getString(R.string.select_value_message));
        }
    }

    private void initPobox(String pobox){
        //Pobox
        etpobox.setText(pobox);
    }

    private void allViews(){
        coordinatorLayout = findViewById(R.id.update_section_address_coordinator);

        ImageButton btnClose = findViewById(R.id.btn_close_update_section_address);
        btnClose.setOnClickListener(this);
        ImageButton btnSave = findViewById(R.id.btn_validation_update_section_address);
        btnSave.setOnClickListener(this);

        RelativeLayout containerCountry = findViewById(R.id.container_tv_update_country);
        containerCountry.setOnClickListener(this);
        tvCountry = findViewById(R.id.tv_profile_country_value);

        RelativeLayout containerCity = findViewById(R.id.container_tv_update_city);
        containerCity.setOnClickListener(this);
        tvCity = findViewById(R.id.tv_profile_city_value);

        RelativeLayout containerDistrict = findViewById(R.id.container_tv_update_district);
        containerDistrict.setOnClickListener(this);
        tvDistrict = findViewById(R.id.tv_profile_district_value);

        etpobox = findViewById(R.id.edittext_pobox_update);
        etpobox.addTextChangedListener(new UpdateTextWatcher(etpobox));
        layoutInputPobox = findViewById(R.id.layout_input_pobox_update);
    }

    //Validation du formulairee
    private void validateFormUpdate() {
        String country = tvCountry.getText().toString();
        if(!country.isEmpty()){
            int position = StringUtils.getPositionOfStringInArray(StringConfig.getLocaleArrayCountries(getApplicationContext()), country);
            if(position != -1){
                country = StringConfig.getDefaultArrayCountries(getApplicationContext())[position];
            }
        }

        String city = tvCity.getText().toString();
        String district = tvDistrict.getText().toString();
        String pobox = etpobox.getText().toString().trim();

        boolean validPobox = formUtils.validatePoBox(layoutInputPobox, pobox);

        if(validPobox && !district.equalsIgnoreCase(getResources().getString(R.string.select_value_message))){
            if(currentUser != null){
                currentUser.getAddress().setCountry(country);
                currentUser.getAddress().setCity(city);
                currentUser.getAddress().setStreet(district);
                currentUser.getAddress().setPoBox(pobox);

                updateTask.makeRequest(currentUser);
            }
        }
    }

    //Getters et setters

    public CoordinatorLayout getCoordinatorLayout() {
        return this.coordinatorLayout;
    }

    //Classes internes

    private class UpdateTextWatcher implements TextWatcher {

        private final View view;

        private UpdateTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.edittext_pobox_update:
                    formUtils.validatePoBox(layoutInputPobox, etpobox.getText().toString().trim());
                    break;
            }
        }
    }
}
