package com.inductivtechnologies.quantmarkapp.activities.account;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.activities.profile.UserProfileActivity;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.bottomsheets.BottomSheetProgress;
import com.inductivtechnologies.quantmarkapp.callbacks.HttpCallback;
import com.inductivtechnologies.quantmarkapp.fragments.account.FragmentSignup;
import com.inductivtechnologies.quantmarkapp.httptask.account.ValidateAccountTask;
import com.inductivtechnologies.quantmarkapp.models.entities.User;
import com.inductivtechnologies.quantmarkapp.models.http.HttpResponseObject;
import com.inductivtechnologies.quantmarkapp.store.repositories.AddressRepository;
import com.inductivtechnologies.quantmarkapp.store.repositories.UserRepository;
import com.inductivtechnologies.quantmarkapp.store.session.SessionManager;
import com.inductivtechnologies.quantmarkapp.utils.DesignUtils;
import com.inductivtechnologies.quantmarkapp.utils.StringUtils;

import org.json.JSONObject;

import java.util.List;

/**
 * ValidateAccountActivity
 * Permet de valider le compte de l'utilisateur après son inscription
 */
@SuppressWarnings("ConstantConditions")
public class ValidateAccountActivity extends AppCompatActivity implements View.OnClickListener, HttpCallback {

    /**
     * Le task permettant d'ajouter la requête réseau à la file des requêtes
     */
    public static final String VALIDATION_ACCOUNT_TASK = "VALIDATION_ACCOUNT_TASK";

    /**
     * Les vues
     */
    private CoordinatorLayout coordinatorLayout;
    private TextView tvFirstMessage;
    private TextView tvSecondMessage;
    private TextInputEditText codeInput;
    private TextView tvError;

    /**
     * Volley instance singleton
     */
    private AppController appController;

    //Dialog
    private BottomSheetProgress progress;

    //Repo
    private UserRepository userRepo;
    //Repositories
    private AddressRepository addressRepo;
    //Session
    private SessionManager sessionManager;

    private User currentUser;

    private String validationCode;
    //Permet de savoir qui a lancé cette activité
    private int validateAccountRequestCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_validate_account);

        //Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        validateAccountRequestCode = getIntent()
                .getIntExtra(FragmentSignup.INTENT_VALIDATION_ACCOUNT_REQUEST, 0);

        //Init
        initObjects();

        //Les vues
        findAllViews();

        if(currentUser != null){
            initViews(currentUser.getFirstName(), currentUser.getAddress().getEmail());
        }
    }

    @Override
    public void onDestroy() {
        // On annule la requête
        this.appController.cancelPendingRequests(VALIDATION_ACCOUNT_TASK);

        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.btn_validation :

                String code = codeInput.getText().toString().trim();
                if(!code.isEmpty() && code.length() == 6 && code.equals(validationCode)){
                    tvError.setVisibility(View.INVISIBLE);

                    if(currentUser != null){
                        currentUser.getAddress().setValide(true);
                        //On lance le task
                        new ValidateAccountTask(this, appController).makeRequest(currentUser);
                    }
                }else{
                    tvError.setVisibility(View.VISIBLE);
                }

                break;
        }
    }

    //Les méthodes

    private void initObjects() {
        //Volley
        this.appController = AppController.getInstance();
        //Session
        sessionManager = new SessionManager(getApplicationContext());

        //Progress
        this.progress = new BottomSheetProgress(this);

        userRepo = new UserRepository(getApplicationContext());
        this.addressRepo = new AddressRepository(getApplicationContext());

        currentUser = getCurrentUser();

        validationCode = sessionManager.getValidationCode();
    }

    private User getCurrentUser(){
        List<User> users = userRepo.getAllUsers();
        if(users != null) return users.get(0);

        return null;
    }

    private void findAllViews() {
        coordinatorLayout = findViewById(R.id.validate_account_coordinator);

        tvFirstMessage = findViewById(R.id.tv_first_message);
        tvSecondMessage = findViewById(R.id.tv_second_message);

        codeInput = findViewById(R.id.edittext_validation_code);
        tvError = findViewById(R.id.tv_error_validation);

        Button btnValidate = findViewById(R.id.btn_validation);
        btnValidate.setOnClickListener(this);
    }

    private void initViews(String name, String email) {
        tvFirstMessage.setText(getResources().getString(R.string.validate_account_message_first,
                StringUtils.toUpperFirstChar(name)));

        tvSecondMessage.setText(StringUtils.getSpannedText(getResources().getString(R.string.validate_account_message_second,
                email)));
    }

    //Getters et setters

    //Task Http

    private void updateProgress(boolean isVisible){
        if(isVisible){
            if(progress != null)
                progress.show(getResources().getString(R.string.login_progress_message));
        }else{
            if(progress != null)
                progress.hide();
        }
    }

    private void makeSnackbar(String message){
        DesignUtils.makeSnackbar(message, coordinatorLayout, getApplicationContext());
    }

    @Override
    public void beforeExecute() {
        updateProgress(true);
    }

    @Override
    public void afterExecute(JSONObject response) {
        //Vérification
        final int statusCode = response.optInt(HttpResponseObject.CODE_STR);

        switch(statusCode) {
            case HttpResponseObject.SUCCESS_CODE:

                if(addressRepo.updateAddress(currentUser.getAddress()) > 0){
                    //On supprime le code dans la session
                    sessionManager.deleteValidationCode();

                    //On termine l'activité
                    if(validateAccountRequestCode == 120){
                        startActivity(new Intent(this, UserProfileActivity.class));
                        finish();
                    }else{
                        finish();
                    }
                }
                break;
            default:
                makeSnackbar(getResources().getString(R.string.label_unknow_error_message));
                break;
        }
        updateProgress(false);
    }

    @Override
    public void whenErrors() {
        updateProgress(false);
        //Le snackbar
        makeSnackbar(getResources().getString(R.string.network_message_error));
    }
}
