package com.inductivtechnologies.quantmarkapp.activities.others;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.adapters.others.SelectItemRecyclerviewAdapter;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.configuration.NumberConfig;
import com.inductivtechnologies.quantmarkapp.configuration.StringConfig;
import com.inductivtechnologies.quantmarkapp.configuration.UrlRestApiConfig;
import com.inductivtechnologies.quantmarkapp.httptask.others.LoadSingleDataTask;
import com.inductivtechnologies.quantmarkapp.listeners.others.RecyclerTouchListener;

import java.util.Arrays;
import java.util.List;

/**
 * SelectCityAndDistrictActivity : Permet de construire un recycler view contennat une
 * liste de données de type string
 */
@SuppressWarnings("ConstantConditions")
public class SelectRecyclerviewItemActivity  extends AppCompatActivity implements View.OnClickListener{

    ///Pour annuler les requêtes
    public static final String LOAD_DATA_TASK = "LOAD_DATA_TASK";

    private CoordinatorLayout coordinatorLayout;

    private TextView tvTitle;
    //Recyclerview
    private RecyclerView recyclerViewSelectItem;

    //Widget serach
    private SearchView searchView;

    //Adaptateur
    private SelectItemRecyclerviewAdapter selectitemAdapter;

    //Controller
    private AppController appController;

    private Intent currentIntent;
    //L'activité elle même
    private SelectRecyclerviewItemActivity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recyclerview_select_item);

        //Toolbar
        Toolbar toolbar = findViewById(R.id.select_activity_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        initObjects();

        allViews();

        int requestIntent = getIntent().getIntExtra(StringConfig.UPDATE_ADDR_INTENT_EXTRA, 0);

        switch(requestIntent){
            case NumberConfig.NB_LOAD_COUNTRIES :
                tvTitle.setText(getResources().getString(R.string.recyclerview_choice_item_country));
                initRecyclerViewSelectItem(Arrays.asList(StringConfig.getLocaleArrayCountries(getApplicationContext())));
                break;
            case NumberConfig.NB_LOAD_CITY :
                tvTitle.setText(getResources().getString(R.string.recyclerview_choice_item_city));
                initRecyclerViewSelectItem(Arrays.asList(StringConfig.cities));
                break;
            case NumberConfig.NB_LOAD_DISTRICT :
                //On lance le chargement
                tvTitle.setText(getResources().getString(R.string.recyclerview_choice_item_district));
                new LoadSingleDataTask(this, appController).makeRequest(UrlRestApiConfig.LOAD_DISTRICTS_ADDR);
                break;
        }
    }

    //Override

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_recyclerview_select_item, menu);

        // On associe la configuration du searchable au SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

        //On récupère l'item du menu
        searchView = (SearchView) menu.findItem(R.id.recyclerviewItem_action_search).getActionView();
        searchView.setQueryHint(Html.fromHtml("<font color=#ffffff>" + getResources().getString(R.string.recyclerview_choice_item_label_find) + "</font>"));

        assert searchManager != null;
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        //On écoute en cas de changemnt de valuer textuelle
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                //On appele le filtre de l'adaptateur du recyclerview
                selectitemAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                selectitemAdapter.getFilter().filter(query);
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //On gère la selction du menu ici
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.recyclerviewItem_action_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        //Avant de faire l'action par défaut du bouton retour
        //On remet le le widget search dans son état initial
        if (!searchView.isIconified()) {
            searchView.setIconified(true);
            return;
        }
        super.onBackPressed();
    }

    @Override
    public void onDestroy() {
        // On annule la requête
        this.appController.cancelPendingRequests(LOAD_DATA_TASK);

        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.btn_close_select_activity :
                finish();
                break;
        }
    }

    //Méthodes

    private void allViews(){
        coordinatorLayout = findViewById(R.id.select_activity_coordinatorlayout);

        ImageButton btnBack = findViewById(R.id.btn_close_select_activity);
        btnBack.setOnClickListener(this);
        tvTitle = findViewById(R.id.tv_title_select_activity);

        recyclerViewSelectItem = findViewById(R.id.recyclerview_select_item_activity);
    }

    public void initRecyclerViewSelectItem(final List<String> data){

        selectitemAdapter = new SelectItemRecyclerviewAdapter(this, data);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerViewSelectItem.setLayoutManager(mLayoutManager);
        recyclerViewSelectItem.setItemAnimator(new DefaultItemAnimator());
        recyclerViewSelectItem.setAdapter(selectitemAdapter);


        recyclerViewSelectItem.addOnItemTouchListener(new RecyclerTouchListener(this,
                recyclerViewSelectItem, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                String content = selectitemAdapter.getDataFiltered().get(position);

                activity.setResult(Activity.RESULT_OK, currentIntent);
                currentIntent.putExtra(StringConfig.STR_SELECT_DATA, content.toLowerCase());
                activity.finish();
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));
    }

    private void initObjects() {
        //Instance singleton du controlleur volley
        appController = AppController.getInstance();

        currentIntent = getIntent();
        activity = this;
    }

    public CoordinatorLayout getCoordinatorLayout() {
        return coordinatorLayout;
    }
}
