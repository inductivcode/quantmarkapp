package com.inductivtechnologies.quantmarkapp.activities.main;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.activities.about.AboutAppActivity;
import com.inductivtechnologies.quantmarkapp.activities.account.UserLoginActivity;
import com.inductivtechnologies.quantmarkapp.activities.profile.UserProfileActivity;
import com.inductivtechnologies.quantmarkapp.activities.search.SearchActivity;
import com.inductivtechnologies.quantmarkapp.design.DisplayLargeNumber;
import com.inductivtechnologies.quantmarkapp.design.OvalTextView;
import com.inductivtechnologies.quantmarkapp.fragments.cart.CartFragment;
import com.inductivtechnologies.quantmarkapp.fragments.category.CategoryFragment;
import com.inductivtechnologies.quantmarkapp.fragments.favorite.FavoriteFragment;
import com.inductivtechnologies.quantmarkapp.fragments.home.HomeFragment;
import com.inductivtechnologies.quantmarkapp.store.repositories.CartItemRepository;
import com.inductivtechnologies.quantmarkapp.store.session.SessionManager;

/**
 * MainActivity : Interface d'accueil pour l'affichage des offres
 */
@SuppressWarnings("ConstantConditions")
public class MainActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener, View.OnClickListener{

    private final int LOGIN_INTENT_REQUEST = 10;

    //Gestionnaire de fragment
    private FragmentManager fragmentManager;

    //Les vues
    private CoordinatorLayout coordinatorLayout;

    //Titre
    private TextView fragmentTitle;

    //Le tablayout
    private TabLayout mainTablayout;

    //Les fragments
    private HomeFragment homeFragment;
    private FavoriteFragment favoriteFragment;
    private CartFragment cartFragment;
    private CategoryFragment categoryFragment;

    //Les icones du tablayout (seléctionnées)
    private final int[] selectedIcons = {
            R.drawable.ic_tabitem_home_selected,
            R.drawable.ic_tabitem_favori_selected,
            R.drawable.ic_tabitem_cart_selected,
            R.drawable.ic_tabitem_category_selected
    };
    //Les icones du tablayout (non seléctionnées)
    private final int[] unselectedIcons = {
            R.drawable.ic_tabitem_home_unselected,
            R.drawable.ic_tabitem_favori_unselected,
            R.drawable.ic_tabitem_cart_unselected,
            R.drawable.ic_tabitem_category_unselected
    };

    //Les titres des fragments
    private final int[] fragmentTitles = {
            R.string.home_title,
            R.string.favori_title,
            R.string.cart_title,
            R.string.category_title
    };

    //La session
    private SessionManager sessionManager;
    // Repositoires
    private CartItemRepository cartItemRepo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Toolbar
        Toolbar toolbar = findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //Initialisation des objets
        initObjects();

        ///Les vues
        findAllViews();

        createFragments();

        // Fragment par défaut
        fragmentManager.beginTransaction().add(R.id.main_content, homeFragment).commit();
        fragmentTitle.setText(getResources().getString(fragmentTitles[0]));
    }

    //Override

    @Override
    public void onResume() {
        super.onResume();

        //On initialise labdge
        long  count = cartItemRepo.countCartItem();
        setBagde(count);
    }


    @Override
    public void onBackPressed() {
        if (mainTablayout.getSelectedTabPosition() != 0) {
            mainTablayout.getTabAt(0).select();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        //On change les icones
        changeTabItemSelectedIcon(tab);

        //Position courrante
        int position = tab.getPosition();

        //Titre des fragments
        fragmentTitle.setText(getResources().getString(fragmentTitles[position]));

        switch(position){
            case 0 :
                showFragment(homeFragment);
                break;
            case 1 :
                showFragment(favoriteFragment);
                break;
            case 2 :
                showFragment(cartFragment);
                break;
            case 3 :
                showFragment(categoryFragment);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if(requestCode == LOGIN_INTENT_REQUEST){
            if(resultCode == Activity.RESULT_OK){
                startActivity(new Intent(this, UserProfileActivity.class));
            }
        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
        //On change les icones
        changeTabItemUnselectedIcon(tab);

        //Position courrante
        int position = tab.getPosition();

        switch(position) {
            case 0:
                hideFragment(homeFragment);
                break;
            case 1:
                hideFragment(favoriteFragment);
                break;
            case 2:
                hideFragment(cartFragment);
                break;
            case 3:
                hideFragment(categoryFragment);
                break;
        }
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.main_btn_display_profile :
                if(sessionManager.isLoggedIn()){
                    startActivity(new Intent(this, UserProfileActivity.class));
                }else{
                    startActivityForResult(new Intent(this, UserLoginActivity.class), LOGIN_INTENT_REQUEST);
                }
                break;
            case R.id.main_btn_about_app :
                startActivity(new Intent(this, AboutAppActivity.class));
                break;
            case R.id.main_btn_search :
                startActivity(new Intent(this, SearchActivity.class));
                break;
        }
    }

    //Méthodes

    //Affiche un fragment
    private void showFragment(Fragment fragment){
        FragmentTransaction ft = fragmentManager.beginTransaction();
        if(fragment.isAdded()){
            ft.show(fragment);
        }else{
            ft.add(R.id.main_content, fragment);
        }
        ft.commit();
    }

    //Cache un frgament
    private void hideFragment(Fragment fragment){
        FragmentTransaction ft = fragmentManager.beginTransaction();
        if(fragment.isAdded()){
            ft.hide(fragment);
        }
        ft.commit();
    }

    //Instancie tous les fragments
    private void createFragments(){
        //Les fragments
        homeFragment = HomeFragment.newInstance();
        favoriteFragment = FavoriteFragment.newInstance();
        cartFragment = CartFragment.newInstance();
        categoryFragment = CategoryFragment.newInstance();
    }

    private void changeTabItemSelectedIcon(TabLayout.Tab tab){
        int position = tab.getPosition();
        ImageView icon = tab.getCustomView().findViewById(R.id.tabitem_icon);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            icon.setImageDrawable(getResources().getDrawable(selectedIcons[position], null));
        }else{
            icon.setImageDrawable(getResources().getDrawable(selectedIcons[position]));
        }
    }

    private void changeTabItemUnselectedIcon(TabLayout.Tab tab){
        int position = tab.getPosition();
        ImageView icon = tab.getCustomView().findViewById(R.id.tabitem_icon);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            icon.setImageDrawable(getResources().getDrawable(unselectedIcons[position], null));
        }else{
            icon.setImageDrawable(getResources().getDrawable(unselectedIcons[position]));
        }
    }

    //Initialise les objets
    private void initObjects() {
        //Gestionnaire de fragment
        fragmentManager = getSupportFragmentManager();

        sessionManager = new SessionManager(getApplicationContext());
        // Repositories
        cartItemRepo = new CartItemRepository(getApplicationContext());
    }

    // On recupère les vues
    private void findAllViews(){
        coordinatorLayout = findViewById(R.id.main_coordinator);

        //Tablayout
        mainTablayout = findViewById(R.id.main_tablayout);
        initTablayout();
        mainTablayout.addOnTabSelectedListener(this);

        //Titre
        fragmentTitle = findViewById(R.id.main_title);
        ImageButton btnDisplayProfile = findViewById(R.id.main_btn_display_profile);
        btnDisplayProfile.setOnClickListener(this);

        ImageButton btnAboutApp = findViewById(R.id.main_btn_about_app);
        btnAboutApp.setOnClickListener(this);

        ImageButton btnSearch = findViewById(R.id.main_btn_search);
        btnSearch.setOnClickListener(this);
    }

    //Initialisation du tablayout
    private void initTablayout(){
        int tabCount = mainTablayout.getTabCount();
        OvalTextView badge;

        for(int i= 0; i < tabCount; i++){
            if(i == 2) {
                @SuppressLint("InflateParams")
                View tabitemContentBadge = getLayoutInflater()
                        .inflate(R.layout.custom_tabitem_badge_tablayout_main, null);
                ImageView iconBadge = tabitemContentBadge.findViewById(R.id.tabitem_icon);
                badge = tabitemContentBadge.findViewById(R.id.tv_bagde);

                badge.setSolidColor("#0277BD");

                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                    iconBadge.setImageDrawable(getResources().getDrawable(unselectedIcons[i], null));
                }else{
                    iconBadge.setImageDrawable(getResources().getDrawable(unselectedIcons[i]));
                }
                mainTablayout.getTabAt(i).setCustomView(tabitemContentBadge);
            }else{
                @SuppressLint("InflateParams")
                View tabitemContent = getLayoutInflater()
                        .inflate(R.layout.custom_tab_item_main, null);
                ImageView icon = tabitemContent.findViewById(R.id.tabitem_icon);

                if(i == 0){
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                        icon.setImageDrawable(getResources().getDrawable(selectedIcons[i], null));
                    }else{
                        icon.setImageDrawable(getResources().getDrawable(selectedIcons[i]));
                    }
                }else{
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                        icon.setImageDrawable(getResources().getDrawable(unselectedIcons[i], null));
                    }else{
                        icon.setImageDrawable(getResources().getDrawable(unselectedIcons[i]));
                    }
                }
                mainTablayout.getTabAt(i).setCustomView(tabitemContent);
            }
        }
    }

    //On initialise le badge
    public void setBagde(long count){
        TabLayout.Tab tabBadge = mainTablayout.getTabAt(2);
        assert tabBadge != null;
        View contentLayout = tabBadge.getCustomView();
        assert contentLayout != null;
        OvalTextView badgeText = contentLayout.findViewById(R.id.tv_bagde);

        badgeText.setText(DisplayLargeNumber.smartLargeNumber(count));
    }

    //Getters et setters

    public CoordinatorLayout getCoordinatorLayout() {
        return coordinatorLayout;
    }

    public TabLayout getMainTablayout() {
        return mainTablayout;
    }
}
