package com.inductivtechnologies.quantmarkapp.activities.cart;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.activities.account.UserLoginActivity;
import com.inductivtechnologies.quantmarkapp.adapters.cart.CartActivityAdapter;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.bottomsheets.BottomsheetDeliveryMethod;
import com.inductivtechnologies.quantmarkapp.comparators.CartItemComparator;
import com.inductivtechnologies.quantmarkapp.components.GlideComponent;
import com.inductivtechnologies.quantmarkapp.configuration.StringConfig;
import com.inductivtechnologies.quantmarkapp.configuration.UrlRestApiConfig;
import com.inductivtechnologies.quantmarkapp.httptask.cart.CartActivityTask;
import com.inductivtechnologies.quantmarkapp.models.display.MarketOfferDisplay;
import com.inductivtechnologies.quantmarkapp.models.entities.CartItem;
import com.inductivtechnologies.quantmarkapp.store.repositories.CartItemRepository;
import com.inductivtechnologies.quantmarkapp.store.session.SessionManager;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * CartActivity :
 */
@SuppressWarnings("ConstantConditions")
public class CartActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener,
        View.OnClickListener{

    public static final String CART_ACTIVITY_TASK = "CART_ACTIVITY_TASK";

    private ImageButton btnClearCart;
    private CoordinatorLayout coordinatorLayout;
    private SwipeRefreshLayout swipeRefreshLayout;

    private Button btnOrder;
    private TextView tvTotal;

    public LinearLayout llContainerEmptyCart;

    // Recycler view
    private RecyclerView recyclerView;
    private CartActivityAdapter adapter;

    //Le task
    private CartActivityTask cartTask;

    //Les données
    private List<CartItem> cartitems;

    //Volley instance singleton
    private AppController appController;
    //Repo
    private CartItemRepository cartRepository;
    // Session
    private SessionManager sessionManager;

    //Pour choisir la méthode de livraison
    private BottomsheetDeliveryMethod bstDeliveryMethod;

    //Méthode de livraison
    private int deliveryMethod;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        Toolbar toolbar = findViewById(R.id.cart_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //Initialisation des objets
        initObjects();

        //Les vues
        findAllViews();

        //Recyclerview
        initRecyclerView();
    }

    @Override
    public void onResume(){
        super.onResume();

        //Init
        initCart();
    }

    @Override
    public void onDestroy() {
        // On annule la requête
        this.appController.cancelPendingRequests(CART_ACTIVITY_TASK);

        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.cart_toolbar_back :
                finish();
                break;
            case R.id.btn_buy_now_cart :
                if(sessionManager.isLoggedIn()){
                    bstDeliveryMethod.show();
                }else{
                    //Connxion
                    Intent intent = new Intent(CartActivity.this, UserLoginActivity.class);
                    startActivity(intent);
                }
                break;
            case R.id.btn_start_shopping :
                finish();
                break;
            case R.id.btn_clear_cart :
                clearCart();
                break;
        }
    }

    @Override
    public void onRefresh() {
        //Task
        initCart();
    }

    //Les methodes

    private void initObjects() {
        //Repo
        this.cartRepository = new CartItemRepository(getApplicationContext());

        //Volley
        this.appController = AppController.getInstance();

        //La session
        this.sessionManager = new SessionManager(getApplicationContext());

        //Data
        this.cartitems = new ArrayList<>();

        //Task
        this.cartTask = new CartActivityTask(this, appController);

        bstDeliveryMethod = new BottomsheetDeliveryMethod(this);
        bstDeliveryMethod.getmBtnActionSave().setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                deliveryMethod = bstDeliveryMethod.getDeliveryMethod();
                //On ferme
                bstDeliveryMethod.hide();

                if(deliveryMethod != -1){
                    Intent intent = new Intent(CartActivity.this, ResumeOrdersActivity.class);
                    intent.putExtra(StringConfig.INTENT_DELIVERY_METHOD, deliveryMethod);
                    intent.putExtra(StringConfig.INTENT_MARKETOFFER_HEX_ID, "");
                    startActivity(intent);
                }
            }
        });
    }

    // On recupère les vues
    private void findAllViews() {
        coordinatorLayout = findViewById(R.id.cart_coordinator);

        ImageButton btnClose = findViewById(R.id.cart_toolbar_back);
        btnClose.setOnClickListener(this);
        btnClearCart = findViewById(R.id.btn_clear_cart);
        btnClearCart.setOnClickListener(this);

        swipeRefreshLayout = findViewById(R.id.cart_swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
        swipeRefreshLayout.setOnRefreshListener(this);

        ImageView ivEmptyCart = findViewById(R.id.iv_empty_cart);
        GlideComponent.simpleGlideComponentWithoutPlaceholder(getApplicationContext(),
                UrlRestApiConfig.IMAGE_RESOURCE_URL + StringConfig.DEFAULT_EMPTY_CART, ivEmptyCart);
        llContainerEmptyCart = findViewById(R.id.container_empty_cart);
        Button btnStartShopping = findViewById(R.id.btn_start_shopping);
        btnStartShopping.setOnClickListener(this);

        recyclerView = findViewById(R.id.cartitems__recycler_view);

        btnOrder = findViewById(R.id.btn_buy_now_cart);
        btnOrder.setOnClickListener(this);
        btnOrder.setEnabled(false);
        btnOrder.setText(getResources().getString(R.string.text_display_label_order, cartitems.size()));
        tvTotal = findViewById(R.id.tv_total_cart);
    }

    //Construit la liste des ids des markets offers contenus dans le panier
    private ArrayList<String> buildIdsHexList(){
        List<CartItem> cartitems = cartRepository.getAllCartitems();
        ArrayList<String> idsHex = new ArrayList<>();

        if(cartitems.size() != 0) {
            for (CartItem cartitem : cartitems) {
                idsHex.add(cartitem.getIdHexMarketOffer());
            }
        }

        return idsHex;
    }

    //Initialisation du panier
    private void initCart(){
        ArrayList<String> params = buildIdsHexList();
        if(params.size() != 0){
            cartTask.makeRequest(params);
        }else{
            swipeRefreshLayout.setRefreshing(false);

            if(cartitems.size() != 0){
                cartitems.clear();
                adapter.notifyDataSetChanged();

                //Le total
                setTvTotalCart();
            }
            //On affiche le message
            llContainerEmptyCart.setVisibility(View.VISIBLE);
            //On cache le recyclerview
            recyclerView.setVisibility(View.GONE);
            //On modifie le message du bouton
            getBtnOrder().setEnabled(false);
            btnOrder.setText(getResources()
                    .getString(R.string.text_display_label_order, cartitems.size()));
            //On cache le bouton clear
            btnClearCart.setVisibility(View.GONE);
        }
    }

    private void clearCart(){
        //on vide le panier
        cartitems.clear();
        adapter.notifyDataSetChanged();
        cartRepository.deleteAllCartitems();

        //On affiche le message panier vide
        llContainerEmptyCart.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);

        //On met a jour le total
        setTvTotalCart();
        //On modifie le message du bouton
        getBtnOrder().setEnabled(false);
        btnOrder.setText(getResources()
                .getString(R.string.text_display_label_order, cartitems.size()));

        //Et on cache de nouveau le bouton
        btnClearCart.setVisibility(View.GONE);
    }

    // Initialisation du recyclerview avec les données recues
    private void initRecyclerView(){
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new CartActivityAdapter(this, cartitems);
        recyclerView.setAdapter(adapter);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
    }

    // On raffraichi les données
    public  List<CartItem> refreshCartItems(final List<MarketOfferDisplay> marketOffersDisplay){
        List<CartItem> cartItemsFinal = cartRepository.refreshData(marketOffersDisplay);

        //On trie avant de retourner
        Collections.sort(cartItemsFinal, new CartItemComparator());
        return cartItemsFinal;
    }

    public void setTvTotalCart() {
        double total = 0;
        for(CartItem cartitem : cartitems){
            total = total + cartitem.getTotalPrice();
        }

        if(total == 0){
            btnOrder.setEnabled(false);
        }else{
            btnOrder.setEnabled(true);
        }
        this.tvTotal.setText(getResources().getString(R.string.display_price, NumberFormat.getIntegerInstance().format(total)));
    }

    //Getters et setters

    public CoordinatorLayout getCoordinatorLayout() {
        return coordinatorLayout;
    }

    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return swipeRefreshLayout;
    }

    public RecyclerView getRecyclerView() {
        return recyclerView;
    }

    public CartActivityAdapter getAdapter() {
        return adapter;
    }

    public List<CartItem> getCartitems() {
        return cartitems;
    }

    public Button getBtnOrder() {
        return btnOrder;
    }

    public ImageButton getBtnClearCart() {
        return btnClearCart;
    }
}
