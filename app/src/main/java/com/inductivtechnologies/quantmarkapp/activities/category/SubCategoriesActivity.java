package com.inductivtechnologies.quantmarkapp.activities.category;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.activities.cart.CartActivity;
import com.inductivtechnologies.quantmarkapp.activities.account.UserLoginActivity;
import com.inductivtechnologies.quantmarkapp.activities.profile.UserProfileActivity;
import com.inductivtechnologies.quantmarkapp.adapters.category.SubCategoryGridAdapter;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.components.GlideComponent;
import com.inductivtechnologies.quantmarkapp.configuration.StringConfig;
import com.inductivtechnologies.quantmarkapp.configuration.UrlRestApiConfig;
import com.inductivtechnologies.quantmarkapp.design.DisplayLargeNumber;
import com.inductivtechnologies.quantmarkapp.design.GridSpacingItemDecoration;
import com.inductivtechnologies.quantmarkapp.design.OvalTextView;
import com.inductivtechnologies.quantmarkapp.httptask.category.SubCategoryTask;
import com.inductivtechnologies.quantmarkapp.listeners.others.RecyclerTouchListener;
import com.inductivtechnologies.quantmarkapp.models.display.ProductTypeDisplay;
import com.inductivtechnologies.quantmarkapp.models.utils.ProductTypeGroup;
import com.inductivtechnologies.quantmarkapp.models.utils.ProductTypeGroupCustom;
import com.inductivtechnologies.quantmarkapp.store.repositories.CartItemRepository;
import com.inductivtechnologies.quantmarkapp.store.session.SessionManager;
import com.inductivtechnologies.quantmarkapp.utils.CalculationUtils;
import com.inductivtechnologies.quantmarkapp.utils.DesignUtils;
import com.inductivtechnologies.quantmarkapp.utils.LanguageDisolay;
import com.inductivtechnologies.quantmarkapp.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * SubCategoriesActivity : Pou afficher les sous catégories
 */
@SuppressWarnings("ConstantConditions")
public class SubCategoriesActivity extends AppCompatActivity implements View.OnClickListener , SwipeRefreshLayout.OnRefreshListener{

    ///Pour annuler les requêtes
    public static final String SUB_CATEGORY_TASK = "SUB_CATEGORY_TASK";

    private CoordinatorLayout coordinator;

    private AppBarLayout appBar;

    private SwipeRefreshLayout swipeRefreshLayout;

    private CollapsingToolbarLayout collapsedToolbar;
    private ImageView thumbmailCategory;

    private LinearLayout containerTitleToolbar;

    private TextView collapsingSubtitleSubcategory;
    private TextView collapsingTitleSubcategory;

    private TextView toolbarSubtitleSubcategory;
    private TextView toolbarTitleSubcategory;

    private OvalTextView tvBadge;

    //Controller
    private AppController appController;
    //La session
    private SessionManager sessionManager;

    //Task
    private SubCategoryTask subCategoryTask;

    private RecyclerView recyclerView;
    private SubCategoryGridAdapter adapter;
    private GridLayoutManager layoutManager;
    private GridSpacingItemDecoration gridSpacingDecation;

    //Les données
    private List<ProductTypeGroupCustom> subCategories;

    private String idHexCategory ;

    private CartItemRepository cartRepo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subcategories);

        Toolbar toolbar = findViewById(R.id.subcategories_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //Barre de statut transparente
        DesignUtils.makeFullTransparentStatutBar(this);

        String title = getIntent().getStringExtra(StringConfig.INTENT_CATEGORY_TITLE);
        String subtitle = getIntent().getStringExtra(StringConfig.INTENT_CATEGORY_SUBTITLE);
        String imageName = getIntent().getStringExtra(StringConfig.INTENT_CATEGORY_IMAGE_NAME);
        idHexCategory = getIntent().getStringExtra(StringConfig.INTENT_CATEGORY_HEX_ID);

        //Objets
        initObjects();

        //Les vues
        findAllViews();

        //Collapsing
        initCollapsingToolbar();

        setAppbarLayout(subtitle, title, imageName);

        initRecyclerViewSubcategoryItem();

        //Task
        launchTask();
    }

    @Override
    public void onResume() {
        super.onResume();

        //Le badge
        setBadge(cartRepo.getAllCartitems().size());
    }

    @Override
    public void onDestroy() {
        //Annulation des requêtes
        this.appController.cancelPendingRequests(SUB_CATEGORY_TASK);

        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.btn_close_subcategories :
                finish();
                break;
            case R.id.container_badge :
                //On ouvre le panier
                startActivity(new Intent(SubCategoriesActivity.this, CartActivity.class));
                break;
            case R.id.btn_display_profile :
                if(sessionManager.isLoggedIn()){
                    startActivity(new Intent(this, UserProfileActivity.class));
                }else{
                    startActivity(new Intent(this, UserLoginActivity.class));
                }
                break;
        }
    }

    @Override
    public void onRefresh() {
        subCategories.clear();
        launchTask();
    }

    //Les méthodes

    private void launchTask(){
        subCategoryTask.makeRequest(idHexCategory);
    }

    //Initialisation des objets
    private void initObjects() {
        //Instance singleton du controlleur volley
        this.appController = AppController.getInstance();

        sessionManager = new SessionManager(getApplicationContext());

        //Les données
        this.subCategories = new ArrayList<>();

        //Task
        subCategoryTask = new SubCategoryTask(this, appController);

        cartRepo = new CartItemRepository(getApplicationContext());

        layoutManager = new GridLayoutManager(this, 3);

        adapter = new SubCategoryGridAdapter(this, subCategories);

        //Grid decoration
        gridSpacingDecation = new GridSpacingItemDecoration(1, CalculationUtils.dpToPx(this, 2), true);
    }

    private void findAllViews(){
        coordinator = findViewById(R.id.subcategories_coordinator);

        appBar = findViewById(R.id.appbar_toolbar_subcategories);

        collapsedToolbar = findViewById(R.id.collapsing_toolbar_subcategories);
        thumbmailCategory = findViewById(R.id.thumbmail_subcategories);

        containerTitleToolbar = findViewById(R.id.container_toolbar_title_subcategories);
        //LinearLayout containerTitleCollapsing = findViewById(R.id.containet_collapsing_title_subcategories);

        collapsingSubtitleSubcategory = findViewById(R.id.tv_collapsing_subtitle_subcategories);
        collapsingTitleSubcategory = findViewById(R.id.tv_collapsing_title_subcategories);

        toolbarSubtitleSubcategory = findViewById(R.id.tv_toolbar_subtitle_subcategories);
        toolbarTitleSubcategory = findViewById(R.id.tv_toolbar_title_subcategories);

        ImageButton btnBack = findViewById(R.id.btn_close_subcategories);
        btnBack.setOnClickListener(this);

        //Le badge
        RelativeLayout containerBadge = findViewById(R.id.container_badge);
        containerBadge.setOnClickListener(this);
        tvBadge = findViewById(R.id.tv_bagde);
        tvBadge.setSolidColor("#ffffff");

        swipeRefreshLayout = findViewById(R.id.subcategories_swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
        swipeRefreshLayout.setOnRefreshListener(this);

        recyclerView = findViewById(R.id.subcategories_items__recycler_view);

        ImageButton btnDisplayProfile = findViewById(R.id.btn_display_profile);
        btnDisplayProfile.setOnClickListener(this);
    }

    //Initialise le collapsed
    private void initCollapsingToolbar() {
        collapsedToolbar.setTitle(" ");
        appBar.setExpanded(true);

        appBar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    containerTitleToolbar.setAlpha(1f);
                    isShow = true;
                } else if (isShow) {
                    containerTitleToolbar.setAlpha(0);
                    isShow = false;
                }
            }
        });
    }

    //Initialise le appbar
    private void setAppbarLayout(String subTitle, String title, String imageName){
        collapsingSubtitleSubcategory.setText(StringUtils.toUpperFirstChar(subTitle));
        collapsingTitleSubcategory.setText(StringUtils.toUpperFirstChar(title));

        toolbarSubtitleSubcategory.setText(StringUtils.toUpperFirstChar(subTitle));
        toolbarTitleSubcategory.setText(StringUtils.toUpperFirstChar(title));

        GlideComponent.simpleGlideComponentWithoutPlaceholder(this,
                UrlRestApiConfig.IMAGE_RESOURCE_URL + imageName, thumbmailCategory);
    }

    // Initialisation du recyclerview avec les données recues
    private void initRecyclerViewSubcategoryItem(){
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(gridSpacingDecation);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this,
                recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                ProductTypeGroupCustom productTypeGroupCustom = subCategories.get(position);

                if(productTypeGroupCustom instanceof ProductTypeGroup){
                    ProductTypeGroup productTypeGroup = (ProductTypeGroup) productTypeGroupCustom;

                    long nbSubcategoryGroupMarket = productTypeGroup.getNbMarketOffers();
                    String imageName = productTypeGroup.getSubCategoriesDisplay().get(0).getSubCategory().getImage();
                    String title = productTypeGroup.getLabel();
                    String subtitle;
                    if(nbSubcategoryGroupMarket > 1){
                        subtitle = getResources().getString(R.string.display_nb_article_plural,
                                DisplayLargeNumber.smartLargeNumber(nbSubcategoryGroupMarket));
                    }else{
                        subtitle = getResources().getString(R.string.display_nb_article_sin,
                                DisplayLargeNumber.smartLargeNumber(nbSubcategoryGroupMarket));
                    }

                    ArrayList<String> idsHexSubcategory= new ArrayList<>();
                    for(ProductTypeDisplay productTypeDisplay : productTypeGroup.getSubCategoriesDisplay()){
                        idsHexSubcategory.add(productTypeDisplay.getSubCategory().getIdHexType());
                    }

                    // On passe à l'activité suivante
                    Intent intent = new Intent(SubCategoriesActivity.this, SubCategoriesGroupActivity.class);
                    intent.putExtra(StringConfig.INTENT_SUBCATEGORY_TITLE, title);
                    intent.putExtra(StringConfig.INTENT_SUBCATEGORY_SUBTITLE, subtitle);
                    intent.putExtra(StringConfig.INTENT_SUBCATEGORY_IMAGE_NAME, imageName);
                    intent.putExtra(StringConfig.INTENT_SUBCATEGORY_HEX_ID, idsHexSubcategory);
                    startActivity(intent);
                }else{
                    ProductTypeDisplay productTypeDisplay = (ProductTypeDisplay) productTypeGroupCustom;
                    long nbSubcategoryGroupMarket = productTypeDisplay.getNbOfMarketOffer();

                    String title = (String) LanguageDisolay
                            .displaySubCategory(productTypeDisplay.getSubCategory()).get(LanguageDisolay.FIELD_TITLE);

                    String subtitle;
                    if(nbSubcategoryGroupMarket > 1){
                        subtitle = getResources().getString(R.string.display_nb_article_plural,
                                DisplayLargeNumber.smartLargeNumber(nbSubcategoryGroupMarket));
                    }else{
                        subtitle = getResources().getString(R.string.display_nb_article_sin,
                                DisplayLargeNumber.smartLargeNumber(nbSubcategoryGroupMarket));
                    }

                    String idHexSubCategory = productTypeDisplay.getSubCategory().getIdHexType();
                    String imageName = productTypeDisplay.getSubCategory().getImage();

                    // On passe à l'activité suivante
                    Intent intent = new Intent(SubCategoriesActivity.this, SubCategoryMarketofferActivity.class);
                    intent.putExtra(StringConfig.INTENT_SUBCATEGORY_TITLE, title);
                    intent.putExtra(StringConfig.INTENT_SUBCATEGORY_SUBTITLE, subtitle);
                    intent.putExtra(StringConfig.INTENT_SUBCATEGORY_IMAGE_NAME, imageName);
                    intent.putExtra(StringConfig.INTENT_SUBCATEGORY_HEX_ID, idHexSubCategory);
                    intent.putExtra(StringConfig.INTENT_SUBCATEGORY_NB_MARKETOFFER, nbSubcategoryGroupMarket);
                    startActivity(intent);
                }
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));
    }

    //Met à jour le badge
    private void setBadge(long count){
        tvBadge.setText(DisplayLargeNumber.smartLargeNumber(count));
    }

    //Getters et setters

    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return swipeRefreshLayout;
    }

    public List<ProductTypeGroupCustom> getSubCategories() {
        return subCategories;
    }

    public SubCategoryGridAdapter getAdapter() {
        return adapter;
    }

    public CoordinatorLayout getCoordinatorLayout() {
        return coordinator;
    }
}
