package com.inductivtechnologies.quantmarkapp.activities.search;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.activities.cart.CartActivity;
import com.inductivtechnologies.quantmarkapp.activities.marketofferdetails.MarketofferDetails;
import com.inductivtechnologies.quantmarkapp.activities.account.UserLoginActivity;
import com.inductivtechnologies.quantmarkapp.activities.profile.UserProfileActivity;
import com.inductivtechnologies.quantmarkapp.adapters.search.SearchResultMarketofferGridAdapter;
import com.inductivtechnologies.quantmarkapp.adapters.search.SearchResultMarketofferListAdapter;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.bottomsheets.BottomsheetSortMode;
import com.inductivtechnologies.quantmarkapp.callbacks.OnLoadMoreListener;
import com.inductivtechnologies.quantmarkapp.configuration.NumberConfig;
import com.inductivtechnologies.quantmarkapp.configuration.StringConfig;
import com.inductivtechnologies.quantmarkapp.design.DisplayLargeNumber;
import com.inductivtechnologies.quantmarkapp.design.GridSpacingItemDecoration;
import com.inductivtechnologies.quantmarkapp.design.OvalTextView;
import com.inductivtechnologies.quantmarkapp.httptask.search.SearchResultTask;
import com.inductivtechnologies.quantmarkapp.listeners.others.RecyclerTouchListener;
import com.inductivtechnologies.quantmarkapp.models.display.LoadMoreMarketOfferDisplay;
import com.inductivtechnologies.quantmarkapp.models.display.MarketOfferDisplay;
import com.inductivtechnologies.quantmarkapp.models.utils.MarketOfferDisplayCustom;
import com.inductivtechnologies.quantmarkapp.store.repositories.CartItemRepository;
import com.inductivtechnologies.quantmarkapp.store.session.SessionManager;
import com.inductivtechnologies.quantmarkapp.utils.CalculationUtils;
import com.inductivtechnologies.quantmarkapp.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * SearchResultActivity : Résultat de la recherche
 */
@SuppressWarnings("ConstantConditions")
public class SearchResultActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener,
        View.OnClickListener{

    public static final String SEARCH_RESULT_TASK = "SEARCH_RESULT_TASK";

    //Les vues

    private CoordinatorLayout coordinator;
    private SwipeRefreshLayout swipeRefreshLayout;

    private OvalTextView tvBadge;

    private ImageButton btnListDisplay;
    private ImageButton btnGridDisplay;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter recyclerviewAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private GridSpacingItemDecoration itemDecoration;

    private GridLayoutManager gridLayoutManager;
    private SearchResultMarketofferGridAdapter gridAdapter;
    private LinearLayoutManager linearLayoutManager;
    private SearchResultMarketofferListAdapter listAdapter;

    //Les objets

    //Controller
    private AppController appController;
    //La session
    private SessionManager sessionManager;

    //Le task
    private SearchResultTask searchResultTask;

    //Les données
    private List<MarketOfferDisplayCustom> marketOffers;

    private String idHex;
    private String searchLabelValue;
    //Nombre total d'article pour la sous catégorie
    private long nbMarketOffers;
    //Filtre de recherche
    private int searchFilterValue;
    //Offset lors de l'interrogation de la base de données
    private int skip = 0;
    //Pour iniquer le mode d'affichage
    private boolean isGridDisplay = true;
    //Mode de trie
    private int sortMode;

    //Repositoire
    private CartItemRepository cartRepo;

    //Dialog qui permet de choisir le mode de trie
    private BottomsheetSortMode bshSortMode;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);

        Toolbar toolbar = findViewById(R.id.toolbar_search_result);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //On récupère les données de l'intent
        nbMarketOffers = getIntent().getLongExtra(StringConfig.INTENT_SEARCH_NB_ITEM, 0);
        idHex = getIntent().getStringExtra(StringConfig.INTENT_SEARCH_ID_HEX);
        searchLabelValue = getIntent().getStringExtra(StringConfig.INTENT_SEARCH_LABEL);
        searchFilterValue = getIntent().getIntExtra(StringConfig.INTENT_SEARCH_FILTER, 0);

        //Objets
        initObjects();

        //Les vues
        findAllViews();

        //Ecouteurs
        addListeners();

        initRecyclerView();

        //Task
        launchTask();
    }

    @Override
    public void onResume() {
        super.onResume();

        //Le badge
        setBadge(cartRepo.getAllCartitems().size());
    }

    @Override
    public void onDestroy() {
        //Annulation des requêtes
        this.appController.cancelPendingRequests(SEARCH_RESULT_TASK);

        super.onDestroy();
    }

    @Override
    public void onRefresh() {
        marketOffers.clear();
        skip = 0;
        launchTask();
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.btn_close_search_result:
                finish();
                break;
            case R.id.btn_display_profile:
                if (sessionManager.isLoggedIn()) {
                    startActivity(new Intent(SearchResultActivity.this, UserProfileActivity.class));
                } else {
                    startActivity(new Intent(SearchResultActivity.this, UserLoginActivity.class));
                }
                break;
            case R.id.container_badge:
                //On ouvre le panier
                startActivity(new Intent(SearchResultActivity.this, CartActivity.class));
                break;
            case R.id.btn_search_result_list_display :
                if(isGridDisplay){
                    isGridDisplay = false;
                    btnGridDisplay.setImageDrawable(getResources().getDrawable(R.drawable.ic_action_display_grid_unselected));
                    btnListDisplay.setImageDrawable(getResources().getDrawable(R.drawable.ic_action_display_list_selected));

                    listConfiguration();
                }
                break;
            case R.id.btn_search_result_grid_display :
                if(!isGridDisplay){
                    isGridDisplay = true;
                    btnGridDisplay.setImageDrawable(getResources().getDrawable(R.drawable.ic_action_display_grid_selected));
                    btnListDisplay.setImageDrawable(getResources().getDrawable(R.drawable.ic_action_display_list_unselected));

                    gridConfiguration();
                }
                break;
            case R.id.btn_select_sort_mode :
                bshSortMode.show();
                break;
            case R.id.bsd_sort_mode_btn_action_save :
                bshSortMode.hide();

                //On modifie le mode de trie
                sortMode = bshSortMode.getSortMode();

                //On raffraichi avec le task
                onRefresh();

                break;
        }
    }

    //Les méthodes
    private void launchTask(){
        searchResultTask.makeRequest(searchFilterValue, idHex, NumberConfig.NB_WIDTH_DATA, skip, sortMode);
    }

    private void onLoadMoreData(){
        marketOffers.remove(marketOffers.size() - 1);
        recyclerviewAdapter.notifyItemRemoved(marketOffers.size());

        if(recyclerviewAdapter instanceof SearchResultMarketofferGridAdapter){
            ((SearchResultMarketofferGridAdapter) recyclerviewAdapter).setWithLoadMore(false);
        }else{
            ((SearchResultMarketofferListAdapter) recyclerviewAdapter).setWithLoadMore(false);
        }

        //On charge les données de nouveau
        launchTask();
    }

    //Utilisée pour savoir si les données ont été totalemnt chargées et quand ajouter le load more
    public void loadMoreAsyn(){
        if(marketOffers.size() < nbMarketOffers){
            marketOffers.add(new LoadMoreMarketOfferDisplay());
            if(recyclerviewAdapter instanceof SearchResultMarketofferGridAdapter){
                ((SearchResultMarketofferGridAdapter)recyclerviewAdapter).setWithLoadMore(true);
            }else{
                ((SearchResultMarketofferListAdapter)recyclerviewAdapter).setWithLoadMore(true);
            }
        }
    }

    //Initialisation des objets
    private void initObjects() {
        //Instance singleton du controlleur volley
        this.appController = AppController.getInstance();

        //La session
        sessionManager = new SessionManager(getApplicationContext());

        //Les données
        this.marketOffers = new ArrayList<>();

        //Pour choisir le mode de trie
        bshSortMode = new BottomsheetSortMode(this);
        sortMode = bshSortMode.getSortMode();

        //Item decoration
        itemDecoration = new GridSpacingItemDecoration(2,
                CalculationUtils.dpToPx(this, 2), true);

        //Adaptateurs
        gridAdapter = new SearchResultMarketofferGridAdapter(this, this.marketOffers);
        listAdapter = new SearchResultMarketofferListAdapter(this, this.marketOffers);

        //Layout managers
        gridLayoutManager = new GridLayoutManager(this, 2);
        linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        //Task
        searchResultTask = new SearchResultTask(this, appController);

        // Repo
        cartRepo = new CartItemRepository(getApplicationContext());
    }

    //Ecouteurs
    private void addListeners() {
        bshSortMode.getmBtnActionSave().setOnClickListener(this);
    }

    private void findAllViews(){
        coordinator = findViewById(R.id.search_result_coordinator);
        swipeRefreshLayout = findViewById(R.id.search_result_swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
        swipeRefreshLayout.setOnRefreshListener(this);

        ImageButton btnBack = findViewById(R.id.btn_close_search_result);
        btnBack.setOnClickListener(this);

        //Le badge
        RelativeLayout containerBadge = findViewById(R.id.container_badge);
        containerBadge.setOnClickListener(this);
        tvBadge = findViewById(R.id.tv_bagde);
        tvBadge.setSolidColor("#ffffff");

        recyclerView = findViewById(R.id.recycler_view_search_result);

        ImageButton btnDisplayProfile = findViewById(R.id.btn_display_profile);
        btnDisplayProfile.setOnClickListener(this);
        ImageButton btnSortMode = findViewById(R.id.btn_select_sort_mode);
        btnSortMode.setOnClickListener(this);

        TextView searchLabel = findViewById(R.id.search_result_label);
        searchLabel.setText(StringUtils.toUpperFirstChar(searchLabelValue));

        btnListDisplay = findViewById(R.id.btn_search_result_list_display);
        btnListDisplay.setOnClickListener(this);
        btnGridDisplay = findViewById(R.id.btn_search_result_grid_display);
        btnGridDisplay.setOnClickListener(this);
    }

    // Initialisation du recyclerview avec les données recues
    private void initRecyclerView(){
        gridConfiguration();
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this,
                recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                MarketOfferDisplayCustom item = marketOffers.get(position);

                if(item instanceof MarketOfferDisplay){
                    MarketOfferDisplay marketOfferDisplay = (MarketOfferDisplay) marketOffers.get(position);

                    Intent intent = new Intent(SearchResultActivity.this, MarketofferDetails.class);
                    intent.putExtra(StringConfig.INTENT_MARKETOFFER_HEX_ID, marketOfferDisplay.getMarketOffer().getIdHexMarketOffer());
                    startActivity(intent);
                }else{
                    onLoadMoreData();
                }
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));
    }

    //Affichage en grid
    private void gridConfiguration(){
        layoutManager = gridLayoutManager;
        recyclerviewAdapter = gridAdapter;

        //Permet de modifier la disposition du layout manager en fonction de l'élément à inserer dans le recyclerview
        ((GridLayoutManager) layoutManager).setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return ((SearchResultMarketofferGridAdapter) recyclerviewAdapter).isPositionLoadMore(position) ? ((GridLayoutManager) layoutManager).getSpanCount() : 1;
            }
        });

        ((SearchResultMarketofferGridAdapter) recyclerviewAdapter).setLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                onLoadMoreData();
            }
        });

        //Manager
        recyclerView.setLayoutManager(layoutManager);
        //On ajoute la decoration pour l'affichage en grille
        recyclerView.addItemDecoration(itemDecoration);
        //On attache l'adaptateur
        recyclerView.setAdapter(recyclerviewAdapter);
    }

    //Affichage en list
    private void listConfiguration(){
        layoutManager = linearLayoutManager;
        recyclerviewAdapter = listAdapter;

        ((SearchResultMarketofferListAdapter) recyclerviewAdapter).setLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                onLoadMoreData();
            }
        });

        //On retire la decoration pour l'affichage en grille
        recyclerView.removeItemDecoration(itemDecoration);
        //Manager
        recyclerView.setLayoutManager(layoutManager);
        //On attache l'adaptateur
        recyclerView.setAdapter(recyclerviewAdapter);
    }

    //Met à jour le badge
    private void setBadge(long count){
        tvBadge.setText(DisplayLargeNumber.smartLargeNumber(count));
    }

    //Getters et setters

    public void setSkip() {
        this.skip += 10;
    }

    public CoordinatorLayout getCoordinator() {
        return coordinator;
    }

    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return swipeRefreshLayout;
    }

    public List<MarketOfferDisplayCustom> getMarketOffers() {
        return marketOffers;
    }

    public RecyclerView.Adapter getRecyclerviewAdapter() {
        return recyclerviewAdapter;
    }
}
