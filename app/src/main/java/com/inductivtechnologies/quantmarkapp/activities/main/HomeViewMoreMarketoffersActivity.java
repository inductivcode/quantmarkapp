package com.inductivtechnologies.quantmarkapp.activities.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.inductivtechnologies.quantmarkapp.R;
import com.inductivtechnologies.quantmarkapp.activities.marketofferdetails.MarketofferDetails;
import com.inductivtechnologies.quantmarkapp.adapters.home.HomeViewMoreMarketoffersAdapter;
import com.inductivtechnologies.quantmarkapp.application.app.AppController;
import com.inductivtechnologies.quantmarkapp.bottomsheets.BottomsheetSortMode;
import com.inductivtechnologies.quantmarkapp.configuration.NumberConfig;
import com.inductivtechnologies.quantmarkapp.configuration.StringConfig;
import com.inductivtechnologies.quantmarkapp.configuration.UrlRestApiConfig;
import com.inductivtechnologies.quantmarkapp.design.GridSpacingItemDecoration;
import com.inductivtechnologies.quantmarkapp.httptask.home.HomeViewMoreMarketOffersTask;
import com.inductivtechnologies.quantmarkapp.listeners.others.RecyclerTouchListener;
import com.inductivtechnologies.quantmarkapp.models.display.LoadMoreMarketOfferDisplay;
import com.inductivtechnologies.quantmarkapp.models.display.MarketOfferDisplay;
import com.inductivtechnologies.quantmarkapp.models.utils.MarketOfferDisplayCustom;
import com.inductivtechnologies.quantmarkapp.utils.CalculationUtils;
import com.inductivtechnologies.quantmarkapp.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * HomeViewMoreMarketoffersActivity
 */
@SuppressWarnings("ConstantConditions")
public class HomeViewMoreMarketoffersActivity extends AppCompatActivity
        implements View.OnClickListener , SwipeRefreshLayout.OnRefreshListener{

    ///Pour annuler les requêtes
    public static final String VIEW_MORE_MARKETOFFERS_TASK = "VIEW_MORE_MARKETOFFERS_TASK";

    //Les vues

    private TextView tvTitle;

    private CoordinatorLayout coordinator;
    private SwipeRefreshLayout swipeRefreshLayout;
    
    private RecyclerView recyclerView;
    private HomeViewMoreMarketoffersAdapter adapter;
    private StaggeredGridLayoutManager layoutManager;
    private GridSpacingItemDecoration itemDecoration;

    private TextView tvSortValue;

    //Controller
    private AppController appController;

    //Task
    private HomeViewMoreMarketOffersTask httpTask;

    //Dialog qui permet de choisir le mode de trie
    private BottomsheetSortMode bshSortMode;

    //Mode de trie
    private int sortMode;

    private String[] strSortMode;

    //Le titre de l'activité
    private String title;

    //Offset lors de l'interrogation de la base de données
    private int skip = 0;

    //Les données
    private List<MarketOfferDisplayCustom> marketOffers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_view_more_marketoffers);
        
        //Toolbar 
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //Objets
        initObjects();

        //Les vues
        findAllViews();

        //Les écouteurs
        addListeners();

        //Titre de l'activité
        title = getIntent().getStringExtra(StringConfig.INTENT_HOME_TITLE);
        tvTitle.setText(StringUtils.toUpperFirstChar(title));

        setSortValueView();

        //Init
        initRecyclerView();

        //Task
        launchTask();
    }

    @Override
    public void onDestroy() {
        //Annulation des requêtes
        this.appController.cancelPendingRequests(VIEW_MORE_MARKETOFFERS_TASK);

        super.onDestroy();
    }

    @Override
    public void onRefresh() {
        marketOffers.clear();
        skip = 0;
        launchTask();
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.toolbar_btn_back :
                finish();
                break;
            case R.id.container_sort_value :
                bshSortMode.show();
                break;
            case R.id.bsd_sort_mode_btn_action_save :
                bshSortMode.hide();

                //On modifie le mode de trie
                sortMode = bshSortMode.getSortMode();

                //On raffraichi avec le task
                onRefresh();

                //On raffraichi
                setSortValueView();

                break;
        }
    }
    
    //Les méthodes

    private void initObjects() {
        //Instance singleton du controlleur volley
        this.appController = AppController.getInstance();

        httpTask = new HomeViewMoreMarketOffersTask(this, appController);

        marketOffers = new ArrayList<>();

        //Pour choisir le mode de trie
        bshSortMode = new BottomsheetSortMode(this);
        sortMode = bshSortMode.getSortMode();

        strSortMode = getResources().getStringArray(R.array.sort_mode_array);

        //Item decoration
        itemDecoration = new GridSpacingItemDecoration(2,
                CalculationUtils.dpToPx(this, 2), true);

        //Layout manager
        layoutManager = new StaggeredGridLayoutManager(2,
                StaggeredGridLayoutManager.VERTICAL);

        //Adapter
        adapter = new HomeViewMoreMarketoffersAdapter(this, marketOffers);
    }

    //Listeners
    private void addListeners() {
        bshSortMode.getmBtnActionSave().setOnClickListener(this);
    }

    private void findAllViews(){
        coordinator = findViewById(R.id.coordinator);

        ImageButton btnBack = findViewById(R.id.toolbar_btn_back);
        btnBack.setOnClickListener(this);

        tvTitle = findViewById(R.id.title);

        swipeRefreshLayout = findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeColors(getResources().getColor(R.color.colorPrimary));
        swipeRefreshLayout.setOnRefreshListener(this);

        recyclerView = findViewById(R.id.recycler_view);

        RelativeLayout containerSortValue = findViewById(R.id.container_sort_value);
        containerSortValue.setOnClickListener(this);
        tvSortValue = findViewById(R.id.sort_value);
    }

    private void launchTask() {
        //Url de la requête
        String urlRequest = "";

        if(title.equals(getResources().getString(R.string.home_title_low_cost_offers))){
            urlRequest = String.format(Locale.getDefault(),
                    UrlRestApiConfig.HOME_MORE_LOWCOST_OFFERS_URL,
                    NumberConfig.NB_WIDTH_DATA, skip, sortMode);
        }

        if(title.equals(getResources().getString(R.string.home_title_promotion_offers))){
            urlRequest = String.format(Locale.getDefault(),
                    UrlRestApiConfig.HOME_MORE_PROMOTION_OFFERS_URL,
                    NumberConfig.NB_WIDTH_DATA, skip, sortMode);
        }

        //On lance
        httpTask.makeRequest(urlRequest);
    }

    private void onLoadMoreData(){
        marketOffers.remove(marketOffers.size() - 1);
        adapter.notifyItemRemoved(marketOffers.size());

        adapter.setWithLoadMore(false);

        //On charge les données de nouveau
        launchTask();
    }

    //Utilisée pour savoir si les données ont été totalemnt chargées et quand ajouter le load more
    public void loadMoreAsyn(boolean dataIsComplete){
        if(!dataIsComplete){
            marketOffers.add(new LoadMoreMarketOfferDisplay());
            adapter.setWithLoadMore(true);
        }
    }

    //Modifie le label du mode de trie
    private void setSortValueView(){
        tvSortValue.setText(StringUtils.getSpannedText(getResources()
                .getString(R.string.text_sort_value, strSortMode[sortMode])));
    }

    // Initialisation du recyclerview avec les données recues
    private void initRecyclerView(){
        //Layout manager
        recyclerView.setLayoutManager(layoutManager);

        // Set adapter
        recyclerView.setAdapter(adapter);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(itemDecoration);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(this,
                recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                MarketOfferDisplayCustom marketCustom = marketOffers.get(position);

                if(marketCustom instanceof MarketOfferDisplay){
                    MarketOfferDisplay marketOffer = (MarketOfferDisplay) marketCustom;

                    Intent intent = new Intent(HomeViewMoreMarketoffersActivity.this
                            , MarketofferDetails.class);
                    intent.putExtra(StringConfig.INTENT_MARKETOFFER_HEX_ID,
                            marketOffer.getMarketOffer().getIdHexMarketOffer());

                    startActivity(intent);
                }else{
                    onLoadMoreData();
                }
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));
    }

    //Getters et setters

    public CoordinatorLayout getCoordinator() {
        return coordinator;
    }

    public SwipeRefreshLayout getSwipeRefreshLayout() {
        return swipeRefreshLayout;
    }

    public List<MarketOfferDisplayCustom> getMarketOffers() {
        return marketOffers;
    }

    public HomeViewMoreMarketoffersAdapter getAdapter() {
        return adapter;
    }

    public void setSkip() {
        this.skip += 10;
    }
}
